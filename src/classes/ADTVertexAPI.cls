/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION : Serves as a class to be used for invocation of Vertex Service and handling the API response
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               7/23/2021                              - Original Version
* Charles Varner				09/28/2021        SFCPQ-124             Change code for experience API.  Incorporate call originating in QCP Javascript plugin for synchrous calcluation
* Matt Parrella                 1/11/2022          N/A                 - Removing hard-coded endpoint url, replacing with dynamic logic
*/
@RestResource(urlMapping='/ADTVertexAPI/*')
global with sharing class ADTVertexAPI {
   /**
    * This method constructs the JSON & performs callout
    * @Param Id reqId
    * @ReturnType PageReference
    **/
    @AuraEnabled
    @HttpPost
    global static String ADTVertexAPIManager(String quoteBody, String quoteLines) {
	//public static String ADTVertexAPIManager(SBQQ__Quote__c quoteBody, List<SBQQ__QuoteLine__c> lines) {
        /*When method has no parameters it returns the body with the json in RestRequest and you can retrieve the 
        JSON with request BODY. This allows the json to be retrieved no matter the content.  */
        //RestRequest postRequest = RestContext.request;
        //Blob JsonBody = postRequest.requestBody;
        String supplementalQSPCode = 'AddQSP'; // KF: global variable for Supplemental QSP ProductCode.

        // Final Request 
		//SBQQ__Quote__c tempQuote = quoteBody;
		System.debug('quoteBody: ' + quoteBody);
		//SBQQ__Quote__c tempQuote = (SBQQ__Quote__c)JSON.deserialize(quoteBody, SBQQ__Quote__c.class);
		Map<String, Object> m =  (Map<String, Object>)JSON.deserializeUntyped(quoteBody);
		System.debug('tempQuote :' + m);
		System.debug('quoteBody :' + quoteBody);
		System.debug('quoteLines : ' + quoteLines);
		List<Object> lines = (List<Object>)JSON.deserializeUntyped(quoteLines);
		//Map<String, Object> lines =  (Map<String, Object>)JSON.deserializeUntyped(quoteLines);
		//List<SBQQ__QuoteLine__c> lines = (List<SBQQ__QuoteLine__c>)JSON.deserialize(quoteLines, List<SBQQ__QuoteLine__c>.class);
		Account acct = [SELECT Id, SiteStreet__c, SiteStreet2__c, SiteCity__c, SiteState__c, SitePostalCode__c, SitePostalCodeAddOn__c FROM Account WHERE Id = :(Id)m.get('SBQQ__Account__c')];
        ReturnsCodeField req = new ReturnsCodeField();
        //Request Fields
        //req.value = '';
        //req.name = '';
        req.documentNumber = (String)m.get('OrderNumber__c');
		req.documentDate = String.valueOf(System.today());
        //req.accumulationDocumentNumber = '';
        //req.accumulationCustomerNumber = '';
        //req.documentType = '';
        //req.billingType = '';
        //req.orderType = '';
        //req.postingDate = '';
        //req.locationCode = '';
        //req.returnAssistedParametersIndicator = false;
        //req.returnGeneratedLineItemsIndicator = false;
        //req.deliveryTerm = '';
        //req.documentDate = '';
        req.transactionId = (String)m.get('OrderNumber__c');
        req.transactionType = 'SALE';
        //req.simplificationCode = '';
        //req.roundAtLineLevel = false;
        //req.paymentDate = '';
        //req.documentSequenceId = '';
        //req.taxPointDate ='';
        //req.proratePercentage = 0;
        //-----------Currency__z-----------------//
        //Currency_Z currencyz = new Currency_Z();
        //currencyz.isoCurrencyName = '';
        //currencyz.isoCurrencyCodeAlpha = '';
        //currencyz.isoCurrencyCodeNum = 0;
        //req.currency_Z = currencyz;
        //-----------OriginalCurrency------------//
        //Currency_Z originalCurrency = new Currency_Z();
        //originalCurrency.isoCurrencyName = '';
        //originalCurrency.isoCurrencyCodeAlpha = '';
        //originalCurrency.isoCurrencyCodeNum = 0;
        //req.originalCurrency = originalCurrency;
        //-----------companyCodeCurrency---------//
        //Currency_Z companyCodeCurrency = new Currency_Z();
        //companyCodeCurrency.isoCurrencyName = '';
        //companyCodeCurrency.isoCurrencyCodeAlpha = '';
        //companyCodeCurrency.isoCurrencyCodeNum = 0;
        //req.companyCodeCurrency = companyCodeCurrency;
        //-----------Seller----------------------//
        Seller Seller = new Seller();
        // Seller.nexusIndicator = false;
        // Seller.nexusReasonCode = '0';
        Seller.company = '001';
        Seller.division = '433';
        //Seller.department = '';
        //Seller.utilityProvider = '';
        /*++++++ Dispatcher for Seller ++++++++ */
        //Dispatcher dispatcher = new Dispatcher();
        //dispatcher.dispatcherCode = '';
        //dispatcher.classCode = '';
        
        //List<TaxRegistration> taxRegistration = new List<TaxRegistration>();
        //TaxRegistration addTaxRegistration = new TaxRegistration();
        //addTaxRegistration.isoCountryCode = '';
        //addTaxRegistration.mainDivision = '';
        //addTaxRegistration.hasPhysicalPresenceIndicator = false;
        //addTaxRegistration.jurisdictionId = 0;
        //addTaxRegistration.taxRegistrationNumber = '';
        //Nexus Override for Tax Registration
        //List<NexusOverride> nexusOverride = new  List<NexusOverride>();
        //NexusOverride addNexusOverride = new NexusOverride();
        //addNexusOverride.locationRole = '';
        //addNexusOverride.country = false;
        //addNexusOverride.mainDivision = false;
        //addNexusOverride.subDivision = false;
        //addNexusOverride.city = false;
        //addNexusOverride.district = false;
        //nexusOverride.add(addNexusOverride);
        //Add Nexus Overiide to Tax Registration
        //addTaxRegistration.nexusOverride = nexusOverride;
        //Physical Location
        /*List<PhysicalLocation> physicalLocation = new List<physicalLocation>();
        physicalLocation addphysicalLocation = new physicalLocation();
        //Physical location fields
        addphysicalLocation.taxAreaId = 0;
        addphysicalLocation.streetAddress1 = '';
        addphysicalLocation.streetAddress2 = '';
        addphysicalLocation.city = '';
        addphysicalLocation.mainDivision = '';
        addphysicalLocation.subDivision = '';
        addphysicalLocation.postalCode = '';
        addphysicalLocation.country = '';
        physicalLocation.add(addphysicalLocation);
        addTaxRegistration.physicalLocation = physicalLocation;
        //Add Emission type to Tax Registration
        ImpositionType impositionType = new ImpositionType();
        impositionType.typeDescription = '';
        impositionType.userDefined = false;
        impositionType.impositionTypeId = 0;
        impositionType.withholdingType = '';
        addTaxRegistration.impositionType = impositionType;
        TaxRegistration.add(addTaxRegistration);*/
        //Add Tax Registration for Dispatcher
        //dispatcher.taxRegistration = taxRegistration;
        //Add Dispatcher to seller
        //Seller.dispatcher = dispatcher;
        //Add Tax regustration for Seller Temporrily Same as Dispatcher
        //seller.taxRegistration = taxRegistration;
        /*++++++ PhysicalOrigin for Seller ++++++++ */
        PhysicalOrigin physicalOrigin = new PhysicalOrigin();
        physicalOrigin.taxAreaId = null;
        physicalOrigin.latitude = null;
        physicalOrigin.longitude = null;
        physicalOrigin.locationCustomsStatus = null;
        physicalOrigin.locationCode = null;
        physicalOrigin.externalJurisdictionCode = null;
        // physicalOrigin.streetAddress1 = '6931 Vista Pkwy N';
        physicalOrigin.streetAddress1 = null;
        physicalOrigin.streetAddress2 = null;
        physicalOrigin.city = 'FL';
        physicalOrigin.mainDivision = 'West Palm Beach';
        physicalOrigin.subDivision = null;
        physicalOrigin.postalCode = null;
        physicalOrigin.country = null;
        /*++++++ Currency Conversion for PhysicalOrigin /*++++++ */
        /*CurrencyConversion currencyConversion = new CurrencyConversion();
        currencyConversion.isoCurrencyCodeNum = 0;
        currencyConversion.isoCurrencyName = '';
        currencyConversion.conversionRate = 0;
        currencyConversion.isoCurrencyCodeAlpha = '';
        physicalOrigin.currencyConversion = currencyConversion;*/
        //Set Physical Origin to seller
        seller.PhysicalOrigin = physicalOrigin;
        PhysicalOrigin administrativeOrigin = new PhysicalOrigin();
        seller.administrativeOrigin = physicalOrigin;
        //seller.administrativeOrigin = administrativeOrigin;
        req.seller = Seller;


        //-----------Customer--------------------//
        Customer Customer = new Customer();
        Customer.isTaxExempt = false;
        Customer.exemptionReasonCode = null;
        CustomerCode customerCode = new CustomerCode();
        //Customer Code Fields
        customerCode.code= '';
        customerCode.isBusinessIndicator = false;
        customerCode.classCode = '';
        //Customer.customerCode = customerCode;
        PhysicalOrigin destination = new PhysicalOrigin();
		destination.streetAddress1 = acct.SiteStreet__c;
		destination.streetAddress2 = String.isBlank(acct.SiteStreet2__c) ? null : acct.SiteStreet2__c;
		destination.city = acct.SiteCity__c;
		destination.mainDivision = acct.SiteState__c;
		destination.postalCode = acct.SitePostalCodeAddOn__c != null ? acct.SitePostalCode__c + '-' + acct.SitePostalCodeAddOn__c : acct.SitePostalCode__c;
		destination.subDivision = null ;
		destination.country = 'US';
        Customer.destination = destination;
        PhysicalOrigin administrativeDestination = new PhysicalOrigin();
        //Customer.administrativeDestination = physicalOrigin;
        ExemptionCertificate exemptionCertificate  = new ExemptionCertificate();
        //ExemptionCertificate Fields
        exemptionCertificate.details = '';
        exemptionCertificate.exemptionCertificateNumber = '';
        //Customer.exemptionCertificate = exemptionCertificate;
        //Customer.taxRegistration = taxRegistration;
        req.customer = Customer;
        
        //-----------TaxOverride-----------------//
        /*TaxOverride TaxOverride = new TaxOverride();
        TaxOverride.overrideType = '';
        TaxOverride.overrideReasonCode = null;
        req.taxOverride = TaxOverride;*/
        //-----------impositionToProcess-----------------//
        //List<ImpositionToProcess> impositionToProcess = new List<ImpositionToProcess>();
        //ImpositionToProcess addImposition = new ImpositionToProcess();
        //addImposition.jurisdictionLevel = '';
        //Imposition Type Fields

        //Add Imposition
        //addImposition.impositionType = impositionType;
        //impositionToProcess.add(addImposition);
        //Add Imposition List to request
        //req.impositionToProcess = impositionToProcess;

        //-----------jurisdictionOverride-----------------//
        /*List<JurisdictionOverride> jurisdictionOverride = new List<JurisdictionOverride>();
        JurisdictionOverride addJurisdictionOverride = new JurisdictionOverride();
        addJurisdictionOverride.jurisdictionLevel = '';
        DeductionOverride deductionOverride= new DeductionOverride();
        ExemptOverride exemptOverride = new ExemptOverride();
        exemptOverride.amount = 0;
        exemptOverride.overrideExemptReasonCode = '';
        deductionOverride.exemptOverride  = exemptOverride;
        NonTaxableOverride nonTaxableOverride = new NonTaxableOverride();
        nonTaxableOverride.amount = 0;
        nonTaxableOverride.overrideNonTaxableReasonCode = '';
        deductionOverride.nonTaxableOverride  = nonTaxableOverride;
        addJurisdictionOverride.deductionOverride = deductionOverride;
        addJurisdictionOverride.rateOverride = 0;
        addJurisdictionOverride.impositionType = impositionType;
        jurisdictionOverride.add(addJurisdictionOverride);
        req.jurisdictionOverride = jurisdictionOverride;*/

        //-----------SitusOverride-----------------//
        /*SitusOverride situsOverride = new SitusOverride();
        situsOverride.taxingLocation = '';
        req.situsOverride = situsOverride;*/

        //-----------discount-----------------//
        /*Discount discount = new Discount();
        discount.userDefinedDiscountCode = '';
        discount.amount = 0;
        discount.percent = 0;
        req.discount = discount;*/
        
        //-----------currencyConversionFactors-----------------//
        //List<CurrencyConversionFactors> currencyConversionFactors = new List<CurrencyConversionFactors>();

        //req.currencyConversionFactors = currencyConversionFactors;
		/*
			This code is used to pull the extn_bill_code__c from the quoteline because the QCP plugin javascript won't pull
			the extn_bill_code__c formula field from the quote line and pass it in JSON.
		*/
		Set<Id> productIdSet = new Set<Id>();		
		for(Object oLine : lines){
		    System.debug('JSON.serialize(oLine) :' + JSON.serialize(oLine));
		    Map<String, Object> line = (Map<String, Object>)oLine;
			productIdSet.add((Id)line.get('SBQQ__Product__c'));
		}
	    //Map<Id, Product2> productMap = new Map<Id, Product2>([SELECT Id, ExtnBillCode__c FROM Product2 WHERE Id IN :productIdSet]);
        //-----------lineItems-----------------//
        List<LineItems_Z> lineItems = new List<LineItems_Z>();
		for(Object oLine : lines){
		    //System.debug('JSON.serialize(oLine) :' + JSON.serialize(oLine));
		    Map<String, Object> line = (Map<String, Object>)oLine;
			if((Decimal)line.get('Install__c') != 0)
				addLineItem(line, lineItems, (String)line.get('ADSC_Bill_Code__c'), (Decimal)line.get('Install__c'), null, null, null, null);
                if((Decimal)line.get('Monitoring__c') > 0){
                    if(line.get('SBQQ__ProductCode__c') == 'AddQSP'){
                        System.debug('supplemental QSP line, skipping monitoring call');
                    } else {
                        addLineItem(line, lineItems,null, null, (String)line.get('ANSC_Bill_Code__c'), monitoringMinusQSP((Decimal)line.get('Monitoring__c'), (Decimal)line.get('QSP__c')), null, null);
                    }		
                }
			if((Decimal)line.get('QSP__c') > 0)
				addLineItem(line, lineItems, null, null, null, null, (String)line.get('QSP_Bill_Code__c'), (Decimal)line.get('QSP__c'));
		}
		req.lineItems = lineItems;

        /* MParrella | 1-11-2021 | Commenting out old code for retrieving API information, using new helper class: START */
        //Get Endpoints and Credentials for callout
//        Mulesoft_API_Settings__mdt msft;
//        //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
//
//        Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
//        if(environment)
//        {
//            msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
//        }
//        else
//        {
//            msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
//        }

        // Make call to get all Mulesoft integration data
        IntegrationsHelper.IntegrationsWrapper integrationsWrapper = IntegrationsHelper.retrieveCreds();


        /* MParrella | 1-11-2021 | Commenting out old code for retrieving API information, using new helper class: STOP */
        
        //Get a Client Id and Secret for Callout
        String clientId = integrationsWrapper.Credentials_SFCPQ_Mulesoft_ClientId;
        String clientSecret = integrationsWrapper.Credentials_SFCPQ_Mulesoft_ClientSecret;
        String endpoint = integrationsWrapper.Endpoint_Mulesoft_Base_URL + integrationsWrapper.vertexInfo.Endpoint_Calculate_Tax;
        System.debug('endpoint: ' + endpoint);
        // Callout 
        Http http = new Http();
        //Hold Response 
        HttpResponse response = new HttpResponse();
        //Build Request for callout
        HttpRequest request = new HttpRequest();
        //Must enable endpoint in Salesforce Setup
        //Note:Add endpoint to remote site settings
//        request.setEndpoint(endpoint+'tax-calculation-exp-api/v1/api/taxCalculation'); // MParrella: Commented out 1-11-2021
        request.setEndpoint(endpoint); // MParrella | 1-11-2021
		//request.setEndpoint('https://eng55bc2owaxgwx.m.pipedream.net');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('client_id', clientId);
        request.setHeader('client_secret', clientSecret);
        request.setHeader('tracing_id', IntegrationsHelper.generateTracingId());
		request.setTimeout(120000);
        //Replace all reserved keywords in wrapper class to correct format for json before sending
        String jsonBody = json.serialize(req, true).replace('_Z','');
        system.debug('JSON: '+jsonBody);
        //Set the json to the request body
        request.setBody(jsonBody);

        
        //This line is commented until endpoint is provided
        if(test.isRunningTest()){
            response = http.send(request);
        }
        else{
            //Send request
            response = http.send(request);
        }
        
        
        //Declare Response 
        response resp = new response();
        //Class for Response
        //ReturnsCodeField respClass = new ReturnsCodeField();
        system.debug('Response: '+response);
		System.debug('response body: ' + response.getBody());
        status stat = new status();
        // If the request is successful, parse the JSON response.
		Map<Decimal, qcpLineItems> tempQuoteLineMap = new Map<Decimal, qcpLineItems>();
		qcpPluginResponse returnResponse = new qcpPluginResponse();
        if (response.getStatusCode() == 200) {
            //200 - Success, reponds with 'timeBands200Response'

            // Deserialize the JSON string into collections of primitive data types.
            VertexResponse vertResp = (VertexResponse)JSON.deserialize(response.getBody(), VertexResponse.class);  
            //Deserialize the json string into the defined wrapper class
			System.debug('results: ' + vertResp);
			for(VertexResponse.cls_lineItems lineItem : vertResp.lineItems){
                System.debug('lineItem: ' + lineItem);
                System.debug('lineItem.totalTax: ' + lineItem.totalTax);
				if(!tempQuoteLineMap.containsKey(lineItem.lineItemNumber)){
					tempQuoteLineMap.put(lineItem.lineItemNumber, new qcpLineItems());
				}
				if(lineItem.usageClass == 'ADSC'){
					tempQuoteLineMap.get(lineItem.lineItemNumber).ADSCTax = Decimal.valueOf(lineItem.totalTax);
				}
				else if(lineItem.usageClass == 'ANSC'){
					tempQuoteLineMap.get(lineItem.lineItemNumber).ANSCTax = Decimal.valueOf(lineItem.totalTax);
				}
				else if(lineItem.usageClass == 'QSP'){
					tempQuoteLineMap.get(lineItem.lineItemNumber).QSPTax = Decimal.valueOf(lineItem.totalTax);
				}
				// else{
					tempQuoteLineMap.get(lineItem.lineItemNumber).totalTax = Decimal.valueOf(lineItem.totalTax);
				// }
				tempQuoteLineMap.get(lineItem.lineItemNumber).lineItemNumber = lineItem.lineItemNumber;
			}
			returnResponse.lineItems = new List<qcpLineItems>();
			for(qcpLineItems lineItem : tempQuoteLineMap.values()){
				returnResponse.lineItems.add(lineItem);
			}
            //respClass = (ReturnsCodeField) JSON.deserializeUntyped(response.getBody());  
            //Get lineItems[0].lineItemNumber and lineitems[0].totalTax
            //Note: Temporary Object for response
            //List<Object> lineItemsResp = (List<Object>) results.get('lineItems');
            //String lineItemNumber = lineitems[0].lineItemNumber;
            //String totalTax = lineitems[0].totalTax;

            //Set Response 
            stat.code = '200';
			return JSON.serialize(returnResponse);
        }//end if status code is 200
        else if (response.getStatusCode() == 400) {
            //400 - Error, responds with 'standardErrorResponse'
            stat.code = '400';
             //Set Response 
        }//end if status code is 200
        else if (response.getStatusCode() == 401) {
            //401 - Unauthorized, no body in response mesage
            stat.code = '401';
             //Set Response 
        }//end if status code is 200
        else if (response.getStatusCode() == 500) {
            //500 - Error, responds with 'standardErrorResponse
            stat.code = '500';
             //Set Response 
        }//end if status code is 200
        //Set Status to response 
        //resp.status = stat;
        //system.debug('Resp Result: '+resp);


        //return JSON.serialize(resp);
		return response.getBody();
    }//end post method

    private static Decimal monitoringMinusQSP(Decimal monitoring, Decimal qsp){
        System.debug('monitoringMinusQSP');
        if(qsp != null){
        System.debug('qsp != null, sending monitoring(' + monitoring + ') - qsp(' + qsp + ') = ' + (monitoring - qsp) + ' as ANSCAmount.');
        return monitoring - qsp;
        } else {
        System.debug('qsp null, monitoring value = ' + monitoring);
        return monitoring;
        }
    }


private static void addLineItem(Map<String, Object> line, List<LineItems_Z> lineItems, String ADSCBillCode, Decimal ADSCAmount, String ANSCBillCode, Decimal ANSCAmount, String QSPBillCode, Decimal QSPAmount){
	
		LineItems_Z addLineItem = new LineItems_Z();
		System.debug('Deserialized line: ' + line);
		addLineItem.lineItemNumber= (Integer)(line.get('SBQQ__Number__c'));
		addLineItem.quantity = (Integer)(line.get('SBQQ__Quantity__c'));
		addLineItem.extendedPrice = ADSCAmount > 0 ? ADSCAmount : ANSCAmount > 0 ? ANSCAmount : QSPAmount > 0 ? QSPAmount : null;
		//addLineItem.taxDate = '' ;
		//addLineItem.isMulticomponent = false ;
		//addLineItem.locationCode = '' ;
		//addLineItem.deliveryTerm = '' ;
		//addLineItem.postingDate = '' ;
		//addLineItem.costCenter = '' ;
		//addLineItem.departmentCode = '' ;
		//addLineItem.generalLedgerAccount = '' ;
		//addLineItem.materialCode = '' ;
		//addLineItem.projectNumber = '' ;
		//addLineItem.usage = '' ;
		addLineItem.usageClass = ADSCAmount > 0 ? 'ADSC' : ANSCAmount > 0 ? 'ANSC' : QSPAmount > 0 ? 'QSP' : 'RegularTax';  //productMap.get((Id)line.get('SBQQ__Product__c')).ExtnBillCode__c;;
		//addLineItem.vendorSKU = '' ;
		//addLineItem.countryOfOriginISOCode = '' ;
		//addLineItem.modeOfTransport= 0 ;
		//addLineItem.natureOfTransaction= 0 ;
		//addLineItem.intrastatCommodityCode = '' ;
		//addLineItem.netMassKilograms= 0 ;
		//addLineItem.lineItemId= '' ;
		// addLineItem.taxIncludedIndicator = false;
		//addLineItem.simplificationCode = '' ;
		// addLineItem.titleTransfer = '' ;
		//addLineItem.chainTransactionPhase = '' ;
		//  addLineItem.exportProcedure = '' ;
		//addLineItem.materialOrigin = '' ;
		//Set Seller to line Item
		//addLineItem.Seller = seller;
		//Set Customer to line Item
		//addLineItem.Customer = customer;
		//Set Tax Override to Line Item
		//addLineItem.taxOverride = taxOverride;
		//Add Imposition Process to line item
		//addLineItem.impositionToProcess = impositionToProcess;
		//Add jurisdiction override to line item
		//addLineItem.jurisdictionOverride = jurisdictionOverride;
		//Add Sitrus Override
		//addLineItem.situsOverride = situsOverride;
		Product product = new Product();
		//System.debug('(String)line.get(Extn_Bill_Code__c) : ' + productMap.get((Id)line.get('SBQQ__Product__c')).ExtnBillCode__c);
		
		product.productCode = ADSCAmount > 0 ? ADSCBillCode : ANSCAmount > 0 ? ANSCBillCode : QSPAmount > 0 ? QSPBillCode : null;  //productMap.get((Id)line.get('SBQQ__Product__c')).ExtnBillCode__c;
		product.productClass = ADSCAmount > 0 ? ADSCBillCode : ANSCAmount > 0 ? ANSCBillCode : QSPAmount > 0 ? QSPBillCode : null; //productMap.get((Id)line.get('SBQQ__Product__c')).ExtnBillCode__c;
		//Add Product to line item
		addLineItem.product = product;
		/*LineType lineType =  new LineType();
		lineType.lineTypeCode = '';
		lineType.direction = '';
		lineType.content = '';
		lineType.status = '';
		lineType.accumulationLocation = '';
		addLineItem.lineType = lineType;*/
		//Add line item 
		addLineItem.flexibleCodeFieldArray = new FlexibleCodeFieldArray();
		List<FlexibleCodeField> flexibleCodeFields = new List<FlexibleCodeField>();
		// List<FlexibleNumericField> flexibleNumericFields = new List<FlexibleNumericField>();
		// List<FlexibleCodeField> flexibleDateFields = new List<FlexibleCodeField>();
		FlexibleCodeField flexibleCodeField = new FlexibleCodeField();
		flexibleCodeField.value = product.productCode == 'MON' || product.productCode == 'QSP' ? 'RECUR' : 'JOB';
		flexibleCodeField.fieldId = 2;
		flexibleCodeFields.add(flexibleCodeField);
		// FlexibleNumericField flexibleNumericField = new FlexibleNumericField();
		// flexibleNumericField.fieldId = 1;
		// flexibleNumericField.value = 0;
		// flexibleNumericFields.add(flexibleNumericField);
		// FlexibleCodeField flexibleDateField = new FlexibleCodeField();
		// flexibleDateField.value = String.valueOf(System.today());
		// flexibleDateField.fieldId = 2;
		// flexibleDateFields.add(flexibleDateField);
        FlexibleCodeField flexibleAddOnField = new FlexibleCodeField();
		flexibleAddOnField.value = 'Y';
		flexibleAddOnField.fieldId = 4;
		flexibleCodeFields.add(flexibleAddOnField);
        FlexibleCodeField flexibleOMSField = new FlexibleCodeField();
		flexibleOMSField.value = 'N';
		flexibleOMSField.fieldId = 6;
		flexibleCodeFields.add(flexibleOMSField);
		addLineItem.flexibleCodeFieldArray.flexibleCodeField = flexibleCodeFields;
		// addLineItem.flexibleCodeFieldArray.flexibleNumericField = flexibleNumericFields;
		// addLineItem.flexibleCodeFieldArray.flexibleDateField = flexibleDateFields;
		lineItems.add(addLineItem);
}
/*
*---------------------------------------------------------------------------------------------------------------------------------------------------
                                                                WRAPPER CLASSES
*---------------------------------------------------------------------------------------------------------------------------------------------------
*/


/*
    * This class holds the response 
*/
public class qcpPluginResponse{
	public List<qcpLineItems> lineItems{get;set;}
}
public class qcpLineItems{
	public Decimal lineItemNumber{get;set;}
	public Decimal totalTax{get;set;}
	public Decimal ANSCTax{get;set;}
	public Decimal ADSCTax{get;set;}
	public Decimal QSPTax{get;set;}
}
public class response {
    public String correlationId {get;set;}/*Required */
    public String tracingId {get;set;}
    public status status {get;set;}/*Required */

}
public class status {
    public String code {get;set;}/*Required */
    public list<message> messages {get;set;}/*Required */
}
public class message {
    public String type {get;set;}/*Required */
    public String severity {get;set;}
    public String reasonCode {get;set;}
    public String message {get;set;}/*Required */
    public String contect {get;set;} 
}

public class StatisticalValue {
    public Integer invoiceValue {get;set;} 
    public Integer isoCurrencyCodeNum {get;set;} 
    public String isoCurrencyName {get;set;} 
    public String isoCurrencyCodeAlpha {get;set;} 

}

public class FlexibleCodeFieldArray {
    public List<FlexibleCodeField> flexibleCodeField {get;set;} 
    public List<FlexibleNumericField> flexibleNumericField {get;set;} 
    public List<FlexibleCodeField> flexibleDateField {get;set;}
    
}

public class Discount {
    public String userDefinedDiscountCode {get;set;} 
    public Integer amount {get;set;} 
    public Integer percent {get;set;} 

}

public class ReturnsCodeField {
    //public String value {get;set;} 
    //public String name {get;set;} 
    public String documentNumber {get;set;} 
   // public String accumulationDocumentNumber {get;set;} 
    //public String accumulationCustomerNumber {get;set;} 
    //public String documentType {get;set;} 
    //public String billingType {get;set;} 
    //public String orderType {get;set;} 
    //public String postingDate {get;set;} 
    //public String locationCode {get;set;} 
    //public Boolean returnAssistedParametersIndicator {get;set;} 
    //public Boolean returnGeneratedLineItemsIndicator {get;set;} 
    //public String deliveryTerm {get;set;} 
    public String documentDate {get;set;} 
    public String transactionId {get;set;} 
    public String transactionType {get;set;} 
    //public String simplificationCode {get;set;} 
    //public Boolean roundAtLineLevel {get;set;} 
    //public String paymentDate {get;set;} 
    //public String documentSequenceId {get;set;} 
    //public String taxPointDate {get;set;} 
    //public Currency_Z currency_Z {get;set;} // in json: currency
    //public Currency_Z originalCurrency {get;set;} 
    //public Currency_Z companyCodeCurrency {get;set;} 
    public Seller seller {get;set;} 
    public Customer customer {get;set;} 
    public TaxOverride taxOverride {get;set;} 
    //public List<ImpositionToProcess> impositionToProcess {get;set;} 
    //public List<JurisdictionOverride> jurisdictionOverride {get;set;} 
    //public SitusOverride situsOverride {get;set;} 
    //public Discount discount {get;set;} 
    //public Integer proratePercentage {get;set;} 
    //public List<CurrencyConversionFactors> currencyConversionFactors {get;set;} 
    public List<LineItems_Z> lineItems {get;set;} 


}

public class Customer {
    public Boolean isTaxExempt {get;set;} 
    public String exemptionReasonCode {get;set;} 
    //public CustomerCode customerCode {get;set;} 
    public PhysicalOrigin destination {get;set;} 
    //public PhysicalOrigin administrativeDestination {get;set;} 
    //public ExemptionCertificate exemptionCertificate {get;set;} 
    //public List<TaxRegistration> taxRegistration {get;set;} 

   
}

public class NonTaxableOverride {
    public Integer amount {get;set;} 
    public String overrideNonTaxableReasonCode {get;set;} 

   
}

public class CurrencyConversion {
    public Integer isoCurrencyCodeNum {get;set;} 
    public String isoCurrencyName {get;set;} 
    public Integer conversionRate {get;set;} 
    public String isoCurrencyCodeAlpha {get;set;} 

   
}

public class Product {
    public String productCode {get;set;} 
    public String productClass {get;set;} 

   
}

public class Dispatcher {
    public String dispatcherCode {get;set;} 
    public String classCode {get;set;} 
    public List<TaxRegistration> taxRegistration {get;set;} 

   
}

public class ReturnsNumericField {
    public Integer value {get;set;} 
    public String name {get;set;} 

   
}

public class TaxOverride {
    public String overrideType {get;set;} 
    public String overrideReasonCode {get;set;} 

    
}

public class ExemptOverride {
    public Integer amount {get;set;} 
    public String overrideExemptReasonCode {get;set;} 

   
}

public class DeductionOverride {
    public ExemptOverride exemptOverride {get;set;} 
    public NonTaxableOverride nonTaxableOverride {get;set;} 

   
}

public class ImpositionType {
    public String typeDescription {get;set;} 
    public Boolean userDefined {get;set;} 
    public Integer impositionTypeId {get;set;} 
    public String withholdingType {get;set;} 

   
}

public class ImpositionToProcess {
    public String jurisdictionLevel {get;set;} 
    public ImpositionType impositionType {get;set;} 

    
}

public class LineType {
    public String lineTypeCode {get;set;} 
    public String direction {get;set;} 
    public String content {get;set;} 
    public String status {get;set;} 
    public String accumulationLocation {get;set;} 

   
}

public class PhysicalLocation {
    public Integer taxAreaId {get;set;} 
    public String streetAddress1 {get;set;} 
    public String streetAddress2 {get;set;} 
    public String city {get;set;} 
    public String mainDivision {get;set;} 
    public String subDivision {get;set;} 
    public String postalCode {get;set;} 
    public String country {get;set;} 

    
}

public class LineItems {

    
}

public class TaxRegistration {
    public String isoCountryCode {get;set;} 
    public String mainDivision {get;set;} 
    public Boolean hasPhysicalPresenceIndicator {get;set;} 
    public Integer jurisdictionId {get;set;} 
    public String taxRegistrationNumber {get;set;} 
    public List<NexusOverride> nexusOverride {get;set;} 
    public List<PhysicalLocation> physicalLocation {get;set;} 
    public ImpositionType impositionType {get;set;} 

    
}

public class CustomerCode {
    public String code {get;set;} 
    public Boolean isBusinessIndicator {get;set;} 
    public String classCode {get;set;} 

   
}

public class JurisdictionOverride {
    public String jurisdictionLevel {get;set;} 
    public DeductionOverride deductionOverride {get;set;} 
    public Integer rateOverride {get;set;} 
    public ImpositionType impositionType {get;set;} 

   
}

public class ReturnsFields {
    public List<ReturnsCodeField> returnsCodeField {get;set;} 
    public List<ReturnsNumericField> returnsNumericField {get;set;} 
    public List<ReturnsCodeField> returnsDateField {get;set;} 
    public List<ReturnsIndicatorField> returnsIndicatorField {get;set;} 

   
}

public class Currency_Z {
    public String isoCurrencyName {get;set;} 
    public String isoCurrencyCodeAlpha {get;set;} 
    public Integer isoCurrencyCodeNum {get;set;} 

    
}

public class ExemptionCertificate {
    public String details {get;set;} 
    public String exemptionCertificateNumber {get;set;} 

   
}

public class PhysicalOrigin {
    public Integer taxAreaId {get;set;} 
    public String latitude {get;set;} 
    public String longitude {get;set;} 
    public String locationCustomsStatus {get;set;} 
    public String locationCode {get;set;} 
    public String externalJurisdictionCode {get;set;} 
    public String streetAddress1 {get;set;} 
    public String streetAddress2 {get;set;} 
    public String city {get;set;} 
    public String mainDivision {get;set;} 
    public String subDivision {get;set;} 
    public String postalCode {get;set;} 
    public String country {get;set;} 
    public CurrencyConversion currencyConversion {get;set;} 

    
}

public class SitusOverride {
    public String taxingLocation {get;set;} 

   
}

public class ReturnsIndicatorField {
    public Boolean value {get;set;} 
    public String name {get;set;} 

}

public class CurrencyConversionFactors {
    public Currency_Z sourceCurrency {get;set;} 
    public Currency_Z targetCurrency {get;set;} 
    public Integer conversionFactor {get;set;} 

}

public class FlexibleCodeField {
    public String value {get;set;} 
    public Integer fieldId {get;set;} 

}

public class FlexibleNumericField {
    public Integer value {get;set;} 
    public Integer fieldId {get;set;} 

}

public class LineItems_Z {
    public Integer lineItemNumber {get;set;} 
    public String taxDate {get;set;} 
    public Boolean isMulticomponent {get;set;} 
    public String locationCode {get;set;} 
    public String deliveryTerm {get;set;} 
    public String postingDate {get;set;} 
    public String costCenter {get;set;} 
    public String departmentCode {get;set;} 
    public String generalLedgerAccount {get;set;} 
    public String materialCode {get;set;} 
    public String projectNumber {get;set;} 
    public String usage {get;set;} 
    public String usageClass {get;set;} 
    public String vendorSKU {get;set;} 
    public String countryOfOriginISOCode {get;set;} 
    public Integer modeOfTransport {get;set;} 
    public Integer natureOfTransaction {get;set;} 
    public String intrastatCommodityCode {get;set;} 
    public Integer netMassKilograms {get;set;} 
    public String lineItemId {get;set;} 
    public Boolean taxIncludedIndicator {get;set;} 
    public String transactionType {get;set;} 
    public String simplificationCode {get;set;} 
    public String titleTransfer {get;set;} 
    public String chainTransactionPhase {get;set;} 
    public String exportProcedure {get;set;} 
    public String materialOrigin {get;set;} 
    //public Seller seller {get;set;} 
    //public Customer customer {get;set;} 
    public TaxOverride taxOverride {get;set;} 
    public List<ImpositionToProcess> impositionToProcess {get;set;} 
    public List<JurisdictionOverride> jurisdictionOverride {get;set;} 
    public SitusOverride situsOverride {get;set;} 
    public Product product {get;set;} 
    public LineType lineType {get;set;} 
    public String commodityCode {get;set;} 
    public String commodityCodeType {get;set;} 
    public Integer quantity {get;set;} 
    public String quantityMeasureUnit {get;set;} 
    public Integer weight {get;set;} 
    public String weightMeasureUnit {get;set;} 
    public Integer volume {get;set;} 
    public String volumeMeasureUnit {get;set;} 
    public Integer supplementaryUnit {get;set;} 
    public String supplementaryUnitType {get;set;} 
    public StatisticalValue statisticalValue {get;set;} 
    public Integer freight {get;set;} 
    public Integer fairMarketValue {get;set;} 
    public Integer cost {get;set;} 
    public Integer unitPrice {get;set;} 
    public Decimal extendedPrice {get;set;} 
    public Integer landedCost {get;set;} 
    public Discount discount {get;set;} 
    public Integer amountBilledToDate {get;set;} 
    public Integer companyCodeCurrencyTaxableAmount {get;set;} 
    public Integer companyCodeCurrencyTaxAmount {get;set;} 
    public flexibleCodeFieldArray flexibleCodeFieldArray {get;set;} 
    public ReturnsFields returnsFields {get;set;} 
    public List<LineItems> lineItems {get;set;} 

   
}

public class Seller {
    public Boolean nexusIndicator {get;set;} 
    public String nexusReasonCode {get;set;} 
    public String company {get;set;} 
    public String division {get;set;} 
    //public String department {get;set;} 
    //public String utilityProvider {get;set;} 
    public Dispatcher dispatcher {get;set;} 
    public PhysicalOrigin physicalOrigin {get;set;} 
    public PhysicalOrigin administrativeOrigin {get;set;} 
    //public List<TaxRegistration> taxRegistration {get;set;} 

}

public class NexusOverride {
    public String locationRole {get;set;} 
    public Boolean country {get;set;} 
    public Boolean mainDivision {get;set;} 
    public Boolean subDivision {get;set;} 
    public Boolean city {get;set;} 
    public Boolean district {get;set;} 
}


}//end apex class