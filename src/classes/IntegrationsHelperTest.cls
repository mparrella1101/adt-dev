/**
 * @description Test class to cover the 'IntegrationsHelper' Apex Class
 */
 /* Version      Author                  Company                JIRA                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           N/A                  1/10/2022       Initial version
 */
@IsTest
public with sharing class IntegrationsHelperTest {

    /** @description Test to validate the IntegrationsWrapper instantiates correctly */
    @IsTest
    static void integrationsWrapper_Test() {
        IntegrationsHelper.IntegrationsWrapper integWrap = new IntegrationsHelper.IntegrationsWrapper();
        System.assertEquals(true, integWrap.hasData);
    }

    /** @description Test to validate retrieveCreds() behavior */
    @IsTest
    static void retrieveCreds_Test() {
        IntegrationsHelper.IntegrationsWrapper result = IntegrationsHelper.retrieveCreds();
        System.assertNotEquals(false, result.hasData);
    }

    /** @description Test to validate generateTracingId() behavior */
    @IsTest
    static void generateTracingId_Test() {
        String result = IntegrationsHelper.generateTracingId();
        System.assertNotEquals('', result);
        System.assertEquals('SFCPQ_', result.substring(0,6));
    }

    /** @description Test to validate getMMBEmployeeId() behavior */
    @IsTest
    static void getMMBEmployeeId_Test() {
        User testUsr = [SELECT Id, MMB_Employee_ID__c FROM User WHERE IsActive = TRUE AND MMB_Employee_ID__c != NULL LIMIT 1];
        String result = '';
        System.runAs(testUsr){
            result = IntegrationsHelper.getMMBEmployeeId();
        }
        System.assertNotEquals('', result);
    }
}