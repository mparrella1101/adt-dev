/**
 * @description This class will handle all call-outs to the Partner API (currently being used in place of OMS for the SFCPQ Phone Sales Pilot)
 */
/*
    Version      Author                  Company                 Jira                           Date                       Description
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           n/a                             September 22, 2021          Initial build
    1.1         Matt Parrella           Coastal Cloud           SFCPQ-147                       November 1, 2021            Modifying/testing 'submitOrder' callout
    1.2         Matt Parrella           Coastal Cloud           n/a                             January 12, 2022            Implementing use of IntegrationsHelper class
    1.3         Matt Parrella           Coastal Cloud           SFCPQ-147(Defect: 4824)         January 19, 2022            Implementing Promotions data into Submit Order callout
    1.4         Matt Parrella           Coastal Cloud           SFCPQ-147(Defect: 4934)         February 10, 2022           Populating correct parameters to prevent amounts mismatch between SFCPQ and StCPQ
    1.5         Matt Parrella           Coastal Cloud           (N/A?) PROD HOT FIX             March 03, 2022              Adding hot fix that queries for WorkOrders after order submit to close out transaction
*/


public with sharing class ADTRedVentureAPI {
    /* Class variables: START */

    /** @description Hard-coded Decimal value to indicate the maximum amount that can be financed */
    public static Decimal maxFinanceValue = 5000;

    /** @description Hard-coded Decimal value to indicate the minimum amount that can be financed */
    public static Decimal minFinanceValue = 599;

    /** @description Hard-coded Decimal value to indicate the minimum amount for 3-Pay */
    public static Decimal minThreeValue = 200;

    /* Class variables: STOP */

    /* WRAPPER CLASSES: START */

    /**
     * @description Wrapper class that will contain information about the success/failure of the callout and will contain any error messages
     * (if applicable), as well as payload data (if applicable)
     */
    public class ResponseWrapper {
        /** @description Boolean flag to determine if callout was successful (TRUE -> Successful, no errors; FALSE -> error encountered, check 'errorMsg' parameter for details) */
        @AuraEnabled public Boolean isSuccess;

        /** @description A SubmitOrderRespWrapper object containing the response information from the Partner API */
        @AuraEnabled public SubmitOrderRespWrapper submitOrderResp;

        /** @description String variable to hold details of any errors encountered (only populated when 'isSuccess' parameter is FALSE) */
        @AuraEnabled public String errorMsg;

        /** @description Constructor method with passed-in String value intended for use when an error is encountered. Will automatically set the 'isSuccess' parameter to FALSE when this constructor is called */
        public ResponseWrapper(String inputError){
            this.isSuccess = false;
            this.errorMsg = inputError;
        }

        /** @description Empty argument constructor, sets 'isSuccess' parameter to TRUE by default on instantiation */
        public ResponseWrapper(){
            this.isSuccess = true;
        }
    }

    /**
     * @description Wrapper class to parse/store the response from the Submit Order request
    */
    public class SubmitOrderRespWrapper {
        /** @description The Order's related Salesforce OpportunityId */
        @AuraEnabled public String opportunityId;

        /** @description Message returned from Partner API: either 'Success', or contains error information */
        @AuraEnabled public String message;

        /** @description The OMS Order Number & SFCPQ Quote Id (one in the same, for tracking purposes) */
        @AuraEnabled public String orderNo;
    }

    /* WRAPPER CLASSES: STOP */

    /**
     *  @description This method will submit an order to the Partner API
     *
     *  @param quoteId (String) The Salesforce 18-digit record Id of the Quote record that is getting submitted
     *
     *  @return respWrapper (ResponseWrapper) ResponseWrapper object containing information about the results of the callout (success/failure/data)
     */
    @AuraEnabled
    public static ResponseWrapper submitOrder(String quoteId){
        ResponseWrapper respWrapper = new ResponseWrapper();

        // MParrella | 1-12-2022 | Implementing use of IntegrationsHelper class to get callout information
        IntegrationsHelper.IntegrationsWrapper integrationsWrapper = IntegrationsHelper.retrieveCreds();

        // Check if there was a failure obtaining the Custom MDT records
        if (!integrationsWrapper.hasData) {
            System.debug('An exception has been encountered while trying to obtain Mulesoft API Setting custom metadata records.');
            return new ResponseWrapper('An exception has been encountered while trying to obtain Mulesoft API Setting custom metadata records.');
        }

        if (quoteId == '' || quoteId == null) {
            respWrapper.isSuccess = false;
            respWrapper.errorMsg = 'ADTRedVentureAPI.submitOrder did not have a valid \'quoteId\' parameter passed to it.';
            return respWrapper;
        }

        // Use callId parameter value within the 'submitOrder' callout
        String reqBody = generateSubmitOrderReqBody(quoteId);
        if (reqBody == '' || reqBody == null){
            respWrapper.isSuccess = false;
            respWrapper.errorMsg = 'Error encountered when constructing the \'Submit Order\' request payload.';
            return respWrapper;
        }
        else if (reqBody.contains('ERROR: ')){
            System.debug('*** ERROR: ' + reqBody);
            respWrapper.isSuccess = false;
            respWrapper.errorMsg = reqBody;
            return respWrapper;
        }
        else if (reqBody.contains('MISSING: ')){
            respWrapper.isSuccess = false;
            respWrapper.errorMsg = 'Missing field value for: ' + reqBody.replace('MISSING: ', '');
            return respWrapper;
        }

        // Build the callout
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        String username = integrationsWrapper.partnerAPIInfo.Credentials_PartnerAPI_ClientID;
        String password = integrationsWrapper.partnerAPIInfo.Credentials_PartnerAPI_ClientSecret;

        Blob headerValue = Blob.valueOf(username + ':' + password);

        String authHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

        req.setMethod('POST');
        req.setEndpoint(integrationsWrapper.partnerAPIInfo.Endpoint_PartnerAPI_Base_URL + integrationsWrapper.partnerAPIInfo.Endpoint_Submit_Order);
        System.debug('Endpoint generated: ' + integrationsWrapper.partnerAPIInfo.Endpoint_PartnerAPI_Base_URL + integrationsWrapper.partnerAPIInfo.Endpoint_Submit_Order);
        req.setBody(reqBody);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Authorization', authHeader);

        // Callout
        HttpResponse resp = new HttpResponse();
        try {
            resp = http.send(req);
            System.debug('*** Response: ' + resp.getBody());
        }
        Catch(Exception e){
            System.debug('***s Exception Caught: ADTRedVentureAPI.submitOrder encountered an exception when performing the call-out. Details: ' + e.getMessage());
            respWrapper.isSuccess = false;
            respWrapper.errorMsg = 'Exception Caught: ADTRedVentureAPI.submitOrder encountered an exception when performing the call-out. Details: ' + e.getMessage();
            return respWrapper;
        }

        // Handle/parse the response
        if (resp.getStatusCode() == 200){
            SubmitOrderRespWrapper wrap = new SubmitOrderRespWrapper();
            JSONParser parser = JSON.createParser(resp.getBody());
            while (parser.nextToken() != null) {
                if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                    while (parser.nextToken() != null) {
                        if (parser.getCurrentToken() == JSONToken.START_OBJECT){
                            wrap = (ADTRedVentureAPI.SubmitOrderRespWrapper) parser.readValueAs(ADTRedVentureAPI.SubmitOrderRespWrapper.class);
                        }
                    }
                }
            }
            respWrapper.submitOrderResp = wrap;
            return respWrapper;
        } else {
            // Deserializing and re-serializing to extract the 'errorMessage' parameter dynamically
            Map<String,Object> respMap = (Map<String,Object>) JSON.deserializeUntyped(resp.getBody());
            String serializedMsg = JSON.serialize(respMap.get('errors'));
            serializedMsg = serializedMsg.replace('[','');
            serializedMsg = serializedMsg.replace(']','');
            Map<String,Object> errorMap = (Map<String,Object>) JSON.deserializeUntyped(serializedMsg);
            String errorMsg = errorMap.get('errorMessage').toString();
            System.debug('*** ERROR: ' + errorMsg);
            respWrapper.isSuccess = false;
            respWrapper.errorMsg = '\'submitOrder\' Endpoint error: ' + errorMsg;
            return respWrapper;
        }

    }

    /**
     *  @description This method will construct the body of the 'Submit Order' request.
     *
     *  @param quoteId (String) The Salesforce 18-digit record Id of the Quote record that is getting submitted
     *
     *  @return reqBody (String) The request body payload in JSON format
    */
    public static String generateSubmitOrderReqBody(String quoteId) {
        if (quoteId == '' || quoteId == null){
            System.debug('***s API ERROR: ADTRedVentureAPI.generateSubmitOrderReqBody did not have a valid \'quoteId\' or \'callId\' parameter passed to it.');
            return '';
        }

        // Get the related record data for the request body
        SBQQ__Quote__c quoteRecord;
        try {
            quoteRecord = [SELECT   Address_Line_2__c,
                    Deposit_Trip_Fee_Paid__c,
                    DOA_Approval_Status__c,
                    EasyPay_Enrollment__c,
                    Has_ACH_Info__c,
                    Has_CC_Info__c,
                    Installation_Discount_Pct__c,
                    Installation_Total_Discount_Amt__c,
                    Is_Financing__c,
                    MMB_Order_Type__c,
                    Monthly_Monitoring_Charge__c,
                    Monthly_Monitoring_Discount_Pct__c,
                    Monthly_Monitoring_Total_Discount_Amt__c,
                    OMS_Deposit_Paid__c,
                    OrderNumber__c,
                    Password__c,
                    Payment_Interval__c,
                    Payment_Terms_Formula__c,
                    Payment_Type__c,
                    Payment_Type_Service__c,
                    Promotion_Code__c,
                    Promotion_Description__c,
                    QuoteValueMonthly__c,
                    SBQQ__Account__r.DOB_encrypted__c,
                    SBQQ__Account__r.Email__c,
                    SBQQ__Account__r.FirstName__c,
                    SBQQ__Account__r.LastName__c,
                    SBQQ__Account__r.LeadExternalID__c,
                    SBQQ__Account__r.MMBCustomerNumber__c,
                    SBQQ__Account__r.MMBOrderType__c,
                    SBQQ__Account__r.Name,
                    SBQQ__Account__r.Phone,
                    SBQQ__Account__r.Profile_BuildingType__c,
                    SBQQ__Account__r.Profile_RentOwn__c,
                    SBQQ__Account__r.Profile_YearsInResidence__c,
                    SBQQ__Account__r.Status__c,
                    SBQQ__BillingName__c,
                    SBQQ__BillingStreet__c,
                    SBQQ__BillingCity__c,
                    SBQQ__BillingState__c,
                    SBQQ__BillingPostalCode__c,
                    SBQQ__BillingCountry__c,
                    SBQQ__MasterContract__r.Description,
                    SBQQ__Notes__c,
                    SBQQ__Opportunity2__c,
                    SBQQ__Partner__c,
                    SBQQ__Partner__r.Name,
                    SBQQ__PaymentTerms__c,
                    SBQQ__TotalCustomerDiscountAmount__c,
                    SFCPQ_Global_ID__c,
                    Signed_Date_Time__c,
                    Total_Install_Charge__c,
                    Total_Monthly_Charge__c,
                    Transaction_Timestamp__c,
                    TX_Reference_Number__c,
                    TXN_Amount_Authorized__c,
                    TXN_Bank_Account_Number__c,
                    TXN_Bank_Routing_Number__c,
                    TXN_Bank_Credit_Union_Name__c,
                    TXN_Card_Type__c,
                    TXN_Exp_Date__c,
                    TXN_Giact_Token__c,
                    TXN_Last_4_Used__c,
                    TXN_Name_on_Account__c,
                    TXN_Name_on_CC__c,
                    TXN_GUID__c,
                    X3_Payment_Option__c,
            (SELECT Id, First_Name__c, Last_Name__c, Home_Phone__c, Work_Phone__c, Cell_Phone__c FROM Emergency_Contacts__r),
            (SELECT Id, SBQQ__Number__c, SBQQ__ProductCode__c, SBQQ__ProductOption__r.SelectionCode__c, SBQQ__Description__c, SBQQ__Quantity__c, Installation__c, SBQQ__SubscriptionPricing__c, QSP__c FROM SBQQ__LineItems__r WHERE SBQQ__ProductOption__r.SelectionCode__c != NULL)
            FROM    SBQQ__Quote__c
            WHERE   Id =: quoteId
            LIMIT   1];
        }
        catch (QueryException e) {
            System.debug('***s API ERROR: ADTRedVentureAPI.generateSubmitOrderReqBody was unable to retrieve the Quote record. Details: ' + e.getMessage());
            return '';
        }

        try {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeFieldName('submitOrderRequest');
            gen.writeStartObject();
            gen.writeStringField('partnerId', 'SFCPQ');
            gen.writeStringField('callId', '000000000000000000');
            if (quoteRecord.SBQQ__Opportunity2__c == null) { return 'MISSING: SBQQ__Opportunity2__c'; }
            gen.writeStringField('opportunityId', quoteRecord.SBQQ__Opportunity2__c != null ? quoteRecord.SBQQ__Opportunity2__c : '');
            gen.writeStringField('partnerRepName', quoteRecord.SBQQ__Partner__r?.Name != null ? quoteRecord.SBQQ__Partner__r.Name : 'test');
            if (quoteRecord.SBQQ__Account__c == null) { return 'MISSING: SBQQ__Account__c'; }
            gen.writeFieldName('account');
            gen.writeStartObject(); // Start 'account' object
            gen.writeFieldName('name');
            gen.writeStartObject();
            gen.writeStringField('first', quoteRecord.SBQQ__Account__r.FirstName__c);
            gen.writeStringField('last', quoteRecord.SBQQ__Account__r.LastName__c);
            gen.writeEndObject();
            Date formattedDate;
            if (quoteRecord.SBQQ__Account__r?.DOB_encrypted__c != null) {
                Date existingDate = Date.parse(quoteRecord.SBQQ__Account__r.DOB_encrypted__c);
                formattedDate = Date.newInstance(existingDate.year(), existingDate.month(), existingDate.day());
            } else {
                return 'MISSING: Account.DOB_encrypted__c';
            }
            gen.writeStringField('dateOfBirth', String.valueOf(formattedDate));
            gen.writeFieldName('phones');
            gen.writeStartObject();
            if (quoteRecord.SBQQ__Account__r.Phone == null) { return 'MISSING: Account.Phone'; }
            gen.writeStringField('phone', quoteRecord.SBQQ__Account__r.Phone != null ? formatPhoneNumber(quoteRecord.SBQQ__Account__r.Phone) : '');
            gen.writeEndObject();
            if (quoteRecord.SBQQ__Account__r.Email__c == null) { return 'MISSING: Account.Email__c'; }
            gen.writeStringField('email', quoteRecord.SBQQ__Account__r.Email__c);
            gen.writeFieldName('mmb');
            gen.writeStartObject();
            gen.writeStringField('customerId', quoteRecord.SBQQ__Account__r.MMBCustomerNumber__c != null ? quoteRecord.SBQQ__Account__r.MMBCustomerNumber__c : '');
            if (quoteRecord.SBQQ__Account__r.MMBOrderType__c == null) { return 'MISSING: Account.MMBOrderType__c'; }
            gen.writeStringField('orderType', quoteRecord.SBQQ__Account__r.MMBOrderType__c);
            gen.writeEndObject();
            gen.writeFieldName('profile');
            gen.writeStartObject();
            if (quoteRecord.SBQQ__Account__r.Profile_RentOwn__c == null) { return 'MISSING: Account.Profile_RentOwn__c'; }
            // Convert the Rent/Own into a value that the Partner API can accept
            String ownershipValue = '';
            if (quoteRecord.SBQQ__Account__r.Profile_RentOwn__c == 'O') {
                ownershipValue = 'Own';
            }
            else {
                ownershipValue = 'Rent';
            }
            gen.writeStringField('ownership', ownershipValue);
            gen.writeStringField('buildingType', quoteRecord.SBQQ__Account__r.Profile_BuildingType__c != null ? quoteRecord.SBQQ__Account__r.Profile_BuildingType__c : 'SFH');
            gen.writeNumberField('yearsThere', quoteRecord.SBQQ__Account__r.Profile_YearsInResidence__c != null ? Integer.valueOf(quoteRecord.SBQQ__Account__r.Profile_YearsInResidence__c) : 0);
            gen.writeEndObject();
            gen.writeEndObject(); // End 'account' object
            gen.writeFieldName('order');
            gen.writeStartObject(); // Start 'order' object
            gen.writeFieldName('payment');
            gen.writeStartObject(); // Start 'payment' object

            /* Financing & 3Pay Logic: START */
            Boolean multiPayFlag = false;
            Integer multiPayInstallmentsVar = 0;

            // Scenario: 3-Pay Option is selected
            if (quoteRecord.X3_Payment_Option__c){
                multiPayFlag = true;
                multiPayInstallmentsVar = 3;
            }
            // Scenario: Financing option is selected
            else if (quoteRecord.Is_Financing__c){
                multiPayFlag = true;
                multiPayInstallmentsVar = Integer.valueOf(quoteRecord.Payment_Terms_Formula__c);
            }
            /* Financing & 3Pay Logic: STOP */

            // Extract terms number
            Integer numMonths = 0;
            if (quoteRecord.SBQQ__PaymentTerms__c != null) {
                if (quoteRecord.SBQQ__PaymentTerms__c.contains('Months')) {
                    numMonths = Integer.valueOf(quoteRecord.SBQQ__PaymentTerms__c.split(' ')[0]);
                }
            } else {
                return 'MISSING: SBQQ__PaymentTerms__c';
            }
            gen.writeNumberField('terms', numMonths);
            gen.writeBooleanField('easyPay', quoteRecord.EasyPay_Enrollment__c);
            gen.writeBooleanField('multiPay', multiPayFlag);
            gen.writeNumberField('multiPayInstallments', multiPayInstallmentsVar);
            if (quoteRecord.Payment_Type__c == null) { return 'MISSING: Payment_Type__c'; }
            gen.writeStringField('depositPaymentMethod', quoteRecord.Payment_Type__c == 'Credit Card' ? 'CC' : 'ACH');
            if (quoteRecord.Payment_Type_Service__c == null) { return 'MISSING: Payment_Type_Service__c'; }
            gen.writeStringField('recurPaymentMethod', quoteRecord.Payment_Type_Service__c == 'Credit Card' ? 'CC' : 'ACH');
            String recurPayMethod = '';
            switch on quoteRecord.Payment_Interval__c {
                when 'Monthly' {
                    recurPayMethod = 'M';
                }
                when 'Quarterly' {
                    recurPayMethod = 'Q';
                }
                when 'Annually' {
                    recurPayMethod = 'A';
                }
                when 'Semi-Annually' {
                    recurPayMethod = 'S';
                }
                when else {
                    return 'MISSING: Payment_Interval__c';
                }
            }
            gen.writeStringField('recurPaymentInterval', recurPayMethod);
            gen.writeNumberField('depositPaid', quoteRecord.Deposit_Trip_Fee_Paid__c != null ? quoteRecord.Deposit_Trip_Fee_Paid__c : 0.0);
            gen.writeStringField('transactionDate', quoteRecord.Transaction_Timestamp__c != null ? (quoteRecord.Transaction_Timestamp__c).format('yyyy-MM-dd') : '');
            gen.writeFieldName('instruments');
            gen.writeStartObject();
            if (quoteRecord.Has_ACH_Info__c) {
                gen.writeFieldName('ach');
                gen.writeStartObject();
                gen.writeStringField('routingNumber', quoteRecord.TXN_Bank_Routing_Number__c);
                gen.writeStringField('bankName', quoteRecord.TXN_Bank_Credit_Union_Name__c);
                gen.writeStringField('accountNumber', quoteRecord.TXN_Bank_Account_Number__c);
                gen.writeStringField('accountName', quoteRecord.TXN_Name_on_Account__c);
                gen.writeStringField('accountType', 'checking'); //Hard coding, doesn't matter as ACH is now out of scope for pilot so this code won't get executed
                gen.writeEndObject(); // End 'ach' instrument
            }
            if (quoteRecord.Has_CC_Info__c) {
                gen.writeFieldName('creditCard');
                gen.writeStartObject();
                gen.writeStringField('brand', quoteRecord.TXN_Card_Type__c);
                gen.writeStringField('cardholdersName', quoteRecord.TXN_Name_on_CC__c);
                gen.writeStringField('maskedCardNumber', 'XXXXXXXXXXXX' + quoteRecord.TXN_Last_4_Used__c);
                gen.writeStringField('cardExpiry', quoteRecord.TXN_Exp_Date__c);
                gen.writeStringField('refNumber', quoteRecord.TX_Reference_Number__c);
                gen.writeStringField('transactionNumber', quoteRecord.TXN_GUID__c != null ? quoteRecord.TXN_GUID__c : '');
                gen.writeEndObject(); // End Credit Card instrument
            }
            gen.writeEndObject(); // End 'instruments' object
            gen.writeEndObject(); // End 'payment' object
            gen.writeStringField('orderNumber', String.valueOf(quoteRecord.SFCPQ_Global_ID__c));
            gen.writeFieldName('doaDiscount');
            gen.writeStartObject();
            gen.writeNumberField('installPercent', quoteRecord.Installation_Discount_Pct__c != null ? quoteRecord.Installation_Discount_Pct__c : 0);
            gen.writeNumberField('recurringPercent', quoteRecord.Monthly_Monitoring_Discount_Pct__c != null ? quoteRecord.Monthly_Monitoring_Discount_Pct__c : 0);
            gen.writeEndObject(); // End 'doaDiscount' object
            gen.writeFieldName('additionalInfo');
            gen.writeStartObject();
            gen.writeBooleanField('rejectFamiliarization', false); // TODO: Replace with field reference
            if (quoteRecord.Password__c == null) { return 'ERROR: Additional Information is missing, please make correction first.'; }
            gen.writeStringField('globalPic', quoteRecord.Password__c);
            gen.writeFieldName('contacts');
            gen.writeStartArray(); // Contacts array: START
            Integer i = 1;
            if (quoteRecord.Emergency_Contacts__r?.size() == 0){
                return 'ERROR: Additional Information is missing, please make correction first.';
            }
            for (Emergency_Contact__c con : quoteRecord.Emergency_Contacts__r) {
                gen.writeStartObject();
                gen.writeNumberField('seqNumber', i);
                gen.writeStringField('firstName', con.First_Name__c);
                gen.writeStringField('lastName', con.Last_Name__c);
                gen.writeBooleanField('ecv', false); // TODO: Replace with field reference
                gen.writeStringField('homePhone', con.Home_Phone__c != null ? formatPhoneNumber(con.Home_Phone__c) : '');
                gen.writeStringField('cellPhone', con.Cell_Phone__c != null ? formatPhoneNumber(con.Cell_Phone__c) : formatPhoneNumber(con.Home_Phone__c));
                gen.writeStringField('workPhone', con.Work_Phone__c != null ? formatPhoneNumber(con.Work_Phone__c) : formatPhoneNumber(con.Home_Phone__c));
                gen.writeEndObject();
                i++;
            }
            gen.writeEndArray(); // Contacts array: STOP
            gen.writeEndObject();
            /* Order Totals: START */
            gen.writeFieldName('orderTotals');
            gen.writeStartObject();
            gen.writeNumberField('Install', quoteRecord.Total_Install_Charge__c != null ? quoteRecord.Total_Install_Charge__c : 0);
            gen.writeNumberField('InstallDiscount', quoteRecord.Installation_Total_Discount_Amt__c != null ? quoteRecord.Installation_Total_Discount_Amt__c :0); // Promo Discount Amt + DOA Discount Amt for install charges
            gen.writeNumberField('recurring', quoteRecord.Monthly_Monitoring_Charge__c != null ? quoteRecord.Monthly_Monitoring_Charge__c : 0);
            gen.writeNumberField('recurringDiscount', quoteRecord.Monthly_Monitoring_Total_Discount_Amt__c != null? quoteRecord.Monthly_Monitoring_Total_Discount_Amt__c :0); // Promo Discount Amt + DOA Discount Amt for recurring charges
            // KF: ALM Defect 4754 fixes START
            gen.writeNumberField('paymentFinanceMinimum', minFinanceValue);
            gen.writeNumberField('paymentFinanceMaximum', maxFinanceValue);
            gen.writeNumberField('paymentThreeMinimum', minThreeValue);
            gen.writeNumberField('paymentSixtyMinimum', minFinanceValue);
            // KF: ALM Defect 4754 fixes END
            gen.writeEndObject();
            /* Order Totals: STOP */
            gen.writeStringField('orderDate', String.valueOf(Date.today()));
            gen.writeFieldName('orderLines');
            gen.writeStartArray(); // Order Items loop: START
            for (SBQQ__QuoteLine__c quoteLine : quoteRecord.SBQQ__LineItems__r) {
                if (quoteLine.SBQQ__ProductOption__r?.SelectionCode__c != null && quoteLine.SBQQ__ProductOption__r?.SelectionCode__c != '') {
                    String selectionCode = quoteLine.SBQQ__ProductOption__r.SelectionCode__c;
                    if (selectionCode.contains(' ')){
                        selectionCode = selectionCode.replaceAll(' ', '');
                    }
                    gen.writeStartObject();
                    gen.writeNumberField('lineNumber', quoteLine.SBQQ__Number__c != null ? quoteLine.SBQQ__Number__c : 0);
                    gen.writeStringField('code', selectionCode);
                    gen.writeStringField('description', quoteLine.SBQQ__Description__c != null ? quoteLine.SBQQ__Description__c : 'No description found.');
                    gen.writeNumberField('quantity', quoteLine.SBQQ__Quantity__c != null ? Integer.valueOf(quoteLine.SBQQ__Quantity__c) : 0);
                    gen.writeNumberField('installPrice', quoteLine.Installation__c != null ? quoteLine.Installation__c : 0);
                    gen.writeNumberField('recurPrice', quoteLine.SBQQ__SubscriptionPricing__c != null ? Integer.valueOf(quoteLine.SBQQ__SubscriptionPricing__c) : 0);
                    gen.writeNumberField('qspPrice', quoteLine.QSP__c != null ? quoteLine.QSP__c : 0);
                    gen.writeEndObject();
                }
            }
            gen.writeEndArray(); // Order Items loop: STOP
            /* MParrella | 1-19-2022 | Defect 4824: Adding Promotion-related data to Submit Order request: START */
            if (quoteRecord.Promotion_Code__c != null && quoteRecord.Promotion_Code__c != ''){
                gen.writeFieldName('promotions');
                gen.writeStartArray();
                String promoCode = quoteRecord.Promotion_Code__c;
                String promoDescription = quoteRecord.Promotion_Description__c != null ? quoteRecord.Promotion_Description__c : '';
                gen.writeStartObject();
                gen.writeStringField('code', promoCode);
                gen.writeStringField('description', promoDescription);
                gen.writeEndObject();
                gen.writeEndArray();
            }
            /* MParrella | 1-19-2022 | Defect 4824: Adding Promotion-related data to Submit Order request: STOP */
            gen.writeFieldName('billTo');
            gen.writeStartObject(); // BillTo: START
            gen.writeStringField('firstName', quoteRecord.SBQQ__Account__r.FirstName__c);
            gen.writeStringField('lastName', quoteRecord.SBQQ__Account__r.LastName__c);
            if (quoteRecord.SBQQ__BillingStreet__c == null) { return 'MISSING: SBQQ__BillingStreet__c'; }
            gen.writeStringField('addrLine1', quoteRecord.SBQQ__BillingStreet__c);
            gen.writeStringField('addrLine2', quoteRecord.Address_Line_2__c != null ? quoteRecord.Address_Line_2__c : '');
            if (quoteRecord.SBQQ__BillingCity__c == null) { return 'MISSING: SBQQ__BillingCity__c'; }
            gen.writeStringField('city', quoteRecord.SBQQ__BillingCity__c);
            if (quoteRecord.SBQQ__BillingState__c == null) { return 'MISSING: SBQQ__BillingState__c'; }
            gen.writeStringField('state', quoteRecord.SBQQ__BillingState__c);
            if (quoteRecord.SBQQ__BillingPostalCode__c == null) { return 'MISSING: SBQQ__BillingPostalCode__c'; }
            gen.writeStringField('postalCode', quoteRecord.SBQQ__BillingPostalCode__c);
            if (quoteRecord.SBQQ__BillingCountry__c == null) { return 'MISSING: SBQQ__BillingCountry__c'; }
            gen.writeStringField('country', quoteRecord.SBQQ__BillingCountry__c != null ? quoteRecord.SBQQ__BillingCountry__c : 'US');
            gen.writeEndObject(); // BillTo: STOP
            if (quoteRecord.SBQQ__Notes__c != null) {
                gen.writeFieldName('notes');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeNumberField('seq', 1);
                gen.writeStringField('note', quoteRecord.SBQQ__Notes__c);
                gen.writeEndObject();
                gen.writeEndArray();
            }
            gen.writeEndObject();// End 'order' object
            gen.writeStringField('createdDateTime', Datetime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\''));
            gen.writeEndObject();
            gen.close();

            System.debug('*** Request Body: ' + gen.getAsString());

            return gen.getAsString();
        }
        catch(Exception ex){
            return 'ERROR: ' + ex.getMessage() + ' on LINE NUMBER: ' + ex.getLineNumber() + ' STACK TRACE: ' +
                    ex.getStackTraceString();
        }
    }

    /**
     * @description Helper method to format the returned Salesforce Phone number field values into an acceptable format for the
     * 'submitOrder' endpoint (Salesforce format: xxxxxxxxxx ; PartnerAPI Acceptable format: xxx-xxx-xxxx)
     *
     * @param phoneNumber (String) The Phone Number that needs formatting
     *
     * @return (String) The phone number in the following format: xxx-xxx-xxxx
     */
    public static String formatPhoneNumber(String phoneNumber){
        if (phoneNumber.contains('-') && !phoneNumber.contains('(') && !phoneNumber.contains(')')){
            return phoneNumber;
        }
        if (phoneNumber.contains('(') && phoneNumber.contains(')')){
            phoneNumber = phoneNumber.replace('(','');
            phoneNumber = phoneNumber.replace(')','');
            String[] phoneArray = phoneNumber.split(' ');
            return phoneArray[0] + '-' + phoneArray[1];
        }
        if (phoneNumber.length() != 10){
            return '';
        }
        String newNumber = '';
        String[] phoneNumberArray = phoneNumber.split('');
        Integer count = 0;
        for (String s : phoneNumberArray) {
            if (count == 3){
                newNumber += '-' + s;
            }
            else if (count == 6) {
                newNumber += '-' + s;
            }
            else {
                newNumber += s;
            }
            count++;
        }
        return newNumber;
    }

    // MParrella | 03-03-2022 | PROD HOT FIX: START
    /**
     * @description Helper method to query for any WorkOrder records (with MMB data) related to the SFCPQ Quote. Used to close Submit Order transaction via the Partner API
     *
     * @param quoteId The Salesforce 18-character Quote record Id
     *
     * @return Boolean result. TRUE -> Found a WorkOrder record with MMB data; FALSE -> No Workorder found
     */
    @AuraEnabled
    public static Boolean queryWorkOrderRecords(String quoteId){
        return [SELECT Id, Job_Number__c FROM WorkOrder WHERE Quote__c =: quoteId AND Job_Number__c != NULL LIMIT 1].size() == 1  ? true : false;
    }
    // MParrella | 03-03-2022 | PROD HOT FIX: STOP
}