public with sharing class QuotePaymentController 
{
    
    // @AuraEnabled
    // public static SBQQ__Quote__c getQuoteData(Id quoteId){
    //     return [SELECT Id, Total_Install_Charge__c, Minimum_Payment__c, Additional_Payment__c, Due_at_Sale__c, Finance_Eligible_at_Submit__c, Three_Pay_Eligible_at_Submit__c, X3_Payment_Option__c, Financing__c
    //             FROM SBQQ__Quote__c
    //             WHERE Id = :quoteId];
    // }

    @AuraEnabled(cacheable=false)
    public static List<Payment_Info__c> getValidatedPaymentInfo(String accountId)
    {
        List<Payment_Info__c> validPaymentMethods = [SELECT Account__c,ACHAccount__c,ACHBank__c,ACHName__c,ACHRouting__c,ACHType__c,CardExpiration__c,CardLast4__c,CardName__c,CardProfileId__c,CardType__c,CreatedById,CreatedDate,Has_ACH_Info__c,Has_CC_Info__c,Id,IsDeleted,LastModifiedById,LastModifiedDate,LastReferencedDate,LastViewedDate,Name,OwnerId,PayType__c,Quote__c,Remote_Pay_ID__c,SystemModstamp,Validated__c 
        FROM Payment_Info__c 
        WHERE Account__r.Id =:accountId AND Validated__c = true ORDER BY LastModifiedDate ASC];

        return validPaymentMethods;
    }


}