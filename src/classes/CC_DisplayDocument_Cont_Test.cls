/**
 * Created by Matt on 7/30/2021.
 */
/**
 * Apex Test class to cover the 'CC_DisplayDocument_Controller' Apex Class.
 *
 *
 * Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           July 30, 2021           Creating initial version
 */

@IsTest
public with sharing class CC_DisplayDocument_Cont_Test {
    @IsTest
    static void testConstructorBehavior(){
        // Create instance of VF Page
        PageReference testPageRef = Page.CC_DisplayDocument;

        // Set the Test to use the VF Page
        Test.setCurrentPage(testPageRef);

        // Test Document
        String testDocument = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

        // Encode it
        String encodedDoc = EncodingUtil.base64Encode(Blob.valueOf(testDocument));

        // Set the 'docData' URL Parameter
        testPageRef.getParameters().put('docData', encodedDoc);


        Test.startTest();
        CC_DisplayDocument_Controller cont = new CC_DisplayDocument_Controller();
        Test.stopTest();

        System.assertEquals(cont.pdf, testDocument, 'Values should be the same.');
    }
}