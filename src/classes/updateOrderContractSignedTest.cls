/**
 * @description       : 
 * @author            : William F. Miranda
 * @group             : 
 * @last modified on  : 08-23-2021
 * @last modified by  : William F. Miranda
**/
@isTest
public class updateOrderContractSignedTest {
    @isTest static void updateOrderContractSignedTest() {

        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://yourInstance.my.salesforce.com/services/apexrest/updateOrderContractSigned/';
        request.httpMethod = 'POST';
        //set request body 
        request.requestBody = blob.valueOf('{"OrderNo":"1","ContractLink":"2"}');
        //Set Request
        RestContext.request = request;

        //Call method
        updateOrderContractSigned.doPost();
        
        
        
        updateOrderContractSigned.request req = new  updateOrderContractSigned.request();
        req.OrderNo = '';
        req.EnvelopeID = '';
        req.ContractStatus = '';
        req.SignatureDate = dateTime.now();
        req.TimeZone = '';
        req.TimeZoneOffset = 0;


      

                
            }

}