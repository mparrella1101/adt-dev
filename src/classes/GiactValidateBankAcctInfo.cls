/**
* Description: This class is a payment method trigger handler.  It is currently only firing on before insert to prevent multiple active payment methods related to an order.
* ------------------------------------------------------------------------------------------------------------------
* Date            Developer                 Jira#                        Comments
* --------------------------------------------------------------------------------------------------------------------
* 07/23/2021      Jack Gimbert				SFQ-284  Initial development
* 10/28/2021      Charles Varner            SFB-24   Prevent multiple active payment methods related to same order. Write to Billing PaymentMethod object*/

public with sharing class GiactValidateBankAcctInfo {

    @AuraEnabled
    public static responseWrapper sendPost(String tracingId, String routingNumber, String bankAccountNumber, String institutionName, String accountType, Id recordId)
    {
        //Instantiate response wrapper to store output
        responseWrapper output = new responseWrapper();

        //Retrieve Giact endpoint, Client ID & Client Secret From Mulesoft API Settings Custom Metadata, if the record is not found, then an error will be logged in the console
        Mulesoft_API_Settings__mdt msft;
        try
        {
            //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
            Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
            if(environment)
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, Giact_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
            }
            else
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, Giact_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
            }
        }
        catch(QueryException ex)
        {
            return new responseWrapper('Error retrieving custom metadata record from Mulesoft_API_Settings__mdt');
        }



        //Store Giact endpoint, Client ID & Client Secret so that they can be passed into the POST request as parameters
        String clientId = msft.client_id__c;
        String clientSecret = msft.client_secret__c;
        String endpointURL = msft.Endpoint_base_url__c + msft.Giact_Endpoint__c;

        //Create the new request
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        //Create instance of request body object to store values that will be sent in the POST request
        requestObj reqBody = new requestObj();

        //Set values for request body object that will be sent in the POST request
        reqBody.tracingID = tracingID;
        reqBody.routingNumber = routingNumber;
        reqBody.bankAccountNumber = bankAccountNumber;
        reqBody.institutionName = institutionName.remove(' ');

        String username = msft.client_id__c;
        String password = msft.client_secret__c;
        Blob headerValue = Blob.valueOf(username + ':' + password);

        // Encode it
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        //This is a mock endpoint for mulesoft, actual endpoint is TBD
        //Set endpoint, headers and body for request
        request.setEndpoint(endpointURL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;');
        //request.setHeader('client_id', clientId);
        //request.setHeader('client_secret', clientSecret);
        request.setHeader('Authorization', authorizationHeader);
        request.setHeader('tracing_id', tracingID);
        request.setBody(JSON.serialize(reqBody));

        //Send the POST request
        HttpResponse response = http.send(request);

        /*
            This checks the status code of the response, and calls the appropriate method based on what status code is returned.
            IF status code is equal to 200, then parse200 is called, ELSE parseError is called
        */

        if (response.getStatusCode() == 200)
        {
            System.debug('response.getBody(): ' + response.getBody());
            output = parse200(response.getBody(), response.getStatusCode());
            if(output.verificationResponse == 'Pass'){
                savePaymentMethod(output, accountType, routingNumber, bankAccountNumber, institutionName, recordId);
            }
            else{
                output.verificationResponse = getErrorMessage(output.accountResponseCode);
//					output.verificationResponse = output.accountResponseCode == 'ACH_BLOCKED_MMB' ? 'This bank is forbidden for consumer use.' : 'Routing/Account Number Combination Invalid';
            }
            System.debug('Success');
        }
        else
        {
            output = parseError(response.getBody(), response.getStatusCode());

            System.debug('Error');
        }

        return output;
    }

    /*
        This method takes a JSON response from the endpoint as well as the status code associated with that JSON response as parameters
        IF the status code is 200, this method creates a wrapper object of the responseWrapper type from the response and returns it
    */ 

    private static void savePaymentMethod(responseWrapper output, String accountType,String routingNumber, String bankAccountNumber, String bankName, Id recordId){
        blng__PaymentMethod__c pMethod;
        if(recordId.getSobjectType() == SBQQ__Quote__c.sObjectType){
            SBQQ__Quote__c quoteRec = [SELECT Id, SBQQ__Account__c, Account_Email__c, Account_Phone__c,SBQQ__Account__r.FirstName__c,SBQQ__Account__r.LastName__c FROM SBQQ__Quote__c WHERE Id = :recordId];
            String bankAccountName = quoteRec.SBQQ__Account__r.FirstName__c + ' ' + quoteRec.SBQQ__Account__r.LastName__c + ' ' + accountType;
            pMethod = new blng__PaymentMethod__c(Quote__c = recordId, blng__Account__c = quoteRec.SBQQ__Account__c,blng__Active__c = true, blng__BillingFirstName__c = quoteRec.SBQQ__Account__r.FirstName__c, blng__BillingLastName__c = quoteRec.SBQQ__Account__r.LastName__c, blng__PaymentType__c='ACH', blng__BankAccountType__c = accountType, blng__BankName__c = bankName, blng__BankAccountName__c =bankAccountName, blng__BankAccountNumber__c =bankAccountNumber,blng__BankRoutingCode__c = routingNumber/*, blng__PaymentGatewayToken__c = output.itemReferenceId*/);
        }
        else if(recordId.getSobjectType() == Order.sObjectType){
            Order orderRec = [SELECT Id, AccountId, Account.Email__c, Account.Phone,Account.FirstName__c,Account.LastName__c, SBQQ__Quote__c FROM Order WHERE Id = :recordId];
            String bankAccountName = orderRec.Account.FirstName__c + ' ' + orderRec.Account.LastName__c + ' ' + accountType;
            pMethod = new blng__PaymentMethod__c(Quote__c = orderRec.SBQQ__Quote__c, Order__c= recordId, blng__Account__c = orderRec.AccountId,blng__Active__c = true, blng__BillingFirstName__c = orderRec.Account.FirstName__c, blng__BillingLastName__c = orderRec.Account.LastName__c, blng__PaymentType__c='ACH', blng__BankAccountType__c = accountType, blng__BankName__c = bankName, blng__BankAccountName__c =bankAccountName, blng__BankAccountNumber__c =bankAccountNumber,blng__BankRoutingCode__c = routingNumber/*, blng__AutoPay__c = true*//*, blng__PaymentGatewayToken__c = output.itemReferenceId*/ );
        }
        insert pMethod;
    }

    /**
     * @description This method will take in an 'accountResponseCode' parameter value from the 'achVaildateResponse' response message
     * and return the appropriate error message to be displayed on the front-end.
     *
     * @param respCode (String) The 'accountResponseCode' value from the callout response
     *
     * @return errorMsg (String) The error message
     */
    public static String getErrorMessage(String respCode){
        String resp = '';
        switch on respCode {
            when 'GN01','ACH BLOCKED MMB' {
                resp = 'Declined: Account error being reported';
            }
            when 'RT00' {
                resp = 'Declined: Please ensure correct account information entered';
            }
            when 'GS01', 'GN05' {
                resp = 'Declined: Invalid/Unassigned Routing #';
            }
            when 'GP01', 'RT01', 'RT02' {
                resp = 'Declined: Account error being reported';
            }
            when else {
                resp = 'An error was encountered while trying to ACH validation for this quote. Try again and if the problem persists contact IT support.';
            }
        }

        return resp;
    }


    public static responseWrapper parse200(String json, Integer statusCode)
    {
        return (responseWrapper) System.JSON.deserialize(json, responseWrapper.class);
    }

    /*
        This method takes a JSON response from the endpoint as well as the status code associated with that JSON response as parameters
        IF the status code is not 200, this method creates a wrapper object of the responseWrapper type from the response and returns it
    */

    public static responseWrapper parseError(String json, Integer statusCode)
    {
        return (responseWrapper) System.Json.deserialize(json, responseWrapper.class);
    }

    /*
        Wrapper Classes
    */

    //Used to store input from parameters of sendPost method
    public class requestObj
    {
        @AuraEnabled public String tracingID;
        @AuraEnabled public String routingNumber;
        @AuraEnabled public String bankAccountNumber;
        @AuraEnabled public String institutionName;
    }

    //Used to construct the response in Apex from the endpoint
    public class responseWrapper
    {
        @AuraEnabled public String correlationId;
        @AuraEnabled public String tracingId;
        @AuraEnabled public String itemReferenceId;
        @AuraEnabled public String verificationResponse;
        @AuraEnabled public String accountResponseCode;
        @AuraEnabled public String createdDate;
        @AuraEnabled public String ErrorMessage;

        @AuraEnabled public Status status;
        @AuraEnabled public Messages[] messageArray;

        public responseWrapper(){
            this.correlationId = '';
            this.tracingId = '';
            this.itemReferenceId = '';
            this.verificationResponse = '';
            this.accountResponseCode = '';
            this.createdDate = '';
            this.messageArray = new List<Messages>();
        }

        public responseWrapper(String errorMsg){
            this.correlationId = '';
            this.tracingId = '';
            this.itemReferenceId = '';
            this.verificationResponse = '';
            this.accountResponseCode = '';
            this.createdDate = '';
            this.ErrorMessage = errorMsg;
            this.messageArray = new List<Messages>();
        }
    }

    //This is a wrapper object that is referenced inside responseWrapper
    public class Status
    {
        @AuraEnabled public String code;
        @AuraEnabled public List<Messages> messages;
    }

    //This is a wrapper object that is referenced inside responseWrapper
    public class Messages
    {
        @AuraEnabled public String type;
        @AuraEnabled public String reasonCode;
        @AuraEnabled public String context;
        @AuraEnabled public String message;
        @AuraEnabled public String severity;

    }
}