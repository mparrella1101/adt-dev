/**
 * Apex Controller for 'CC_DisplayDocument' VF Page. The main purpose of this controller is to
 * extract encoded document contents from the URL, decode them, and then display them within
 * and iFrame so the user can view the document's contents without having to download the document
 * to Salesforce first.
 *
 *
 * Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           July 29, 2021           Creating initial version
 */

public with sharing class CC_DisplayDocument_Controller {
    public String docData {get;set;}

    public CC_DisplayDocument_Controller(){
        String urlData = '';
        // get the 'docURI' url parameter and display the data
        urlData = System.currentPageReference().getParameters().get('docData');

        // for whatever reason, when the encoded document is passed from back-end to front-end, all '+' symbols are replaced
        // with a whitespace character, so here we're just putting those symbols back in for proper decoding
        if (urlData.contains(' ')){
            urlData = urlData.replaceAll(' ', '+');
        }

        // Decode in Base64 and get the String value of that result
        docData = EncodingUtil.base64Decode(urlData).toString();
    }

    public String pdf {
        get {
            return docData;
        }
    }
}