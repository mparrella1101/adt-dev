/**
 * @description Test class to cover the 'ADTOrderMgmtSysAPI' Apex Class
 *
 * @author  Matt Parrella
 * @date    August 18, 2021
 * @group   CoastalCloud
 */
@isTest
public with sharing class ADTOrderMgmtSysAPI_Test {

    /* Sub-classes for mocking responses: START */

    /**
     * @description Mock 200 Response for 'MMB Order Submission' endpoint
     */
    public class SubmitOrder200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeFieldName('Order');
            gen.writeStartObject();
            gen.writeFieldName('Extn');
            gen.writeStartObject();
            gen.writeFieldName('ADTJobList');
            gen.writeStartArray();
            // Job: Start
            gen.writeStartObject();
            gen.writeStringField('AdjustedJobDuration','0.00');
            gen.writeStringField('AdscBeforeDiscount','0.00');
            gen.writeStringField('AdscDiscount','0.00');
            gen.writeStringField('AdscDoa','0.00');
            gen.writeStringField('AdscFee','0.00');
            gen.writeStringField('AdscTax','0.00');
            gen.writeStringField('AnscBeforeDiscount','9.99');
            gen.writeStringField('AnscDiscount','-2.00');
            gen.writeStringField('AnscDoa','0.00');
            gen.writeStringField('AnscFee','0.00');
            gen.writeStringField('AnscIntroDiscount','0.00');
            gen.writeStringField('AnscTax','0.56');
            gen.writeStringField('DefaultContract','CS004');
            gen.writeStringField('Deposit','8.55');
            gen.writeStringField('DoaLock','N');
            gen.writeStringField('EquipmentType','999');
            gen.writeStringField('Installable','N');
            gen.writeStringField('InstallerAdscDoa','0.00');
            gen.writeStringField('InstallerAnscDoa','0.00');
            gen.writeStringField('InstallerQspDoa','0.00');
            gen.writeStringField('InstallerSaleDoa','0.00');
            gen.writeStringField('JobDuration','0.00');
            gen.writeStringField('JobKey','2021070116491011479610-1');
            gen.writeStringField('JobSaleType','NewSale');
            gen.writeStringField('JobType','INSTCY');
            gen.writeStringField('LineAdscDiscount','0.00');
            gen.writeStringField('LineAnscDiscount','0.00');
            gen.writeStringField('LineQspDiscount','0.00');
            gen.writeStringField('LineSaleDiscount','0.00');
            gen.writeStringField('Modifyprogid','Field_Sales');
            gen.writeStringField('Modifyts','2021-07-01T16:51:02');
            gen.writeStringField('Modifyuserid','michellegray@adt.com.adttest3');
            gen.writeStringField('MultiSystem','1');
            gen.writeStringField('NonFinanceable','0.00');
            gen.writeStringField('ProductFamily','Cyber1');
            gen.writeStringField('ProfileCode','20,CI,CI,,,,,,');
            gen.writeStringField('QspBeforeDiscount','0.00');
            gen.writeStringField('QspDiscount','0.00');
            gen.writeStringField('QspDoa','0.00');
            gen.writeStringField('QspFee','0.00');
            gen.writeStringField('QspIntroDiscount','0.00');
            gen.writeStringField('QspTax','0.00');
            gen.writeStringField('RequireElectrician','N');
            gen.writeStringField('RequireLocksmith','N');
            gen.writeStringField('SaleBeforeDiscount','0.00');
            gen.writeStringField('SaleDiscount','0.00');
            gen.writeStringField('SaleDoa','0.00');
            gen.writeStringField('SaleFee','0.00');
            gen.writeStringField('SaleTax','0.00');
            gen.writeStringField('Subscription','8.55');
            gen.writeStringField('SystemTechnologyId','1033');
            gen.writeStringField('SystemType','CYBPID');
            gen.writeStringField('TaskCode','CY1');
            gen.writeStringField('TechnologyId','1033');
            gen.writeStringField('TemplateId','DIY-CYBPID');
            gen.writeStringField('isHistory','N');
            gen.writeEndObject();
            // Job: End
            gen.writeEndArray();
            gen.writeStringField('ExtnCPQTimeStamp','2021-06-29T12:56:35');
            gen.writeStringField('ExtnChannel','Health');
            gen.writeStringField('ExtnContractNumbers','');
            gen.writeStringField('ExtnContractSigned','N');
            gen.writeStringField('ExtnCustomerNumber','');
            gen.writeStringField('ExtnDocusignEnvelopeID','');
            gen.writeStringField('ExtnFinanceEligible','N');
            gen.writeStringField('ExtnJobNumbers','');
            gen.writeStringField('ExtnMMBBranchNumber','');
            gen.writeStringField('ExtnOrderSubType','UNKNOWN');
            gen.writeStringField('ExtnOrderType','NEW');
            gen.writeStringField('ExtnPaymentAtInstall','');
            gen.writeStringField('ExtnPaymentEasyPay','Y');
            gen.writeStringField('ExtnPaymentRecurringInterval','M');
            gen.writeStringField('ExtnProspectNumber','');
            gen.writeStringField('ExtnSalesAgentType','PHONE');
            gen.writeStringField('ExtnSiteNumber','');
            gen.writeEndObject();
            gen.writeStringField('CustomerEMailID','matt.parrella@coastalcloud.us');
            gen.writeStringField('CustomerFirstName','Matt');
            gen.writeStringField('CustomerLastName','Parrella');
            gen.writeStringField('EnteredBy','nsctestresiuser4@adt.com.adttest3');
            gen.writeStringField('EntryType','CPQ');
            gen.writeStringField('Modifyts','2021-06-29T12:56:35');
            gen.writeStringField('OrderDate','2021-06-29T12:18:08');
            gen.writeStringField('OrderNo','500039697');
            gen.writeStringField('SourceType','');
            gen.writeStringField('Status','submitted');
            gen.writeEndObject();
            gen.writeEndObject();

            // Build the response and return it
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-type', 'application/json');
            response.setBody(gen.getAsString());
            response.setStatusCode(200);
            return response;
        }
    }

    /**
     * @description Mock 200 response for the 'Generate Contract' endpoint
     */
    public class GenerateContract200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {

            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('EnvelopeID','apex-test-envelope-id');
            gen.writeStringField('emailSent','true');
            gen.writeStringField('SigningURL','');
            gen.writeStringField('ErrorFlag','');
            gen.writeStringField('WarningFlag','');
            gen.writeStringField('ErrorMessage','');
            gen.writeEndObject();

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-type', 'application/json');
            response.setBody(gen.getAsString());
            response.setStatusCode(200);
            return response;
        }
    }

    /**
     * @description Mock 200 response for the 'Void Contract' endpoint
     */
    public class VoidContract200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId','brett-delawyer-a-denial-correlation');
            gen.writeStringField('tracingId','apex-trace-test-123');
            gen.writeStringField('envelopeId','apex-test-envelope-id');
            gen.writeStringField('orderId','1234567');
            gen.writeStringField('envelopeStatus','voided');
            gen.writeEndObject();

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-type', 'application/json');
            response.setBody(gen.getAsString());
            response.setStatusCode(200);
            return response;
        }
    }

    /**
     * @description Mock 200 response for the 'Envelope Status' endpoint
    */
    public class EnvelopeStatus200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId','apex-test-correlation-id');
            gen.writeStringField('tracingId','apex-trace-test-123');
            gen.writeStringField('envelopeId','apex-test-envelope-id');
            gen.writeStringField('orderId','1234567');
            gen.writeStringField('envelopeStatus','completed');
            gen.writeEndObject();

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-type', 'application/json');
            response.setBody(gen.getAsString());
            response.setStatusCode(200);
            return response;
        }
    }

    /**
     * @description Mock 200 response for the 'Email Confirmation' endpoint
    */
    public class EmailConfirmation200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex-test-correlation-id');
            gen.writeStringField('tracingId', 'apex-trace-test-123');
            gen.writeEndObject();

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-type', 'application/json');
            response.setBody(gen.getAsString());
            response.setStatusCode(200);
            return response;
        }
    }

    /**
     * @description Mock 401 Error Response
     */
    public class ErrorResponse401Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(401);
            return resp;
        }
    }

    /**
     * @description Mock 404 Error Response
     */
    public class ErrorResponse404Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(404);
            return resp;
        }
    }

    /**
     * @description Mock 400 Error Response
     */
    public class ErrorResponse400Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            HttpResponse resp = new HttpResponse();
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex-test-correlation-id');
            gen.writeStringField('tracingId', 'test-tracing-id');
            gen.writeFieldName('status');
            gen.writeStartObject();
            gen.writeStringField('code', '400');
            gen.writeFieldName('messages');
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('type', 'Error');
            gen.writeStringField('reasonCode', '400');
            gen.writeStringField('message', 'An error occurred.');
            gen.writeStringField('context', 'OMS Backend API');
            gen.writeEndObject();
            gen.writeEndArray();
            gen.close();

            resp.setHeader('Content-Type', 'application/json');
            resp.setBody(gen.getAsString());
            resp.setStatusCode(400);
            return resp;
        }
    }

    /* Sub-classes for mocking responses: STOP */

    /**
     * @description Test method to test OMSResponseWrapper constructors
     */
    @IsTest
    public static void wrapperConstructor_Tests(){
        // Empty constructor test
        ADTOrderMgmtSysAPI.OMSResponseWrapper omsResp = new ADTOrderMgmtSysAPI.OMSResponseWrapper();
        System.assertEquals(true, omsResp.isSuccess, 'Should be defaulted to TRUE, when empty constructor is used.');
        System.assertNotEquals(null, omsResp.errorMsg, 'Parameter should have been initialized to an empty string.');
        System.assertNotEquals(null, omsResp.submissionResp, 'Parameter should have been initialized as a new instance.');
        System.assertNotEquals(null, omsResp.contractWrap, 'Parameter should have been initialized as a new instance.');
        System.assertNotEquals(null, omsResp.contractCreateResp, 'Parameter should have been initialized as a new instance.');
        System.assertEquals(false, omsResp.orderEmailSent, 'Should be defaulted to FALSE.');

        // Overloaded constructor (w/ parameter) test
        ADTOrderMgmtSysAPI.OMSResponseWrapper omsResp2 = new ADTOrderMgmtSysAPI.OMSResponseWrapper('Apex Test Error Msg');
        System.assertEquals(false, omsResp2.isSuccess, 'Should be defaulted to FALSE, when overloaded constructor is used.');
        System.assertEquals('Apex Test Error Msg', omsResp2.errorMsg, 'Unexpected parameter value. Should match the passed-in String.');
        System.assertEquals(false, omsResp2.orderEmailSent, 'Should be defaulted to FALSE.');
    }

    /**
     * @description Test method to test the successful submission of an Order to OMS/MMB
     */
    @IsTest
    public static void submitOMSOrder200_Test() {
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new SubmitOrder200Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.submitOMSOrder('abc123');

        // Assert the results
        System.assertNotEquals(null, respWrap.submissionResp, 'Parameter should have been populated from response.');
        System.assertEquals(1, respWrap.submissionResp.Extn.ADTJobList.size(), 'Should be only 1 Job in the response.');
        System.assertEquals('matt.parrella@coastalcloud.us', respWrap.submissionResp.CustomerEMailID, 'CustomerEMailId parameter value mismatch.');
        System.assertEquals('submitted', respWrap.submissionResp.Status, 'Was expecting a status of \'submitted\'.');
    }

    /**
     * @description Test method to test the successful generation of a Contract
    */
    @IsTest
    public static void generateContract200_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new GenerateContract200Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.generateContract('abc123');

        // Assert the results
        System.assertNotEquals(null, respWrap.contractCreateResp, 'Parameter should have been populated from response.');
        System.assertEquals('true', respWrap.contractCreateResp.emailSent, 'Expected a value of TRUE for the \'emailSent\' parameter.');
        System.assertNotEquals(null, respWrap.contractCreateResp.EnvelopeID, '\'EnvelopeID\' parameter should not be null.');
    }

    /**
     * @description Test method to test a successful attempt of 'voiding' a contract
    */
    @IsTest
    public static void voidContract200_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new VoidContract200Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.handleVoidContract('abc123');

        // Assert the results
        System.assertNotEquals(null, respWrap.contractWrap, 'Parameter should have been populated from response.');
        System.assertEquals('voided', respWrap.contractWrap.envelopeStatus.toLowerCase(), 'Expected a \'voided\' value for the \'envelopeStatus\' parameter.');
    }

    /**
     * @description Test method to test a successful response from '/envelope/status' endpoint
    */
    @IsTest
    public static void getEnvelopeStatus200_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new EnvelopeStatus200Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.getEnvelopeStatus('abc123');

        // Assert the results
        System.assertNotEquals(null, respWrap.contractWrap, 'Parameter should have been populated from response.');
        System.assertEquals('completed', respWrap.contractWrap.envelopeStatus.toLowerCase(), 'Expected a \'completed\' status for the \'envelopeStatus\' parameter value.');
    }

    /**
     * @description Test method to test a successful resending of an email confirmation
    */
    @IsTest
    public static void sendConfirmationEmail200_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new EmailConfirmation200Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.sendOrderEmailConfirmation('abc123');

        // Assert the results
        System.assertEquals(true, respWrap.orderEmailSent, 'Should have been set to TRUE with a successful response.');
    }

    /**
     * @description Test method to test a Error Response of 401
    */
    @IsTest
    public static void handle401Response_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new ErrorResponse401Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.submitOMSOrder('abc123');

        // Assert the results
        System.assertEquals(false, respWrap.isSuccess, 'Should be set to FALSE in a failure scenario.');
        System.assertEquals('Unauthorized or invalid client application credentials', respWrap.errorMsg, 'Unexpected mismatch on returned error message.');
    }

    /**
     * @description Test method to test a Error Response of 404
    */
    @IsTest
    public static void handle404Response_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new ErrorResponse404Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.submitOMSOrder('abc123');

        // Assert the results
        System.assertEquals(false, respWrap.isSuccess, 'Should be set to FALSE in a failure scenario.');
        System.assertEquals('(404) No Endpoint Listener found!', respWrap.errorMsg, 'Unexpected mismatch on returned error message.');
    }

    /**
     * @description Test method to test a Error Response of 400
    */
    @IsTest
    public static void handle400Response_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new ErrorResponse400Mock());

        ADTOrderMgmtSysAPI.OMSResponseWrapper respWrap = ADTOrderMgmtSysAPI.submitOMSOrder('abc123');

        // Assert the results
        System.assertEquals(false, respWrap.isSuccess, 'Should be set to FALSE in a failure scenario.');
        System.assertEquals('Response Error(s): An error occurred.; ', respWrap.errorMsg, 'Unexpected mismatch on returned error message.');
    }
}