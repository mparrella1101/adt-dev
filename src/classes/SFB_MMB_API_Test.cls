@isTest
public class SFB_MMB_API_Test {
    //Test callouts
    @isTest static void testCallout() {
        HTTPResponse resp =  SFB_MMB_API.callMMBIncident('Test');
		 SFB_MMB_API.CreditDetails creditBody = new SFB_MMB_API.CreditDetails();
		creditBody.siteId = 123;
		//creditBody.siteId = 47266607;
		creditBody.numberOfAllowedCredits = 999;
		creditBody.creditReasonCode = 'SFBILL';
		creditBody.billCode = 'SFBILL';
		creditBody.applicationClientName = 'SFBILL';
		creditBody.guardRailByPassCode = 'BYP_ALL';
		creditBody.referenceNumber = '123';
		//creditBody.employeeId =ord.Account.MMBCustomerNumber__c != null ? Integer.valueOf(ord.Account.MMBCustomerNumber__c) : null;//Customer Id? No... 
		creditBody.employeeId = 125861;//Currently needs to be hardcoded
		creditBody.adjustAmount = 100.00;//???
		creditBody.adjustComment = 'Transfer to Salesforce Billing';//'suggested $'+creditBody.adjustAmount;
		//Serialize json to send to callout method
		String RequestIncidentJSON = JSON.serialize(creditBody, true);        
        HTTPResponse resp2 =  SFB_MMB_API.callMMBCredit(RequestIncidentJSON, 'https://test.com/');
        SFB_MMB_API.CreditDetails wrp = new  SFB_MMB_API.CreditDetails();
        wrp.siteId = 123;
        wrp.applicationClientName = 'Test';
        wrp.adjustAmount = 123;
        wrp.employeeId = 123;
        wrp.guardRailByPassCode = '';
        wrp.numberOfAllowedCredits = 123;
        wrp.referenceNumber = '';
        wrp.creditReasonCode = '';
        wrp.billCode = '';
        wrp.adjustComment = '';
       // /Account acc = new Account(Name = 'Test');
       // insert acc;
       // blng__PaymentMethod__c pm = new blng__PaymentMethod__c();
       // pm.blng__Account__c = acc.Id ;
      //  pm.blng__PaymentType__c = 'ACH';
      //  insert pm;

        SFB_MMB_API.createIncidentRequest((Id)'01q30000000A1iW','123','123',dateTime.now(),dateTime.now(),'123','123','Test','Test');
    }
}