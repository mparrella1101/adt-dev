/**
 * Test class to cover the 'CC_JobToQuoteRedirectCont' Apex Controller
 *
 *
 * Version      Author                  Company                 Date                        Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           October 25, 2021            Initial version
 */

@IsTest
public with sharing class CC_JobToQuoteRedirectCont_Test {
    @TestSetup
    static void setup_data() {
        // Create a quote record
        SBQQ__Quote__c newQuote = new SBQQ__Quote__c(
                MMB_Job__c = '123456',
                SBQQ__Notes__c = 'Apex Test Quote!'
        );

        insert newQuote;
    }

    @IsTest
    static void test_behavior_positive() {
        // Set the current page to the vf page
        Test.setCurrentPage(new PageReference('Page.cc_jobToQuoteRedirect'));

        // Set the 'ident' url param
        System.currentPageReference().getParameters().put('ident', '123456');

        Test.startTest();
        CC_JobToQuoteRedirectCont cont = new CC_JobToQuoteRedirectCont();
        cont.init();
        Test.stopTest();

        SBQQ__Quote__c testQuote = [SELECT Id, SBQQ__Notes__c FROM SBQQ__Quote__c WHERE Id =: cont.quote.id LIMIT 1];

        System.assertNotEquals(null, testQuote);

        System.assertEquals('Apex Test Quote!', testQuote.SBQQ__Notes__c);

    }

    @IsTest
    static void test_behavior_negative() {
        // Set the current page to the vf page
        Test.setCurrentPage(new PageReference('Page.cc_jobToQuoteRedirect'));

        // Set the 'ident' url param
        System.currentPageReference().getParameters().put('ident', '');

        Test.startTest();
        CC_JobToQuoteRedirectCont cont = new CC_JobToQuoteRedirectCont();
        cont.init();
        Test.stopTest();


        System.assertEquals(null, cont.quote);
        System.assertEquals('Error: No Job Number passed in!', cont.statusMsg);
    }

    @IsTest
    static void test_behavior_exception() {
        // Set the current page to the vf page
        Test.setCurrentPage(new PageReference('Page.cc_jobToQuoteRedirect'));

        // Set the 'ident' url param
        System.currentPageReference().getParameters().put('ident', '000000');

        Test.startTest();
        CC_JobToQuoteRedirectCont cont = new CC_JobToQuoteRedirectCont();
        cont.init();
        Test.stopTest();


        System.assertEquals(null, cont.quote);
        String expectedErrorMsg = 'Error: Could not locate SalesforceCPQ Quote record. Details: List has no rows for assignment to SObject';
        System.assertEquals(expectedErrorMsg, cont.statusMsg);
    }
}