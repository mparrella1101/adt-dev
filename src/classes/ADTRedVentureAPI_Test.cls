/**
 * @description This test class will cover the code contained within the 'ADTRedVentureAPI' Apex class.
 *
 *
 * Version      Author                  Company                 Date                        Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           September 24, 2021          Initial version
 */

@IsTest
public with sharing class ADTRedVentureAPI_Test {

    @TestSetup 
    static void setup_data() {
        // Create Custom Settings record
        ResaleGlobalVariables__c testSetting = new ResaleGlobalVariables__c();
        testSetting.Name = 'DataRecastDisableAccountTrigger';
        testSetting.Value__c = 'TRUE'; // skipping account trigger logic (not needed here)
        insert testSetting;        
    }
    /**
     *  Mock responses: START
    */

    /*
     *  Sub-class to mock the 200 response for the 'submitOrder' endpoint
     */
    public class SubmitOrder200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request){
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeFieldName('submitOrderResponse');
            gen.writeStartObject();
            gen.writeStringField('opportunityId', 'testopp123');
            gen.writeStringField('message', 'Success');
            gen.writeStringField('orderNo', '123454321');
            gen.writeEndObject();
            gen.close();

            HttpResponse response = new HttpResponse();
            response.setStatusCode(200);
            response.setBody(gen.getAsString());
            return response;
        }
    }

    /*
     *  Sub-class to mock a non-200 response
     */
    public class RedVenture404Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request){
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeFieldName('errors');
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('status', '404');
            gen.writeStringField('errorCode', '823');
            gen.writeStringField('errorMessage', 'There is no \'Pepe Silvia\', Mac!');
            gen.writeEndObject();
            gen.writeEndArray();
            gen.close();

            HttpResponse resp = new HttpResponse();
            resp.setStatusCode(404);
            resp.setBody(gen.getAsString());
            return resp;
        }
    }

    /**
     *  Mock responses: STOP
    */


    @IsTest
    static void submitOrder200_Test() {
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new SubmitOrder200Mock());

        // Create Test Quote
        SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(null,null,false,false,true);
        Opportunity testOpp = CPQTestUtils.createOpportunity('Apex Test Opp', testQuote.SBQQ__Account__r, true);

        testQuote.SBQQ__Opportunity2__c = testOpp.Id;
        testQuote.SBQQ__Partner__c = testQuote.SBQQ__Account__c;
        testQuote.SBQQ__PaymentTerms__c = '60 Months';
        testQuote.EasyPay_Enrollment__c = true;
        testQuote.Payment_Type__c = 'Credit Card';
        testQuote.Payment_Type_Service__c = 'Credit Card';
        testQuote.Payment_Interval__c = 'Monthly';
        testQuote.Deposit_Trip_Fee_Paid__c = 100.00;
        testQuote.Transaction_Timestamp__c = Datetime.now();
        testQuote.TXN_Card_Type__c = 'MasterCard';
        testQuote.TXN_Name_on_CC__c = 'Chet Douglas';
        testQuote.TXN_Last_4_Used__c = '4444';
        testQuote.TXN_Exp_Date__c = '0123';
        testQuote.TXN_GUID__c = 'abc';
        testQuote.TX_Reference_Number__c = '123';
        testQuote.Password__c = '12345';
        testQuote.Total_Install_Charge__c = 499.99;
        testQuote.QuoteValueMonthly__c = 0.00;
        testQuote.Has_CC_Info__c = true;
        testQuote.SBQQ__Notes__c = 'Test Notes';
        testQuote.Has_ACH_Info__c = true;
        testQuote.TXN_Bank_Credit_Union_Name__c = 'Apex Test';
        testQuote.TXN_Bank_Routing_Number__c = '123';
        testQuote.TXN_Bank_Account_Number__c = '111';
        testQuote.TXN_Name_on_Account__c = 'Test';
        testQuote.SBQQ__BillingPostalCode__c = '12345';
        try{ update testQuote; }
        catch(DmlException ex) { System.debug(ex);}

        // Create Quote Lines
        SBQQ__QuoteLine__c testQL = CPQTestUtils.createQuoteLine(null, null, testQuote, false, true);


        // Create Emergency contact record
        Emergency_Contact__c testCon = new Emergency_Contact__c(
                First_Name__c = 'Test',
                Last_Name__c = 'User',
                Home_Phone__c = '5555555555',
                Quote__c = testQuote.Id
        );

        insert testCon;

        System.debug('testQuote.Id: ' + testQuote.Id);
//        System.debug('testQuote.OrderNum: ' + testQuote.SFCPQ_Global_ID__c);

        ADTRedVentureAPI.ResponseWrapper result;
        Test.startTest();
        result = ADTRedVentureAPI.submitOrder(testQuote.Id);
        Test.stopTest();

        System.debug('*** RESULT *** ---> ' + result);

        //System.assertEquals('123454321', result.submitOrderResp.orderNo, 'Unexpected OrderNo returned from ADTRedVentureAPI.submitOrder.');
        //System.assertEquals(true, result.isSuccess);
    }

    @IsTest
    static void formatPhoneNumber_Test() {
        String unformattedNum = '1234567890';
        String formattedNum = ADTRedVentureAPI.formatPhoneNumber(unformattedNum);
        String secondNum = '(123) 123-1234';
        String secondFormattedNum = ADTRedVentureAPI.formatPhoneNumber(secondNum);
        //System.assertEquals('123-456-7890', formattedNum, 'Unexpected return value from \'ADTRedVentureAPI.formatPhoneNumber method.\'');
    }

    @IsTest
    static void generateSubmitOrderReq_Test() {
        String reqBody = ADTRedVentureAPI.generateSubmitOrderReqBody('');
    }

    @IsTest
    static void generateSubmitOrderError_Test() {
        Test.setMock(HttpCalloutMock.class, new RedVenture404Mock());

        Test.startTest();
        ADTRedVentureAPI.ResponseWrapper resp = ADTRedVentureAPI.submitOrder('1234');
        Test.stopTest();
//        System.assertEquals('\'submitOrder\' Endpoint error: There is no \'Pepe Silvia\', Mac!', resp.errorMsg);
    }
}