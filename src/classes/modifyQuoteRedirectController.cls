public with sharing class modifyQuoteRedirectController 
{
    @AuraEnabled
    public static QuoteWrapper getQuote(Id quoteId){
        SBQQ__Quote__c quote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c WHERE Id = :quoteId];
        SBQQ__CustomAction__c customAction = [SELECT Id, Name FROM SBQQ__CustomAction__c WHERE Name = 'Add Home Security Services'];
        Product2 resiServicesBundleProduct = [SELECT Id, Name FROM Product2 WHERE Name = 'Residential Services'];

        QuoteWrapper quoteWrapper = new QuoteWrapper();
        quoteWrapper.quote = quote;
        quoteWrapper.customActionId = customAction.id;
        quoteWrapper.productId = resiServicesBundleProduct.id;
        return quoteWrapper;
    }    

    public class QuoteWrapper{   
        @AuraEnabled
        public SBQQ__Quote__c quote {get;set;}
        @AuraEnabled
        public String customActionId {get;set;}  
        @AuraEnabled 
        public String productId {get;set;}
        
    }
}