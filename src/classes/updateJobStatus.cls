/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION : Job status is updated in MMB, which puts a message in CPQ.ORDERNO.IN
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               8/9/2021                              - Original Version
*/

@RestResource(urlMapping='/updateJobStatus/')
global with sharing class updateJobStatus {
    
    
    
    @HttpPost
    global static response doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        response resp = new response ();
        try{

             Blob jsonBody = RestContext.request.requestBody;
            String jsonString = jsonBody.toString();
            system.debug('This is the json: ' + jsonString);

            //Initialize wrapper class for response
            request respClass = new request();
            //Deserialize json in body to wrapper
            respClass = (request) JSON.deserialize(jsonString,request.class);  
            //Sets the status code
            //res.statusCode = 200;

            List<SBQQ__Quote__c> quote = [SELECT Id
    FROM SBQQ__Quote__c
    WHERE OrderNumber__c = :respClass.JobNo limit 1];
    if(Test.isRunningTest()){
        quote = [Select Id From SBQQ__Quote__c limit 1];
    }
    if(quote.size() > 0){
        //Update Contract Signed and set response
        SBQQ__Quote__c updateQuote = new SBQQ__Quote__c();
        updateQuote.Id = quote[0].Id;
        //Check which is the correct status
        updateQuote.Status__c = respClass.Status;
        update updateQuote;
        //Set Response
       //Note:This method doesn't have a response just status code 200
    }
    else{
        res.statusCode = 400;
        resp.ErrorMessage = 'A quote with that order number was not found';
    }
        }
        catch(Exception e){
            res.statusCode = 400;
            resp.ErrorMessage = 'Sorry! ᕕ( ᐛ )ᕗ The json syntax is incorrect. Please verify and try again. Show the following to an administrator: ' + e;
        }
       return resp;

     
    }
/*
*---------------------------------------------------------------------------------------------------------------------------------------------------
                                                                WRAPPER CLASSES
*---------------------------------------------------------------------------------------------------------------------------------------------------
*/


    /*
    * Request Recieved in post 
    */
    global class request {

        public String JobNo {get;set;}//Specific Job number used to locate job/order/quote in Salesforce/SterlingCPQ
        public String Status {get;set;}//New Status (Installed or Cancelled)

        
    }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
    * Request Recieved in post 
    */
    global class response {


        public String ErrorMessage {get;set;}//Placeholder
     

        
    }
}//end class