@isTest
public with sharing class GiactValidateBankAcctInfoTest 
{
    /**
     *  Sub-class for mocking a 200 response
     */
	@TestSetup
	static void setup(){
		 ResaleGlobalVariables__c testSetting = new ResaleGlobalVariables__c();
        testSetting.Name = 'DataRecastDisableAccountTrigger';
        testSetting.Value__c = 'TRUE'; // skipping account trigger logic (not needed here)
        insert testSetting;

		Address__c addr = new Address__c();
        addr.Latitude__c = 29.91760000000000;
        addr.Longitude__c = -90.17870000000000;
        addr.Street__c = '56528 E Crystal Ct';
        addr.City__c = 'Westwego';
        addr.State__c = 'LA';
        addr.PostalCode__c = '70094';
        addr.OriginalAddress__c = '56528 E CRYSTAL CT, WESTWEGO, LA, 70094';
        insert addr;

        // Create Test Account
         Account testAccount = new Account();
        testAccount.AddressID__c = addr.Id;
        testAccount.Name = 'Test Account';
        testAccount.FirstName__c = 'Test';
        testAccount.LastName__c = 'Account';
        testAccount.LeadStatus__c = 'Active';
        testAccount.UnassignedLead__c = false;
        testAccount.InService__c = true;
        testAccount.Business_Id__c = '1100 - Residential';          
         
        testAccount.ShippingPostalCode = '22102';
        testAccount.Data_Source__c = 'TEST';
        insert testAccount;     

        // Create Test Quote
        SBQQ__Quote__c testQuote = new SBQQ__Quote__c();
        testQuote.SBQQ__Account__c = testAccount.Id;
        testQuote.OMS_Order_No__c = '123456';
        insert testQuote;
	}
    public class mock200 implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mock200Response = '';
            //Create a mock 200 status code JSON response using JSONGenerator class
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'd5f6fbf8-6774-4a95-9b59-15348943abd4');
            gen.writeStringField('tracingId', 'A19283745');
            gen.writeStringField('itemReferenceId', '203661837');
            gen.writeStringField('verificationResponse', 'Pass');
            gen.writeStringField('accountResponseCode', '_1111');
            gen.writeStringField('createdDate', string.valueOf(Date.today()));
            gen.writeEndObject();

            mock200Response = gen.getAsString();

            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mock200Response);
            res.setStatusCode(200);
            return res;
        }
    } 

    /**
     *  Sub-class for mocking an error response
     */
    public class mockError implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mockErrorResponse = '';
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'd5f6fbf8-6774-4a95-9b59-15348943abd4');
            gen.writeStringField('tracingId', 'A19283745');
            gen.writeFieldName('status');
                gen.writeStartObject();
                gen.writeStringField('code', '400');
                gen.writeFieldName('messages');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('type', 'Error');
                gen.writeStringField('severity', '1');
                gen.writeStringField('reasonCode', 'ADT-400');
                gen.writeStringField('context', 'ACH-Validation-Mock');
                gen.writeStringField('message', 'Test 400 Error');
                gen.writeEndObject();
                gen.writeEndArray();
                gen.writeEndObject();
                gen.writeEndObject();

            mockErrorResponse = gen.getAsString();
            System.debug(mockErrorResponse);

            //Create a fake error status code response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mockErrorResponse);
            res.setStatusCode(400);
            return res;
        }
    } 

    //This test tests that a valid response is received when the status code returned is 200
    @isTest
    public static void test200Callout()
    {
		Test.startTest();
		SBQQ__Quote__c qt = [SELECT Id FROM SBQQ__Quote__c WHERE OMS_Order_No__c = '123456'];
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200());
        //Sending dummy values as the response is pre-set
        GiactValidateBankAcctInfo.responseWrapper resp = GiactValidateBankAcctInfo.sendPost('1', '2', '3', '4', 'Checking', qt.Id);
        //Assert values passed into mock response are returned
        System.assertEquals('d5f6fbf8-6774-4a95-9b59-15348943abd4', resp.correlationId, 'ERROR: CorrelationId');
        System.assertEquals('A19283745', resp.tracingId, 'ERROR: tracingId');
        System.assertEquals('203661837', resp.itemReferenceId, 'ERROR: itemReferenceId');
        System.assertEquals('Pass', resp.verificationResponse, 'ERROR: verificationResponse');
        System.assertEquals('_1111', resp.accountResponseCode, 'ERROR: CorrelationId');
        System.assertEquals(string.valueOf(Date.today()), resp.createdDate, 'ERROR: createdDate');
		Test.stopTest();
    }

    //This test tests that a valid response is received when the status code returned is not 200
    @isTest 
    public static void testErrorCallout()
    {
        //Set mock callout class
		Test.startTest();
		SBQQ__Quote__c qt = [SELECT Id FROM SBQQ__Quote__c WHERE OMS_Order_No__c = '123456'];
        Test.setMock(HttpCalloutMock.class, new mockError());
        GiactValidateBankAcctInfo.responseWrapper resp = GiactValidateBankAcctInfo.sendPost('1', '2', '3', '4', 'Checking', qt.Id);
        System.assertEquals('d5f6fbf8-6774-4a95-9b59-15348943abd4', resp.correlationId, 'ERROR: CorrelationId');
        System.assertEquals('A19283745', resp.tracingId, 'ERROR: tracingId');
        System.assertEquals('400', resp.status.code, 'ERROR: Status Code' );
        System.assertEquals('Error', resp.status.messages[0].type, 'Error: Status Type');
        System.assertEquals('1', resp.status.messages[0].severity, 'Error: Status Severity');
        System.assertEquals('ADT-400', resp.status.messages[0].reasonCode, 'Error: Status Reason Code');
        System.assertEquals('ACH-Validation-Mock', resp.status.messages[0].context, 'Error: Status Context');
        System.assertEquals('Test 400 Error', resp.status.messages[0].message, 'Error: Status Message');
		Test.stopTest();
    }
	@isTest
	public static void testErrorMessage(){
		GiactValidateBankAcctInfo.getErrorMessage('GN01');
		GiactValidateBankAcctInfo.getErrorMessage('RT00');
		GiactValidateBankAcctInfo.getErrorMessage('GS01');
		GiactValidateBankAcctInfo.getErrorMessage('GP01');
	}
}