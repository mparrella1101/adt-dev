@isTest
private class VertexResponseTest 
{
    @isTest
    public static void VertexResponseTest()
    {
        // Sub-classes: START
        VertexResponse.cls_administrativeOrigin origin = new VertexResponse.cls_administrativeOrigin();
        origin.streetAddress1 = 'test';
        System.assertEquals('test', origin.streetAddress1);
        origin.city = 'test';
        System.assertEquals('test', origin.city);
        origin.mainDivision = 'test';
        System.assertEquals('test', origin.mainDivision);

        VertexREsponse.cls_physicalOrigin physical = new VertexResponse.cls_physicalOrigin();
        physical.streetAddress1 = 'test';
        System.assertEquals('test', physical.streetAddress1);
        physical.city = 'test';
        System.assertEquals('test', physical.city);
        physical.mainDivision = 'test';
        System.assertEquals('test', physical.mainDivision);

        VertexResponse.cls_assistedParameters assistParams = new VertexResponse.cls_assistedParameters();
        assistParams.paramName = 'test';
        System.assertEquals('test', assistParams.paramName);
        assistParams.value = 'test';
        System.assertEquals('test', assistParams.value);
        assistParams.phase = 'test';
        System.assertEquals('test', assistParams.phase);
        assistParams.ruleName = 'test';
        System.assertEquals('test', assistParams.ruleName);

        VertexResponse.cls_customer customer = new VertexResponse.cls_customer();

        VertexResponse.cls_destination destination = new VertexResponse.cls_destination();
        destination.streetAddress1 = 'test';
        System.assertEquals('test', destination.streetAddress1);
        destination.city = 'test';
        System.assertEquals('test', destination.city);
        destination.mainDivision = 'test';
        System.assertEquals('test', destination.mainDivision);
        destination.postalCod = 'test';
        System.assertEquals('test', destination.postalCod);

        VertexResponse.cls_product product = new VertexResponse.cls_product();

        VertexResponse.cls_taxes taxes = new VertexResponse.cls_taxes();

        VertexResponse.cls_jurisdiction jurisdiction = new VertexResponse.cls_jurisdiction();
        jurisdiction.jurisdictionId = 1;
        System.assertEquals(1, jurisdiction.jurisdictionId);
        jurisdiction.jurisdictionLevel = '5000';
        System.assertEquals('5000', jurisdiction.jurisdictionLevel);
        jurisdiction.jurisdictionName = 'test';
        System.assertEquals('test', jurisdiction.jurisdictionName);

        VertexResponse.cls_imposition imposition = new VertexResponse.cls_imposition();
        imposition.impositionId = 1;
        System.assertEquals(1, imposition.impositionId);
        imposition.name = 'test';
        System.assertEquals('test', imposition.name);

        VertexResponse.cls_impositionType impType = new VertexResponse.cls_impositionType();
        impType.typeDescription = 'test';
        System.assertEquals('test', impType.typeDescription);
        impType.impositionTypeId = 1;
        System.assertEquals(1, impType.impositionTypeId);

        VertexResponse.cls_taxRuleId taxRule = new VertexResponse.cls_taxRuleId();
        taxRule.ruleId = 'test';
        System.assertEquals('test', taxRule.ruleId);

        VertexResponse.cls_inclusionRuleId incRuleId = new VertexResponse.cls_inclusionRuleId();
        incRuleId.ruleId = 'test';
        System.assertEquals('test', incRuleId.ruleId);

        VertexResponse.cls_flexibleCodeField flexCodeField = new VertexResponse.cls_flexibleCodeField();
        flexCodeField.fieldId = 1;
        System.assertEquals(1, flexCodeField.fieldId);
        flexCodeField.value = 'test';
        System.assertEquals('test', flexCodeField.value);

        VertexResponse.cls_flexibleNumericField flexNumField = new VertexResponse.cls_flexibleNumericField();
        flexNumField.fieldId = 1;
        System.assertEquals(1, flexNumField.fieldId);
        flexNumField.value = 'test';
        System.assertEquals('test', flexNumField.value);

        VertexResponse.cls_flexibleDateField flexDateField = new VertexResponse.cls_flexibleDateField();
        flexDateField.fieldId = 1;
        System.assertEquals(1, flexDateField.fieldId);
        flexDateField.value = 'test';
        System.assertEquals('test', flexDateField.value);

        VertexResponse.cls_seller sellercls = new VertexResponse.cls_seller();
        sellercls.company = 'test';
        System.assertEquals('test', sellercls.company);
        sellercls.division = 'test';
        System.assertEquals('test', sellercls.division);

        VertexResponse.cls_flexibleFields flexFields = new VertexResponse.cls_flexibleFields();
        flexFields.flexibleCodeField = new List<VertexResponse.cls_flexibleCodeField> { flexCodeField };
        System.assertEquals(1, flexFields.flexibleCodeField.size());
        flexFields.flexibleDateField = new List<VertexResponse.cls_flexibleDateField> { flexDateField };
        System.assertEquals(1, flexFields.flexibleDateField.size());
        flexFields.flexibleNumericField = new List<VertexResponse.cls_flexibleNumericField> { flexNumField };
        System.assertEquals(1, flexFields.flexibleNumericField.size());

        VertexResponse.cls_lineItems lineItems = new VertexResponse.cls_lineItems();
        lineItems.lineItemNumber = 1.0;
        System.assertEquals(1.0, lineItems.lineItemNumber);
        lineItems.usageClass = 'test';
        System.assertEquals('test', lineItems.usageClass);
        lineItems.product = product;
        System.assertEquals(product, lineItems.product);
        lineItems.quantity = 1.0;
        System.assertEquals(1.0, lineItems.quantity);
        lineItems.extendedPrice = 1.0;
        System.assertEquals(1.0, lineItems.extendedPrice);
        lineItems.taxes = new List<VertexResponse.cls_taxes>{ taxes };
        System.assertEquals(1, lineItems.taxes.size());
        lineItems.totalTax = 'test';
        System.assertEquals('test', lineItems.totalTax);
        lineItems.flexibleFields = flexFields;
        System.assertEquals(flexFields, lineItems.flexibleFields);
        lineItems.assistedParameters = new List<VertexResponse.cls_assistedParameters> { assistParams };
        System.assertEquals(1, lineItems.assistedParameters.size());


        // Sub-classes: STOP
        VertexResponse resp = new VertexResponse();
        resp.tracingId = 'test';
        System.assertEquals('test', resp.tracingId);
        resp.correlationId = 'test';
        System.assertEquals('test', resp.correlationId);
        resp.documentNumber = '111';
        System.assertEquals('111', resp.documentNumber);
        resp.postingDate = 'test';
        System.assertEquals('test', resp.postingDate);
        resp.documentDate = 'test';
        System.assertEquals('test', resp.documentDate);
        resp.transactionId = 'test';
        System.assertEquals('test', resp.transactionId);
        resp.transactionType = 'test';
        System.assertEquals('test', resp.transactionType);
        resp.subTotal = 0.00;
        System.assertEquals(0.00, resp.subTotal);
        resp.total = 0.00;
        System.assertEquals(0.00, resp.total);
        resp.totalTax = 0.00;
        System.assertEquals(0.00, resp.totalTax);
        resp.seller = sellercls;
        System.assertEquals(sellercls, resp.seller);




        
        String str = JSON.serialize(resp);
        VertexResponse.parse(str);
    }
}