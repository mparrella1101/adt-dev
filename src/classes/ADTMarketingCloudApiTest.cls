@isTest
public with sharing class ADTMarketingCloudApiTest 
{
    
    /**
     *  Sub-class for mocking a 204 response
     */
    
    public class mock204 implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mock204Response = '';
            //Create a mock 204 status code JSON response using JSONGenerator class
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeEndObject();
            mock204Response = gen.getAsString();
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mock204Response);
            res.setStatusCode(204);
            return res;
        }
    } 

    /**
     *  Sub-class for mocking a 401 response
     */
    
    public class mock401 implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mock401Response = '';
            //Create a mock 401 status code JSON response using JSONGenerator class
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'd5f6fbf8-6774-4a95-9b59-15348943abd4');
            gen.writeStringField('tracingId', 'A19283745');
            gen.writeFieldName('status');
                gen.writeStartObject();
                gen.writeStringField('code', '401');
                gen.writeFieldName('messages');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('type', 'Error');
                gen.writeStringField('severity', '1');
                gen.writeStringField('reasonCode', 'ADT-401');
                gen.writeStringField('context', 'SFDC-Marketing-Cloud-API');
                gen.writeStringField('message', 'Test 401 Error');
                gen.writeEndObject();
                gen.writeEndArray();
                gen.writeEndObject();
                gen.writeEndObject();

            mock401Response = gen.getAsString();
            System.debug(mock401Response);

            //Create a fake error status code response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mock401Response);
            res.setStatusCode(401);
            return res;
        }
    } 

    /**
     *  Sub-class for mocking a 500 response
     */

    public class mock500 implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mock500Response = '';
            //Create a mock 500 status code JSON response using JSONGenerator class
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'd5f6fbf8-6774-4a95-9b59-15348943abd4');
            gen.writeStringField('tracingId', 'A19283745');
            gen.writeFieldName('status');
                gen.writeStartObject();
                gen.writeStringField('code', '500');
                gen.writeFieldName('messages');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('type', 'Error');
                gen.writeStringField('severity', '1');
                gen.writeStringField('reasonCode', 'ADT-500');
                gen.writeStringField('context', 'SFDC-Marketing-Cloud-API');
                gen.writeStringField('message', 'Test 500 Error');
                gen.writeEndObject();
                gen.writeEndArray();
                gen.writeEndObject();
                gen.writeEndObject();

            mock500Response = gen.getAsString();
            System.debug(mock500Response);

            //Create a fake error status code response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mock500Response);
            res.setStatusCode(500);
            return res;
        }
    } 

    //This tests that a valid response is received when the status code returned is 204
    @isTest
    public static void test204Callout()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock204());
        //Sending dummy values as the response is pre-set
        List<ADTMarketingCloudApi.Attributes> attributesTest = new List<ADTMarketingCloudApi.Attributes>();
        ADTMarketingCloudApi.responseWrapper resp = ADTMarketingCloudApi.sendPost('emailLayout', 'emailAddress', 12345);

        //Assert that the responseWrapper specified for a 204 response code in the switch statement is returned
        System.assertEquals('SUCCESS: The POST email request was successful!', resp.responseMessage);
       
    }

    //This tests that a valid response is received when the status code returned is 401
    @isTest 
    public static void test401Callout()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock401());
        List<ADTMarketingCloudApi.Attributes> attributesTest = new List<ADTMarketingCloudApi.Attributes>();
        ADTMarketingCloudApi.responseWrapper resp = ADTMarketingCloudApi.sendPost('emailLayout', 'emailAddress', 12345);
        System.assertEquals('Test 401 Error', resp.responseMessage);

    }
    //This tests that a valid response is received when the status code returned is 500
    @isTest 
    public static void test500Callout()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTMarketingCloudApi.responseWrapper resp = ADTMarketingCloudApi.sendPost('emailLayout', 'emailAddress', 12345);
        System.assertEquals('d5f6fbf8-6774-4a95-9b59-15348943abd4', resp.correlationId, 'ERROR: CorrelationId');
        System.assertEquals('A19283745', resp.tracingId, 'ERROR: tracingId');
        System.assertEquals('500', resp.status.code, 'ERROR: Status Code' );
        System.assertEquals('Error', resp.status.messages[0].type, 'Error: Status Type');
        System.assertEquals('1', resp.status.messages[0].severity, 'Error: Status Severity');
        System.assertEquals('ADT-500', resp.status.messages[0].reasonCode, 'Error: Status Reason Code');
        System.assertEquals('SFDC-Marketing-Cloud-API', resp.status.messages[0].context, 'Error: Status Context');
        System.assertEquals('Test 500 Error', resp.status.messages[0].message, 'Error: Status Message');
    }

    //All input parameters for 'sendPost()' are required, this method tests that when one of the values is null, that the appropriate responseWrapper message is returned
    @isTest
    public static void testNullCheck()
    {

        ADTMarketingCloudApi.responseWrapper resp = ADTMarketingCloudApi.sendPost('emailLayout', 'emailAddress', null);
        System.assertEquals('ERROR: All parameters are required. These are the values you passed in: emailLayout: emailLayout, emailAddress: emailAddress, customerId: null', resp.responseMessage);
    }
}