/**
 * Apex Test class to cover and test logic of the 'ADTContentMgmtAPI' Apex class.
 *
 *
 * Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           July 30, 2021           Creating initial version
    1.1         Matt Parrella           Coastal Cloud           August 2, 2021          Finishing up unit tests (current coverage = ~90%)
 */

@IsTest
public with sharing class ADTContentMgmtAPITest {

    /**
     *  Sub-class to mock the 200 response for 'Get Documents' endpoint
     */
    public class GetDocuments200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request){
            String mockGetDocuments200Response = '';

            // Build mock 200 response for 'Get Documents': START
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');

            // Documents array
            gen.writeFieldName('document');
            gen.writeStartArray();
            for(Integer i = 0; i < 3; i++){
                gen.writeStartObject();
                gen.writeStringField('docId', String.valueOf(i));
                gen.writeStringField('docURI', '/apex/test/uri_' + i);
                gen.writeStringField('docSize', String.valueOf((Math.random() * 10000).round()).replace('.',''));
                gen.writeStringField('MIMEType', 'application/pdf');
                gen.writeStringField('accountType', 'Residential');
                gen.writeStringField('customerName', 'Pepe Silvia');
                gen.writeStringField('importDate', String.valueOf(Date.today().addDays(i)));
                gen.writeStringField('customerNumber', String.valueOf((Math.random() * 100000).round()).replace('.',''));
                gen.writeStringField('siteNumber', String.valueOf((Math.random() * 100000).round()).replace('.',''));
                gen.writeStringField('orderNumber', String.valueOf((Math.random() * 100000).round()).replace('.',''));
                gen.writeStringField('documentType', 'Contract');
                gen.writeStringField('jobNumber', String.valueOf((Math.random() * 100000).round()).replace('.',''));
                gen.writeEndObject();
            }
            gen.writeEndArray();
            gen.writeEndObject();
            mockGetDocuments200Response = gen.getAsString();

            HttpResponse response = new HttpResponse();
            response.setHeader('Content-type', 'application/json');
            response.setBody(mockGetDocuments200Response);
            response.setStatusCode(200);
            return response;
        }
    }

    /**
     *  Sub-class to mock the 200 response from /Document/Content (getDocumentContent) endpoint
     */
    public class getDocumentContent200Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {
            String mockGetDocumentContent200Response = '';

            // Set Test Document Contents
            String testDocumentContents = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

            // Encode the doc in Base64
            String encodedDoc = EncodingUtil.base64Encode(Blob.valueOf(testDocumentContents));

            // Build Mock 200 Response for 'Get Document Content': Start
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            gen.writeStringField('content', encodedDoc);
            gen.writeEndObject();
            // Build Mock 200 Response for 'Get Document Content': Stop

            mockGetDocumentContent200Response = gen.getAsString();

            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-type', 'application/json');
            resp.setBody(mockGetDocumentContent200Response);
            resp.setStatusCode(200);
            return resp;
        }
    }


    /**
     * Sub-class to mock the 400 response from 'Get Documents'/'Get Document Content' endpoint
     */
    public class getDocuments400Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request) {

            String mockGetDocuments400Response = '';

            // Build Mock 400 Response for 'Get Documents': START
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            // Status object
            gen.writeFieldName('status');
            gen.writeStartObject();
            gen.writeStringField('code', '400');
            gen.writeFieldName('messages');
            // Messages array: start
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('type', 'Error');
            gen.writeStringField('severity', '1');
            gen.writeStringField('reasonCode', 'ADT-400');
            gen.writeStringField('context', 'content-mgmt-exp-api-MOCK');
            gen.writeStringField('message', 'Test 400 error message');
            gen.writeEndObject();
            gen.writeEndArray();
            // Messages array: end
            gen.writeEndObject();
            // Build Mock 400 Response for 'Get Documents': STOP

            mockGetDocuments400Response = gen.getAsString();

            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-type', 'application/json');
            resp.setBody(mockGetDocuments400Response);
            resp.setStatusCode(400);
            return resp;
        }
    }

    /**
     *  Sub-class to mock 404 response from 'Get Documents'/'Get Document Content' endpoint
     */
    public class getDocuments404Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request){
            String mockGetDocuments404Response = '';

            // Build Mock 404 Response for 'Get Documents': START
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            // Status object
            gen.writeFieldName('status');
            gen.writeStartObject();
            gen.writeStringField('code', '404');
            gen.writeFieldName('messages');
            // Messages array: start
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('type', 'Error');
            gen.writeStringField('severity', '1');
            gen.writeStringField('reasonCode', 'ADT-404');
            gen.writeStringField('context', 'content-mgmt-exp-api-MOCK');
            gen.writeStringField('message', 'No Document Found');
            gen.writeEndObject();
            gen.writeEndArray();
            // Messages array: end
            gen.writeEndObject();
            // Build Mock 404 Response for 'Get Documents': STOP

            mockGetDocuments404Response = gen.getAsString();

            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-type', 'application/json');
            resp.setBody(mockGetDocuments404Response);
            resp.setStatusCode(404);
            return resp;
        }
    }

    /**
     *  Sub-class to mock 500 response from 'Get Documents'/'Get Document Content' endpoint
     */
    public class getDocuments500Mock implements  HttpCalloutMock {
        public HttpResponse respond(HttpRequest request){
            String mockGetDocuments500Response = '';

            // Build Mock 500 Response for 'Get Documents': START
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            // Status object
            gen.writeFieldName('status');
            gen.writeStartObject();
            gen.writeStringField('code', '404');
            gen.writeFieldName('messages');
            // Messages array: start
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('type', 'Error');
            gen.writeStringField('severity', '1');
            gen.writeStringField('reasonCode', 'ADT-404');
            gen.writeStringField('context', 'content-mgmt-exp-api-MOCK');
            gen.writeStringField('message', 'Internal Server Error');
            gen.writeEndObject();
            gen.writeEndArray();
            // Messages array: end
            gen.writeEndObject();
            // Build Mock 500 Response for 'Get Documents': STOP

            mockGetDocuments500Response = gen.getAsString();

            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-type', 'application/json');
            resp.setBody(mockGetDocuments500Response);
            resp.setStatusCode(500);
            return resp;
        }
    }


    @IsTest
    static void responseWrapperConstructor_Test(){
        // Overloaded Constructor (w/ params) test
        ADTContentMgmtAPI.ResponseWrapper respWrapper = new ADTContentMgmtAPI.ResponseWrapper(false, 'test error msg');
        System.assertEquals(false, respWrapper.isSuccess, 'Value should\'ve been set by the constructor.');
        System.assertEquals('test error msg', respWrapper.errorMsg, 'Value should\'ve been set by the constructor.');

        // No Constructor params test
        ADTContentMgmtAPI.ResponseWrapper responseWrapper2 = new ADTContentMgmtAPI.responseWrapper();
        System.assertEquals(true, responseWrapper2.isSuccess, 'Should be defaulted to TRUE.');
        System.assertNotEquals(null, responseWrapper2.payload, 'Should\'ve been initialized.');
        System.assertNotEquals(null, responseWrapper2.messages, 'Should\'ve been initialized.');
    }

    @IsTest
    static void documentContentWrapperConstructor_Test(){
        // Constructor test
        ADTContentMgmtAPI.DocumentContentWrapper dcw = new ADTContentMgmtAPI.DocumentContentWrapper();
        System.assertEquals('', dcw.correlationId, 'Should\'ve been initialized.');
        System.assertEquals('', dcw.tracingId, 'Should\'ve been initialized.');
        System.assertEquals('', dcw.content, 'Should\'ve been initialized.');
    }

    @IsTest
    static void getDocuments_200_Test(){
        // Set the Mock
        Test.setMock(HttpCalloutMock.class, new GetDocuments200Mock());

        ADTContentMgmtAPI.ResponseWrapper response = ADTContentMgmtAPI.getDocuments('test');

        // Assert the 'payload' array is not null and pull out returned docs
        System.assertNotEquals(0, response.payload.size(), 'Should have returned 3 documents.');

        System.assertEquals('', response.errorMsg, 'Should be no error messages.');
        System.assertEquals(true, response.isSuccess, 'Should be TRUE (successful).');

        // Iterate through returned docs and assert no value is null
        for(ADTContentMgmtAPI.QuoteDocumentWrapper quoteDoc : response.payload){
            System.assertEquals('application/pdf', quoteDoc.MIMEType, 'MIMEType should be application/pdf');
            System.assertEquals('Residential', quoteDoc.accountType, 'Account Type should be \'Residential\'');
            System.assertEquals('Pepe Silvia', quoteDoc.customerName, 'Customer Name should be \'Pepe Silvia\'');
            System.assertEquals('Contract', quoteDoc.documentType, 'Document Type should be \'Contract\'');
            System.assertNotEquals(null, quoteDoc.customerNumber, 'Customer Number should not be null.');
            System.assertNotEquals(null, quoteDoc.docSize, 'Doc Size should not be null.');
            System.assertNotEquals(null, quoteDoc.docURI, 'Doc URI should not be null.');
            System.assertNotEquals(null, quoteDoc.importDate, 'Import data should not be null.');
            System.assertNotEquals(null, quoteDoc.jobNumber, 'Job Number should not be null.');
            System.assertNotEquals(null, quoteDoc.orderNumber, 'Order Number should not be null.');
            System.assertNotEquals(null, quoteDoc.siteNumber, 'Site Number should not be null.');
            System.assertNotEquals(null, quoteDoc.uID, 'Unique ID should not be null.');
        }
    }

    @IsTest
    static void getDocuments_400_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new getDocuments400Mock());

        ADTContentMgmtAPI.ResponseWrapper respWrapper = ADTContentMgmtAPI.getDocuments('test');

        System.assertEquals(0, respWrapper.payload.size(), 'Should\'ve returned 0 documents.');
        System.assertNotEquals(0, respWrapper.messages.size(), 'Should have at least 1 message returned.');
        System.assertEquals('Response Error(s): Test 400 error message; ', respWrapper.errorMsg, 'Unexpected \'errorMsg\' attribute value.');
    }

    @IsTest
    static void getDocuments_404_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new getDocuments404Mock());

        ADTContentMgmtAPI.ResponseWrapper respWrapper = ADTContentMgmtAPI.getDocuments('test');

        System.assertEquals(0, respWrapper.payload.size(), 'Should\'ve returned 0 documents.');
        System.assertNotEquals(0, respWrapper.messages.size(), 'Should have at least 1 message returned.');
        System.assertEquals('Response Error(s): No Document Found; ', respWrapper.errorMsg, 'Unexpected \'errorMsg\' attribute value.');
    }

    @IsTest
    static void getDocuments_500_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new getDocuments500Mock());

        ADTContentMgmtAPI.ResponseWrapper respWrapper = ADTContentMgmtAPI.getDocuments('test');

        System.assertEquals(0, respWrapper.payload.size(), 'Should\'ve returned 0 documents.');
        System.assertNotEquals(0, respWrapper.messages.size(), 'Should have at least 1 message returned.');
        System.assertEquals('Response Error(s): Internal Server Error; ', respWrapper.errorMsg, 'Unexpected \'errorMsg\' attribute value.');
    }

    @IsTest
    static void getDocumentContents_200_Test(){
        // Set the mock
        Test.setMock(HttpCalloutMock.class, new getDocumentContent200Mock());

        ADTContentMgmtAPI.ResponseWrapper respWrapper = ADTContentMgmtAPI.getDocumentContent('test_uri');

        System.assertNotEquals(null, respWrapper.docContents, 'Document contents should not be null.');
        String decodedDocContents = EncodingUtil.base64Decode(respWrapper.docContents).toString();
        System.assertEquals('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', decodedDocContents, 'Document contents don\'t match after encoding/decoding.');
    }

}