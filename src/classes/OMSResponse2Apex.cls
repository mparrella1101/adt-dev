/**
 * Created by Matt on 8/17/2021.
 */
/**
 * [CLASS DESCRIPTION GOES HERE]
 *
 *
 * Version      Author                  Company                 Date            Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           [DATE]          [DESCRIPTION]
 */

public class OMSResponse2Apex {

    public class Order {
       @AuraEnabled public Extn Extn;
       @AuraEnabled public String CustomerEMailID;
       @AuraEnabled public String CustomerFirstName;
       @AuraEnabled public String CustomerLastName;
       @AuraEnabled public String EnteredBy;
       @AuraEnabled public String EntryType;
       @AuraEnabled public String Modifyts;
       @AuraEnabled public String OrderDate;
       @AuraEnabled public String OrderNo;
       @AuraEnabled public String SourceType;
       @AuraEnabled public String Status;
    }

    @AuraEnabled public Order Order;

    public class ADTJobList {
        @AuraEnabled public String AdjustedJobDuration;
        @AuraEnabled public String AdscBeforeDiscount;
        @AuraEnabled public String AdscDiscount;
        @AuraEnabled public String AdscDoa;
        @AuraEnabled public String AdscFee;
        @AuraEnabled public String AdscTax;
        @AuraEnabled public String AnscBeforeDiscount;
        @AuraEnabled public String AnscDiscount;
        @AuraEnabled public String AnscDoa;
        @AuraEnabled public String AnscFee;
        @AuraEnabled public String AnscIntroDiscount;
        @AuraEnabled public String AnscTax;
        @AuraEnabled public String DefaultContract;
        @AuraEnabled public String Deposit;
        @AuraEnabled public String DoaLock;
        @AuraEnabled public String EquipmentType;
        @AuraEnabled public String Installable;
        @AuraEnabled public String InstallerAdscDoa;
        @AuraEnabled public String InstallerAnscDoa;
        @AuraEnabled public String InstallerQspDoa;
        @AuraEnabled public String InstallerSaleDoa;
        @AuraEnabled public String JobDuration;
        @AuraEnabled public String JobKey;
        @AuraEnabled public String JobSaleType;
        @AuraEnabled public String JobType;
        @AuraEnabled public String LineAdscDiscount;
        @AuraEnabled public String LineAnscDiscount;
        @AuraEnabled public String LineQspDiscount;
        @AuraEnabled public String LineSaleDiscount;
        @AuraEnabled public String Modifyprogid;
        @AuraEnabled public String Modifyts;
        @AuraEnabled public String Modifyuserid;
        @AuraEnabled public String MultiSystem;
        @AuraEnabled public String NonFinanceable;
        @AuraEnabled public String ProductFamily;
        @AuraEnabled public String ProfileCode;
        @AuraEnabled public String QspBeforeDiscount;
        @AuraEnabled public String QspDiscount;
        @AuraEnabled public String QspDoa;
        @AuraEnabled public String QspFee;
        @AuraEnabled public String QspIntroDiscount;
        @AuraEnabled public String QspTax;
        @AuraEnabled public String RequireElectrician;
        @AuraEnabled public String RequireLocksmith;
        @AuraEnabled public String SaleBeforeDiscount;
        @AuraEnabled public String SaleDiscount;
        @AuraEnabled public String SaleDoa;
        @AuraEnabled public String SaleFee;
        @AuraEnabled public String SaleTax;
        @AuraEnabled public String Subscription;
        @AuraEnabled public String SystemTechnologyId;
        @AuraEnabled public String SystemType;
        @AuraEnabled public String TaskCode;
        @AuraEnabled public String TechnologyId;
        @AuraEnabled public String TemplateId;
        @AuraEnabled public String isHistory;
    }

    public class Extn {
        @AuraEnabled public List<ADTJobList> ADTJobList;
        @AuraEnabled public String ExtnCPQTimeStamp;
        @AuraEnabled public String ExtnChannel;
        @AuraEnabled public String ExtnContractNumbers;
        @AuraEnabled public String ExtnContractSigned;
        @AuraEnabled public String ExtnCustomerNumber;
        @AuraEnabled public String ExtnDocusignEnvelopeID;
        @AuraEnabled public String ExtnFinanceEligible;
        @AuraEnabled public String ExtnJobNumbers;
        @AuraEnabled public String ExtnMMBBranchNumber;
        @AuraEnabled public String ExtnOrderSubType;
        @AuraEnabled public String ExtnOrderType;
        @AuraEnabled public String ExtnPaymentAtInstall;
        @AuraEnabled public String ExtnPaymentEasyPay;
        @AuraEnabled public String ExtnPaymentRecurringInterval;
        @AuraEnabled public String ExtnProspectNumber;
        @AuraEnabled public String ExtnSalesAgentType;
        @AuraEnabled public String ExtnSiteNumber;
    }


    public static OMSResponse2Apex parse(String json) {
        return (OMSResponse2Apex) System.JSON.deserialize(json, OMSResponse2Apex.class);
    }
}