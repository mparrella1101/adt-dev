public with sharing class ADTstringUtil 
{
    @AuraEnabled
    public static void emailShortcode(String quoteId)
    {
        /**
         * Retrieve the quote record in question by taking it's ID and retrieving it
         */


        SBQQ__Quote__c quoteRecord = [SELECT Id, OrderNumber__c, SBQQ__Account__r.Email__c, SBQQ__Account__c FROM SBQQ__Quote__c WHERE Id =:quoteId];

        /**
         * Get the Email address stored on the account related to the quote in question
         */

        List<String> userEmailAddress = new List<String>();
        userEmailAddress.add(quoteRecord.SBQQ__Account__r.Email__c);
        
        String shortcode = generateShortCode(quoteRecord.OrderNumber__c.remove('Q-'));

        Payment_Info__c paymentinfo = new Payment_Info__c(Remote_Pay_Id__c = shortcode, Account__c = quoteRecord.SBQQ__Account__c, Quote__c = quoteRecord.Id);
        insert paymentinfo;

        /**
         * Create email message object to send to the customer
         */

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject('ADT Remote Pay');
        mail.setToAddresses(userEmailAddress);

        /**
         * Get Custom Metadata record that stores the base url for myadt, that is sent to the customer with the shortcode
         */
        Mulesoft_API_Settings__mdt msft;
        
        msft = [SELECT MasterLabel, ShortCode_Email_Base_URL__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
        try 
        {
            //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
            Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
            if(environment)
            {
                msft = [SELECT MasterLabel, ShortCode_Email_Base_URL__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
            }
            else
            {
                msft = [SELECT MasterLabel, ShortCode_Email_Base_URL__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
            }
        }
        catch(QueryException ex)
        {
            System.debug('Unable to find the appropriate custom metadata record for type: Mulesoft API Setting ');
        }
        

        String htmlBody = '<h1>Your shortcode is: ' + shortcode + '</h1> <p><br /><a href="' + msft.ShortCode_Email_Base_URL__c + shortcode + '"><br>Please click here to navigate back to ADT</a></p>';
        mail.setHtmlBody(htmlBody);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

    }


    public static String generateShortCode(String orderNumber)
    {

        /**
         * Flip the ordernumber per ADT method
         */

        Integer flippedOrderNo = Integer.valueOf(orderNumber.substring(0,1)+orderNumber.substring(orderNumber.length()-1)+orderNumber.substring(1,orderNumber.length()-1));

        /**
         * myBool => Variable used to indicate when the while loop should end that generates the base26 string
         * base26 => Variable used to store the output of this method
         */
 
        boolean myBool = false;
        String base26 = '';

        /**
         *  base26Map => Map for storing the base26 cypher
         */

        Map<String, String> base26Map = new Map<String,String>
        {'0' => '0', 
        '1' => '1', 
        '2' => '2', 
        '3' => '3', 
        '4' => '4', 
        '5' => '5', 
        '6' => '6', 
        '7' => '7', 
        '8' => '8', 
        '9' => '9', 
        '10' => 'A', 
        '11' => 'B', 
        '12' => 'C',
        '13' => 'D',
        '14' => 'E',
        '15' => 'F',
        '16' => 'G',
        '17' => 'H',
        '18' => 'I',
        '19' => 'J',
        '20' => 'K',
        '21' => 'L',
        '22' => 'M',
        '23' => 'N',
        '24' => 'O',
        '25' => 'P'};

        /**
         * Convert the base10 integer to base26
         */

        while(myBool == false)
        {
            if(flippedOrderNo <= 26)
            {
                myBool = true;
            }
            base26 += base26Map.get(String.valueof(Math.mod(flippedOrderNo, 26)));

            flippedOrderNo = flippedOrderNo/26;
        }

        /**
         * Added this here to match how ADT replaces 0-9 with Q-Z in the base26 cypher.
         * This was not implemented directly into the cyper map for the event where they would like to change this up.
         */

        base26 = base26.reverse()
                .replaceAll('0', 'Q')
				.replaceAll('1', 'R') 
				.replaceAll('2', 'S')
				.replaceAll('3', 'T')
				.replaceAll('4', 'U')
				.replaceAll('5', 'V')
				.replaceAll('6', 'W')
				.replaceAll('7', 'X')
				.replaceAll('8', 'Y')
				.replaceAll('9', 'Z');

        base26 = 'P' + base26;

        return base26;
    }

}