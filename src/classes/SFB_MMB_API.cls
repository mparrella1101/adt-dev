/**
* Description: MMB API Master Class to handle all Callout related logic (get creds, handle callouts, etc.)
* ------------------------------------------------------------------------------------------------------------------
* Date            Developer                 Jira#                        Comments
* --------------------------------------------------------------------------------------------------------------------
* 
* */
public class SFB_MMB_API {

   
    //Method to get credentials based on environment and sandbox 
    public static MMBCredentials retrieveCreds(){
        MMBCredentials returnCred = new MMBCredentials();
        Mulesoft_API_Settings__mdt msft;
        String SalesforceSandboxName = String.valueOf(URL.getSalesforceBaseUrl()).substringBetween('--','.').toUpperCase();
		System.debug('SalesforceSandboxName: ' + SalesforceSandboxName);
        Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
        String MasterLabel = '';
        //Set Variable for MasterLabel and retrieve sandbox records based on instance 
        switch on SalesforceSandboxName {
            when 'SFSBILLING' {		
                MasterLabel = 'SFSBilling';//This is the only other Record in SFS Billing Right now 
            }	
            when 'ADTTEST3' {		
                MasterLabel = 'Test3';//This record was created in Test3
            }
            when 'CPQDEV' {		
                MasterLabel = 'CPQTest';
            }
            when 'CPDDATA' {		
                MasterLabel = 'CPQTest';
            }
            when 'Dev1' {		
                MasterLabel = 'DEV1';
            }			
            when else {		 
                MasterLabel = '';
            }
        }
		System.debug('MasterLabel: ' + MasterLabel);
        try {
                //If it's a sandbox use MasterLabel to query record based on which sandbox. If prod. Always use record labeld Prod
                if(environment)
                {
                    msft = [SELECT MasterLabel, client_id__c, client_secret__c, SFB_client_Id__c, SFB_client_secret__c, Endpoint_base_url__c, MMB_Incident_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel=:MasterLabel LIMIT 1];
                }
                else
                {
                    msft = [SELECT MasterLabel, client_id__c, client_secret__c, SFB_client_Id__c, SFB_client_secret__c, Endpoint_base_url__c,MMB_Incident_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
                }      
                
                returnCred.clientId = msft.client_id__c;
                returnCred.ClientSecret = msft.client_secret__c;
				returnCred.SFBclientId = msft.SFB_client_Id__c;
				returnCred.SFBClientSecret = msft.SFB_client_secret__c;
                returnCred.MMBEndpoint = msft.Endpoint_base_url__c + msft.MMB_Incident_Endpoint__c;
                returnCred.MMB_BaseURL =  msft.Endpoint_base_url__c;
                returnCred.MMB_Incident_Endpoint =  msft.MMB_Incident_Endpoint__c;

				System.debug('returnCred: ' + returnCred);

        }//end try 
        catch(Exception e){

        }//end catch

        return returnCred;

    }

	public static void createIncidentRequest(Id sfRecordId, String mmbCustomerId, String mmbSiteId, Datetime createdDate, Datetime lastModifiedDate, String mmbIssueCode, String mmbProblemCode, String commentText, String callerName ){
	    System.debug('Enter createIncidentRequest');
		Long customFieldLong = createdDate.getTime() ;
        Long currentDTLong = lastModifiedDate.getTime();
        Long milliseconds = currentDTLong - customFieldLong;
        Long seconds = milliseconds / 1000;
        Long Mintes = seconds/60;
        Long Hours = Mintes/60;
        LogIncidentInMMBReqWrapper lIMMB = new LogIncidentInMMBReqWrapper();
        lIMMB.clientApplicationId = 125861;//String.isNotBlank(usr.MMB_Employee_ID__c)?Integer.valueof(usr.MMB_Employee_ID__c):null;
        lIMMB.customerId = String.isNotBlank(mmbCustomerId) ? Integer.valueOf(mmbCustomerId) : null;
        lIMMB.siteId = String.isNotBlank(mmbSiteId) ? Integer.valueOf(mmbSiteId) : null;
        LogIncidentInMMBReqWrapper.cls_incident objIncident = new LogIncidentInMMBReqWrapper.cls_incident();
        objIncident.addIncident =true;
        objIncident.addIssue = true;
        objIncident.addEmployee = true;
        objIncident.addComment = false;
        objIncident.durationInSeconds = 10;
        //Added for SFB 270

        objIncident.comment = commentText;//Had to switch with issue comment 
        objIncident.callerName =  callerName;//Caller name
        LogIncidentInMMBReqWrapper.cls_issue objIssue = new LogIncidentInMMBReqWrapper.cls_issue();
       // if(objCase.Origin == 'Email'){
        objIssue.issueCode = mmbIssueCode;//Issue Id? SFBEZP
       // objIssue.startDateTime =  string.valueOf(dateTime.now());//Place Holder
       // objIssue.endDateTime =  string.valueOf(dateTime.now());//Place Holder
        //objIssue.problemCode = mmbProblemCode;//Completion Code?
        //objIncident.callerName = objCase.ContactID!=null && String.isNotBlank(objCase.contact.Name)?objCase.contact.Name:String.isNotBlank(objCase.SuppliedEmail)?objCase.SuppliedEmail:'';
        //}
        /*if(objCase.Origin == 'Survey'){
            objIssue.issueCode = 'SFNPS';
            objIncident.callerName = String.isNotBlank(objCase.SpeakingWith__c)?objCase.SpeakingWith__c:'';              
        }
        if(objCase.Origin == 'Web - Chat'){
            objIssue.issueCode = 'SFCHAT';
            objIncident.callerName = String.isNotBlank(objCase.SpeakingWith__c)?objCase.SpeakingWith__c:'';
        }*/
        objIssue.durationInSeconds = 10; 
        objIncident.issue = objIssue;
        LogIncidentInMMBReqWrapper.cls_employee objEmployee = new  LogIncidentInMMBReqWrapper.cls_employee();
        objEmployee.completionId = 'IR';//Completion Code
        objEmployee.durationInSeconds = 10;
        objIssue.employee = objEmployee;
        /*LogIncidentInMMBReqWrapper.cls_comment objComments = new LogIncidentInMMBReqWrapper.cls_comment();
        objComments.text = commentText;
        objIssue.comment = objComments;*/
        /* if(pm.Inactive_Due_to_Hard_Decline__c){
            objIssue.createdBy = pm.blng__Account__r.MMBCustomerNumber__c ; 
        }
        else {
            objIssue.createdBy = pm.createdBy.Name ;//place holder 
        } 
        */
        lIMMB.incident = objIncident;
        String RequestIncidentJSON = JSON.serialize(lIMMB, true);
        system.debug('Request JSON'+RequestIncidentJSON);
        HTTPResponse res;
        
        
        /*|||||||||||||||||||||||||||||||||||||||||||||| Add Incident Indicating the Payment Method has been Disabled ||||||||||||||||||||||||||||||||||||||||||||||*/
       /* HTTPResponse incidentRes;
        LogIncidentInMMBReqWrapper.cls_declineIncident wrapIncident = new LogIncidentInMMBReqWrapper.cls_declineIncident();
		wrapIncident.IssueId = 'SFBEZP'; //Place Holder atm
		wrapIncident.Description = 'Salesforce Pay Method Event';
		wrapIncident.CompletionCode = 'IR';
		wrapIncident.issueStart = date.today();//Place Holder atm
        wrapIncident.issueEnd = date.today();//Place Holder atm
		wrapIncident.AutomatedIssueComment = 'Payment Method has been disabled';
		wrapIncident.CallerName = pm.blng__Account__r.MMBCustomerNumber__c;//Caller name
		//Depends if Disabled or Decilned
        if(pm.Inactive_Due_to_Hard_Decline__c){
            wrapIncident.createdBy = pm.blng__Account__r.MMBCustomerNumber__c ; 
        }
        else {
            wrapIncident.createdBy = pm.createdBy.Name ;//place holder 
        }
       
        String incidentJsonBody = JSON.serialize(wrapIncident);
        incidentRes = LogIncidentinMMB.callMMBIncidentDecline(incidentJsonBody);   */
        /*|||||||||||||||||||||||||||||||||||||||||||||| End Incident Indicating the Payment Method has been Disabled ||||||||||||||||||||||||||||||||||||||||||||||*/        
        
        if(String.isNotBlank(mmbCustomerId) && String.isNotBlank(mmbSiteId)){
            res = callMMBIncident(RequestIncidentJSON);    
        }else {
            res = new HTTPResponse();
            res.setBody('No MMBCustomer# and MMBSite# On Case and Account Assosiated with Case');
        }

        system.debug(res.getstatuscode());
        if(res != null && res.getstatuscode() == 200) {
            String sIncidentMMBResponse = res.getBody();
			System.debug('sIncidentMMBResponse: ' + sIncidentMMBResponse);
            /*pm.MMB_Incident_Response_JSON__c = sIncidentMMBResponse;   
            system.debug(pm.MMB_Incident_Response_JSON__c);
            update pm;*/
        } else {
            ADTLog__c adtlog = new ADTLog__c();
            adtlog.name = 'MMB Incident Log API Error';
            adtlog.Message_Description__c = res != null && res.getbody()!=null?res.getbody():'No Response';
            adtlog.Reported_By__c = UserInfo.getUserId();
            adtlog.Message__c = res != null && res.getStatuscode()!=null?string.valueof(res.getStatuscode()) :'Request Not Sent To MMB';
            adtlog.Unique_ID__c = sfRecordId;
            insert adtlog;   
        }               


	}//end create incident mmb method    

	private static void createIncidentRequest(blng__PaymentMethod__c pm){
	    System.debug('Enter createIncidentRequest');
		Long customFieldLong = pm.CreatedDate.getTime() ;
        Long currentDTLong = pm.LastModifiedDate.getTime();
        Long milliseconds = currentDTLong - customFieldLong;
        Long seconds = milliseconds / 1000;
        Long Mintes = seconds/60;
        Long Hours = Mintes/60;
        LogIncidentInMMBReqWrapper lIMMB = new LogIncidentInMMBReqWrapper();
        lIMMB.clientApplicationId = 1;//String.isNotBlank(usr.MMB_Employee_ID__c)?Integer.valueof(usr.MMB_Employee_ID__c):null;
        lIMMB.customerId = String.isNotBlank(pm.blng__Account__r.MMBCustomerNumber__c) ? Integer.valueOf(pm.blng__Account__r.MMBCustomerNumber__c) : null;
        lIMMB.siteId = String.isNotBlank(pm.blng__Account__r.MMBSiteNumber__c) ? Integer.valueOf(pm.blng__Account__r.MMBSiteNumber__c) : null;
        LogIncidentInMMBReqWrapper.cls_incident objIncident = new LogIncidentInMMBReqWrapper.cls_incident();
        objIncident.addIncident =true;
        objIncident.addIssue = true;
        objIncident.addEmployee = true;
        objIncident.addComment = true;
        objIncident.durationInSeconds = seconds!=null?Integer.valueof(seconds):null;
        //Added for SFB 270

        objIncident.comment = 'Salesforce Pay Method Event on '+ System.today();//Had to switch with issue comment 
        objIncident.callerName =  pm.blng__Account__r.MMBCustomerNumber__c;//Caller name
        LogIncidentInMMBReqWrapper.cls_issue objIssue = new LogIncidentInMMBReqWrapper.cls_issue();
       // if(objCase.Origin == 'Email'){
        objIssue.issueCode = 'WCAMPR';//Issue Id? SFBEZP
       // objIssue.startDateTime =  string.valueOf(dateTime.now());//Place Holder
       // objIssue.endDateTime =  string.valueOf(dateTime.now());//Place Holder
        objIssue.problemCode = 'IR';//Completion Code?
        //objIncident.callerName = objCase.ContactID!=null && String.isNotBlank(objCase.contact.Name)?objCase.contact.Name:String.isNotBlank(objCase.SuppliedEmail)?objCase.SuppliedEmail:'';
        //}
        /*if(objCase.Origin == 'Survey'){
            objIssue.issueCode = 'SFNPS';
            objIncident.callerName = String.isNotBlank(objCase.SpeakingWith__c)?objCase.SpeakingWith__c:'';              
        }
        if(objCase.Origin == 'Web - Chat'){
            objIssue.issueCode = 'SFCHAT';
            objIncident.callerName = String.isNotBlank(objCase.SpeakingWith__c)?objCase.SpeakingWith__c:'';
        }*/
        objIssue.durationInSeconds = seconds!=null?Integer.valueof(seconds):null; 
        objIncident.issue = objIssue;
        LogIncidentInMMBReqWrapper.cls_employee objEmployee = new  LogIncidentInMMBReqWrapper.cls_employee();
        objEmployee.completionId = 'IR';//Completion Code
        objEmployee.durationInSeconds = seconds!=null?Integer.valueof(seconds):null;
        objIssue.employee = objEmployee;
        LogIncidentInMMBReqWrapper.cls_comment objComments = new LogIncidentInMMBReqWrapper.cls_comment();
        objComments.text = 'Payment Method has been disabled';//had to switch with incident comment
        //objComments.startDateTime = string.valueOf(date.today());//Added Placeholder 
        objIssue.comment = objComments;
        /* if(pm.Inactive_Due_to_Hard_Decline__c){
            objIssue.createdBy = pm.blng__Account__r.MMBCustomerNumber__c ; 
        }
        else {
            objIssue.createdBy = pm.createdBy.Name ;//place holder 
        } 
        */
        lIMMB.incident = objIncident;
        String RequestIncidentJSON = JSON.serialize(lIMMB, true);
        system.debug('Request JSON'+RequestIncidentJSON);
        HTTPResponse res;
        
        
        /*|||||||||||||||||||||||||||||||||||||||||||||| Add Incident Indicating the Payment Method has been Disabled ||||||||||||||||||||||||||||||||||||||||||||||*/
       /* HTTPResponse incidentRes;
        LogIncidentInMMBReqWrapper.cls_declineIncident wrapIncident = new LogIncidentInMMBReqWrapper.cls_declineIncident();
		wrapIncident.IssueId = 'SFBEZP'; //Place Holder atm
		wrapIncident.Description = 'Salesforce Pay Method Event';
		wrapIncident.CompletionCode = 'IR';
		wrapIncident.issueStart = date.today();//Place Holder atm
        wrapIncident.issueEnd = date.today();//Place Holder atm
		wrapIncident.AutomatedIssueComment = 'Payment Method has been disabled';
		wrapIncident.CallerName = pm.blng__Account__r.MMBCustomerNumber__c;//Caller name
		//Depends if Disabled or Decilned
        if(pm.Inactive_Due_to_Hard_Decline__c){
            wrapIncident.createdBy = pm.blng__Account__r.MMBCustomerNumber__c ; 
        }
        else {
            wrapIncident.createdBy = pm.createdBy.Name ;//place holder 
        }
       
        String incidentJsonBody = JSON.serialize(wrapIncident);
        incidentRes = LogIncidentinMMB.callMMBIncidentDecline(incidentJsonBody);   */
        /*|||||||||||||||||||||||||||||||||||||||||||||| End Incident Indicating the Payment Method has been Disabled ||||||||||||||||||||||||||||||||||||||||||||||*/        
        
        if(String.isNotBlank(pm.blng__Account__r.MMBCustomerNumber__c) && String.isNotBlank(pm.blng__Account__r.MMBSiteNumber__c)){
            res = LogIncidentinMMB.callMMBIncident(RequestIncidentJSON);    
        }else {
            res = new HTTPResponse();
            res.setBody('No MMBCustomer# and MMBSite# On Case and Account Assosiated with Case');
        }

        system.debug(res.getstatuscode());
        if(res != null && res.getstatuscode() == 200) {
            String sIncidentMMBResponse = res.getBody();
            pm.MMB_Incident_Response_JSON__c = sIncidentMMBResponse;   
            system.debug(pm.MMB_Incident_Response_JSON__c);
            update pm;
        } else {
            ADTLog__c adtlog = new ADTLog__c();
            adtlog.name = 'MMB Incident Log API Error';
            adtlog.Message_Description__c = res != null && res.getbody()!=null?res.getbody():'No Response';
            adtlog.Reported_By__c = UserInfo.getUserId();
            adtlog.Message__c = res != null && res.getStatuscode()!=null?string.valueof(res.getStatuscode()) :'Request Not Sent To MMB';
            adtlog.Unique_ID__c = pm.Id;
            insert adtlog;   
        }               


	}//end create incident mmb method    



    public static HTTPResponse callMMBIncident(String RequestIncidentJSON){
        HTTPResponse res = new HTTPResponse();

		MMBCredentials MMBCreds = retrieveCreds();
        
        try {
            String sUniqueID = '';
            String username = MMBCreds.SFBclientId;
            String password = MMBCreds.SFBClientSecret;			
            Blob headerValue = Blob.valueOf(username + ':' + password);
			System.debug('MMBCreds.clientId: ' + username);
			System.debug('MMBCreds.ClientSecret: ' + password);
            // Encode it
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            HttpRequest req = new HttpRequest();
			System.debug('MMBCreds.MMBEndpoint: ' + MMBCreds.MMBEndpoint);
            req.setEndpoint(MMBCreds.MMBEndpoint);
            req.setMethod('POST');
            req.setHeader('Authorization', authorizationHeader);

            req.setHeader('Content-Type', 'application/json');
            sUniqueID = getUUID();
            req.setHeader('tracing_id', sUniqueID);
            req.setBody(RequestIncidentJSON);
            Http http = new Http();
            res = http.send(req);
            System.debug(res.getBody());
            System.debug(res.getstatuscode()); 
        }
        Catch(Exception e){
            system.debug('Exception Caught'+e.getmessage());
            ADTApplicationMonitor.log(e);
        }
    	return res;   
    }
    public static HTTPResponse callMMBCredit(String RequestIncidentJSON, String endpoint){
        HTTPResponse res = new HTTPResponse();
		MMBCredentials MMBCreds = retrieveCreds();        
        try {
            String sUniqueID = '';
            String username = MMBCreds.SFBclientId;
            String password = MMBCreds.SFBClientSecret;			     
            Blob headerValue = Blob.valueOf(username + ':' + password);

            // Encode it
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            HttpRequest req = new HttpRequest();
            req.setEndpoint(endpoint);
            req.setMethod('POST');
            req.setHeader('Authorization', authorizationHeader);

            req.setHeader('Content-Type', 'application/json');
            sUniqueID = getUUID();
            req.setHeader('tracing_id', sUniqueID);
            req.setBody(RequestIncidentJSON);
            system.debug(req);
            Http http = new Http();
            res = http.send(req);
            System.debug(res.getBody());
            System.debug(res.getstatuscode()); 
        }
        Catch(Exception e){
            system.debug('Exception Caught'+e.getmessage());
            ADTApplicationMonitor.log(e);
        }
    	return res;   
    }
     //This Method is used to generate the uniques ID
     public static String getUUID()
     {
         Blob b = Crypto.GenerateAESKey(128);
         String h = EncodingUtil.ConvertTohex(b);
         String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
         system.debug(guid);
         return guid;
     }
     ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     /*                                                 Wrapper Classes                                                                          */
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     //Use to store and get MMB Credentials 
    public class MMBCredentials {

        public String clientId {get;set;}
        public String ClientSecret {get;set;}
		public String SFBclientId {get;set;}
        public String SFBClientSecret {get;set;}
        public String MMBEndpoint {get;set;}
        public String MMB_BaseURL {get;set;}
        public String MMB_Incident_Endpoint {get;set;}
    } 
    //--------------------------------------------- Create Credit Wrapper ----------------------------------------------------------//

      public class CreditDetails {

        public Integer siteId {get;set;}
        public String applicationClientName {get;set;} //SFBILL
        public Decimal adjustAmount {get;set;} //Currency 
        public Integer employeeId {get;set;}
        public String guardRailByPassCode {get;set;} //BYP_ALL
        public Integer numberOfAllowedCredits {get;set;} //999
        public String referenceNumber {get;set;} //???
        public String creditReasonCode {get;set;} //??? DISAST
        public String billCode {get;set;} //??? DISAST
        public String adjustComment {get;set;} //???
   
    } 
    
}