/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION : In the message that OMS sends to queue CPQ.ORDERNO.IN, replace CPQ_UPDATE_CONTRACT with SFDCCPQ_UPDATE_CONTRACT in the name of the main element.
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               8/9/2021                              - Original Version
* Matt Parrella                 11/10/2021         SFCPQ-147            Adding logic for updating contract status
*/
@RestResource(urlMapping='/updateOrderContractSigned/')
global with sharing class updateOrderContractSigned {
    @HttpPost
    global static response doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        response resp = new response ();
        try{

            Blob jsonBody = RestContext.request.requestBody;
            String jsonString = jsonBody.toString();
            system.debug('This is the json: ' + jsonString);

            //Initialize wrapper class for response
            request respClass = new request();
            //Deserialize json in body to wrapper
            respClass = (request) JSON.deserialize(jsonString,request.class);

            SBQQ__Quote__c quoteRecord;
            try {
                quoteRecord = [
                        SELECT Id,
                                Contract_Signed_Date__c,
                                Contract_Status__c,
                                DocuSign_Envelope_ID__c,
                                SBQQ__Status__c
//                                SFCPQ_Global_ID__c
                        FROM SBQQ__Quote__c
//                        WHERE SFCPQ_Global_ID__c = :respClass.OrderNo
                        LIMIT 1
                ];
            }
            catch(QueryException ex) {
                resp.statusCode = 400;
                resp.ErrorMessage = 'A quote with that order number was not found';
                System.debug('updateOrderContractSigned Inbound API ERROR: ' + ex.getMessage());
                return resp;
            }

            //Update Contract Signed and set response
//            quoteRecord.Contract_Status__c = respClass.ContractStatus;
//            quoteRecord.Contract_Signed_Date__c = respClass.SignatureDate;

            update quoteRecord;

            resp.statusCode = 200;
            resp.ErrorMessage = '';
            //Set Response
            //Note:This method doesn't have a response
        }
        //Sets the status code
        catch(Exception e){
            resp.statusCode = 400;
            resp.ErrorMessage = 'Show the following to an administrator: ' + e;
            return resp;
        }
        return resp;
    }
    /*
    *---------------------------------------------------------------------------------------------------------------------------------------------------
                                                                    WRAPPER CLASSES
    *---------------------------------------------------------------------------------------------------------------------------------------------------
    */



    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    //Request Recieved in post
    global class request {
        public String OrderNo    {get;set;} //Quote Id
        public String EnvelopeID    {get;set;} //Link to Contract document
        public String ContractStatus    {get;set;} //Status of DocuSign Contract
        public DateTime SignatureDate    {get;set;} //Datetime stamp of Contract signature
        public String TimeZone    {get;set;}   //Timezone in which the Document was signed
        public Integer TimeZoneOffset    {get;set;} //The offset (in hours) represented by the timezone


    }
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
    * Request Recieved in post
    */
    global class response {

        public Integer statusCode {get;set;}
        public String ErrorMessage {get;set;}//Placeholder


    }
}//end class