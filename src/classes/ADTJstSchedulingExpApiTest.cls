@isTest
public with sharing class ADTJstSchedulingExpApiTest 
{

/**
 * START MOCK CLASSES
 */
    /**
     *  Sub-class for mocking a 400 response
     */
    public class mock200GetJobs implements HttpCalloutMock
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"","timeBands":[{"jstTimeBandId":101,"mmbTimeBandId":"MMB101","name":"Morning Hours","startTime":"0800","endTime":"1200"}],"status":{"code":"200","messages":[{"type":"","severity":"","reasonCode":"","message":"","context":""}]}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200DeleteJobs implements HttpCalloutMock
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678"}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200DeleteAppointment implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678"}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200PostOffers implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","offers":[{"timeBandId":101,"date":"2021-02-25","startDateTime":"2021-02-25T08:00:00","endDateTime":"2021-02-25T12:00:00"},{"timeBandId":102,"date":"2021-02-25","startDateTime":"2021-02-25T12:00:00","endDateTime":"2021-02-25T17:00:00"},{"timeBandId":103,"date":"2021-02-25","startDateTime":"2021-02-25T17:00:00","endDateTime":"2021-02-25T21:00:00"}]}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200PostSchedule implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","appointment":{"assignedEmployeeId":101,"sequenceId":1,"startDateTime":"2021-02-23T08:00:00","endDateTime":"2021-02-23T12:00:00"}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200PutReschedule implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","appointment":{"assignedEmployeeId":101,"sequenceId":1,"startDateTime":"2021-02-23T08:00:00","endDateTime":"2021-02-23T12:00:00"}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200PostEarliest implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","appointment":{"assignedEmployeeId":101,"sequenceId":1,"startDateTime":"2021-02-23T08:00:00","endDateTime":"2021-02-23T12:00:00"}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200PutEarliest implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","appointment":{"assignedEmployeeId":101,"sequenceId":1,"startDateTime":"2021-02-23T08:00:00","endDateTime":"2021-02-23T12:00:00"}}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200PostEmployee implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","jobs":[{"companyId":2,"jobId":1127898,"jobTypeId":"ABCXYZ","jobGroupDescription":"Name of Job Group in JST","employeeId":11497,"jobStatusId":"A","isOptimized":true,"employeeStatusId":"XY","jobAppointmentSeqId":1,"jobEmployeeSeqId":2,"appointmentStartDateTime":"2021-02-25T08:00:00","appointmentEndDateTime":"2021-02-25T12:00:00","assignDateTime":"2021-02-23T12:00:00","enrouteDateTime":"2021-02-25T12:00:00","onsiteDateTime":"2021-02-25T12:00:00","clearDateTime":"2021-02-28T12:00:00","estimatedTravelTimeInMins":45,"jobEmployeeEstimatedTimeInMins":55},{"companyId":2,"jobId":1127890,"jobTypeId":"ABCXYZ","jobGroupDescription":"Name of Job Group in JST","employeeId":11497,"jobStatusId":"A","isOptimized":true,"employeeStatusId":"XY","jobAppointmentSeqId":1,"jobEmployeeSeqId":2,"appointmentStartDateTime":"2021-03-01T08:00:00","appointmentEndDateTime":"2021-03-01T12:00:00","assignDateTime":"2021-02-28T08:00:00","enrouteDateTime":"2021-03-01T08:00:00","onsiteDateTime":"2021-03-01T08:00:00","clearDateTime":"2021-03-04T08:00:00","estimatedTravelTimeInMins":45,"jobEmployeeEstimatedTimeInMins":55},{"companyId":2,"jobId":11278101,"jobTypeId":"ABCXYZ","jobGroupDescription":"Name of Job Group in JST","employeeId":11497,"jobStatusId":"A","isOptimized":true,"employeeStatusId":"XY","jobAppointmentSeqId":1,"jobEmployeeSeqId":2,"appointmentStartDateTime":"2021-03-05T08:00:00","appointmentEndDateTime":"2021-03-05T12:00:00","assignDateTime":"2021-03-01T08:00:00","enrouteDateTime":"2021-03-05T08:00:00","onsiteDateTime":"2021-03-05T08:00:00","clearDateTime":"2021-03-08T08:00:00","estimatedTravelTimeInMins":45,"jobEmployeeEstimatedTimeInMins":55}]}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }
    
    public class mock200PostSearch implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","jobs":[{"companyId":2,"jobId":107665980,"siteId":48359619,"siteName":"EVANS, DAVID","siteAddress":{"addressLine1":"130 CHITTOMWOOD DR","addressLine2":"add","city":"GUNTERSVILLE","state":"AL","postalCode":"35976-8800"},"longitude":-86.255794,"latitude":34.390506,"phoneNumber1":"2565724506","phoneNumber2":"9999999999","branchId":324,"billToCustomerId":311760070,"serviceCustomerId":311760070,"jobEstHours":0.75,"csId":"U381985600","systemId":31133641,"equipmentTypeId":"46","probDiagId":"PROJ-O","problemDescription":"CELL UPGRADE PROJECT","jobStatusId":"L","jobStatusDescription":"Canceled","jobTypeId":"RESI","jobTypeDescription":"SERVICE - RESI","jobClassId":"PROJEC","jobGroupId":76,"jobGroupDescription":"Cell Project – Broadview","jobRequestId":"V","commitDateTime":"2021-06-10T03:01:13","completeDateTime":"2021-06-10T03:01:13","scheduleDateTime":"2021-06-10T03:01:13","requestedBy":"David","requestorPhone":"2565724506","requestTypeDescription":"PROJECT WORK","jobComment":"Radio Replacement Only   Waive Charges","jobMaxHours":2160,"installFlag":false,"userDefinedFields":[{"name":"Udf3","value":"SPEC"},{"name":"Udf4","value":"A"},{"name":"Udf2","value":"LTE006"}],"appointments":[{"sequenceId":1,"appointmentDateTime":"2021-06-10T12:00:00","endDateTime":"2021-06-10T17:00:00","status":"L","jobEmployeeSequenceId":1,"returnReasonId":"123","returnReasonDescription":"example"},{"sequenceId":2,"appointmentDateTime":"2021-06-10T12:00:00","endDateTime":"2021-06-10T17:00:00","status":"L","jobEmployeeSequenceId":2}]},{"companyId":2,"jobId":109074462,"siteId":48359619,"siteName":"EVANS, DAVID","siteAddress":{"addressLine1":"130 CHITTOMWOOD DR","city":"GUNTERSVILLE","state":"AL","postalCode":"35976-8800"},"longitude":-86.255794,"latitude":34.390506,"phoneNumber1":"2565724506","phoneNumber2":"9999999999","branchId":324,"billToCustomerId":311760070,"serviceCustomerId":311760070,"jobEstHours":0.75,"csId":"U381985600","systemId":31133641,"equipmentTypeId":"46","probDiagId":"PROJ-O","problemDescription":"CELL UPGRADE PROJECT","jobStatusId":"","jobStatusDescription":"Assigned","priority":400,"jobTypeId":"RES","jobTypeDescription":"SERVICE - RESI","jobClassId":"PROJEC","jobGroupId":76,"jobGroupDescription":"Cell Project – Broadview","jobRequestId":"V","commitDateTime":"2021-08-17T00:00:00","commitStartTime":1200,"commitEndTime":1600,"scheduleDateTime":"2021-08-17T12:00:00","requestedBy":"David","requestorPhone":"2565724506","requestTypeDescription":"PROJECT WORK","jobComment":"Radio Replacement Only   Waive Charges","jobMaxHours":2160,"installFlag":false,"userDefinedFields":[{"name":"Udf3","value":"SPEC"},{"name":"Udf2","value":"LTE006"}],"technicians":[{"sequenceId":1,"estimatedHours":1.0714,"activeHours":1.0714,"employeeId":195433,"firstName":"Jacob","lastName":"Benning - TEK Systems LLC","status":"A","assignDateTime":"2021-08-17T12:00:00","enrouteDateTime":"2021-08-17T12:00:00","onsiteDateTime":"2021-08-17T12:00:00","clearDateTime":"2021-08-17T12:00:00","estimatedArrivalDateTime":"2021-08-17T12:00:00","travelTime":"55"}],"appointments":[{"sequenceId":1,"appointmentDateTime":"2021-06-10T12:00:00","endDateTime":"2021-08-17T16:00:00","status":"O","jobEmployeeSequenceId":1}]}]}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }

    public class mock200PostSMSCallback implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String response = '{"correlationId":"cdk6fbf8-6774-4a95-9b59-15348943abd4","tracingId":"10054678","message":"Successfully submitted"}';
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(response);
            res.setStatusCode(200);
            return res;
        }
    }    

    public class mock400 implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mock400Response = '';
            //Create a mock 400 status code JSON response using JSONGenerator class
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'd5f6fbf8-6774-4a95-9b59-15348943abd4');
            gen.writeStringField('tracingId', 'A19283745');
            gen.writeFieldName('status');
                gen.writeStartObject();
                gen.writeStringField('code', '400');
                gen.writeFieldName('messages');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('type', 'Error');
                gen.writeStringField('severity', '1');
                gen.writeStringField('reasonCode', 'ADT-400');
                gen.writeStringField('context', 'JST-Scheduling-Exp-Api');
                gen.writeStringField('message', 'Test 400 Error');
                gen.writeEndObject();
                gen.writeEndArray();
                gen.writeEndObject();
                gen.writeEndObject();

            mock400Response = gen.getAsString();
            //Create a fake error status code response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mock400Response);
            res.setStatusCode(400);
            return res;
        }
    }

    /**
     *  Sub-class for mocking a 404 response
     */

    public class mock404 implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mock404Response = '';
            //Create a mock 404 status code JSON response using JSONGenerator class
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'd5f6fbf8-6774-4a95-9b59-15348943abd4');
            gen.writeStringField('tracingId', 'A19283745');
            gen.writeFieldName('status');
                gen.writeStartObject();
                gen.writeStringField('code', '404');
                gen.writeFieldName('messages');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('type', 'Error');
                gen.writeStringField('severity', '1');
                gen.writeStringField('reasonCode', 'ADT-404');
                gen.writeStringField('context', 'JST-Scheduling-Exp-Api');
                gen.writeStringField('message', 'Test 404 Error');
                gen.writeEndObject();
                gen.writeEndArray();
                gen.writeEndObject();
                gen.writeEndObject();

            mock404Response = gen.getAsString();
            //Create a fake error status code response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mock404Response);
            res.setStatusCode(404);
            return res;
        }
    } 

    /**
     *  Sub-class for mocking a 500 response
     */

    public class mock500 implements HttpCalloutMock 
    {
        public HTTPResponse respond(HTTPRequest req) 
        {
            String mock500Response = '';
            //Create a mock 401 status code JSON response using JSONGenerator class
            JSONGenerator gen = JSON.createGenerator(True);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'd5f6fbf8-6774-4a95-9b59-15348943abd4');
            gen.writeStringField('tracingId', 'A19283745');
            gen.writeFieldName('status');
                gen.writeStartObject();
                gen.writeStringField('code', '500');
                gen.writeFieldName('messages');
                gen.writeStartArray();
                gen.writeStartObject();
                gen.writeStringField('type', 'Error');
                gen.writeStringField('severity', '1');
                gen.writeStringField('reasonCode', 'ADT-500');
                gen.writeStringField('context', 'JST-Scheduling-Exp-Api');
                gen.writeStringField('message', 'Test 500 Error');
                gen.writeEndObject();
                gen.writeEndArray();
                gen.writeEndObject();
                gen.writeEndObject();

            mock500Response = gen.getAsString();
            //Create a fake error status code response
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(mock500Response);
            res.setStatusCode(500);
            return res;
        }
    }
/**
 * END MOCK CLASSES
 */ 
/**
 * START TEST METHODS
 */

    /**
     * getTimeBands Tests
     */

    //Test getTimebands 200 Response
    @isTest
    public static void test200GetJobs()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200GetJobs());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.getTimeBands();
        System.assertEquals('200', resp.status.code);
    }
    //Test getTimebands 400 Response
    @isTest
    public static void test400GetJobs()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.getTimeBands();
        System.assertEquals('400', resp.status.code);
    }
    //Test getTimebands 500 Response
    @isTest
    public static void test500GetJobs()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.getTimeBands();
        System.assertEquals('500', resp.status.code);
    }

    /**
     * deleteJobs Tests
     */

    //Test deleteJobs 200 Response
    @isTest
    public static void test200DeleteJobs()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200DeleteJobs());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteJob(123);
//        System.assertEquals('cdk6fbf8-6774-4a95-9b59-15348943abd4', resp.correlationId);
    }
    //Test deleteJobs 400 Response
    @isTest
    public static void test400DeleteJobs()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteJob(123);
//        System.assertEquals('400', resp.status.code);
    }
    //Test deleteJobs 404 Response
    @isTest
    public static void test404DeleteJobs()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteJob(123);
//        System.assertEquals('404', resp.status.code);
    }
    //Test deleteJobs 500 Response
    @isTest
    public static void test500DeleteJobs()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteJob(123);
//        System.assertEquals('500', resp.status.code);
    }

    /**
     * deleteAppointment Tests
     */

    //Test deleteAppointment 200 Response
    @isTest
    public static void test200deleteAppointment()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200DeleteAppointment());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteAppointment(1,'123');
        System.assertEquals('cdk6fbf8-6774-4a95-9b59-15348943abd4', resp.correlationId);
    }
    //Test deleteAppointment 400 Response
    @isTest
    public static void test400deleteAppointment()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteAppointment(1,'123');
        System.assertEquals('400', resp.status.code);
    }
    //Test deleteAppointment 404 Response
    @isTest
    public static void test404deleteAppointment()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteAppointment(1,'123');
        System.assertEquals('404', resp.status.code);
    }
    //Test deleteAppointment 500 Response
    @isTest
    public static void test500deleteAppointment()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.deleteAppointment(1,'123');
        System.assertEquals('500', resp.status.code);
    }

    /**
     * postOffers Tests
     */

    //Test postOffers 200 Response
    @isTest
    public static void test200postOffers()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PostOffers());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postOffers(10000001, 'startDate', 'endDate');
        System.assertEquals(true, resp.isSuccess);
    }
    //Test postOffers 400 Response
    @isTest
    public static void test400postOffers()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postOffers(123, 'startDate', 'endDate');
        System.assertEquals(false, resp.isSuccess);
    }
    //Test postOffers 404 Response
    @isTest
    public static void test404postOffers()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postOffers(123, 'startDate', 'endDate');
        System.assertEquals(false, resp.isSuccess);
    }
    //Test postOffers 500 Response
    @isTest
    public static void test500postOffers()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postOffers(123, 'startDate', 'endDate');
        System.assertEquals(false, resp.isSuccess);
    }

    /**
     * postSchedule Tests
     */

    //Test postSchedule 200 Response
    @isTest
    public static void test200postSchedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PostSchedule());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSchedule(123, 456, 'date', '');
        System.assertEquals(101, resp.appointment.assignedEmployeeId);
        System.assertEquals(1, resp.appointment.sequenceId);
    }
    //Test postSchedule 400 Response
    @isTest
    public static void test400postSchedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSchedule(123, 456, 'date', '');
        System.assertEquals('400', resp.status.code);
    }
    //Test postSchedule 404 Response
    @isTest
    public static void test404postSchedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSchedule(123, 456, 'date', '');
        System.assertEquals('404', resp.status.code);
    }
    //Test postSchedule 500 Response
    @isTest
    public static void test500postSchedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSchedule(123, 456, 'date', '');
        System.assertEquals('500', resp.status.code);
    }

    /**
     * putSchedule Tests
     */

    //Test putSchedule 200 Response
    @isTest
    public static void test200putReschedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PutReschedule());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putReschedule('', 10000001, 456, 'date');
        System.assertEquals(101, resp.appointment.assignedEmployeeId);
        System.assertEquals(1, resp.appointment.sequenceId);
    }
    //Test putSchedule 400 Response
    @isTest
    public static void test400putReschedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putReschedule('', 10000001, 456, 'date');
        System.assertEquals('400', resp.status.code);
    }
    //Test putSchedule 404 Response
    @isTest
    public static void test404putReschedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putReschedule('', 10000001, 456, 'date');
        System.assertEquals('404', resp.status.code);
    }
    //Test putSchedule 500 Response
    @isTest
    public static void test500putReschedule()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putReschedule('', 10000001, 456, 'date');
        System.assertEquals('500', resp.status.code);
    }

    /**
     * postEarliest Tests
     */

    //Test postEarliest 200 Response
    @isTest
    public static void test200postEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PostEarliest());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEarliest();
        System.assertEquals(101, resp.appointment.assignedEmployeeId);
        System.assertEquals(1, resp.appointment.sequenceId);
    }
    //Test postEarliest 400 Response
    @isTest
    public static void test400postEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEarliest();
        System.assertEquals('400', resp.status.code);
    }
    //Test postEarliest 404 Response
    @isTest
    public static void test404postEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEarliest();
        System.assertEquals('404', resp.status.code);
    }
    //Test postEarliest 500 Response
    @isTest
    public static void test500postEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEarliest();
        System.assertEquals('500', resp.status.code);
    }

    /**
     * putEarliest Tests
     */

    //Test putEarliest 200 Response
    @isTest
    public static void test200putEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PutEarliest());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putEarliest();
        System.assertEquals(101, resp.appointment.assignedEmployeeId);
        System.assertEquals(1, resp.appointment.sequenceId);
    }
    //Test putEarliest 400 Response
    @isTest
    public static void test400putEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putEarliest();
        System.assertEquals('400', resp.status.code);
    }
    //Test putEarliest 404 Response
    @isTest
    public static void test404putEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putEarliest();
        System.assertEquals('404', resp.status.code);
    }
    //Test putEarliest 500 Response
    @isTest
    public static void test500putEarliest()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.putEarliest();
        System.assertEquals('500', resp.status.code);
    }

    /**
     * postEmployee Tests
     */

    //Test postEmployee 200 Response
    @isTest
    public static void test200postEmployee()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PostEmployee());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEmployee();
        System.assertEquals(3, resp.jobs.size());
    }
    //Test postEmployee 400 Response
    @isTest
    public static void test400postEmployee()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEmployee();
        System.assertEquals('400', resp.status.code);
    }
    //Test postEmployee 404 Response
    @isTest
    public static void test404postEmployee()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEmployee();
        System.assertEquals('404', resp.status.code);
    }
    //Test postEmployee 500 Response
    @isTest
    public static void test500postEmployee()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postEmployee();
        System.assertEquals('500', resp.status.code);
    }

    /**
     * postSearch Tests
     */

    //Test postSearch 200 Response
    @isTest
    public static void test200postSearch()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PostSearch());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSearch();
        System.assertEquals(2, resp.jobs.size());
        System.assertEquals(2, resp.jobs[0].appointments.size());
        System.assertEquals(1, resp.jobs[1].technicians.size());
    }
    //Test postSearch 400 Response
    @isTest
    public static void test400postSearch()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSearch();
        System.assertEquals('400', resp.status.code);
    }
    //Test postSearch 404 Response
    @isTest
    public static void test404postSearch()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSearch();
        System.assertEquals('404', resp.status.code);
    }
    //Test postSearch 500 Response
    @isTest
    public static void test500postSearch()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSearch();
        System.assertEquals('500', resp.status.code);
    }

    /**
     * postSMSCallback Tests
     */

    //Test postSMSCallback 200 Response
    @isTest
    public static void test200postSMSCallback()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200PostSMSCallback());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSMSCallback();
        System.assertEquals('Successfully submitted',resp.message);
    }
    //Test postSMSCallback 400 Response
    @isTest
    public static void test400postSMSCallback()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock400());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSMSCallback();
        System.assertEquals('400', resp.status.code);
    }
    //Test postSMSCallback 404 Response
    @isTest
    public static void test404postSMSCallback()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock404());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSMSCallback();
        System.assertEquals('404', resp.status.code);
    }
    //Test postSMSCallback 500 Response
    @isTest
    public static void test500postSMSCallback()
    {
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock500());
        ADTJstSchedulingExpApi.responseWrapper resp = ADTJstSchedulingExpApi.postSMSCallback();
        System.assertEquals('500', resp.status.code);
    }
 /**
 * END TEST METHODS
 */
}