/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION : Job schedule is updated in MMB, which puts message in queue CPQ.ORDERNO.IN
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               8/9/2021                              - Original Version
*/
@RestResource(urlMapping='/updateJobSchedule/')
global with sharing class updateJobSchedule {
    @HttpPost
    global static response doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        response resp = new response();
        try{
            Blob jsonBody = RestContext.request.requestBody;
            String jsonString = jsonBody.toString();
            system.debug('This is the json: ' + jsonString);
    
            //Initialize wrapper class for response
            request respClass = new request();
            //Deserialize json in body to wrapper
            respClass = (request) JSON.deserialize(jsonString,request.class);  
            List<SBQQ__Quote__c> quote = [SELECT Id, OwnerId
                                        FROM SBQQ__Quote__c 
                                        WHERE OrderNumber__c = :respClass.JobNo limit 1];
            List<ServiceAppointment>  ServiceAppointments = [Select Id From ServiceAppointment Where AppointmentNumber  = :respClass.JobNo limit 1] ;
 
    if(ServiceAppointments.size() > 0){
        //Update Contract Signed and set response 
        ServiceAppointment updateService = new ServiceAppointment();
        updateService.Id = ServiceAppointments[0].Id;
        updateService.SchedStartTime = respClass.InstallDate;
        updateService.SchedEndTime = respClass.InstallDate; //Update time with EndTime
        update updateService;
        //Set Response 
       //Note:This method doesn't have a response just status code 200
    }
    else{
        res.statusCode = 400;
        resp.ErrorMessage = 'A quote with that order number was not found';
    }

        }
        //Sets the status code
        catch(Exception e){
            res.statusCode = 400;
            resp.ErrorMessage = 'Sorry! ᕕ( ᐛ )ᕗ The json syntax is incorrect. Please verify and try again. Show the following to an administrator: ' + e;
        }
       return resp;
    }
/*
*---------------------------------------------------------------------------------------------------------------------------------------------------
                                                                WRAPPER CLASSES
*---------------------------------------------------------------------------------------------------------------------------------------------------
*/


  
    /*
    * Request Recieved in post 
    */


    global class request {

        public String JobNo {get;set;}//Job Number for specific job that is getting rescheduled
        public String Token {get;set;}
        public DateTime InstallDate {get;set;}//New Start Date
        public Time InstallEndTime {get;set;}//New End Time (Date will be the same as start)

        
    }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
    * Response Sent from post 
    */


    global class response {
        public String       errorMessage                 {get;set;}
        public  Double      AdscBeforeDiscount           {get;set;} 
        public  Double      AdscDiscount                 {get;set;}
        public  Double      AdscDoa                      {get;set;}
        public  Double      AdscFee                      {get;set;}
        public  Double      AdscTax                      {get;set;}
        public  Double      AnscBeforeDiscount           {get;set;}
        public  Double      AnscDiscount                 {get;set;}
        public  Double      AnscDoa                      {get;set;}
        public  Double      AnscFee                      {get;set;}
        public  Double      AnscTax                      {get;set;}
        public  String      Createprogid                 {get;set;}
        public  DateTime    Createts                     {get;set;}
        public  String      Createuserid                 {get;set;}
        public  DateTime    InstallDate                  {get;set;}
        public  Time        InstallEndTime               {get;set;}
        public  String      InstallToken                 {get;set;}
        public  Double      JobDuration                  {get;set;}
        public  String      JobKey                       {get;set;}
        public  Integer     JobNo                        {get;set;}
        public  Integer     Lockid                       {get;set;}
        public  String      Modifyprogid                 {get;set;}
        public  DateTime    Modifyts                     {get;set;}
        public  String      Modifyuserid                 {get;set;}
        public  String      OrderHeaderKey               {get;set;}
        public  String      ProductFamily                {get;set;}
        public  Double      QspBeforeDiscount            {get;set;}
        public  Double      QspDiscount                  {get;set;}
        public  Double      QspDoa                       {get;set;}
        public  Double      QspFee                       {get;set;}
        public  Double      QspTax                       {get;set;}
        public  Double      SaleBeforeDiscount           {get;set;}
        public  Double      SaleDiscount                 {get;set;}
        public  Double      SaleDoa                      {get;set;}
        public  Double      SaleFee                      {get;set;}
        public  Double      SaleTax                      {get;set;}
        public  Boolean     isHistory                    {get;set;}
             
    }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
}//end class