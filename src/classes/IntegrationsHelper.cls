/**
 * @description This is a helper class that is used to retrieve the proper Custom Metadata record containing integration callout data (such as endpoint urls and credentials),
 * based on the current org's domain.
 *
 */
/*
 * Version      Author                  Company             JIRA#                 Date                  Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud       N/A                   1/10/2022             Initial version
    1.2         Matt Parrella           Coastal Cloud       N/A                   1/19/2022             Adding 'Org-specific Data' section fields
    1.3         William Miranda         Coastal Cloud       N/A                   2/1/2022              Adding SF Billing Payment Batch Fields
    1.4         Marcus Hobbs            Coastal Cloud       N/A                   2/1/2022              Initialize payment file info
 */

public with sharing class IntegrationsHelper {

    /**
     * @description This method will parse the current url to determine the correct 'Mulesoft API Settings' Custom Metadata record
     * to populate and return. Will return a wrapper object containing all data.
     *
     * @return (IntegrationsWrapper) Wrapper object containing all Mulesoft & PartnerAPI Integration Data
     */
    public static IntegrationsWrapper retrieveCreds() {
        IntegrationsWrapper integrationWrapper = new IntegrationsWrapper();
        Mulesoft_API_Settings__mdt msftConfig;
        String recordLabel; // Holds the Master Label of the Custom MDT record we are trying to obtain

        // Extract the name of the org from the domain



        // Determine if current org is a Sandbox or not
        Boolean isSandbox = [SELECT Id, isSandbox FROM Organization LIMIT 1].isSandbox;

        // Assign correct value to 'recordLabel' (since base url naming != Custom MDT records naming)
        if (isSandbox){
            recordLabel = String.valueOf(URL.getSalesforceBaseUrl()).substringBetween('--','.').toUpperCase();
            switch on recordLabel {
                when 'SFSBILLING' {
                    recordLabel = 'SFSBilling';
                }
                when 'ADTTEST3' {
                    recordLabel = 'Test3';
                }
                when 'CPQDEV', 'CPDDATA' {
                    recordLabel = 'CPQTest';
                }
                when 'ADTDEV1' {
                    recordLabel = 'DEV1';
                }
                when 'ADTTEST1' {
                    recordLabel = 'Test1';
                }
                when else {
                    recordLabel = 'CPQTest'; // MParrella | 1-12-2022 | Defaulting to 'CPQTest' if this is deployed in an unknown/new org to prevent test failures
                }
            }
        } else {
            recordLabel = 'Production';
        }

        // Get the Mulesoft API Settings Custom MDT record
        try {
            msftConfig = Mulesoft_API_Settings__mdt.getInstance(recordLabel);
            System.debug(msftConfig);
        }
        catch(Exception ex){
            integrationWrapper.hasData = false;
            System.debug('*** ERROR: IntegrationsHelper.retrieveCreds() was unable to locate the Mulesoft API Settings Custom Metadata record!');
            return integrationWrapper;
        }

        /* Populate the IntegrationsWrapper class with Custom MDT data */

        // JST
        JSTWrapper jstWrap = new JSTWrapper();
        jstWrap.Endpoint_deleteAppointment = msftConfig.JST_deleteAppointment__c;
        jstWrap.Endpoint_deleteJob = msftConfig.JST_deleteJob__c;
        jstWrap.Endpoint_getTimeBands = msftConfig.JST_getJobs__c;
        jstWrap.Endpoint_postAppointmentOffers = msftConfig.JST_postAppointmentOffers__c;
        jstWrap.Endpoint_postJobsAssignedToEmployee = msftConfig.JST_postJobsAssignedToEmployee__c;
        jstWrap.Endpoint_postJobsSearch = msftConfig.JST_postJobsSearch__c;
        jstWrap.Endpoint_postScheduledAppointment = msftConfig.JST_postScheduleAppointment__c;
        jstWrap.Endpoint_postScheduleEarliestAppointment = msftConfig.JST_postScheduleEarliestAppointment__c;
        jstWrap.Endpoint_postSMSCallback = msftConfig.JST_postSMSCallback__c;
        jstWrap.Endpoint_putRescheduleAppointment = msftConfig.JST_putRescheduleAppointment__c;
        jstWrap.Endpoint_putRescheduleEarliestAppointment = msftConfig.JST_putRescheduleEarliestAppointment__c;

        // CM8
        ContentMgmtWrapper cmWrap = new ContentMgmtWrapper();
        cmWrap.Endpoint_getContent = msftConfig.CM8_getContent_Endpoint__c;
        cmWrap.Endpoint_getDocuments = msftConfig.CM8_getDocuments_endpoint__c;

        // OMS
        OMSWrapper omsWrap = new OMSWrapper();
        omsWrap.Endpoint_ContractVoid = msftConfig.OMS_Contract_Void_Endpoint__c;
        omsWrap.Endpoint_Email_Order_Confirmation = msftConfig.OMS_Email_Order_Confirmation_Endpoint__c;
        omsWrap.Endpoint_Envelope_Status = msftConfig.OMS_Envelop_Status_Endpoint__c;
        omsWrap.Endpoint_Generate_Contract = msftConfig.OMS_Generate_Contract_Endpoint__c;
        omsWrap.Endpoint_Order_Submit = msftConfig.OMS_Order_Submit_Endpoint__c;

        // Partner API
        PartnerAPIWrapper partnerWrap = new PartnerAPIWrapper();
        partnerWrap.Credentials_PartnerAPI_ClientID = msftConfig.Red_Ventures_Username__c;
        partnerWrap.Credentials_PartnerAPI_ClientSecret = msftConfig.Red_Ventures_Password__c;
        partnerWrap.Endpoint_PartnerAPI_Base_URL = msftConfig.Red_Ventures_Endpoint_URL__c;
        partnerWrap.Endpoint_Account_Lookup = msftConfig.Red_Ventures_Account_Lookup_Endpoint__c;
        partnerWrap.Endpoint_Customer_Lookup = msftConfig.Red_Ventures_Customer_Lookup_Endpoint__c;
        partnerWrap.Endpoint_Get_Status = msftConfig.Red_Ventures_Get_Status_Endpoint__c;
        partnerWrap.Endpoint_Submit_Order = msftConfig.Red_Ventures_Submit_Order_Endpoint__c;

        // Paymentech
        PaymentechWrapper payWrap = new PaymentechWrapper();
        payWrap.Endpoint_HPP_Base_URL = msftConfig.Paymentech_portal_url__c;
        payWrap.Endpoint_getUID_HPP = msftConfig.PaymentTech_uid_hpp_endpoint__c;
        payWrap.Endpoint_getTransactionData = msftConfig.PaymentTech_Get_Transaction_Endpoint__c;

        // MMB
        MMBWrapper mmbWrap = new MMBWrapper();
        mmbWrap.Endpoint_Incident = msftConfig.MMB_Incident_Endpoint__c;

        // Giact
        GiactWrapper giactWrap = new GiactWrapper();
        giactWrap.Endpoint_Validate_Bank_Info = msftConfig.Giact_Endpoint__c;

        // SFMC
        SFMCWrapper sfmcWrap = new SFMCWrapper();
        sfmcWrap.Endpoint_Email_Request = msftConfig.SFMC_Endpoint__c;

        // Triton IVR
        TritonIVRWrapper tritonWrap = new TritonIVRWrapper();
        tritonWrap.Endpoint_Triton_IVR = msftConfig.Triton_IVR_Endpoint__c;

        // Remote Pay
        RemotePayWrapper remoteWrap = new RemotePayWrapper();
        remoteWrap.Endpoint_ShortCode_Email_Base_URL = msftConfig.ShortCode_Email_Base_URL__c;

        // Vertex
        VertexWrapper vertexWrap = new VertexWrapper();
        vertexWrap.Endpoint_Calculate_Tax = msftConfig.Vertex_Tax_Calc__c;
        
        // PaymentFile
        PaymentFileWrapper payFileWrap = new PaymentFileWrapper();
        payFileWrap.SFB_Payment_Endpoint = msftConfig.SFB_Payment_Endpoint__c;
        
        
        // Primary Wrapper Class

        // Credential data
        integrationWrapper.Credentials_SFCPQ_Mulesoft_ClientId = msftConfig.client_id__c; // SFCPQ ClientId
        integrationWrapper.Credentials_SFCPQ_Mulesoft_ClientSecret = msftConfig.client_secret__c; // SFCPQ Secret
        integrationWrapper.Credentials_SFB_Mulesoft_ClientId = msftConfig.SFB_client_id__c; // SFB ClientId
        integrationWrapper.Credentials_SFB_Mulesoft_ClientSecret = msftConfig.SFB_client_secret__c; // SFB Secret
        integrationWrapper.Credentials_SFS_Mulesoft_ClientId = msftConfig.sfs_client_id__c; // SFS ClientId
        integrationWrapper.Credentials_SFS_Mulesoft_ClientSecret = msftConfig.sfs_client_secret__c; // SFS Secret

        // Org-Specific data

        /* MParrella | 01-19-2022 | Dynamically assign the org domain values: START */
        String currURL = URL.getSalesforceBaseUrl().getHost();
        String[] currURLArray = currURL.split('\\.');

        String orgName = currURLArray[0];

        String orgDomain = 'https://' + orgName + '.lightning.force.com/';
        String orgVFDomain = 'https://' + orgName + '--c.visualforce.com';

        System.debug('Integrations Helper orgDomain: ' + orgDomain);
        System.debug('Integrations Helper orgVFDomain: ' + orgVFDomain);

        integrationWrapper.Org_Domain = orgDomain;
        integrationWrapper.Org_VF_Domain = orgVFDomain;
        /* MParrella | 01-19-2022 | Dynamically assign the org domain values: STOP */


        // API-specific data
        integrationWrapper.Endpoint_Mulesoft_Base_URL = msftConfig.Endpoint_base_url__c;
        integrationWrapper.contentMgmtInfo = cmWrap;
        integrationWrapper.jstInfo = jstWrap;
        integrationWrapper.omsInfo = omsWrap;
        integrationWrapper.partnerAPIInfo = partnerWrap;
        integrationWrapper.paymentechInfo = payWrap;
        integrationWrapper.mmbInfo = mmbWrap;
        integrationWrapper.giactInfo = giactWrap;
        integrationWrapper.sfmcInfo = sfmcWrap;
        integrationWrapper.tritonIVRInfo = tritonWrap;
        integrationWrapper.remotePayInfo = remoteWrap;
        integrationWrapper.vertexInfo = vertexWrap;
        integrationWrapper.paymentFileInfo = payFileWrap;

        return integrationWrapper;
    }

    /**
     *  @description This helper method will build out the 'tracing_id' parameter that's used in the various Mulesoft integrations
     *
     *  @return tracingId The 'tracing_id' parameter value that will be used
    */
    public static String generateTracingId(){
        String rightNow = Datetime.now().format('MM-dd-yyyy-HH-mm-ss-sss');
        String tracingId = 'SFCPQ_' + rightNow.replaceAll('-',''); // Remove '-' characters to have 1 uninterrupted string representing datetime
        return tracingId;
    }

    /**
     * @description This helper method will query for the 'MMB_Employee_ID__c' field value on the current running user's Salesforce record and return that
     * value.
     *
     * @return mmbEmpId The MMB Employee ID field value on the current user's Salesforce record
     */
     public static String getMMBEmployeeId() {
         return [SELECT MMB_Employee_Id__c FROM User WHERE Id =: UserInfo.getUserId() LIMIT 1].MMB_Employee_ID__c;
     }

    /**
     * @description Small helper method to generate a 6-Digit Unique ID for general purpose
     *
     * To achieve the UniqueID format:  (Number of milliseconds between 1/1/1970 and NOW) + (A randomly generated number)
     *
     * @return uniqueId - (String) A 6-Digit UniqueID value
    */
    @AuraEnabled
    public static String generateUID(){
        // Init variables
        Double seconds;
        Double randomNumber;
        String uniqueID = '';

        // Get number of milliseconds between 1/1/1970 and NOW
        Datetime rightNow = Datetime.now();
        seconds = rightNow.getTime();

        // Get Random Number
        randomNumber = Math.random() * 10;

        // Divide the two values to make unique and take the first 6 characters
        uniqueID = String.valueOf((seconds / randomNumber)).substring(0,7);

        // Remove the decimal
        uniqueID = uniqueID.replace('.','');

        return uniqueID;
    }


    /* WRAPPER CLASSES: START */
    /**
     * @description Generic wrapper class containing all endpoints and credential data
    */
    public class IntegrationsWrapper {
        /** @description Used to easily determine if the Custom MDT record retrieval was successful or not. When TRUE => Record retrieved successfully and data is populated, else FALSE => retrieval failed. */
        public Boolean hasData;

        /** @description Used to store the current org's domain */
        public String Org_Domain;

        /** @description Used to store the current org's Visualforce domain */
        public String Org_VF_Domain;

        /** @description Stores the SFCPQ ClientId used for Authenticating with Mulesoft */
        public String Credentials_SFCPQ_Mulesoft_ClientId;

        /** @description Stores the SFCPQ Client Secret used for Authenticating with Mulesoft */
        public String Credentials_SFCPQ_Mulesoft_ClientSecret;

        /** @description Stores the Salesforce Billing ClientId used for Authenticating with Mulesoft */
        public String Credentials_SFB_Mulesoft_ClientId;

        /** @description Stores the Salesforce Billing Client Secret used for Authenticating with Mulesoft */
        public String Credentials_SFB_Mulesoft_ClientSecret;

        /** @description Stores the Salesforce Field Service ClientId used for Authenticating with Mulesoft */
        public String Credentials_SFS_Mulesoft_ClientId;

        /** @description Stores the Salesforce Field Service Client Secret used for Authenticating with Mulesoft */
        public String Credentials_SFS_Mulesoft_ClientSecret;

        /** @description Stores the Mulesoft Base URL from which other Mulesoft endpoints will be built from */
        public String Endpoint_Mulesoft_Base_URL;

        /** @description Stores a JSTWrapper object containing JST-related integration information */
        public JSTWrapper jstInfo;

        /** @description Stores a PaymentechWrapper object containing Paymentech-related integration information */
        public PaymentechWrapper paymentechInfo;

        /** @description Stores a ContentMgmtWrapper object containing CM8-related integration information */
        public ContentMgmtWrapper contentMgmtInfo;

        /** @description Stores an OMSWrapper object containing OMS-related integration information */
        public OMSWrapper omsInfo;

        /** @description Stores an MMBWrapper object containing MMB-related integration information */
        public MMBWrapper mmbInfo;

        /** @description Stores a PartnerAPIWrapper object containing PartnerAPI-related integration information */
        public PartnerAPIWrapper partnerAPIInfo;

        /** @description Stores a SFMCWrapper object containing SFMC-related integration information */
        public SFMCWrapper sfmcInfo;

        /** @description Stores a GiactWrapper object containing Giact-related integration information */
        public GiactWrapper giactInfo;

        /** @description Stores a TritonIVRWrapper object containing Triton-IVR-related integration information */
        public TritonIVRWrapper tritonIVRInfo;

        /** @description Stores a RemotePayWrapper object containing Remote Pay-related integration information */
        public RemotePayWrapper remotePayInfo;

        /** @description Stores a VertexWrapper object containing Vertex-related integration information */
        public VertexWrapper vertexInfo;

        /** @description Stores a PaymentFileWrapper object containing Payment File integration information */
        public PaymentFileWrapper paymentFileInfo;
        
        /** @description Constructor method used to set the 'hasData' attribute to TRUE */
        public IntegrationsWrapper() {
            this.hasData = true;
        }
    }

    /**
     * @description Wrapper class to store JST-related Mulesoft integration data
     */
    public class JSTWrapper {
        /** @description Stores the 'deleteAppointment' Endpoint path used for canceling an appointment */
        public String Endpoint_deleteAppointment;

        /** @description Stores the 'deleteJob' Endpoint path used for canceling an appt and deleting the associated job */
        public String Endpoint_deleteJob;

        /** @description Stores the 'getTimeBands' Endpoint path used for obtaining Timebands data */
        public String Endpoint_getTimeBands;

        /** @description Stores the 'postAppointmentOffers' used for obtaining available datetime appointment slots */
        public String Endpoint_postAppointmentOffers;

        /** @description Stores the 'postJobsAssignedToEmployee' endpoint path */
        public String Endpoint_postJobsAssignedToEmployee;

        /** @description Stores the 'postJobsSearch' endpoint path */
        public String Endpoint_postJobsSearch;

        /** @description Stores the 'postScheduledAppointment' endpoint path */
        public String Endpoint_postScheduledAppointment;

        /** @description Stores the 'postScheduleEarliestAppointment' endpoint path */
        public String Endpoint_postScheduleEarliestAppointment;

        /** @description Stores the 'postSMSCallback' endpoint path */
        public String Endpoint_postSMSCallback;

        /** @description Stores the 'putRescheduleAppointment' Endpoint path */
        public String Endpoint_putRescheduleAppointment;

        /** @description Stores the 'putRescheduleEarliestAppointment' Endpoint path */
        public String Endpoint_putRescheduleEarliestAppointment;
    }

    /**
     * @description Wrapper class to store Paymentech-related Mulesoft integration data
    */
    public class PaymentechWrapper {
        /** @description Stores the 'getUID' Endpoint path used for instantiating an HPP or HPF */
        public String Endpoint_getUID_HPP;

        /** @description Stores the 'getTransactionData' Endpoint path used for obtaining specific information from a transaction */
        public String Endpoint_getTransactionData;

        /** @description Stores the Base URL for the Hosted Pay Page within Paymentech's portal */
        public String Endpoint_HPP_Base_URL;
    }

    /**
     * @description Wrapper class to store CM8-related Mulesoft integration data
    */
    public class ContentMgmtWrapper {
        /** @description Stores the CM8 'getContent' Endpoint path */
        public String Endpoint_getContent;

        /** @description Stores the CM8 'getDocument' Endpoint path */
        public String Endpoint_getDocuments;
    }

    /**
     * @description Wrapper class to store OMS-related Mulesoft integration data
    */
    public class OMSWrapper {
        /** @description Stores the OMS 'ContractVoid' Endpoint path */
        public String Endpoint_ContractVoid;

        /** @description Stores the OMS 'EmailOrderConfirmation' Endpoint path */
        public String Endpoint_Email_Order_Confirmation;

        /** @description Stores the OMS 'EnvelopeStatus' Endpoint path */
        public String Endpoint_Envelope_Status;

        /** @description Stores the OMS 'GenerateContract' Endpoint path */
        public String Endpoint_Generate_Contract;

        /** @description Stores the OMS 'SubmitOrder' Endpoint path */
        public String Endpoint_Order_Submit;
    }

    /**
     *  @description Wrapper class to store MMB-related Mulesoft integration data
    */
    public class MMBWrapper {
        /** @description Stores the MMB 'Incident' Endpoint path */
        public String Endpoint_Incident;
    }

    /**
     *  @description Wrapper class to store PartnerAPI-related Mulesoft integration data
    */
    public class PartnerAPIWrapper {
        /** @description Stores the ClientID for authenticating with the Partner API */
        public String Credentials_PartnerAPI_ClientID;

        /** @description Stores the Client Secret for authenticating with the Partner API */
        public String Credentials_PartnerAPI_ClientSecret;

        /** @description Stores the Partner API Base URL from which all endpoints will be constructed from */
        public String Endpoint_PartnerAPI_Base_URL;

        /** @description Stores the 'Submit Order' endpoint path */
        public String Endpoint_Submit_Order;

        /** @description Stores the 'Get Status' endpoint path */
        public String Endpoint_Get_Status;

        /** @description Stores the 'Account Lookup' endpoint path */
        public String Endpoint_Account_Lookup;

        /** @description Stores the 'Customer Lookup' endpoint path*/
        public String Endpoint_Customer_Lookup;
    }

    /**
     *  @description Wrapper class to store Salesforce Marketing Cloud-related Mulesoft integration data
    */
    public class SFMCWrapper {
        /** @description Stores the 'Email Request' endpoint path */
        public String Endpoint_Email_Request;
    }

    /**
     *  @description Wrapper class to store Giact-related Mulesoft integration data
    */
    public class GiactWrapper {
        /** @description Stores the 'ValidateBankAcctInfo' Endpoint path */
        public String Endpoint_Validate_Bank_Info;
    }

    /**
     *  @description Wrapper class to store Triton IVR-related Mulesoft integration data
    */
    public class TritonIVRWrapper {
        /** @description Stores the Endpoint for the Triton-IVR service */
        public String Endpoint_Triton_IVR;
    }

    /**
     * @description Wrapper class to store Remote Pay-related Mulesoft integration data
    */
    public class RemotePayWrapper {
        /** @description Stores the Base URL for emailing the Short Code */
        public String Endpoint_ShortCode_Email_Base_URL;
    }

    /**
     * @description Wrapper class to store Vertex-related Mulesoft integration data
    */
    public class VertexWrapper {
        /** @description Stores the endpoint for the 'Calculate Tax' endpoint */
        public String Endpoint_Calculate_Tax;
    }
     /**
    * @description Wrapper class to store Payment File Mulesoft integration data
    */
    public class PaymentFileWrapper {
        /** @description Stores the endpoint for the 'Calculate Tax' endpoint */
        public String SFB_Payment_Endpoint;
    }

    /* WRAPPER CLASSES: STOP */
}