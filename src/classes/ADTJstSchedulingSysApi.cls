public with sharing class ADTJstSchedulingSysApi 
{
    /**
     * Method for constructing endpoints for each callout.
     * Param = An identifier used to determine what callout is being made.
     * secondParam = This is used for handling secondary parameters that need to be passed into the request.
     */

    public static endPoint constructEndPoint(String param, String secondParam)
    {
        Mulesoft_API_Settings__mdt msft;
        try
        {
            msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, JST_deleteAppointment__c, JST_deleteJob__c, 
             JST_getJobs__c, JST_postAppointmentOffers__c, JST_postJobsAssignedToEmployee__c, JST_postJobsSearch__c, JST_postScheduleAppointment__c, 
            JST_postScheduleEarliestAppointment__c, JST_postSMSCallback__c, JST_putRescheduleAppointment__c, JST_putRescheduleEarliestAppointment__c
            FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
        }
        catch(QueryException ex)
        {
            return new endPoint('Error retrieving custom metadata record DEV1 from Mulesoft_API_Settings__mdt');
        }

        //endPoint object is used to return the callout method's endPoint
        endPoint constructedEndPoint = new endPoint();

        //Put clientId & clientSecret into the endPoint response object
        constructedEndPoint.client_id = msft.client_id__c;
        constructedEndpoint.client_secret = msft.client_secret__c;

        //Switch statement that switches on the param parameter, then constructs the endpoint based on the callout method, and takes the secondParam into account as needed by each callout method
        switch on param
        {
            when 'getTimeBands'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_getJobs__c;
            }
            when 'deleteJob'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_deleteJob__c;
                constructedEndPoint.endpoint.replace('{jobId}', secondParam);
            }
            when 'deleteAppointment'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_deleteAppointment__c;
                constructedEndPoint.endpoint.replace('{jobId}', secondParam);
            }
            when 'postOffers'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_postAppointmentOffers__c;
                constructedEndPoint.endpoint.replace('{jobId}', secondParam);
            }
            when 'postSchedule'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_postScheduleAppointment__c;
                constructedEndPoint.endpoint.replace('{jobId}', secondParam);
            }
            when 'postEarliest'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_postScheduleEarliestAppointment__c;
                constructedEndPoint.endpoint.replace('{jobId}', secondParam);
            }
            when 'putReschedule'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_putRescheduleAppointment__c;
                constructedEndPoint.endpoint.replace('{jobId}', secondParam);
            }
            when 'putEarliest'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_putRescheduleEarliestAppointment__c;
                constructedEndPoint.endpoint.replace('{jobId}', secondParam);
            }
            when 'postEmployee'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_postJobsAssignedToEmployee__c;
                constructedEndPoint.endpoint.replace('{employeeId}', secondParam);
            }
            when 'postSearch'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_postJobsSearch__c;
            }
            when 'postSMSCallback'
            {
                constructedEndPoint.endpoint = msft.Endpoint_base_url__c + msft.JST_postSMSCallback__c;
            }
        }
        
        return constructedEndPoint;
    }

    public static responseWrapper getHealthApp()
    {
        //Construct the endpoint
        endPoint e = constructEndPoint('getHealthApp', '');
        //Wrapper object for storing output
        responseWrapper output = new responseWrapper();
        //Create the new request
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        //This is a mock endpoint for mulesoft, actual endpoint is TBD
        //Set endpoint and headers for request
        request.setEndpoint(e.endpoint);
        system.debug('Endpoint: ');
        system.debug(e.endpoint);
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/json;');
        request.setHeader('client_id', e.client_id);
        request.setHeader('client_secret', e.client_secret);
        HttpResponse resp = new HttpResponse();
        
        //Send the POST request
        resp = http.send(request);

        //Switch on response code
        switch on resp.getStatusCode() 
        {
            //Successful response, no response body is returned in the response
            when 204 
            {
                output.responseMessage = 'SUCCESS: The POST email request was successful!';
            }
            //Fail - No response body, Unauthorized or invalid client application credentials
            when 401 
            {
                output.responseMessage = 'ERROR: Unauthorized or invalid client application credentials';
            }
            when 500 
            {
                output = (responseWrapper) System.JSON.deserialize(resp.getBody(), responseWrapper.class);
            }
        }
        //Return a wrapper object of the type responseWrapper with the response associated with the status code
        System.debug('Output: ');
        System.debug(output);
        return output;
    }

    /**
     * Start Wrapper Classes
     */

    //Wrapper class for returning an endpoint
    public class endPoint
    {
        @AuraEnabled public String errorMsg;
        @AuraEnabled public String endpoint;
        @AuraEnabled public String client_id;
        @AuraEnabled public String client_secret;

        //Used for scenarios where the custom metadata cannot be found
        public endPoint(String errorMsg)
        {
            this.errorMsg = errorMsg;
        }

        //Default constructor
        public endPoint()
        {
            this.errorMsg = '';
            this.endpoint = '';
        }
    }

    //Response Wrapper is a wrapper class used for all responses from the endpoint
    public class responseWrapper 
        {
            @AuraEnabled public String correlationId;
            @AuraEnabled public String tracingId;
            @AuraEnabled public String itemReferenceId;
            @AuraEnabled public String verificationResponse;
            @AuraEnabled public String accountResponseCode;
            @AuraEnabled public String createdDate;
            @AuraEnabled public String responseMessage;

            @AuraEnabled public Status status;
            @AuraEnabled public Messages[] messageArray;

            @AuraEnabled public List<TimeBands> timeBands;
            
            public responseWrapper(){
                this.correlationId = '';
                this.tracingId = '';
                this.itemReferenceId = '';
                this.verificationResponse = '';
                this.accountResponseCode = '';
                this.createdDate = '';
                this.messageArray = new List<Messages>();
                this.timeBands = new List<TimeBands>();
            }

            public responseWrapper(String errorMsg){
                this.correlationId = '';
                this.tracingId = '';
                this.itemReferenceId = '';
                this.verificationResponse = '';
                this.accountResponseCode = '';
                this.createdDate = '';
                this.responseMessage = errorMsg;
                this.messageArray = new List<Messages>();
            }
        }

        //This is a wrapper object that is referenced inside responseWrapper
        public class Status 
        {
            @AuraEnabled public String code;
            @AuraEnabled public List<Messages> messages;
        }

        //This is a wrapper object that is referenced inside responseWrapper        
        public class Messages 
        {
            @AuraEnabled public String type;
            @AuraEnabled public String reasonCode;
            @AuraEnabled public String context;
            @AuraEnabled public String message;
            @AuraEnabled public String severity;

        }
        //Wrapper class for the getTimeBands method
        public class TimeBands 
        {
            @AuraEnabled public Integer jstTimeBandId;
            @AuraEnabled public String mmbTimeBandId;
            @AuraEnabled public String name;
            @AuraEnabled public String startTime;
            @AuraEnabled public String endTime;
        }

    /**
     * End Wrapper Classes
     */
    
}