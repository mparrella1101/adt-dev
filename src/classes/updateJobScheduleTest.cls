/**
 * @description       : 
 * @author            : William F. Miranda
 * @group             : 
 * @last modified on  : 08-23-2021
 * @last modified by  : William F. Miranda
**/
@isTest
public class updateJobScheduleTest {

    @isTest static void updateJobScheduleTest() {

        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://yourInstance.my.salesforce.com/services/apexrest/updateJobSchedule/';
        request.httpMethod = 'POST';
        //set request body 
        request.requestBody = blob.valueOf('{"OrderNo":"1","ContractLink":"2"}');
        //Set Request
        RestContext.request = request;

        //Call method
        updateJobSchedule.doPost();
        
        
        
        updateJobSchedule.request req = new  updateJobSchedule.request();
        req.JobNo = '';
        req.Token = '';
        req.InstallDate =  dateTime.now();
        //req.InstallEndTime =;
       
        updateJobSchedule.response resp = new  updateJobSchedule.response();
        resp.AdscBeforeDiscount      = 0;
        resp.AdscDiscount       = 0;
        resp.AdscDoa            = 0;
        resp.AdscFee            = 0;
        resp.AdscTax            = 0;
        resp.AnscBeforeDiscount = 0;
        resp.AnscDiscount       = 0;
        resp.AnscDoa            = 0;
        resp.AnscFee            = 0;
        resp.AnscTax            = 0;
        resp.Createprogid       = '';
        resp.Createts           = dateTime.now();
        resp.Createuserid       = '';
        resp.InstallDate        = dateTime.now();
        //resp.InstallEndTime     = dateTime.now();
        resp.InstallToken       = '';
        resp.JobDuration        = 0;
        resp.JobKey             = '';
        resp.JobNo              = 0;
        resp.Lockid             = 0;
        resp.Modifyprogid       = '';
        resp.Modifyts           = dateTime.now();
        resp.Modifyuserid       = '';
        resp.OrderHeaderKey     = '';
        resp.ProductFamily      = '';
        resp.QspBeforeDiscount  = 0;
        resp.QspDiscount        = 0;
        resp.QspDoa             = 0;
        resp.QspFee             = 0;
        resp.QspTax             = 0;
        resp.SaleBeforeDiscount = 0;
        resp.SaleDiscount       = 0;
        resp.SaleDoa            = 0;
        resp.SaleFee            = 0;
        resp.SaleTax            = 0;
        resp.isHistory          = false;

      

                
            }


}