public with sharing class ADTMarketingCloudApi 
{
    @AuraEnabled
    public static responseWrapper sendPost(String emailLayout, String emailAddress, Integer customerId)
    {
        //Instantiate response wrapper using default constructor to store output
        responseWrapper output = new responseWrapper();
        
        Mulesoft_API_Settings__mdt msft;
        try
        {
            //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
            Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
            if(environment)
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, SFMC_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
            }
            else
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, SFMC_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
            }
        }
        catch(QueryException ex)
        {
            return new responseWrapper('Error retrieving custom metadata record from Mulesoft_API_Settings__mdt');
        }

        //Store Salesforce marketing cloud endpoint, Client ID & Client Secret so that they can be passed into the POST request as parameters
        String clientId = msft.client_id__c;
        String clientSecret = msft.client_secret__c;
        String endpointURL = msft.Endpoint_base_url__c + msft.SFMC_Endpoint__c;
        
        //Create the new request
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        //Create instance of request body object to store values that will be sent in the POST request
        requestObj reqBody = new requestObj();

        //Assign values to the request body from the method's parameters
        reqBody.emailLayout = emailLayout;
        reqBody.emailAddress = emailAddress;
        reqBody.customerId = customerId;
    
        //Create attributes to assign to the attributes list 
        List<Attributes> attList = new List<Attributes>();
        //Need a null variable of type String to provide for paymentTypeCode & isCancellable so the compiler knows what type that variable is, otherwise will get an error
        String nullString = null;
        attList.add(new Attributes('referenceId', '184265194'));
        attList.add(new Attributes('effectiveDate', '2020-11-17'));
        attList.add(new Attributes('accountNumberLast4Digits', '9424'));
        attList.add(new Attributes('amount', '50'));
        attList.add(new Attributes('paymentTypeCode', nullString));
        attList.add(new Attributes('paymentType', 'Discover'));
        attList.add(new Attributes('isCancellable', nullString));
        attList.add(new Attributes('isCancelled', 'false'));
        
        //Assign reqBody.attributes to be equal to attList once attList is populated with hardcoded values
        reqBody.attributes = attList;
        //Assign Synchronous to be true, Synchronous should always be set to true
        reqBody.synchronous = true;

        //This is a mock endpoint for mulesoft, actual endpoint is TBD
        //Set endpoint, headers and body for request
        request.setEndpoint(endpointURL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;');
        request.setHeader('client_id', clientId);
        request.setHeader('client_secret', clientSecret);
        request.setHeader('tracing_id', '12345');
        request.setBody(JSON.serialize(reqBody));
        System.debug('This is the request body: ');
        System.debug(request.getBody());
        HttpResponse resp = new HttpResponse();
        
        //Send the POST request
        resp = http.send(request);
        System.debug('Response: ');
        System.debug(resp);
        //Switch on response code
        switch on resp.getStatusCode() 
        {
            //Successful response, no response body is returned
            when 204 
            {
                output.responseMessage = 'SUCCESS: The POST email request was successful!';
                output.isSuccess = true;
            }
            //Fail - No response body, Unauthorized or invalid client application credentials
            when 401 
            {
                output = (responseWrapper) System.JSON.deserialize(resp.getBody(), responseWrapper.class);
                output.isSuccess = false;
                output = transcribeErrors(output);
            }
            when 500 
            {
                output = (responseWrapper) System.JSON.deserialize(resp.getBody(), responseWrapper.class);
                output.isSuccess = false;
                output = transcribeErrors(output);
            }
        }
        //Return a wrapper object of the type responseWrapper with the response associated with the status code
        System.debug('Output: ');
        System.debug(output);
        return output;

    }
    /**
     * Takes an account ID and returns Account's data
     */
    @AuraEnabled
    public static responseWrapper getAccount(String Id)
    {
        responseWrapper response = new responseWrapper();

        try
        {
            Account a = [SELECT Id, Email__c, MMBCustomerNumber__c FROM Account WHERE Id = :Id];
            response.EmailAddress = a.Email__c;
            response.MMBCustomerNumber = a.MMBCustomerNumber__c;
            System.debug(response.EmailAddress);
            System.debug(response.MMBCustomerNumber);
            return response;
        }
        catch(Exception e)
        {
            response.responseMessage = 'No related account found';
            return response;
        }
        
    }

    /**
     * Transcribes errors from the messages array into a more accessible attribute called 'responseMessage'
     * 
     * If an error response code is received, and no error message is included, I add a custom message indicating this has occurred
     */
    public static responseWrapper transcribeErrors(responseWrapper resp)
    {
        String message = '';
        if(resp.status.messages.size() > 0)
        {
            for(Messages m : resp.status.messages)
            {
                message = message + m.message;
            }
            resp.responseMessage = message;
        } 
        else
        {
            resp.responseMessage = 'An error has occurred, but no error message was included in the response';
        }
        system.debug(resp);
        return resp;
        
    }

    /**
     *  Start Wrapper Classes 
     */

    /**
     * Wrapper class for building the message to be sent in the POST request
     */
    public class requestObj 
    {
        @AuraEnabled public String emailLayout;
        @AuraEnabled public String emailAddress;
        @AuraEnabled public Integer customerId;
        @AuraEnabled public List<Attributes> attributes;
        @AuraEnabled public Boolean synchronous;
    }

    /**
     * Wrapper class referenced inside reqBody wrapper class
     */
    public class Attributes 
    {
        @AuraEnabled public String name;
        @AuraEnabled public String value;

        //Default Constructor needed because we are overriding the default constructor below
        public Attributes()
        {

        }
        //Constructor used to more easily create Attributes objects
        public Attributes(String name, String value)
        {
            this.name = name;
            this.value = value;
        }
    }
    /**
     * Wrapper class used to return responses from the endpoint and for validation of parameter input into the sendPost() method
     */
    public class responseWrapper 
        {
            @AuraEnabled public String correlationId;
            @AuraEnabled public String tracingId;
            @AuraEnabled public String itemReferenceId;
            @AuraEnabled public String verificationResponse;
            @AuraEnabled public String accountResponseCode;
            @AuraEnabled public String createdDate;
            @AuraEnabled public String responseMessage;
            @AuraEnabled public Boolean isSuccess;
            @AuraEnabled public Status status;
            @AuraEnabled public Messages[] messageArray;
            @AuraEnabled public String MMBCustomerNumber;
            @AuraEnabled public String EmailAddress;
            
            public responseWrapper(){
                this.correlationId = '';
                this.tracingId = '';
                this.itemReferenceId = '';
                this.verificationResponse = '';
                this.accountResponseCode = '';
                this.createdDate = '';
                this.messageArray = new List<Messages>();
                this.isSuccess = false;
                this.MMBCustomerNumber = '';
                this.EmailAddress = '';

            }

            public responseWrapper(String errorMsg){
                this.correlationId = '';
                this.tracingId = '';
                this.itemReferenceId = '';
                this.verificationResponse = '';
                this.accountResponseCode = '';
                this.createdDate = '';
                this.responseMessage = errorMsg;
                this.messageArray = new List<Messages>();
                this.isSuccess = false;
            }
        }
        
        //This is a wrapper object that is referenced inside responseWrapper
        public class Status 
        {
            @AuraEnabled public String code;
            @AuraEnabled public List<Messages> messages;
        }

        //This is a wrapper object that is referenced inside responseWrapper        
        public class Messages 
        {
            @AuraEnabled public String type;
            @AuraEnabled public String reasonCode;
            @AuraEnabled public String context;
            @AuraEnabled public String message;
            @AuraEnabled public String severity;

        }

    /**
     * End Wrapper Classes
     */
    
}