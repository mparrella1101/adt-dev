/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION : Serves as a class to be used for invocation of Vertex Service and handling the API response
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               7/23/2021                              - Original Version
*/
@isTest
public class ADTVertexAPITest {

        public class mock200Vertex implements HttpCalloutMock 
        {
            public HTTPResponse respond(HTTPRequest req) 
            {
                String response = '{"correlationId":"","tracingId":"","documentNumber":"","postingDate":"","locationCode":"","deliveryTerm":"","documentDate":"","transactionId":"","transactionType":"","simplificationCode":"","seller":{"nexusIndicator":false,"nexusReasonCode":"","company":"","division":"","physicalOrigin":{"streetAddress1":"","streetAddress2":"","city":"","mainDivision":"","subDivision":"","postalCode":"","country":""},"administrativeOrigin":{"streetAddress1":"","streetAddress2":"","city":"","mainDivision":"","subDivision":"","postalCode":"","country":""}},"customer":{"isTaxExempt":false,"exemptionReasonCode":"","destination":{"streetAddress1":"","streetAddress2":"","city":"","mainDivision":"","subDivision":"","postalCode":"","country":""}},"proratePercentage":0,"subTotal":0,"total":0,"totalTax":0,"currencyConversionFactors":[{"sourceCurrency":{"isoCurrencyName":"","isoCurrencyCodeAlpha":"","isoCurrencyCodeNum":0},"targetCurrency":{"isoCurrencyName":"","isoCurrencyCodeAlpha":"","isoCurrencyCodeNum":0},"conversionFactor":0}],"lineItems":[{"lineItemNumber":0,"usageClass":"","product":{"productCode":"","productClass":""},"quantity":0,"extendedPrice":0,"taxes":[{"taxResult":"","taxType":"","maxTaxIndicator":false,"situs":"","notRegisteredIndicator":false,"inputOutputType":"","taxCode":"","vertexTaxCode":"","reasonCode":"","filingCategoryCode":0,"isService":false,"rateClassification":"","taxCollectedFromParty":"","jurisdiction":{"jurisdictionName":"","jurisdictionLevel":"","jurisdictionId":0},"imposition":{"name":"","userDefined":false,"impositionId":0},"impositionType":{"typeDescription":"","userDefined":false,"impositionTypeId":0,"withholdingType":""},"taxRuleId":{"ruleId":0,"userDefined":false,"salesTaxHolidayIndicator":false,"taxRuleType":""},"inclusionRuleId":{"ruleId":0,"userDefined":false,"salesTaxHolidayIndicator":false,"taxRuleType":""},"calculatedTax":0,"effectiveRate":0,"taxable":0,"nonTaxable":0,"includedTax":0,"nominalRate":0}],"totalTax":0,"flexibleFields":{"flexibleCodeField":[{"value":"","fieldId":0}],"flexibleNumericField":[{"value":0,"fieldId":0}],"flexibleDateField":[{"value":"2021-05-17","fieldId":0}]},"assistedParameters":[{"value":"","paramName":"","phase":"","ruleName":"","originalValue":""}]}]}';
                HttpResponse res = new HttpResponse();
                res.setHeader('Content-Type', 'application/json');
                res.setBody(response);
                res.setStatusCode(200);
                return res;
            }
        }
    
    @testSetup static void createData()
    {
        TestHelperClass.createReferenceDataForTestClasses();
        Account acct = TestHelperClass.createAccountData();
        Product2 prod = new Product2(name='Test Product');
        insert prod;
    }

    
    @isTest 
    public static void VertexApiTest() 
    {       

        Account acct = [SELECT Id FROM Account];
        Product2 prod = [SELECT Id FROM Product2];
        
        
        String quoteLineJSON = '[{"attributes": {"type": "SBQQ__QuoteLine__c","url": "/services/data/v53.0/sobjects/SBQQ__QuoteLine__c/a6u1k0000000fhZAAQ"},"Id": "a6u1k0000000fhZAAQ","SBQQ__Quote__c": "a6y1k0000000ygoAAA","SBQQ__RequiredBy__c": "a6u1k0000000fhWAAQ","SBQQ__Product__c": "' + prod.Id + '","SBQQ__ProductOption__c": "a6n1k0000000TU4AAM","SBQQ__Number__c": 11,"SBQQ__DynamicOptionId__c": "a6m1k0000000a1kAAA","SBQQ__PricebookEntryId__c": "01u1k0000061GfCAAU","SBQQ__HasConsumptionSchedule__c": false,"SBQQ__Hidden__c": false,"SBQQ__Taxable__c": false,"SBQQ__SubscriptionBase__c": "List","SBQQ__SubscriptionScope__c": "Quote","SBQQ__ProductSubscriptionType__c": "Renewable","SBQQ__SubscriptionType__c": "Renewable","Name": "QL-0002342","SBQQ__AdditionalDiscount__c": 0,"SBQQ__Bundled__c": false,"SBQQ__ComponentDiscountedByPackage__c": false,"SBQQ__CostEditable__c": false,"SBQQ__CustomerPrice__c": 274,"SBQQ__ListPrice__c": 274,"SBQQ__OriginalPrice__c": 274,"SBQQ__NetPrice__c": 274,"SBQQ__NetTotal__c": 274,"SBQQ__NonDiscountable__c": false,"SBQQ__NonPartnerDiscountable__c": false,"SBQQ__Optional__c": false,"SBQQ__OptionType__c": "Component","SBQQ__BundledQuantity__c": 1,"SBQQ__Bundle__c": true,"SBQQ__PartnerPrice__c": 274,"SBQQ__PriceEditable__c": false,"SBQQ__ProratedListPrice__c": 274,"SBQQ__PricingMethod__c": "List","SBQQ__PricingMethodEditable__c": false,"SBQQ__ProrateMultiplier__c": 1,"SBQQ__Quantity__c": 1,"SBQQ__SpecialPrice__c": 274,"SBQQ__ComponentTotal__c": 75,"SBQQ__ComponentCost__c": 0,"SBQQ__ComponentListTotal__c": 75,"SBQQ__Renewal__c": false,"SBQQ__ProratedPrice__c": 274,"SBQQ__Uplift__c": 0,"SBQQ__UpliftAmount__c": 0,"SBQQ__ComponentUpliftedByPackage__c": false,"SBQQ__ProductName__c": "Command 2X16","SBQQ__Existing__c": false,"SBQQ__OptionLevel__c": 1,"SBQQ__CarryoverLine__c": false,"SBQQ__AllowAssetRefund__c": false,"SBQQ__RegularPrice__c": 274,"Install__c": 274,"UOM__c": "ADSC","Monthly__c": 274,"Section_Name__c": "Wireless","UniqueIdentifier__c": "ADT2X16-ADSC","Product_Name_Code__c": "Command 2X16 (ADT2X16AIO-1)","Monitoring__c": 0,"SBQQ__ProductCode__c": "ADT2X16","SBQQ__EffectiveQuantity__c": 1,"SBQQ__Markup__c": 0,"SBQQ__Product__r": {"attributes": {"type": "Product2","url": "/services/data/v53.0/sobjects/Product2/01t1k000005LRpmAAG"},"Id": "01t1k000005LRpmAAG","Name": "Command 2X16","ProductCode": "ADT2X16","SBQQ__PriceEditable__c": false,"SBQQ__QuantityScale__c": 0,"SBQQ__DefaultQuantity__c": 1,"SBQQ__QuantityEditable__c": true,"SBQQ__CostEditable__c": false,"SBQQ__NonDiscountable__c": false,"SBQQ__NonPartnerDiscountable__c": false,"SBQQ__SubscriptionBase__c": "List","SBQQ__PricingMethod__c": "List","SBQQ__PricingMethodEditable__c": false,"SBQQ__OptionSelectionMethod__c": "Click","SBQQ__ConfigurationType__c": "Allowed","SBQQ__Optional__c": false,"SBQQ__Taxable__c": false,"SBQQ__CustomConfigurationRequired__c": false,"SBQQ__Hidden__c": false,"SBQQ__ReconfigurationDisabled__c": false,"SBQQ__ExcludeFromOpportunity__c": false,"SBQQ__DescriptionLocked__c": false,"SBQQ__ConfigurationEvent__c": "Always","SBQQ__ExcludeFromMaintenance__c": false,"SBQQ__IncludeInMaintenance__c": false,"SBQQ__NewQuoteGroup__c": false,"SBQQ__SubscriptionType__c": "Renewable","SBQQ__AssetConversion__c": "One per quote line","SBQQ__BlockPricingField__c": "Quantity","SBQQ__HasConfigurationAttributes__c": false,"SBQQ__HasConsumptionSchedule__c": false,"SBQQ__ExternallyConfigurable__c": false,"SBQQ__AssetAmendmentBehavior__c": "Default","Section_Name__c": "Wireless","Product_Name_Code__c": "Command 2X16 (ADT2X16AIO-1)","UOM__c": "ADSC","PricebookEntries": {"totalSize": 1,"done": true,"records": [{"attributes": {"type": "PricebookEntry","url": "/services/data/v53.0/sobjects/PricebookEntry/01u1k0000061GfCAAU"},"Product2Id": "01t1k000005LRpmAAG","Id": "01u1k0000061GfCAAU","Pricebook2Id": "01s30000000Jl3DAAS","UnitPrice": 274,"IsActive": true}]}},"SBQQ__ProductOption__r": {"attributes": {"type": "SBQQ__ProductOption__c","url": "/services/data/v53.0/sobjects/SBQQ__ProductOption__c/a6n1k0000000TU4AAM"},"UniqueIdentifier__c": "ADT2X16-ADSC","Name": "PO-000039","Id": "a6n1k0000000TU4AAM"},"SBQQ__RequiredBy__r": {"attributes": {"type": "SBQQ__QuoteLine__c","url": "/services/data/v53.0/sobjects/SBQQ__QuoteLine__c/a6u1k0000000fhWAAQ"},"Id": "a6u1k0000000fhWAAQ","SBQQ__Quote__c": "a6y1k0000000ygoAAA","SBQQ__Product__c": "' + prod.Id + '","SBQQ__Number__c": 1,"SBQQ__PricebookEntryId__c": "01u1k0000061MmQAAU","SBQQ__HasConsumptionSchedule__c": false,"SBQQ__Hidden__c": true,"SBQQ__Taxable__c": false,"SBQQ__SubscriptionBase__c": "List","SBQQ__SubscriptionScope__c": "Quote","SBQQ__ProductSubscriptionType__c": "Renewable","SBQQ__SubscriptionType__c": "Renewable","Name": "QL-0002339","SBQQ__AdditionalDiscount__c": 0,"SBQQ__Bundled__c": false,"SBQQ__ComponentDiscountedByPackage__c": false,"SBQQ__CostEditable__c": false,"SBQQ__CustomerPrice__c": 0,"SBQQ__ListPrice__c": 0,"SBQQ__OriginalPrice__c": 0,"SBQQ__NetPrice__c": 0,"SBQQ__NetTotal__c": 0,"SBQQ__NonDiscountable__c": false,"SBQQ__NonPartnerDiscountable__c": false,"SBQQ__Optional__c": false,"SBQQ__Bundle__c": true,"SBQQ__PartnerPrice__c": 0,"SBQQ__PriceEditable__c": false,"SBQQ__ProratedListPrice__c": 0,"SBQQ__PricingMethod__c": "List","SBQQ__PricingMethodEditable__c": false,"SBQQ__ProrateMultiplier__c": 1,"SBQQ__Quantity__c": 1,"SBQQ__SpecialPrice__c": 0,"SBQQ__ComponentTotal__c": 708.99,"SBQQ__ComponentCost__c": 0,"SBQQ__ComponentListTotal__c": 708.99,"SBQQ__Renewal__c": false,"SBQQ__ProratedPrice__c": 0,"SBQQ__Uplift__c": 0,"SBQQ__UpliftAmount__c": 0,"SBQQ__ComponentUpliftedByPackage__c": false,"SBQQ__ProductName__c": "Residential Services","SBQQ__Existing__c": false,"SBQQ__CarryoverLine__c": false,"SBQQ__AllowAssetRefund__c": false,"SBQQ__RegularPrice__c": 0,"Install__c": 0,"Monthly__c": 0,"UniqueIdentifier__c": null,"Product_Name_Code__c": "Residential Services ()","Monitoring__c": 0,"SBQQ__ProductCode__c": "Residential Services","SBQQ__EffectiveQuantity__c": 1,"SBQQ__Markup__c": 0,"SBQQ__Product__r": {"attributes": {"type": "Product2","url": "/services/data/v53.0/sobjects/Product2/01t1k000005LchJAAS"},"Id": "01t1k000005LchJAAS","Name": "Residential Services","ProductCode": "Residential Services","SBQQ__PriceEditable__c": false,"SBQQ__QuantityScale__c": 0,"SBQQ__DefaultQuantity__c": 1,"SBQQ__QuantityEditable__c": false,"SBQQ__CostEditable__c": false,"SBQQ__NonDiscountable__c": false,"SBQQ__NonPartnerDiscountable__c": false,"SBQQ__SubscriptionBase__c": "List","SBQQ__PricingMethod__c": "List","SBQQ__PricingMethodEditable__c": false,"SBQQ__OptionSelectionMethod__c": "Click","SBQQ__OptionLayout__c": "Wizard","SBQQ__ConfigurationType__c": "Allowed","SBQQ__Optional__c": false,"SBQQ__Taxable__c": false,"SBQQ__CustomConfigurationRequired__c": false,"SBQQ__Hidden__c": true,"SBQQ__ReconfigurationDisabled__c": false,"SBQQ__ExcludeFromOpportunity__c": false,"SBQQ__DescriptionLocked__c": false,"SBQQ__ConfigurationEvent__c": "Always","SBQQ__ExcludeFromMaintenance__c": false,"SBQQ__IncludeInMaintenance__c": false,"SBQQ__NewQuoteGroup__c": false,"SBQQ__SubscriptionType__c": "Renewable","SBQQ__AssetConversion__c": "One per quote line","SBQQ__BlockPricingField__c": "Quantity","SBQQ__HasConfigurationAttributes__c": true,"SBQQ__HasConsumptionSchedule__c": false,"SBQQ__ExternallyConfigurable__c": false,"SBQQ__AssetAmendmentBehavior__c": "Default","Product_Name_Code__c": "Residential Services ()","PricebookEntries": {"totalSize": 1,"done": true,"records": [{"attributes": {"type": "PricebookEntry","url": "/services/data/v53.0/sobjects/PricebookEntry/01u1k0000061MmQAAU"},"Product2Id": "01t1k000005LchJAAS","Id": "01u1k0000061MmQAAU","Pricebook2Id": "01s30000000Jl3DAAS","UnitPrice": 0,"IsActive": true}]},"Section_Name__c": null},"SBQQ__RequiredBy__r": null,"SBQQ__Group__r": null,"SBQQ__Quote__r": {"attributes": {"type": "SBQQ__Quote__c","url": "/services/data/v53.0/sobjects/SBQQ__Quote__c/a6y1k0000000ygoAAA"},"Id": "a6y1k0000000ygoAAA","Name": "Q-00175","SBQQ__Type__c": "Quote","SBQQ__Account__c":"' + acct.Id + '","SBQQ__ExpirationDate__c": "2021-10-19","SBQQ__QuoteProcessId__c": "a6v1k000000bQgkAAE","SBQQ__NetAmount__c": 708.99,"SBQQ__CustomerAmount__c": 708.99,"SBQQ__PricebookId__c": "01s30000000Jl3DAAS","SBQQ__ContractingMethod__c": "By Subscription End Date","SBQQ__Unopened__c": false,"SBQQ__LineItemCount__c": 18,"SBQQ__PaymentTerms__c": "60 Months","SBQQ__WatermarkShown__c": false,"Read_Only_CPQ_Quotes__c": false,"Finance_Eligible_at_Submit__c": false,"X400_off_599_purchase__c": "Selected","Sales_Type__c": "N1","Deposit_Waiver__c": "Not Requested","SBQQ__Status__c": "Configured","Risk_Grade__c": "W","Approval_Type__c": "CAE1","Has_QSP__c": 1,"Minimum_Payment__c": 619,"Total_Install_Charge__c": 619,"Total_Activation_and_Permit_Fees__c": 30,"Monitoring_Guard_Response__c": 30,"QSP__c": 0,"Total_Monthly_Charge__c": 708.99,"Service_Level__c": 59.99,"X400_off_499_install__c": "Visible","OrderNumber__c": "Q-0000000169","SBQQ__Primary__c": false,"SBQQ__LineItemsGrouped__c": false,"SBQQ__Account__r": {"attributes": {"type": "Account","url": "/services/data/v53.0/sobjects/Account/0011k00000ubNMHAA2"},"EquifaxRiskGrade__c": "W","EquifaxApprovalType__c": "CAE1","MMBOrderType__c": "N1","Id": "0011k00000ubNMHAA2","relatedRecord": true},"Installments__c": null,"MMB_Out_Order_Type__c": null,"SBQQ__ListAmount__c": 708.99,"SBQQ__RegularAmount__c": 708.99},"SBQQ__ProductOption__r": null,"SBQQ__TermDiscountSchedule__r": null,"SBQQ__DiscountSchedule__r": null,"SBQQ__ContractedPrice__r": null,"UOM__c": null,"QSP__c": null,"SBQQ__DiscountScheduleType__c": null,"SBQQ__PriorQuantity__c": null,"SBQQ__UpgradedQuantity__c": null,"SBQQ__SubscriptionPricing__c": null,"CartGroup__c": null,"Section_Name__c": "","SBQQ__Description__c": null,"SBQQ__ProductOption__c": null,"SBQQ__ContractedPrice__c": null,"SBQQ__DiscountSchedule__c": null,"SBQQ__TermDiscountSchedule__c": null,"SBQQ__MarkupRate__c": null,"SBQQ__UnitCost__c": null,"SBQQ__MarkupAmount__c": null,"SBQQ__StartDate__c": null,"SBQQ__EndDate__c": null,"SBQQ__SubscriptionTargetPrice__c": null,"SBQQ__AdditionalDiscountAmount__c": null,"SBQQ__Discount__c": null,"SBQQ__OptionDiscount__c": null,"SBQQ__PartnerDiscount__c": null,"SBQQ__VolumeDiscount__c": null,"SBQQ__SpecialPriceDescription__c": "No price from external system","Last_External_Calculated_Time__c": "2021-09-24T16:23:09.212Z","SBQQ__PartnerTotal__c": 0,"SBQQ__ListTotal__c": 0,"SBQQ__CustomerTotal__c": 0,"SBQQ__RegularTotal__c": 0,"SBQQ__GrossProfit__c": null,"SBQQ__PackageTotal__c": 708.99},"SBQQ__Group__r": null,"SBQQ__Quote__r": {"attributes": {"type": "SBQQ__Quote__c","url": "/services/data/v53.0/sobjects/SBQQ__Quote__c/a6y1k0000000ygoAAA"},"Id": "a6y1k0000000ygoAAA","Name": "Q-00175","SBQQ__Type__c": "Quote","SBQQ__Account__c": "' + acct.Id + '","SBQQ__ExpirationDate__c": "2021-10-19","SBQQ__QuoteProcessId__c": "a6v1k000000bQgkAAE","SBQQ__NetAmount__c": 708.99,"SBQQ__CustomerAmount__c": 708.99,"SBQQ__PricebookId__c": "01s30000000Jl3DAAS","SBQQ__ContractingMethod__c": "By Subscription End Date","SBQQ__Unopened__c": false,"SBQQ__LineItemCount__c": 18,"SBQQ__PaymentTerms__c": "60 Months","SBQQ__WatermarkShown__c": false,"Read_Only_CPQ_Quotes__c": false,"Finance_Eligible_at_Submit__c": false,"X400_off_599_purchase__c": "Selected","Sales_Type__c": "N1","Deposit_Waiver__c": "Not Requested","SBQQ__Status__c": "Configured","Risk_Grade__c": "W","Approval_Type__c": "CAE1","Has_QSP__c": 1,"Minimum_Payment__c": 619,"Total_Install_Charge__c": 619,"Total_Activation_and_Permit_Fees__c": 30,"Monitoring_Guard_Response__c": 30,"QSP__c": 0,"Total_Monthly_Charge__c": 708.99,"Service_Level__c": 59.99,"X400_off_499_install__c": "Visible","OrderNumber__c": "Q-0000000169","SBQQ__Primary__c": false,"SBQQ__LineItemsGrouped__c": false,"SBQQ__Account__r": {"attributes": {"type": "Account","url": "/services/data/v53.0/sobjects/Account/0011k00000ubNMHAA2"},"EquifaxRiskGrade__c": "W","EquifaxApprovalType__c": "CAE1","MMBOrderType__c": "N1","Id": "0011k00000ubNMHAA2","relatedRecord": true},"Installments__c": null,"MMB_Out_Order_Type__c": null,"SBQQ__ListAmount__c": 708.99,"SBQQ__RegularAmount__c": 708.99},"SBQQ__TermDiscountSchedule__r": null,"SBQQ__DiscountSchedule__r": null,"SBQQ__ContractedPrice__r": null,"QSP__c": null,"SBQQ__DiscountScheduleType__c": null,"SBQQ__PriorQuantity__c": null,"SBQQ__UpgradedQuantity__c": null,"SBQQ__SubscriptionPricing__c": null,"CartGroup__c": null,"SBQQ__Description__c": null,"SBQQ__ContractedPrice__c": null,"SBQQ__DiscountSchedule__c": null,"SBQQ__TermDiscountSchedule__c": null,"SBQQ__MarkupRate__c": null,"SBQQ__UnitCost__c": null,"SBQQ__MarkupAmount__c": null,"SBQQ__StartDate__c": null,"SBQQ__EndDate__c": null,"SBQQ__SubscriptionTargetPrice__c": null,"SBQQ__AdditionalDiscountAmount__c": null,"SBQQ__Discount__c": null,"SBQQ__OptionDiscount__c": null,"SBQQ__PartnerDiscount__c": null,"SBQQ__VolumeDiscount__c": null,"Taxes__c": "16.44","SBQQ__PartnerTotal__c": 274,"SBQQ__ListTotal__c": 274,"SBQQ__CustomerTotal__c": 274,"SBQQ__RegularTotal__c": 274,"SBQQ__GrossProfit__c": null,"SBQQ__PackageTotal__c": 349}]';

        String quoteJSON = '{"attributes": {"type": "SBQQ__Quote__c","url": "/services/data/v53.0/sobjects/SBQQ__Quote__c/a6y1k0000000yeiAAA"},"Id": "a6y1k0000000yeiAAA","Name": "Q-00149","SBQQ__Type__c": "Quote","SBQQ__Account__c": "' + acct.Id + '","SBQQ__ExpirationDate__c": "2021-10-15","SBQQ__QuoteProcessId__c": "a6v1k000000bQgkAAE","SBQQ__NetAmount__c": 643.99,"SBQQ__CustomerAmount__c": 643.99,"SBQQ__PricebookId__c": "01s30000000Jl3DAAS","SBQQ__ContractingMethod__c": "By Subscription End Date","SBQQ__Unopened__c": false,"SBQQ__LineItemCount__c": 18,"SBQQ__PaymentTerms__c": "60 Months","SBQQ__WatermarkShown__c": false,"Finance_Eligible_at_Submit__c": false,"X400_off_599_purchase__c": "Selected","Read_Only_CPQ_Quotes__c": false,"Deposit_Waiver__c": "Not Requested","Risk_Grade__c": "A","Approval_Type__c": "APPROV","QSP_Checked__c": false,"Total_Activation_and_Permit_Fees__c": 30,"Minimum_Payment__c": 554,"SBQQ__Status__c": "Configured","Total_Monthly_Charge__c": 0,"Total_Install_Charge__c": 554,"X400_off_499_install__c": "Visible","SBQQ__Primary__c": false,"SBQQ__LineItemsGrouped__c": false,"SBQQ__Account__r": {"attributes": {"type": "Account","url": "/services/data/v53.0/sobjects/Account/0011k00000susbNAAQ"},"EquifaxRiskGrade__c": "A","EquifaxApprovalType__c": "APPROV","Id": "0011k00000susbNAAQ","relatedRecord": true,"MMBOrderType__c": null},"Installments__c": null,"MMB_Out_Order_Type__c": null,"Sales_Type__c": "","SBQQ__ListAmount__c": 643.99,"SBQQ__RegularAmount__c": 643.99}';
        Test.startTest();
        //Set mock callout class
        Test.setMock(HttpCalloutMock.class, new mock200Vertex());   
        String resp =  ADTVertexAPI.ADTVertexAPIManager(quoteJSON, quoteLineJSON);
        Test.stopTest();

        ADTVertexAPI.LineItems_Z lineitems = new ADTVertexAPI.LineItems_Z();
        lineitems.lineItemNumber = 0;
        lineitems.taxDate = '';
        lineitems.isMulticomponent = false;
        lineitems.locationCode = '';
        lineitems.deliveryTerm = '';
        lineitems.postingDate = '';
        lineitems.costCenter = '';
        lineitems.departmentCode = '';
        lineitems.generalLedgerAccount = '';
        lineitems.materialCode = '';
        lineitems.projectNumber = '';
        lineitems.usageClass = '';
        lineitems.vendorSKU = '';
        lineitems.countryofOriginISOCode = '';
        lineitems.modeOfTransport = 0;
        lineitems.natureOfTransaction = 0;
        lineitems.intrastatCommodityCode = '';
        lineitems.netMassKilograms = 0;
        lineitems.lineItemId = '';
        lineitems.taxIncludedIndicator = false;
        lineitems.transactionType = '';
        lineitems.simplificationCode = '';
        lineitems.titleTransfer = '';
        lineitems.chainTransactionPhase = '';
        lineitems.exportProcedure = '';
        lineitems.materialOrigin = '';
        lineitems.commodityCode = '';
        lineitems.quantity = 0;
        lineitems.quantityMeasureUnit = '';
        lineitems.weight = 0;
        lineitems.weightMeasureUnit = '';
        lineitems.volume = 0;
        lineitems.volumeMeasureUnit = '';
        lineitems.supplementaryUnit = 0;
        lineitems.supplementaryUnitType = '';
        lineitems.freight = 0;
        lineitems.fairMarketValue = 0;
        lineitems.cost = 0;
        lineitems.extendedPrice = 0;
        lineitems.landedCost = 0;
        lineitems.amountBilledToDate = 0;
        lineitems.companyCodeCurrencyTaxableAmount = 0;

        ADTVertexAPI.Dispatcher dispatcher = new ADTVertexAPI.Dispatcher();
        dispatcher.dispatcherCode = '';
        dispatcher.classCode = '';

        ADTVertexAPI.response resp1 = new ADTVertexAPI.response();
        resp1.correlationId = '';
        resp1.tracingId = '';

        ADTVertexAPI.status stat = new ADTVertexAPI.status();
        stat.code = '';

        ADTVertexAPI.message msg = new ADTVertexAPI.message();
        msg.type = '';
        msg.severity = '';
        msg.reasonCode = '';
        msg.message = '';

        ADTVertexAPI.StatisticalValue statValue = new ADTVertexAPI.StatisticalValue();
        statValue.invoiceValue = 0;
        statValue.isoCurrencyCodeNum = 0;
        statValue.isoCurrencyName = '';
        statValue.isoCurrencyCodeAlpha = '';

        ADTVertexAPI.Discount disc = new ADTVertexAPI.Discount();
        disc.userDefinedDiscountCode = '';
        disc.amount = 0;
        disc.percent = 0;

        ADTVertexAPI.NonTaxableOverride nto = new ADTVertexAPI.NonTaxableOverride();
        nto.amount = 0;
        nto.overrideNonTaxableReasonCode = '';

        ADTVertexAPI.CurrencyConversion currConv = new ADTVertexAPI.CurrencyConversion();
        currConv.isoCurrencyCodeNum = 0;
        currConv.isoCurrencyName = '';
        currConv.conversionRate = 0;
        currConv.isoCurrencyCodeAlpha = '';

        ADTVertexAPI.ReturnsNumericField rnf = new ADTVertexAPI.ReturnsNumericField();
        rnf.value = 0;
        rnf.name = '';

        ADTVertexAPI.TaxOverride taxOver = new ADTVertexAPI.TaxOverride();
        taxOver.overrideType = '';
        taxOver.overrideReasonCode = '';

        ADTVertexAPI.ExemptOverride exemptOver = new ADTVertexAPI.ExemptOverride();
        exemptOver.amount = 0;
        exemptOver.overrideExemptReasonCode = '';

        ADTVertexAPI.DeductionOverride deductOver = new ADTVertexAPI.DeductionOverride();
        deductOver.exemptOverride = exemptOver;
        deductover.nonTaxableOverride = nto;

        ADTVertexAPI.ImpositionType impType = new ADTVertexAPI.ImpositionType();
        impType.typeDescription = '';
        impType.userDefined = false;
        impType.impositionTypeId = 0;
        impType.withholdingType = '';

        ADTVertexAPI.ImpositionToProcess impToProcess = new ADTVertexAPI.ImpositionToProcess();
        impToProcess.jurisdictionLevel = '';
        impToProcess.ImpositionType = impType;

        ADTVertexAPI.LineType lineT = new ADTVertexAPI.LineType();
        lineT.lineTypeCode = '';
        lineT.direction = '';
        lineT.content = '';
        lineT.status = '';
        lineT.accumulationLocation = '';

        ADTVertexAPI.PhysicalLocation py = new ADTVertexAPI.PhysicalLocation();
        py.taxAreaId = 0;
        py.streetAddress1 = '';
        py.streetAddress2 = '';
        py.city = '';
        py.mainDivision = '';
        py.subDivision = '';
        py.postalCode = '';
        py.country = '';

        ADTVertexAPI.TaxRegistration taxReg = new ADTVertexAPI.TaxRegistration();
        taxReg.isoCountryCode = '';
        

        

    }



}