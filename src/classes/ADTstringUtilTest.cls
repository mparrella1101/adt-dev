@isTest
private class ADTstringUtilTest {

    public static Account acc;

    public static testMethod void myUnitTest() 
    {
        User admin = TestHelperClass.createAdminUser();
        System.runAs(admin) {        
        TestHelperClass.createReferenceDataForTestClasses();
        }
        TestHelperClass.createReferenceUserDataForTestClasses();
    
        User u;
        User current = [Select Id from User where Id = :UserInfo.getUserId()];
    
        Affiliate__c aff;
    
        System.runAs(current) 
        { 
            u = TestHelperClass.createSalesRepUser();
            u.Phone = '305231123';
            update u;
        
            //Create Addresses first
            Address__c addr = new Address__c();
            addr.Latitude__c = 38.94686000000000;
            addr.Longitude__c = -77.25470100000000;
            addr.Street__c = '8952 Brook Rd';
            addr.City__c = 'McLean';
            addr.State__c = 'VA';
            addr.PostalCode__c = '221o2';
            addr.County__c = 'County123';
            addr.OriginalAddress__c = '8952 Brook Rd, McLean, VA 221o2';
        
            insert addr;
        
            acc = TestHelperClass.createAccountData(addr, true);
            acc.FirstName__c = 'TestFName';
            acc.LastName__c = 'TestLName';
            acc.Name = 'TestFName TestLName';
            acc.Email__c = 'test@test.com';
            acc.OwnerId = u.Id;
            update acc;

        // MParrella | Coastal Cloud | 01-06-2022 | Fixing Test Class failures: START
        // MP 1/6/22: Commenting out original record creation that was causing VR issues
//        SBQQ__Quote__c quoteRecord = new SBQQ__Quote__c(SBQQ__Account__c = acc.Id);
//        insert quoteRecord;

        // MP 1/6/22: Modifying create quote process to avoid VR issues
        SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(null, null, false, false, true);


        // MP 1/6/22: Commenting out SOQL query and directly supplying QuoteId to method call
//        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];

            // MParrella | Coastal Cloud | 01-06-2022 | Fixing Test Class failures: STOP
        ADTstringUtil.emailShortcode(String.Valueof(testQuote.Id));

        System.AssertEquals('TYTGZYH',ADTstringUtil.generateShortcode('1234567890'));
    }
    }   
}