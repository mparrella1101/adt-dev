/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION : Used to check for remote payment method capture eligibility
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               8/10/2021         2877                 - Original Version
*/


@RestResource(urlMapping='/remotePayCaptureEligible/')
global with sharing class remotePayCaptureEligible {

  
        /*
        *   Method that runs when the endpoint is called and the method is set to post
        */
        @HttpPost
        global static response doPost() {
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            //Declare Response
            response resp = new response();
            //Exception Handleing 
            try{

            Blob jsonBody = RestContext.request.requestBody;
            String jsonString = jsonBody.toString();
            system.debug('This is the json: ' + jsonString);
            //Initialize wrapper class for response
            request respClass = new request();
            //Deserialize json in body to wrapper
            respClass = (request) JSON.deserialize(jsonString,request.class);  
            
            
            Boolean matchingRemotePayID = ([SELECT Id, Remote_Pay_ID__c FROM Payment_Info__c WHERE Remote_Pay_ID__c =:respClass.RemotePayID].size() > 0) ? true : false;
            
            
            if(matchingRemotePayID){
                resp.Eligible = true;
             
            }
            else{
                resp.Eligible = false;
                resp.Reason = 'This PayId is not Eligible';
            }
            
            }//end try
            catch (Exception e){

                    res.statusCode = 400;
                    resp.ErrorMessage = 'Sorry! The json syntax is incorrect. Please verify and try again. Show the following to an administrator: ' + e;
                }

            return resp;
        }
/*
*---------------------------------------------------------------------------------------------------------------------------------------------------
                                                                    WRAPPER CLASSES
*---------------------------------------------------------------------------------------------------------------------------------------------------
*/
    
    
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        /*
        *   Wrapper Class to Handle request received 
        */    
        //Request Recieved in post 
        global class request {
            
            public String RemotePayID {get;set;} //Encoded' RemotePayID that is attached to a specific quote/order
          
        }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        /*
        *   Wrapper Class to Handle request received 
        */    
        //Request Recieved in post 
        global class response {
            public Boolean Eligible {get;set;}//True - Successful, False - Unsuccessful (Reason attribute will be populated)
            public String Reason {get;set;}//Reason for unsuccessful attempt
            public String errorMessage {get;set;}

        }//end response class
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

}