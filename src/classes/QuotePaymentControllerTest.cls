@isTest
private class QuotePaymentControllerTest 
{
    @TestSetup
    static void makeData()
    {
        
        TestHelperClass.createReferenceDataForTestClasses();
        TestHelperClass.createAccountData();
    }
    @isTest
    static void testQuery()
    {
        Account a = [SELECT Id FROM Account LIMIT 1];
        Payment_Info__c p = new Payment_Info__c(Validated__c = true, Account__c = a.Id);
        insert p;
        System.debug(a);
        Test.startTest();
        List<Payment_Info__c> payList = QuotePaymentController.getValidatedPaymentInfo(String.valueOf(a.Id));
        System.debug(payList);
        System.assertEquals(1,payList.size());
        System.assertEquals(true,payList[0].Validated__c);
        Test.stopTest();
    }
}