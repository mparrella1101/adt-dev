/**
 * Visualforce Page Controller for the 'cc_jobToQuoteRedirect' VF Page. Primary purpose is to located
 * a SalesforceCPQ Quote record given an MMB Job Number.
 *
 * Expects a URL parameter ('ident') to be contained within the VF Page url. Example URL:
 *
 * http://{{vfDomain}}/apex/cc_jobToQuoteRedirect?ident=000000
 *
 * Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           October 25, 2021        Initial version
 */

public with sharing class CC_JobToQuoteRedirectCont {
    public String statusMsg {get; set;} // Status message to display (if applicable)
    public SBQQ__Quote__c quote {get; private set;}

    public PageReference init() {
        this.statusMsg = 'Redirecting to SalesforceCPQ Quote Record. Please wait...';

        // Attempt to find a Quote record based on MMB Job Number
        try {
            String identUrlParam = ApexPages.currentPage().getParameters().get('ident');
            if (identUrlParam == null || identUrlParam == ''){
                this.statusMsg = 'Error: No Job Number passed in!';
                return null;
            } else {
                this.quote = [SELECT Id FROM SBQQ__Quote__c WHERE MMB_Job__c != NULL AND MMB_Job__c =: identUrlParam LIMIT 1];
                PageReference quoteRecordPage = new PageReference('/lightning/r/SBQQ__Quote__c/' + this.quote.Id + '/view');
                quoteRecordPage.setRedirect(true);
                return quoteRecordPage;
            }
        }
        catch(QueryException ex){
            this.statusMsg = 'Error: Could not locate SalesforceCPQ Quote record. Details: ' + ex.getMessage();
            return null;
        }
    }
}