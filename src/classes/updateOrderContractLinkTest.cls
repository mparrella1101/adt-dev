@isTest
public class updateOrderContractLinkTest {
    @isTest static void testUpdateOrderContractLink() {
      
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://yourInstance.my.salesforce.com/services/apexrest/updateOrderContractLink/';
        request.httpMethod = 'POST';
        //set request body 
        request.requestBody = blob.valueOf('{"OrderNo":"1","ContractLink":"2"}');
        //Set Request
        RestContext.request = request;
        updateOrderContractLink.response resp = new  updateOrderContractLink.response();
        // Call Method
        resp =  updateOrderContractLink.doPost();
        //Set Response for coverage until logic is added
        resp.ActualPricingDate = datetime.now();
        resp.AdjustmentInvoicePending = false;
        resp.AllAddressesVerified = false;
        resp.ApprovalCycle = 0;
        resp.AuthorizationExpirationDate = datetime.now();
        resp.BillToID = '';
        resp.BillToKey = '';
        resp.BuyerOrganizationCode = '';
        resp.CarrierAccountNo = '';
        resp.CarrierServiceCode = '';
        resp.ChainType = '';
        resp.ChargeActualFreightFlag = false;
        resp.ComplimentaryGiftBoxQty = 0;
        resp.CreatedAtNode = '';
        resp.Createprogid = '';
        resp.Createts = datetime.now();
        resp.Createuserid = '';
        resp.CustCustPONo = '';
        resp.CustomerAgeuble = 0;
        resp.CustomerContactID = '';
        resp.CustomerEMailID = '';
        resp.CustomerFirstName = '';
        resp.CustomerLastName = '';
        resp.CustomerPONo = '';
        resp.CustomerPhoneNo = '';
        resp.CustomerZipCode = '';
        resp.DeliveryCode = '';
        resp.Division = '';
        resp.DocumentType = '';
        resp.DraftOrderFlag = false;
        resp.EnteredBy = '';
        resp.EnterpriseCode = '';
        resp.ExpirationDate = datetime.now();
        resp.FreightTerms = '';
        resp.HasDerivedChild = false;
        resp.HasDerivedParent = false;
        resp.HoldFlag = false;
        resp.HoldReasonCode = '';
        resp.InternalApp = false;
        resp.InvoiceComplete = false;
        resp.Lockid = 0;
        resp.Modifyprogid = '';
        resp.Modifyts = datetime.now();
        resp.Modifyuserid = '';
        resp.NextAlertTs = datetime.now();
        resp.NoOfAuthStrikes = 0;
        resp.NotifyAfterShipmentFlag = false;
        resp.OpportunityKey = '';
        resp.OrderComplete = false;
        resp.OrderDate = datetime.now();
        resp.OrderHeaderKey = '';
        resp.OrderName = '';
        resp.OrderType = '';
        resp.OriginalTaxDouble = 0;
        resp.OriginalTotalAmountDouble = 0;
        resp.OtherChargesDouble = 0;
        resp.Override_Z = false;
        resp.PaymentStatus = '';
        resp.PendingTransferInDouble = 0;
        resp.PersonalizeCode = '';
        resp.PriceOrder = '';
        resp.PriceProgramName = '';
        resp.PriorityCode = '';
        resp.PriorityNumber = 0;
        resp.PropagateCancellations = '';
        resp.Purpose = '';
        resp.ReserveInventoryFlag = '';
        resp.ReturnByGiftRecipient = '';
        resp.SCAC = '';
        resp.SaleVoided = false;
        resp.SearchCriteria1 = '';
        resp.SearchCriteria2 = '';
        resp.SellerOrganizationCode = '';
        resp.ShipToID = '';
        resp.ShipToKey = '';
        resp.SoldToKey = '';
        resp.SourceIPAddress = '';
        resp.TaxExemptFlag = false;
        resp.TaxExemptionCertificate = '';
        resp.TaxJurisdiction = '';
        resp.TaxPayerId = '';
        resp.TermsCode = '';
        resp.TotalAdjustmentAmountDouble = 0;
        resp.isHistory = false;
       updateOrderContractLink.Extn ext = new updateOrderContractLink.Extn();
       ext.ExtnAdditionalAttributes = '';
       ext.ExtnAdditionalInformation = '';
       ext.ExtnContractLink = '';
       ext.ExtnHazardAnswers = '';
       ext.ExtnPaymentDepositDouble = 0;
       ext.ExtnPaymentDepositBankAccountNumber = '';
       ext.ExtnPaymentDepositBankAccountType = '';
       ext.ExtnPaymentDepositBankName = '';
       ext.ExtnPaymentDepositBankRoute = '';
       ext.ExtnPaymentDepositPaymentMethod = '';
       ext.ExtnPaymentRecurringSame = false;
       resp.Extn = ext;
        
        

      
    }

}