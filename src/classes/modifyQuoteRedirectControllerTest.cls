@isTest
private with sharing class modifyQuoteRedirectControllerTest {
    @isTest
    private static void testGetQuote(){
        
        ResaleGlobalVariables__c testGlobalVariable1 = CPQTestUtils.createResaleGlobalVariables('DataRecastDisableAccountTrigger', 'TRUE', false);
        ResaleGlobalVariables__c testGlobalVariable2 = CPQTestUtils.createResaleGlobalVariables('runEwcMarketing', 'TRUE', false);
        ResaleGlobalVariables__c testGlobalVariable3 = CPQTestUtils.createResaleGlobalVariables('BypassQuoteSecurityProfiles', 'TRUE', false);
        insert new List<ResaleGlobalVariables__c>{testGlobalVariable1, testGlobalVariable2, testGlobalVariable3};        
        Account testAccount = CPQTestUtils.createAccount('Test Account KF', true);
        Opportunity testOpp = CPQTestUtils.createOpportunity('Test Opportunity KF', testAccount, true);
        Product2 testProduct = CPQTestUtils.createProduct('Residential Services', null, 'TestProductCode', null, null, null, true, true);
        SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(testAccount, testOpp, true, true, true);
        SBQQ__QuoteLine__c testQuoteLine = CPQTestUtils.createQuoteLine(null, null, testQuote, true, true);
        SBQQ__CustomAction__c testCustomAction = new SBQQ__CustomAction__c(
                                                                        Name = 'Add Home Security Services',
                                                                        SBQQ__DisplayOrder__c = 300,
                                                                        SBQQ__Type__c = 'Menu',
                                                                        SBQQ__Active__c = false,
                                                                        SBQQ__BrandButton__c = false,
                                                                        SBQQ__Default__c = false
                                                                        );
        insert testCustomAction;
        modifyQuoteRedirectController.QuoteWrapper quoteWrapper = modifyQuoteRedirectController.getQuote(testQuote.Id);
        System.debug('quoteWrapper: ' + quoteWrapper);
        System.assertEquals(testQuote.Id, quoteWrapper.quote.Id, 'We expect our quoteWrapper to have returned the correct quote');
        System.assertEquals(testCustomAction.Id, quoteWrapper.customActionId, 'We expect our quoteWrapper to have returned the correct custom action');
        System.assertEquals(testProduct.Id, quotewrapper.productId, 'We expect our quoteWrapper to have returned the correct productId');
        
    }
    
}