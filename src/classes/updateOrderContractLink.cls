/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION : Used to update CPQ with the link to content management where the contract is stored
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               8/10/2021         2877                 - Original Version
*/


@RestResource(urlMapping='/updateOrderContractLink/')
global with sharing class updateOrderContractLink {
    
    
    /*
    *   Method that runs when the endpoint is called and the method is set to post
    */
    @HttpPost
    global static response doPost() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        response resp = new response();
        try{
            
            Blob jsonBody = RestContext.request.requestBody;
            String jsonString = jsonBody.toString();
            system.debug('This is the json: ' + jsonString);

            //Initialize wrapper class for response
            request respClass = new request();
            //Deserialize json in body to wrapper
            respClass = (request) JSON.deserialize(jsonString,request.class);  
        //SBQQ__Quote__c
        List<SBQQ__Quote__c> quote = [SELECT Id
                                FROM SBQQ__Quote__c 
                                WHERE OrderNumber__c = :respClass.OrderNo limit 1];
                                //Check if a Quote was foind with the order no
                                if(quote.size() > 0){
                                    //Update Contract Link and set response 
                                    ///Sample 
                                    //ContractLink="https://somedomain.com/someplace"
                                    SBQQ__Quote__c updateQuote = new SBQQ__Quote__c();
                                    updateQuote.Id = quote[0].Id;
                                    update updateQuote;
                                    //Set Response 
                                    resp.ActualPricingDate = datetime.now();
                                }
                                else{
                                    res.statusCode = 400;
                                    resp.ErrorMessage = 'A quote with that order number was not found';
                                }
        }//end if field validation found an error


            
            catch(Exception e){
                res.statusCode = 400;
                resp.ErrorMessage = 'Sorry! ᕕ( ᐛ )ᕗ The json syntax is incorrect. Please verify and try again. Show the following to an administrator: ' + e;
            }
        

        return resp;
    }
/*
*---------------------------------------------------------------------------------------------------------------------------------------------------
                                                                WRAPPER CLASSES
*---------------------------------------------------------------------------------------------------------------------------------------------------
*/


/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
    *   Wrapper Class to Handle request received 
    */    
    //Request Recieved in post 
    global class request {
        
        public String OrderNo {get;set;}
        public String ContractLink {get;set;}
      

        
    }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
    *   Wrapper Class to Handle Response 
    */
    global class response {
        public String                   ErrorMessage {get;set;}
        public    Datetime               ActualPricingDate          {get;set;}
        public    Boolean                AdjustmentInvoicePending {get;set;}
        public    Boolean                AllAddressesVerified {get;set;}
        public    Integer                ApprovalCycle {get;set;}
        public    Datetime               AuthorizationExpirationDate {get;set;}
        public    String                 BillToID {get;set;} 
        public    String                 BillToKey {get;set;}
        public    String                 BuyerOrganizationCode {get;set;}
        public    String                 CarrierAccountNo {get;set;}
        public    String                 CarrierServiceCode {get;set;}//No Type Set 
        public    String                 ChainType {get;set;}// No Type Setp
        public    Boolean                ChargeActualFreightFlag {get;set;}
        public    Integer                ComplimentaryGiftBoxQty {get;set;}
        public    String                 CreatedAtNode {get;set;} //No Type set
        public    String                 Createprogid {get;set;}
        public    Datetime               Createts {get;set;}
        public    String                 Createuserid {get;set;}
        public    String                 CustCustPONo {get;set;}//No Type Set
        public    Double                 CustomerAgeuble {get;set;}
        public    String                 CustomerContactID {get;set;}
        public    String                 CustomerEMailID {get;set;}
        public    String                 CustomerFirstName {get;set;}
        public    String                 CustomerLastName {get;set;}
        public    String                 CustomerPONo {get;set;}
        public    String                 CustomerPhoneNo {get;set;}
        public    String                 CustomerZipCode {get;set;}
        public    String                 DeliveryCode {get;set;}//No Type set
        public    String                 Division {get;set;}//No Type set
        public    Boolean                DoNotConsolidate {get;set;}
        public    String                 DocumentType {get;set;}
        public    Boolean                DraftOrderFlag {get;set;}
        public    String                 EnteredBy {get;set;}
        public    String                 EnterpriseCode {get;set;}
        public    String                 EntryType {get;set;}
        public    Datetime               ExpirationDate {get;set;}
        public    String                 FreightTerms {get;set;}//No Type set
        public    Boolean                HasDerivedChild {get;set;}
        public    Boolean                HasDerivedParent {get;set;}
        public    Boolean                HoldFlag {get;set;}
        public    String                 HoldReasonCode {get;set;}//No Type set
        public    Boolean                InternalApp {get;set;}
        public    Boolean                InvoiceComplete {get;set;}
        public    Integer                Lockid {get;set;}
        public    String                 Modifyprogid {get;set;}
        public    Datetime               Modifyts {get;set;}
        public    String                 Modifyuserid {get;set;}
        public    Datetime               NextAlertTs {get;set;}
        public    Integer                NoOfAuthStrikes {get;set;}
        public    Boolean                NotifyAfterShipmentFlag {get;set;}
        public    String                 OpportunityKey {get;set;}
        public    Boolean                OrderComplete {get;set;}
        public    Datetime               OrderDate {get;set;}
        public    String                 OrderHeaderKey {get;set;}
        public    String                 OrderName {get;set;}
        public    String                 OrderNo {get;set;}
        public    String                 OrderType {get;set;}//No Type set
        public    Double                 OriginalTaxDouble {get;set;}
        public    Double                 OriginalTotalAmountDouble {get;set;}
        public    Double                 OtherChargesDouble {get;set;}
        public    Boolean                Override_Z {get;set;}
        public    String                 PaymentStatus {get;set;}
        public    Double                 PendingTransferInDouble {get;set;}
        public    String                 PersonalizeCode {get;set;}//No Type set
        public    String                 PriceOrder {get;set;}//No Type set
        public    String                 PriceProgramName {get;set;}//No Type set
        public    String                 PriorityCode {get;set;}//No Type set
        public    Integer                PriorityNumber {get;set;}
        public    String                 PropagateCancellations {get;set;}//No Type set
        public    String                 Purpose {get;set;}//No Type set
        public    String                 ReserveInventoryFlag {get;set;}//No Type set
        public    String                 ReturnByGiftRecipient {get;set;}//No Type set
        public    String                 SCAC {get;set;}//No Type set
        public    Boolean                SaleVoided {get;set;}
        public    String                 SearchCriteria1 {get;set;}//No Type set
        public    String                 SearchCriteria2 {get;set;}//No Type set
        public    String                 SellerOrganizationCode {get;set;}
        public    String                 ShipToID {get;set;}
        public    String                 ShipToKey {get;set;}
        public    String                 SoldToKey {get;set;}
        public    String                 SourceIPAddress {get;set;}//No Type set
        public    Boolean                TaxExemptFlag {get;set;}
        public    String                 TaxExemptionCertificate {get;set;}//No Type set
        public    String                 TaxJurisdiction {get;set;}//No Type set
        public    String                 TaxPayerId {get;set;}//No Type set
        public    String                 TermsCode {get;set;}//No Type set
        public    Double                 TotalAdjustmentAmountDouble {get;set;}
        public    Boolean                isHistory {get;set;}
        public    Extn                   Extn {get;set;}
 
    }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    /*
    *   Wrapper Class to Handle Response 
    */
    global class Extn{
        public    String      ExtnAdditionalAttributes {get;set;}
        public    String      ExtnAdditionalInformation {get;set;}
        public    String      ExtnContractLink {get;set;}
        public    String      ExtnHazardAnswers {get;set;}
        public    Double      ExtnPaymentDepositDouble {get;set;}
        public    String      ExtnPaymentDepositBankAccountNumber {get;set;}
        public    String      ExtnPaymentDepositBankAccountType {get;set;}
        public    String      ExtnPaymentDepositBankName {get;set;}
        public    String      ExtnPaymentDepositBankRoute {get;set;}
        public    String      ExtnPaymentDepositPaymentMethod {get;set;}
        public    Boolean     ExtnPaymentRecurringSame      {get;set;}          


    }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
}