/**
 * @description This class will handle all callouts (and responses) to the 'CM8' Content Management external system for retrieving
 * documents that are related to a specific Quote or Order.
 *
 *
 * Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           July 27, 2021           Creating initial version
    1.1         Matt Parrella           Coastal Cloud           July 28, 2021           Adding response handling
 */
public with sharing class ADTContentMgmtAPI {

    /*
     *  WRAPPER CLASSES: START
     */

    /**
     * @description ResponseWrapper
     *
     * Wrapper class that will contain information about the success/failure of the callout and will contain any
     * error messages (if applicable), as well as payload data (documents, if applicable)
     */

    public class ResponseWrapper{
        @AuraEnabled public Boolean isSuccess;
        @AuraEnabled public ErrorMessageResponse[] messages; // will only be populated with pertinent data when isSuccess = false
        @AuraEnabled public QuoteDocumentWrapper[] payload; // will only be populated with data when isSuccess = true
        @AuraEnabled public String errorMsg; // will only be populated with pertinent data when isSuccess = false
        @AuraEnabled public String docContents; // used to store Document Contents from '/Documents/Content' callout response
        @AuraEnabled public String docName; // used to display document link in front end
        public ResponseWrapper(){
            this.isSuccess = true;
            this.messages = new List<ErrorMessageResponse>(); // Returned error message(s), if any
            this.payload = new List<QuoteDocumentWrapper>(); // Returned list of Document links, if any
            this.errorMsg = '';
            this.docContents = '';
            this.docName = '';
        }
        public ResponseWrapper(Boolean success, String errorDetails){
            this.isSuccess = success;
            this.messages = null;
            this.docContents = '';
            this.payload = null;
            this.errorMsg = errorDetails;
            this.docName = '';
        }
    }

    /**
     * @description DocumentContentWrapper
     *
     * Wrapper class to parse the returned document content from CM8
     *
     */
    public class DocumentContentWrapper {
        @AuraEnabled public String correlationId;
        @AuraEnabled public String tracingId;
        @AuraEnabled public String content;
        public DocumentContentWrapper(){
            this.correlationId = '';
            this.tracingId = '';
            this.content = '';
        }
    }

    /**
     * @description QuoteDocumentWrapper
     *
     *  Wrapper class to parse the returned documents from CM8
     */

    public class QuoteDocumentWrapper {
       @AuraEnabled
       public String orderNumber;
       @AuraEnabled
       public String MIMEType;
       @AuraEnabled
       public String documentType;
       @AuraEnabled
       public String importDate;
       @AuraEnabled
       public String accountType;
       @AuraEnabled
       public Integer docSize;
       @AuraEnabled
       public Integer siteNumber;
       @AuraEnabled
       public String docURI;
       @AuraEnabled
       public Integer customerNumber;
       @AuraEnabled
       public String customerName;
       @AuraEnabled
       public Integer jobNumber;
       @AuraEnabled
       public String uID;
    }

    /**
     * @description ErrorMessageResponse
     *
     *  Wrapper class to parse the 'Messages' array returned with a non-200 response code
     */
    public class ErrorMessageResponse {
        @AuraEnabled public String type;
        @AuraEnabled public String severity;
        @AuraEnabled public String reasonCode;
        @AuraEnabled public String context;
        @AuraEnabled public String message;
    }

    /*
     *  WRAPPER CLASSES: STOP
     */


    /**
     *  @description This method will perform an HTTP GET callout to the Content Management System Mulesoft API in order to obtain
     *  a list of documents that are related to a specific Quote or Order
     *
     *  @param quoteId - The Salesforce 18-character record Id for the Quote that we are retrieving documents for
     *
     *  @return respWrapper - (ResponseWrapper) The representation of the callout response, within a 'ResponseWrapper' object
    */
    @AuraEnabled
    public static ResponseWrapper getDocuments(String quoteId) {
        if (quoteId == '' || quoteId == null){
            return new ResponseWrapper(false, 'Passed-in \'quoteId\' value was null!');
        }

        // Attempt to Retrieve the credentials from the Custom Metadata record
        Mulesoft_API_Settings__mdt msConfig;
        try {
            //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
            Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
            if(environment)
            {
                msConfig = [SELECT Id,
                Client_Id__c,
                Client_Secret__c,
                CM8_getContent_Endpoint__c,
                CM8_getDocuments_Endpoint__c,
                Endpoint_base_url__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
            }
            else
            {
                msConfig = [SELECT Id,
                Client_Id__c,
                Client_Secret__c,
                CM8_getContent_Endpoint__c,
                CM8_getDocuments_Endpoint__c,
                Endpoint_base_url__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
            }
        }
        
        Catch (QueryException ex) {
            System.debug('Could not retrieve or access \'Mulesoft API Settings\' Custom Metadata!');
            return new ResponseWrapper(false, 'Could not retrieve or access \'Mulesoft API Settings\' Custom Metadata! Check permissions.');
        }

        // Attempt to get record Data
        SBQQ__Quote__c quoteRecord;
        try {
            System.debug('quoteId: ' + quoteId);
            quoteRecord = [ SELECT  OMS_Order_No__c,
                                    SBQQ__Account__r.MMBCustomerNumber__c,
                                    SBQQ__Account__r.MMBSiteNumber__c
                            FROM    SBQQ__Quote__c
                            WHERE   Id =: quoteId
                            LIMIT   1];
        }
        Catch(QueryException ex){
            System.debug('Could not retrieve or access the Quote record!');
            return new ResponseWrapper(false, 'Could not retrieve or access the Quote record.');
        }


        // Use Custom Metadata information to setup callout
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        // Build the Auth parameter value
        String username = msConfig.client_id__c;
        String password = msConfig.client_secret__c;

        // Blob it!
        Blob headerValue = Blob.valueOf(username + ':' + password);

        // Encode it
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);

        // Build 'tracing_id' value
        String tracingId = generateTracingId();
        req.setHeader('tracing_id', tracingId);

        // Set HTTP Method
        req.setMethod('GET');

        // Generate the Query Parameters for the endpoint
        String endpointParams = generateURLParams(quoteRecord, null);

        // Set the Endpoint with the added Query Parameters
        req.setEndpoint(msConfig.Endpoint_base_url__c + msConfig.CM8_getDocuments_Endpoint__c + endpointParams);

        // Execute the callout
        HttpResponse resp = new HttpResponse();
        resp = http.send(req);

        // Process the response into a wrapper class for ease of passing to front end (for potential LWC)
        ResponseWrapper respWrapper = handleGetDocumentsResponse(resp, true);
        return respWrapper;
    }

    /**
     * @description This method will perform an HTTP 'GET' callout to the '/Documents/Content' CM8 API Endpoint to retrieve
     * the contents of a specific document, whose URI will be passed-in as a parameter to this method.
     *
     * @param docURI - (String) The URI of the specific document we are attempting to retrieve the content for     *
     *
     * @return respWrapper - (ResponseWrapper) A representation of the success/failure of the callout that contains error messages and data (where applicable)
     */
    @AuraEnabled
    public static ResponseWrapper getDocumentContent(String docURI){
        ResponseWrapper respWrapper = new ResponseWrapper();
        // Attempt to Retrieve the credentials from the Custom Metadata record
        Mulesoft_API_Settings__mdt msConfig;
        try {
            //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
            Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
            if(environment)
            {
                msConfig = [SELECT Id,
                        Client_Id__c,
                        Client_Secret__c,
                        CM8_getContent_Endpoint__c,
                        CM8_getDocuments_Endpoint__c,
                        Endpoint_base_url__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
            }
            else
            {
                msConfig = [SELECT Id,
                        Client_Id__c,
                        Client_Secret__c,
                        CM8_getContent_Endpoint__c,
                        CM8_getDocuments_Endpoint__c,
                        Endpoint_base_url__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
            }
        }

        Catch (QueryException ex) {
            System.debug('Could not retrieve or access \'Mulesoft API Settings\' Custom Metadata!');
            return new ResponseWrapper(false, 'Could not retrieve or access \'Mulesoft API Settings\' Custom Metadata! Check permissions.');
        }

        Http http = new Http();
        HttpRequest req = new HttpRequest();

        // Use Custom Metadata information to setup callout
        // Build the Auth parameter value
        String username = msConfig.client_id__c;
        String password = msConfig.client_secret__c;

        // Blob it (convert to binary)!
        Blob headerValue = Blob.valueOf(username + ':' + password);

        // Encode it (Base64)
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);

        // Build 'tracing_id' value
        String tracingId = generateTracingId();
        req.setHeader('tracing_id', tracingId);
        req.setMethod('GET');

        // Build the URL query parameters
        String endpointParams = generateURLParams(null, docURI);

        // Construct the full endpoint
        String finalEndpoint = msConfig.Endpoint_base_url__c + msConfig.CM8_getContent_Endpoint__c + endpointParams;
        req.setEndpoint(finalEndpoint);

        // Execute the callout
        HttpResponse resp = new HttpResponse();
        resp = http.send(req);

        respWrapper = handleGetDocumentsResponse(resp, false);

        return respWrapper;
    }

    /**
     *  @description This helper method will construct the additional URL attributes that are appended to the endpoint for this callout
     *
     *  Example: /documents?customerNumber=xxx&orderNumber=www&siteNumber=X
     *
     *  @param quoteRecord (SBQQ__Quote__c) The Quote record for which we are trying to retrieve documents for
     *  @param docURI (String) The URI value to retrieve the contents of a specific document. If this value is blank or null,
     *                         the URL parameters for getting a list of documents is used instead
     *
     * @return urlParams - The URL string with added parameters
    */
    private static String generateURLParams(SBQQ__Quote__c quoteRecord, String docURI){
        // TODO: Will need 'customerNumber','orderNumber','siteNumber','documentType' parameter data sources
        String urlParams = '';

        // Dynamically build the query parameters url string: Start
        if (docURI != null && docURI != ''){
            // DocURI is NOT null, parameters for '/Documents/Content' endpoint will be dynamically built and remaining parameters will be omitted
            urlParams += '?docURI=' + docURI;
        } else {
            // DocURI is null, parameters for '/Documents' endpoint will be dynamically built
            if (quoteRecord.SBQQ__Account__r?.MMBCustomerNumber__c != null && quoteRecord.SBQQ__Account__r?.MMBCustomerNumber__c != ''){
                if (urlParams.indexOf('?') == -1){
                    urlParams += '?customerNumber=' + quoteRecord.SBQQ__Account__r.MMBCustomerNumber__c;
                } else {
                    urlParams += '&customerNumber=' + quoteRecord.SBQQ__Account__r.MMBCustomerNumber__c;
                }
            }
            if (quoteRecord.SBQQ__Account__r?.MMBSiteNumber__c != null && quoteRecord.SBQQ__Account__r?.MMBSiteNumber__c != ''){
                if (urlParams.indexOf('?') == -1){
                    urlParams += '?siteNumber=' + quoteRecord.SBQQ__Account__r?.MMBSiteNumber__c;
                } else {
                    urlParams += '&siteNumber=' + quoteRecord.SBQQ__Account__r?.MMBSiteNumber__c;
                }
            }
            if (quoteRecord.OMS_Order_No__c != null && quoteRecord.OMS_Order_No__c != ''){
                if (urlParams.indexOf('?') == -1){
                    urlParams += '?orderNumber=' + quoteRecord.OMS_Order_No__c;
                } else {
                    urlParams += '&orderNumber=' + quoteRecord.OMS_Order_No__c;
                }
            }
        }
        // Dynamically build the query parameters url string: Stop

        return urlParams;
    }

    /**
     *  @description This helper method will take in the response from the callout to CM8 (/Documents) and return a ResponseWrapper object
     *  that will contain information regarding:
     *                    - Success/Failure of callout (Response code of 200 or otherwise)
     *                   - Any error messages accompanying a NON-200 response code
     *                   - Any payload returned, accompanying a 200 response code
     *
     * @param resp - (HttpResponse) The response from the CM8 system
     * @param handleGetDocumentsResp - (Boolean) If TRUE && Response Code = 200, will parse the list of documents returned from GetDocuments endpoint
     *
     * @return respWrapper - (ResponseWrapper) Response data represented via a ResponseWrapper object, which can be passed to front-end LWC
    */
    public static ResponseWrapper handleGetDocumentsResponse(HttpResponse resp, Boolean handleGetDocumentsResp){
        // Create a new instance of the ResponseWrapper object (we will be returning this object)
        ResponseWrapper respWrapper = new ResponseWrapper();

        // Get the response Status Code (200, 401, 404, etc..)
        Integer respCode = resp.getStatusCode();

        switch on respCode {
            // Success!
            when 200 {
                if (handleGetDocumentsResp) {
                    // Will handle the response from '/Documents' endpoint
                    // Extract all the document objects returned and place them into QuoteDocumentWrapper objects
                    JSONParser parser = JSON.createParser(resp.getBody());
                    while (parser.nextToken() != null) {
                        // Start capturing documents returned
                        if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                            while (parser.nextToken() != null) {
                                if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                                    QuoteDocumentWrapper quoteWrap = (QuoteDocumentWrapper) parser.readValueAs(QuoteDocumentWrapper.class);
                                    quoteWrap.uID = generateUID(); // Generate Unique ID for front-end processing
                                    respWrapper.payload.add(quoteWrap);
                                }
                            }
                        }
                    }
                    return respWrapper;
                } else {
                    // Will handle the response from '/Documents/Content' endpoint
                    DocumentContentWrapper docConWrap = (DocumentContentWrapper) JSON.deserialize(resp.getBody(), DocumentContentWrapper.class);
                    respWrapper.docContents = docConWrap.content;
                    return respWrapper;
                }
            }
            // Error occurred within ADT system (contains an 'ADT-xxx' reason code)
            when 400, 404, 500 {
                // Extract the context and message to display on the front-end
                JSONParser parser = JSON.createParser(resp.getBody());
                System.debug('response: ' + resp.getBody());
                respWrapper.isSuccess = false;
                respWrapper.errorMsg = 'Response Error(s): ';
                if (resp.getBody().indexOf('{') != 0){
                    // No JSON body
                    respWrapper.errorMsg += resp.getStatusCode() + ' Error: ' + resp.getStatus();
                } else {
                    while (parser.nextToken() != null) {
                        // Start capturing the message(s) contained within the returned 'Messages' array
                        if (parser.getCurrentToken() == JSONToken.START_ARRAY){
                            while (parser.nextToken() != null){
                                if (parser.getCurrentToken() == JSONToken.START_OBJECT){
                                    ErrorMessageResponse errResp = (ErrorMessageResponse) parser.readValueAs(ErrorMessageResponse.class);
                                    respWrapper.messages.add(errResp);
                                }
                            }

                        }
                    }
                    respWrapper = summarizeRespWrapperErrors(respWrapper);
                }
                return respWrapper;
            }
            when else {
                respWrapper.isSuccess = false;
                respWrapper.errorMsg = 'Unexpected return code: was NOT 200, 400, 404, or 500.';
                return respWrapper;
            }
        }
    }

    /**
     *  @description This helper method will take in a ResponseWrapper object that may or may not have a lot of 'ErrorMessageResponse' objects
     *  in it's 'messages' array. The purpose of this method is to iterate through all the objects in the 'messages' array and build
     *  a single Error String that will list all the error encountered and can be viewed on the front end by the user via Toast Message
     *  or simple gui error.
     *
     *  @param respWrapper - (ResponseWrapper) The ResponseWrapper object that needs to have it's 'errorMsg' string build out
     *
     *  @return newRespWrapper - (ResponseWrapper) A ResponseWrapper object with a summarized 'errorMsg' value
    */
    public static ResponseWrapper summarizeRespWrapperErrors(ResponseWrapper respWrapper){
        // Iterate over the 'messages' attribute of the passed-in ResponseWrapper and build the 'one-string' error string
        if (respWrapper.messages != null){
            for (ErrorMessageResponse emr : respWrapper.messages){
                respWrapper.errorMsg += emr.message + '; ';
            }
        } else {
            return respWrapper;
        }
        return respWrapper;
    }

    /**
     *  @description This helper method will build out the 'tracing_id' parameter that's used in the callouts. This is made available so that we can track
     *  a transaction event to a specific message and vice versa
     *
     *  @return tracingId - (String) The 'tracing_id' parameter value that will be used
    */
    public static String generateTracingId(){
        String rightNow = Datetime.now().format('MM-dd-yyyy-HH-mm-ss-sss');
        String tracingId = 'CC_SCPQ_' + rightNow.replaceAll('-',''); // Remove '-' characters to have 1 uninterrupted string representing datetime
        return tracingId;
    }

    /**
     * @description Small helper method to generate a 6-Digit Unique ID for each Document returned from CM8. This is used in the front-end
     * to uniquely identify each record that is displayed (in the background HTML/js logic).
     *
     * To achieve the UniqueID format:  (Number of milliseconds between 1/1/1970 and NOW) + (A randomly generated number)
     *
     * @return uniqueId - (String) A 6-Digit UniqueID value
    */
    @AuraEnabled
    public static String generateUID(){
        // Init variables
        Double seconds;
        Double randomNumber;
        String uniqueID = '';

        // Get number of milliseconds between 1/1/1970 and NOW
        Datetime rightNow = Datetime.now();
        seconds = rightNow.getTime();

        // Get Random Number
        randomNumber = Math.random() * 10;

        // Divide the two values to make unique and take the first 6 characters
        uniqueID = String.valueOf((seconds / randomNumber)).substring(0,7);

        // Remove the decimal
        uniqueID = uniqueID.replace('.','');

        return uniqueID;
    }

}