/**
 * This class will handle the generation, acceptance of, and transmission of outbound API calls to the Mulesoft PaymentTech endpoints.
 *
 * Mulesoft API: creditcard-exp-api
 * Endpoint(s): /uid/hpp
 *              /uid/{uId}
 */
/* Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           August 2, 2021          Creating initial version
    1.1         Matt Parrella           Coastal Cloud           October 15, 2021        Wiring req params to quote fields
    1.2         Matt Parrella           Coastal Cloud           January 12, 2022        Implementing IntegrationsHelper usage
    1.3         Matt Parrella           Coastal Cloud           January 19, 2022        Moving 'merchant_id' parameter to a class variable for easier maintenance
 */

public without sharing class ADTPaymentTechHPPAPI {

    /* Class Vars: START */
    /** @description String variable to hold the 'merchant_id' parameter value used in all the Paymentech callouts. Currently hard-coded to a specific value. */
    private static final String merchantId = '187910';
    /* Class Vars: STOP */

    /**
     * @description Helper method to retrieve the Mulesoft API Setting custom metadata record to be used in the callouts contained within this class.

     * @return An IntegrationsWrapper object containing all integration information required
     */
    private static IntegrationsHelper.IntegrationsWrapper retrieveIntegrationInfo(){
        return IntegrationsHelper.retrieveCreds();
    }

    /**
     *  This method will request a uID value from PaymentTech to initiate a Payment transaction and PaymentTech Profile Creation
     *
     * @param recordId (String) The 18-character Salesforce record Id tied to the Quote record
     * @param total_amt (Decimal) The amount to be authorized, passed in from the front-end
     *
     * @return paymentWrap - (PaymentResponseWrapper) A PaymentResponseWrapper object containing the uId (if successful), or an error message (if unsuccessful)
     */
    @AuraEnabled
    public static PaymentResponseWrapper generateHPPRequest(Id recordId, Decimal total_amt){
        PaymentResponseWrapper paymentWrap = new PaymentResponseWrapper();

        // MParrella | 1-12-2022 | Implementing use of IntegrationsHelper class to get callout information
        IntegrationsHelper.IntegrationsWrapper integrationsWrapper = retrieveIntegrationInfo();

        // Check if there was a failure obtaining the Custom MDT records
        if (!integrationsWrapper.hasData) {
            System.debug('An exception has been encountered while trying to obtain Mulesoft API Setting custom metadata records.');
            return new PaymentResponseWrapper(false, 'An exception has been encountered while trying to obtain Mulesoft API Setting custom metadata records.');
        }

        // Update the VF Page domain parameter, so it can be passed back to the front-end
        paymentWrap.orgVFDomain = integrationsWrapper.Org_VF_Domain;

        // Use Custom Metadata information to setup callout
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        // Build the Auth parameter value
        String username = integrationsWrapper.Credentials_SFCPQ_Mulesoft_ClientId;
        String password = integrationsWrapper.Credentials_SFCPQ_Mulesoft_ClientSecret;

        // Blob it!
        Blob headerValue = Blob.valueOf(username + ':' + password);

        // Encode it
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

        // Set encoded value to 'Authorization' header attribute
        req.setHeader('Authorization', authorizationHeader);

        // Set 'merchant_id' header attribute
        req.setHeader('merchant_id', merchantId);
        req.setHeader('tracing_id', IntegrationsHelper.generateTracingId());
        req.setHeader('Content-Type', 'application/json');

        // Set the endpoint
        req.setEndPoint(integrationsWrapper.Endpoint_Mulesoft_Base_URL + integrationsWrapper.paymentechInfo.Endpoint_getUID_HPP);
        System.debug('Endpoint url: ' + req.getEndpoint());
        req.setMethod('POST');

        String reqBody = generatePaymentRequest(recordId, total_amt);

        // Error checking the request body
        if(reqBody.indexOf('{') == -1){ // If we don't find any brackets, the message wasn't built and we have an error
            paymentWrap.isSuccess = false;
            if (reqBody.contains('ERROR:')) { // Contains a QueryException error
                paymentWrap.errorMsg = reqBody;
            } else { // Contains a field api name that is missing a value
                paymentWrap.errorMsg = 'Missing required field value: ' + reqBody;
            }
            return paymentWrap;
        }
        System.debug('hpp req: ' + reqBody);

        req.setBody(reqBody);
        HttpResponse resp = new HttpResponse();
        try {
            resp = http.send(req);
        }
        Catch(Exception e){
            paymentWrap.isSuccess = false;
            paymentWrap.errorMsg = e.getMessage();
            return paymentWrap;
        }

        // Parse the response and update the PaymentResponseWrapper accordingly
        paymentWrap = handlePaymentResponse(resp, false, recordId);

        // If request was successful, build the url for the Paymentech portal (with UID)
        if (resp.getStatusCode() == 200){
            paymentWrap.paymentTechURL = integrationsWrapper.paymentechInfo.Endpoint_HPP_Base_URL;
        } else {
            paymentWrap.isSuccess = false;
            paymentWrap.errorMsg = 'Failed to retrieve uID.';
        }

        return paymentWrap;
    }

    /**
     * This helper method will build the 'postHPPPaymentRequest' request body that is used to obtain a unique
     * ID (uID) from PaymentTech's Hosted Pay Page.
     *
     * @param recordId - (String) The Id of the Quote record from which we will be pulling data to populate the request body with
     * @param total_amt (String) Amount to be authorized, passed in from the parent LWC
     *
     * @return requestBody - (String) The request body (in whole) containing all the necessary data and parameters
    */
    @AuraEnabled
    public static String generatePaymentRequest(String recordId, Decimal total_amt){
        String retURL =  Label.Domain_VFPAGE_url + '/apex/cc_paymentechLandingPage';
        SBQQ__Quote__c quoteRecord;
        try {
            quoteRecord = [ SELECT
                    SFCPQ_Global_ID__c,
                    SBQQ__BillingCity__c,
                    SBQQ__BillingState__c,
                    SBQQ__BillingStreet__c,
                    SBQQ__BillingCountry__c,
                    SBQQ__BillingPostalCode__c,
                    SBQQ__Account__r.Billing_First_Name__c,
                    SBQQ__Account__r.Billing_Last_Name__c,
                    SBQQ__Account__r.Name
            FROM SBQQ__Quote__c
            WHERE Id =: recordId
            LIMIT 1];
        }
        catch(QueryException ex){
            return 'ERROR: Could not locate or access the Quote record. Details: ' + ex.getMessage();
        }

        // Build Request Message Body: START
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('contentTemplateUrl', 'https://cpq.adt.com/sfs/cptemplate/gatewayPayment.html');
        gen.writeStringField('returnUrl', retURL);
        gen.writeStringField('cancelUrl', retURL);
        gen.writeStringField('sessionId', IntegrationsHelper.generateUID());
        gen.writeStringField('paymentType', 'Credit_Card');
        gen.writeFieldName('allowedCCTypes');
        // Allowed CC Types array: Start
        gen.writeStartArray();
        gen.writeString('Visa');
        gen.writeString('MasterCard');
        gen.writeString('American Express');
        gen.writeString('Discover');
        gen.writeEndArray();
        // Allowed CC types array: Stop
        gen.writeStringField('transType', 'auth_only');
        gen.writeStringField('hostedTokenize', 'store_authorize');
        if (total_amt == null) { return 'Total Amount';}
        gen.writeNumberField('totalAmount', total_amt * 100);
        gen.writeStringField('orderId', quoteRecord.SFCPQ_Global_ID__c != null ? quoteRecord.SFCPQ_Global_ID__c : '');
        gen.writeStringField('required', 'minimum');
        if (quoteRecord.SBQQ__Account__c == null) { return 'MISSING: SBQQ__Account__c'; }
        String[] accountNameArray = quoteRecord.SBQQ__Account__r?.Name.split(' ');
        gen.writeStringField('customerFirstName', accountNameArray[0]);
        gen.writeStringField('customerLastName', accountNameArray[1]);
        gen.writeFieldName('billingAddress');
        // Billing Address: Start
        gen.writeStartObject();
        if (quoteRecord.SBQQ__BillingStreet__c == null) { return 'SBQQ__BillingStreet__c';}
        gen.writeStringField('addressLine1', quoteRecord.SBQQ__BillingStreet__c);
        if (quoteRecord.SBQQ__BillingCity__c == null) { return 'SBQQ__BillingCity__c';}
        gen.writeStringField('city', quoteRecord.SBQQ__BillingCity__c);
        if (quoteRecord.SBQQ__BillingState__c == null) { return 'SBQQ__BillingState__c';}
        gen.writeStringField('state', quoteRecord.SBQQ__BillingState__C);
        if (quoteRecord.SBQQ__BillingPostalCode__c == null) { return 'SBQQ__BillingPostalCode__c';}
        gen.writeStringField('postalCode', quoteRecord.SBQQ__BillingPostalCode__c);
        if (quoteRecord.SBQQ__BillingCountry__c == null) { return 'SBQQ__BillingCountry__c';}
        gen.writeStringField('country', quoteRecord.SBQQ__BillingCountry__c);
        gen.writeEndObject();
        // Billing Address: Stop
        if (quoteRecord.SBQQ__Account__c == null) { return 'AccountId';}
        gen.writeStringField('companyName', '');
        gen.writeStringField('mitMsgType', 'CSTO');
        gen.writeStringField('mitStoredCredentialInd', 'N');
        gen.writeEndObject();
        // Build Request Message Body: STOP

        return gen.getAsString();
    }

    /**
     * This method will construct an endpoint url appended with the uID obtained from the response to the callout made in the 'generateUID()' method.
     * In return, we will get all the data regarding the transaction (namely, the 'CustomerRefNo')
     *
     * @param uID - (String) The uID obtained from the initial callout
     * @param recordId - (String) The 18-character Salesforce record Id associated to the Quote record
     *
     * @return paymentWrapper - (PaymentResponseWrapper) An object containing all the data regarding the transaction tied to the uID (if successful), or an error message (if unsuccessful)
     */
    @AuraEnabled
    public static PaymentResponseWrapper getTransactionData(String uID, Id recordId){
        PaymentResponseWrapper paymentWrap = new PaymentResponseWrapper();

        // MParrella | 1-12-2022 | Implementing use of IntegrationsHelper class to get callout information
        IntegrationsHelper.IntegrationsWrapper integrationsWrapper = retrieveIntegrationInfo();

        // Check if there was a failure obtaining the Custom MDT records
        if (!integrationsWrapper.hasData) {
            System.debug('An exception has been encountered while trying to obtain Mulesoft API Setting custom metadata records.');
            return new PaymentResponseWrapper(false, 'An exception has been encountered while trying to obtain Mulesoft API Setting custom metadata records.');
        }

        // Use Custom Metadata information to setup callout
        Http http = new Http();
        HttpRequest req = new HttpRequest();

        // Build the Auth parameter value
        String username = integrationsWrapper.Credentials_SFCPQ_Mulesoft_ClientId;
        String password = integrationsWrapper.Credentials_SFCPQ_Mulesoft_ClientSecret;

        // Blob it!
        Blob headerValue = Blob.valueOf(username + ':' + password);

        // Encode it
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);

        // Set encoded value to 'Authorization' header attribute
        req.setHeader('Authorization', authorizationHeader);

        // Set merchant_id header attribute
        req.setHeader('merchant_id', merchantId); //TODO: Hard coding for now

        // Get Endpoint base url
        String baseUrl = integrationsWrapper.Endpoint_Mulesoft_Base_URL;

        // Get Endpoint path
        String endpointDirectory = integrationsWrapper.paymentechInfo.Endpoint_getTransactionData;

        // Set Endpoint
        req.setEndpoint(baseUrl + endpointDirectory + uID);

        req.setMethod('GET');

        HttpResponse resp = new HttpResponse();
        resp = http.send(req);

        paymentWrap = handlePaymentResponse(resp, true, recordId);

        return paymentWrap;
    }

    /**
     *  This method will in an HttpResponse object and based on the return value will parse out the response message to retrieve
     *  either the uID (if code = 200), or the related error message (if code = 404 || 500)
     *
     *  @param response - (HttpResponse) The HttpResponse object we need to parse (returned from the callout)
     *  @param parseGetTransactionResponse - (Boolean) If TRUE and Response Code = 200, will parse the 'getHostedPayment200Response' message
     *                                                 If FALSE and Response Code = 200, will parse the 'postHostedPayment200Response' message
     *                                                 If TRUE/FALSE and Response Code != 200, will process the 404 or 500 error messages
     *  @param recordId - (String) The 18-character Salesforce record Id associated to the Quote record
     *
     *  @return paymentWrap - (PaymentResponseWrapper) A PaymentResponseWrapper that contains either the error message(s) or uID returned from PaymentTech
    */
    public static PaymentResponseWrapper handlePaymentResponse(HttpResponse response, Boolean parseGetTransactionResponse, Id recordId){
        PaymentResponseWrapper paymentWrap = new PaymentResponseWrapper();
        Integer respCode = response.getStatusCode();

        System.debug('resp body: ' + response.getBody());

        switch on respCode {
            when 200 {
                // Check which response we're parsing (only difference is 200 response)
                if (parseGetTransactionResponse){
                    // Parsing the '/uid/{uId}' endpoint response (getHostedPayment200Response)
                    PaymentTransactionWrapper payTranWrap = (PaymentTransactionWrapper) JSON.deserialize( response.getBody(), PaymentTransactionWrapper.class);
                    paymentWrap.paymentTransactionWrap = payTranWrap;
                    //paymentWrap = savePaymentMethod(paymentWrap, recordId);
                    return paymentWrap;
                } else {
                    // Parsing the '/uid/hpp' endpoint response (postHostedPayment200Response)
                    Map<String,Object> respMap = (Map<String,Object>) JSON.deserializeUntyped(response.getBody());
                    paymentWrap.uID = String.valueOf(respMap.get('uId'));
                    return paymentWrap;
                }
            }
            when 400, 404, 500 {
                // Extract the context and message to display on the front-end
                JSONParser parser = JSON.createParser(response.getBody());
                System.debug('response: ' + response.getBody());
                paymentWrap.isSuccess = false;
                paymentWrap.errorMsg = 'Response Error(s): ';
                if (response.getBody().indexOf('{') != 0){
                    // No JSON body
                    paymentWrap.errorMsg += response.getStatusCode() + ' Error: ' + response.getStatus();
                } else {
                    while (parser.nextToken() != null) {
                        // Start capturing the message(s) contained within the returned 'Messages' array
                        if (parser.getCurrentToken() == JSONToken.START_ARRAY){
                            while (parser.nextToken() != null){
                                if (parser.getCurrentToken() == JSONToken.START_OBJECT){
                                    ErrorMessageResponse errResp = (ErrorMessageResponse) parser.readValueAs(ErrorMessageResponse.class);
                                    paymentWrap.messages.add(errResp);
                                }
                            }

                        }
                    }
                    paymentWrap = summarizeRespWrapperErrors(paymentWrap);
                }
                return paymentWrap;
            }
            when else {
                paymentWrap.isSuccess = false;
                paymentWrap.errorMsg = 'Unexpected response code: was not 200, 404, or 500.';
                return paymentWrap;
            }
        }
    }

    /**
     * @description This method will take in CC or ACH data returned from a successful transaction authorization and create PaymentMethod records for each instrument
     *
     * @param recordId - The Salesforce Record ID of the CPQ Quote Record
     * @param ccJSONString - Stringified JSON payload from the reviewQuotePaymentLWC lwc containing CC Authorization data
     * @param achJSONString - Stringified JSON payload from the reviewQuotePaymentLWC lwc containing ACH Authorization data
     *
     * @return (PaymentResponseWrapper) A wrapper object representing the success or failure of the operation
     */

    @AuraEnabled
    public static PaymentResponseWrapper savePaymentMethod(String recordId, String ccJSONString, String achJSONString) {
        System.debug('cc info: ' + ccJSONString);
        System.debug('ach info: ' + achJSONString);
        PaymentResponseWrapper returnWrap = new PaymentResponseWrapper();
        SBQQ__Quote__c quoteRecord;
        try {
            quoteRecord = [
                    SELECT  Id,
                            Due_at_Sale__c,
                            Payment_Interval__c,
                            EasyPay_Enrollment__c,
                            SBQQ__PaymentTerms__c,
                            Installments__c,
                            TXN_Bank_Credit_Union_Name__c,
                            TXN_Bank_Account_Number__c,
                            TXN_Name_on_Account__c,
                            TXN_Card_Type__c,
                            TXN_Name_on_CC__c,
                            TXN_Last_4_Used__c,
                            TXN_Exp_Date__c,
                            Has_CC_Info__c,
                            Has_ACH_Info__c,
                            SBQQ__Account__c,
                            SBQQ__Account__r.FirstName__c,
                            SBQQ__Account__r.LastName__c,
                            SBQQ__Account__r.Billing_Address_1__c,
                            SBQQ__Account__r.Billing_Address_2__c,
                            SBQQ__Account__r.Billing_City__c,
                            SBQQ__Account__r.Billing_State_Province__c,
                            SBQQ__Account__r.Billing_Postal_Code__c
                    FROM SBQQ__Quote__c
                    WHERE Id = :recordId
                    LIMIT 1
            ];
        }
        catch(QueryException ex) {
            returnWrap.isSuccess = false;
            returnWrap.errorMsg = 'Error locating Quote record. Details: ' + ex.getMessage();
            return returnWrap;
        }

        // Check if we're storing ACH, CC, or both
        List<blng__PaymentMethod__c> paymentMethods = new List<blng__PaymentMethod__c>();
        /* Process both CC & ACH Data: START */
//        if (achJSONString != '{}' && achJSONString != null && ccJSONString != '{}' && ccJSONString != null){
//            // Extract the ACH Data
//            achData achInfo = (achData) JSON.deserializeStrict(achJSONString, achData.class);
//
//            // Extract the CC Data
//            PaymentTransactionWrapper ccInfo = (PaymentTransactionWrapper) JSON.deserializeStrict(ccJSONString, PaymentTransactionWrapper.class);
//
//            // Create Payment Method records
//            for (Integer i = 0; i < 2; i++) {
//                // Create the CC Payment Method record
//                if (i == 0){
//                    paymentMethods.add(new blng__PaymentMethod__c(
//                            blng__BillingFirstName__c = quoteRecord.SBQQ__Account__r.FirstName__c,
//                            blng__BillingLastName__c = quoteRecord.SBQQ__Account__r.LastName__c,
//                            blng__Account__c = quoteRecord.SBQQ__Account__c,
//                            blng__PaymentType__c = 'Credit Card',
//                            blng__CardType__c = ccInfo.type.replace('+', ' '),
//                            blng__Nameoncard__c = ccInfo.customerName,
//                            blng__CardExpirationMonth__c = ccInfo.exp.substring(0,2),
//                            blng__CardExpirationYear__c = '20' + ccInfo.exp.substring(2,4),
//                            blng__PaymentGatewayToken__c = ccInfo.customerRefNum,
//                            blng__BillingStreet__c = quoteRecord.SBQQ__Account__r.Billing_Address_1__c,
//                            blng__BillingCity__c = quoteRecord.SBQQ__Account__r.Billing_City__c,
//                            blng__BillingStateProvince__c = quoteRecord.SBQQ__Account__r.Billing_State_Province__c,
//                            blng__CardNumber__c = ccInfo.mPAN.substring(ccInfo.mPAN.length()-4),
//                            blng__BillingZipPostal__c = ccInfo.customerPostalCode,
//                            Zip_Code__c = ccInfo.customerPostalCode,
//                            Quote__c = quoteRecord.Id
//                    ));
//                } else {
//                    // Create the ACH Payment Method record
//                    paymentMethods.add(new blng__PaymentMethod__c(
//                            blng__BillingFirstName__c = quoteRecord.SBQQ__Account__r.FirstName__c,
//                            blng__BillingLastName__c = quoteRecord.SBQQ__Account__r.LastName__c,
//                            blng__Account__c = quoteRecord.SBQQ__Account__c,
//                            blng__PaymentType__c = 'ACH',
//                            Bank_Account_Last_4__c = achInfo.fieldData.accountNumber.substring(achInfo.fieldData.accountNumber.length() - 4),
//                            blng__BankAccountName__c = achInfo.fieldData.accountName,
//                            blng__BankName__c = achInfo.fieldData.bankName,
//                            blng__BankAccountNumber__c = achInfo.fieldData.accountNumber,
//                            blng__BankAccountType__c = achInfo.fieldData.achType,
//                            blng__BankRoutingCode__c = achInfo.fieldData.routingNumber,
//                            blng__BillingStreet__c = quoteRecord.SBQQ__Account__r.Billing_Address_1__c,
//                            blng__BillingCity__c = quoteRecord.SBQQ__Account__r.Billing_City__c,
//                            blng__BillingStateProvince__c = quoteRecord.SBQQ__Account__r.Billing_State_Province__c,
//                            blng__BillingZipPostal__c = quoteRecord.SBQQ__Account__r.Billing_Postal_Code__c,
//                            Zip_Code__c = quoteRecord.SBQQ__Account__r.Billing_Postal_Code__c,
//                            Quote__c = quoteRecord.Id
//                    ));
//                }
//            }
//        }
//
//        /* Process ACH Only: START */
//        else if (achJSONString != '{}' && achJSONString != null){
//            // Extract the ACH Data
//            achData achInfo = (achData) JSON.deserializeStrict(achJSONString, achData.class);
//
//            paymentMethods.add(new blng__PaymentMethod__c(
//                    blng__BillingFirstName__c = quoteRecord.SBQQ__Account__r.FirstName__c,
//                    blng__BillingLastName__c = quoteRecord.SBQQ__Account__r.LastName__c,
//                    blng__Account__c = quoteRecord.SBQQ__Account__c,
//                    blng__PaymentType__c = 'ACH',
//                    Bank_Account_Last_4__c = achInfo.fieldData.accountNumber.substring(achInfo.fieldData.accountNumber.length() - 4),
//                    blng__BankAccountName__c = achInfo.fieldData.accountName,
//                    blng__BankAccountNumber__c = achInfo.fieldData.accountNumber,
//                    blng__BankAccountType__c = achInfo.fieldData.achType,
//                    blng__BankName__c = achInfo.fieldData.bankName,
//                    blng__BankRoutingCode__c = achInfo.fieldData.routingNumber,
//                    blng__BillingStreet__c = quoteRecord.SBQQ__Account__r.Billing_Address_1__c,
//                    blng__BillingCity__c = quoteRecord.SBQQ__Account__r.Billing_City__c,
//                    blng__BillingStateProvince__c = quoteRecord.SBQQ__Account__r.Billing_State_Province__c,
//                    blng__BillingZipPostal__c = quoteRecord.SBQQ__Account__r.Billing_Postal_Code__c,
//                    Zip_Code__c = quoteRecord.SBQQ__Account__r.Billing_Postal_Code__c,
//                    Quote__c = quoteRecord.Id
//            ));
//        }
        /* Process ACH Only: STOP */

        /* Process CC Only: START */
        if (ccJSONString != '{}' && ccJSONString != null){
            // Extract the CC Data
            PaymentTransactionWrapper ccInfo = (PaymentTransactionWrapper) JSON.deserializeStrict(ccJSONString, PaymentTransactionWrapper.class);
            System.debug('ccInfo in back-end: ' + ccInfo);
            paymentMethods.add(new blng__PaymentMethod__c(
                    blng__BillingFirstName__c = quoteRecord.SBQQ__Account__r.FirstName__c,
                    blng__BillingLastName__c = quoteRecord.SBQQ__Account__r.LastName__c,
                    blng__Account__c = quoteRecord.SBQQ__Account__c,
                    blng__PaymentType__c = 'Credit Card',
                    blng__CardType__c = ccInfo.type.contains('+') ? ccInfo.type.replace('+', ' ') : ccInfo.Type,
                    blng__Nameoncard__c = ccInfo.customerName,
                    blng__CardExpirationMonth__c = ccInfo.exp.substring(0,2),
                    blng__CardExpirationYear__c = '20' + ccInfo.exp.substring(2,4),
                    blng__PaymentGatewayToken__c = ccInfo.customerRefNum,
                    blng__BillingStreet__c = quoteRecord.SBQQ__Account__r.Billing_Address_1__c,
                    blng__BillingCity__c = quoteRecord.SBQQ__Account__r.Billing_City__c,
                    blng__BillingStateProvince__c = quoteRecord.SBQQ__Account__r.Billing_State_Province__c,
                    blng__CardNumber__c = ccInfo.mPAN.substring(ccInfo.mPAN.length()-4),
                    blng__BillingZipPostal__c = ccInfo.customerPostalCode,
                    Zip_Code__c = ccInfo.customerPostalCode,
                    Quote__c = quoteRecord.Id
            ));
        }
        /* Process CC Only: STOP */
        try {
            insert paymentMethods;

            return returnWrap;
        }
        catch(DmlException ex){
            returnWrap.isSuccess = false;
            returnWrap.errorMsg = 'Unable to create PaymentMethod record(s). Authorization was successful, however. Database exception: ' + ex.getMessage();
            return returnWrap;
        }
        /* Process both CC & ACH Data: STOP */

    }

    /**
     *  @description This helper method will take in a ResponseWrapper object that may or may not have a lot of 'ErrorMessageResponse' objects
     *  in it's 'messages' array. The purpose of this method is to iterate through all the objects in the 'messages' array and build
     *  a single Error String that will list all the error encountered and can be viewed on the front end by the user via Toast Message
     *  or simple gui error.
     *
     *  @param respWrapper - (ResponseWrapper) The ResponseWrapper object that needs to have it's 'errorMsg' string build out
     *
     *  @return newRespWrapper - (ResponseWrapper) A ResponseWrapper object with a summarized 'errorMsg' value
    */
    public static PaymentResponseWrapper summarizeRespWrapperErrors(PaymentResponseWrapper respWrapper){
        // Iterate over the 'messages' attribute of the passed-in ResponseWrapper and build the 'one-string' error string
        if (respWrapper.messages != null){
            for (ErrorMessageResponse emr : respWrapper.messages){
                respWrapper.errorMsg += emr.message + '; ';
            }
        } else {
            return respWrapper;
        }
        return respWrapper;
    }


    /*  Wrapper Classes: START  */
    /**
     * @description Wrapper class to parse all responses and send the data (or error(s)) to the front-end
    */
    public class PaymentResponseWrapper {
        /** @description Boolean flag to determine if the callout was successful (TRUE -> Success, no errors; FALSE -> error(s) encountered, check 'errorMsg' parameter for details) */
        @AuraEnabled public Boolean isSuccess;

        /** @description String parameter to store the current org's Visualforce domain url */
        @AuraEnabled public String orgVFDomain;

        /** @description String parameter containing error details, if encountered. Will only be populated if 'isSuccess' is FALSE */
        @AuraEnabled public String errorMsg;

        /** @description ErrorMessageResponse array, containing error messages (if multiple) returned from Mulesoft if an endpoint error occurs */
        @AuraEnabled public ErrorMessageResponse[] messages;

        /** @description String parameter containing the uID value retrieved from Paymentech during the HPP Request callout */
        @AuraEnabled public String uID;

        /** @description PaymentTransactionWrapper object containing information returned from the getTransactionData() callout */
        @AuraEnabled public PaymentTransactionWrapper paymentTransactionWrap;

        /** @description String parameter to temporarily hold the Hosted Pay Page base URL, if the uID retrieval is successful */
        @AuraEnabled public String paymentTechURL;

        /** @description No-argument constructor for PaymentResponseWrapper object, defaults 'isSuccess' parameter to TRUE by default */
        public PaymentResponseWrapper(){
            this.isSuccess = true; // Defaulting to 'true'
            this.errorMsg = '';
            this.messages = new List<ErrorMessageResponse>();
            this.uID = '';
        }

        /** @description Constructor for PaymentResponseWrapper that takes in a Boolean parameter to set the 'isSuccess' parameter to, as well as a String parameter to set the 'errorMsg' parameter to */
        public PaymentResponseWrapper(Boolean success, String inputErrorDetail){
            this.isSuccess = success;
            this.errorMsg = inputErrorDetail;
            this.messages = new List<ErrorMessageResponse>();
            this.uID = '';
        }
    }

    /**
     *  @description Wrapper class that will parse the 'getHostedPayment200Response' Response body
     */

    public class PaymentTransactionWrapper {
        /** @description String parameter that stores the 'correlationId' value returned from Mulesoft */
        @AuraEnabled public String correlationId;

        /** @description String parameter that stores the 'tracingId' assigned to this transaction */
        @AuraEnabled public String tracingId;

        /** @description String parameter that stores the last 4 digits of the Credit Card entered into the Hosted Pay Page to authorize an amount */
        @AuraEnabled public String mPAN;

        /** @description String parameter that stores the type of Credit Card used (Visa, MasterCard, AMEX, etc.) */
        @AuraEnabled public String type;

        /** @description String parameter that stores the expiration date that is on the Credit Card entered into the Hosted Pay Page to authorize an amount */
        @AuraEnabled public String exp;

        /** @description String parameter that store the GUID value generated by Paymentech for this specific transaction */
        @AuraEnabled public String txnGUID;

        /** @description String parameter that stores the approval code returned from Paymentech */
        @AuraEnabled public String approvalCode;

        /** @description String parameter that stores type of transaction that occurred (will always be 'AUTH_ONLY') */
        @AuraEnabled public String transType;

        /** @description Currently not used */
        @AuraEnabled public String profileProcStatus;

        /** @description Currently not used */
        @AuraEnabled public String profileProcStatusMsg;

        /** @description Currently not used */
        @AuraEnabled public String ctiAffluentCard;

        /** @description Currently not used */
        @AuraEnabled public String ctiCommercialCard;

        /** @description Currently not used */
        @AuraEnabled public String ctiDurbinExemption;

        /** @description Currently not used */
        @AuraEnabled public String ctiHealthcareCard;

        /** @description Currently not used */
        @AuraEnabled public String ctiLevel3Eligible;

        /** @description Currently not used */
        @AuraEnabled public String ctiPINlessDebitCard;

        /** @description Currently not used */
        @AuraEnabled public String ctiPayrollCard;

        /** @description Currently not used */
        @AuraEnabled public String ctiPrepaidCard;

        /** @description Currently not used */
        @AuraEnabled public String ctiSignatureDebitCard;

        /** @description String parameter that stores the Customer Reference Number returned from Paymentech. This parameter value is very important as it is used in auditing exercises and is therefore stored on the Quote record */
        @AuraEnabled public String customerRefNum;

        /** @description String parameter that stores the Name on the Credit Card that was entered into the Hosted Pay Page to be authorized */
        @AuraEnabled public String customerName;

        /** @description String parameter that stores the first Name of the Cardholder */
        @AuraEnabled public String customerFirstName;

        /** @description String parameter that stores the last Name of the Cardholder */
        @AuraEnabled public String customerLastName;

        /** @description String parameter that stores the Street address of the provided Billing Address */
        @AuraEnabled public String customerAddress;

        /** @description String parameter that stores the Postal Code of the provided Billing Address */
        @AuraEnabled public String customerPostalCode;

        /** @description Currently not used */
        @AuraEnabled public String uIdTrans;

        /** @description String parameter that stores the Order Id that is tied to this transaction */
        @AuraEnabled public String orderId;

        /** @description String parameter that stores the total amount that was authorized on the provided Credit Card */
        @AuraEnabled public Decimal totalAmt;
    }

    /**
     * @description Wrapper class to extract ACH payment data from front-end
     */
    public class achData {
        /** @description String parameter that stores the response code from Giact */
        @AuraEnabled public String accountResponseCode;

        /** @description String parameter that stores the 'correlationId' parameter returned from Mulesoft (not used at all) */
        @AuraEnabled public String correlationId;

        /** @description FieldData object used to capture the information that was entered into the 'achValidation' LWC */
        @AuraEnabled public FieldData fieldData;

        /** @description String parameter that stores the 'itemReferenceId' parameter (not used at all) */
        @AuraEnabled public String itemReferenceId;

        /** @description String parameter that stores the 'tracingID' parameter returned from Mulesoft (not used at all) */
        @AuraEnabled public String tracingId;

        /** @description String parameter that stores the verification response returned from Gicat/Melon */
        @AuraEnabled public String verificationResponse;
    }

    /**
     * @description Sub-wrapper class as part of achData class
     */
    public class FieldData {
        /** @description String parameter that stores the Account Name that was entered into the 'achValidation' LWC (not used in pilot) */
        @AuraEnabled public String accountName;

        /** @description String parameter that stores the Account Number that was entered into the 'achValidation' LWC (not used in pilot) */
        @AuraEnabled public String accountNumber;

        /** @description String parameter that stores the type of account that was selected on the 'achValidation' LWC (not used in pilot) */
        @AuraEnabled public String achType;

        /** @description String parameter that stores the name of the bank entered into the 'achValidation' LWC (not used in pilot) */
        @AuraEnabled public String bankName;

        /** @description String parameter that stores the routing number that was entered into the 'achValidation' LWC (not used in pilot) */
        @AuraEnabled public String routingNumber;
    }

    /**
     * @description Wrapper class that will parse any Error (non-200) Responses
    */
    public class ErrorMessageResponse {
        /** @description String parameter that stores the type of error returned from Mulesoft */
        @AuraEnabled public String type;

        /** @description String parameter that stores the severity of the error returned from Mulesoft */
        @AuraEnabled public String severity;

        /** @description String parameter that stores the reason code of the error returned from Mulesoft */
        @AuraEnabled public String reasonCode;

        /** @description String parameter that stores the context of the error returned from Mulesoft */
        @AuraEnabled public String context;

        /** @description String parameter that stores the message of the error returned from Mulesoft */
        @AuraEnabled public String message;
    }
    /*  Wrapper Classes: STOP   */
}