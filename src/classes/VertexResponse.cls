public class VertexResponse{
	public String correlationId{get; set;}	//6c5a7edc-6d8b-457c-9959-fa1f631960f7
	public String tracingId{get; set;}	//CV_123
	public String documentNumber{get; set;}	//Q-0000000169
	public String postingDate{get; set;}	//2021-09-21
	public String documentDate{get; set;}	//2021-09-21
	public String transactionId{get; set;}	//Q-0000000169
	public String transactionType{get; set;}	//SALE
	public cls_seller seller{get; set;}
	public cls_customer customer{get; set;}
	public Double subTotal{get; set;}	//708.99
	public Double total{get; set;}	//725.43
	public Double totalTax{get; set;}	//16.44
	public cls_lineItems[] lineItems;
	public class cls_seller {
		public String company{get; set;}	//001
		public String division{get; set;}	//433
		public cls_physicalOrigin physicalOrigin{get; set;}
		public cls_administrativeOrigin administrativeOrigin{get; set;}
	}
	public class cls_physicalOrigin {
		public String streetAddress1 {get; set;}	//6931 Vista Pkwy N
		public String city {get; set;}	//FL
		public String mainDivision {get; set;}	//West Palm Beach
	}
	public class cls_administrativeOrigin {
		public String streetAddress1 {get; set;}	//6931 Vista Pkwy N
		public String city {get; set;}	//FL
		public String mainDivision {get; set;}	//West Palm Beach
	}
	public class cls_customer {
		public cls_destination destination {get; set;}
	}
	public class cls_destination {
		public String streetAddress1 {get; set;}	//48841 Greenwich Cir
		public String city {get; set;}	//Canton
		public String mainDivision {get; set;}	//MI
		public String postalCod {get; set;}	//48188
	}
	public class cls_lineItems {
		public Decimal lineItemNumber{get;set;}	//0
		public String usageClass{get;set;}	//N/A
		public cls_product product{get;set;}
		public Decimal quantity{get;set;}	//1
		public Decimal extendedPrice{get;set;}	//0
		public cls_taxes[] taxes{get;set;}
		public String totalTax{get;set;}	//0.0
		public cls_flexibleFields flexibleFields{get;set;}
		public cls_assistedParameters[] assistedParameters{get;set;}
	}
	public class cls_product {
	}
	public class cls_taxes {
		public cls_jurisdiction jurisdiction {get; set;}
		public cls_imposition imposition {get; set;}
		public cls_impositionType impositionType {get; set;}
		public cls_taxRuleId taxRuleId {get; set;}
		public cls_inclusionRuleId inclusionRuleId {get; set;}
		public String taxResult {get; set;}	//TAXABLE
		public String taxType {get; set;}	//SELLER_USE
		public String situs {get; set;}	//DESTINATION
		public String taxCollectedFromParty {get; set;}	//BUYER
		public Decimal calculatedTax {get; set;}	//0
		public Decimal effectiveRate {get; set;}	//0
		public Decimal taxable {get; set;}	//0
		public Decimal includedTax {get; set;}	//0
		public Decimal nominalRate {get; set;}	//0.06
	}
	public class cls_jurisdiction {
		public String jurisdictionName{get; set;}	//MICHIGAN
		public String jurisdictionLevel{get; set;}	//STATE
		public Integer jurisdictionId{get; set;}	//16871
	}
	public class cls_imposition {
		public String name{get; set;}	//Sales and Use Tax
		public Integer impositionId{get; set;}	//1
	}
	public class cls_impositionType {
		public String typeDescription{get; set;}	//General Sales and Use Tax
		public Integer impositionTypeId{get; set;}	//1
	}
	public class cls_taxRuleId {
		public String ruleId{get; set;}	//9738
	}
	public class cls_inclusionRuleId {
		public String ruleId{get; set;}	//1282443
	}
	public class cls_flexibleFields {
		public cls_flexibleCodeField[] flexibleCodeField{get; set;}
		public cls_flexibleNumericField[] flexibleNumericField{get; set;}
		public cls_flexibleDateField[] flexibleDateField{get; set;}
	}
	public class cls_flexibleCodeField {
		public String value{get; set;}	//JOB
		public Integer fieldId{get; set;}	//2
	}
	public class cls_flexibleNumericField {
		public String value{get; set;}	//0
		public Integer fieldId{get; set;}	//1
	}
	public class cls_flexibleDateField {
		public String value{get; set;}	//2021-09-21
		public Integer fieldId{get; set;}	//2
	}
	public class cls_assistedParameters {
		public String value{get; set;}	//null
		public String paramName{get; set;}	//FLEX.input.PostingDt
		public String phase{get; set;}	//PRE
		public String ruleName{get; set;}	//Posting Date Fix
	}
	public static VertexResponse parse(String json){
		return (VertexResponse) System.JSON.deserialize(json, VertexResponse.class);
	}
}