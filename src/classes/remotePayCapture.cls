/************************************* MODIFICATION LOG ********************************************************************************************
* ADTAPI
*
* DESCRIPTION :  Used to capture remote payment method
*
*---------------------------------------------------------------------------------------------------------------------------------------------------
* DEVELOPER                     DATE              TICKET               REASON
*---------------------------------------------------------------------------------------------------------------------------------------------------
* William Miranda               8/10/2021         2877                 - Original Version
* William Miranda               8/26/2021         2877                 - Updated with requested Changes for Validating Individual fields and custom status codes
*/


@RestResource(urlMapping='/remotePayCapture/')
global with sharing class remotePayCapture {

      /*
        *   Method that runs when the endpoint is called and the method is set to post
        */
        @HttpPost
        global static response doPost() {
           
           
            RestRequest req = RestContext.request;
            RestResponse res = RestContext.response;
            response resp = new response();
             try{
            
                Blob jsonBody = RestContext.request.requestBody;
                String jsonString = jsonBody.toString();
                system.debug('This is the json: ' + jsonString);
        
                //Initialize wrapper class for response
                request respClass = new request();
                //Deserialize json in body to wrapper
                respClass = (request) JSON.deserialize(jsonString,request.class);  

                System.debug(respClass);
                //Validate json
                resp.ErrorMessage = validateFields(respClass);
                System.debug('validated fields successfully');
                //If there was an error with one of the fields don't do the requirement logic
                if(resp.ErrorMessage.length() == 0){

                //Find payment info record with matching remotepayid that has not been validated/captured
                Payment_Info__c paymentinfo = [SELECT Id, Remote_Pay_Id__c, Quote__c FROM Payment_Info__c WHERE Remote_Pay_Id__c =:respClass.RemotePayID AND Validated__c = false LIMIT 1];
                
                List<Remote_Pay_Verified__e> platformEventPayload = new List<Remote_Pay_Verified__e>();
                
                
                //Hard Code Responses. When the requirements for the logic are provided they will be handled here
                if(respClass.paymentType == 'Card'){
                    if(respClass.ccDetails.lastFourCardNumber == '3432' && respClass.ccDetails.expiration == '12/2020'){
                        resp.CaptureSuccess = true;
                        resp.Reason = '';

                        
                        //Set Values for Payment Info Object where payment type = 'card'
                        paymentinfo.PayType__c = 'Card';
                        paymentinfo.CardProfileId__c = respClass.ccDetails.profileId;
                        paymentinfo.CardExpiration__c = respClass.ccDetails.expiration;
                        paymentinfo.CardLast4__c = respClass.ccDetails.lastFourCardNumber;
                        paymentinfo.CardType__c = respClass.ccDetails.type;
                        paymentinfo.CardName__c = respClass.ccDetails.cardHolderName;
                        paymentinfo.Validated__c = true;
                        update paymentinfo;

                        //Set remotepayid in platform event to grab payment information to inform the make payment lwc to query payment info records
                        //Account Id is passed in so we can identify the platform events for this account
                        platformEventPayload.add(new Remote_Pay_Verified__e(RemotePayID__c = paymentinfo.Remote_Pay_Id__c, Quote__c = paymentinfo.Quote__c));

                    }
                    else{
                        resp.CaptureSuccess = false;
                        resp.Reason = 'Incorrect Card Number or Expiration Date';
                    }
                }//end if card
              
                else{
                    System.debug('made it to the else');
                    if(respClass.achDetails.routingNumber == '211370545' && respClass.achDetails.accountNumber == '832992357'){
                        resp.CaptureSuccess = true;

                        //Set Values for Payment Info Object where payment type = 'ACH'
                        paymentinfo.PayType__c = 'ACH';
                        paymentinfo.ACHRouting__c = respClass.achDetails.routingNumber;
                        paymentinfo.ACHAccount__c = respClass.achDetails.accountNumber;
                        paymentinfo.ACHType__c = respClass.achDetails.accountType;
                        paymentinfo.ACHBank__c = respClass.achDetails.bankName;
                        paymentinfo.ACHName__c = respClass.achDetails.accountHolderName;
                        paymentinfo.Validated__c = true;

                        update paymentinfo;

                        //Set remotepayid in platform event to grab payment information to bring into make payment lwc
                        platformEventPayload.add(new Remote_Pay_Verified__e(RemotePayID__c = paymentinfo.Remote_Pay_Id__c));

                    }
                    else{
                        resp.CaptureSuccess = false;
                        resp.Reason = 'Incorrect Routing Number or Account Number';
                    }
                }//end if ach
                
                //Send platform event for remote pay
                List<Database.SaveResult> results = EventBus.publish(platformEventPayload);

            }//end if field validation found an error
            //else bad error on specific field
            else{
                res.statusCode = 500;
            }

            }
            catch(Exception e){
                res.statusCode = 400;
                resp.ErrorMessage = 'Sorry! The json syntax is incorrect. Please verify and try again. Show the following to an administrator: ' + e;
            }
           
            
            return resp;
        }

        public static string validateFields(request respClass){
            String errorMessage = '';
            
            
            if(respClass.paymentType == 'Card'){
                system.debug('Total length: '+respClass.ccDetails.expiration.length());
                system.debug('Substring Before: '+respClass.ccDetails.expiration.substringBefore('/').length());
                //Validate Expiration date 
                if(respClass.ccDetails.expiration.length() != 7 && ( respClass.ccDetails.expiration.substringBefore('/').length() !=  2 || respClass.ccDetails.expiration.substringAfter('/').length() != 4) ) {
                    errorMessage = 'There is a Problem with the Expiration Date for the Card.';
                }   
                //Validate Card Number 
                if(respClass.ccDetails.lastFourCardNumber.length() != 4 ){
                    errorMessage = 'There is a problem with the Card Number';
                }
                system.debug(respClass.ccDetails.type);
                 //Validate Card Type
                 if(respClass.ccDetails.type != 'American Express' && respClass.ccDetails.type != 'Discover' && respClass.ccDetails.type != 'Mastercard' && respClass.ccDetails.type != 'Visa'  ){
                    errorMessage = 'There is a problem with the Card Type. Make sure it is American Express, Discover, Mastercard or Visa.';
                }

            }
            else{
                
                //Validate account type
                if(respClass.achDetails.accountType != 'Checking' && respClass.achDetails.accountType != 'Saving' ){
                    System.debug('validating ach 1');
                    errorMessage = 'There is a problem with the Account type. Make sure it is either Checking or Saving.';
                }
                //Validate Routing Number
                if(respClass.achDetails.routingNumber.length()  != 9){
                    System.debug('validating ach 1');
                    errorMessage = 'There is a problem with the Routing Number. Make sure all 9 digits are included.';

                }
                //Validate Account Number 
                if(respClass.achDetails.accountNumber.length()  != 9){
                    System.debug('validating ach 1');
                    errorMessage = 'There is a problem with the Account Number. Make sure all 9 digits are included.';
                }
            }


            return errorMessage;
        }
/*
*---------------------------------------------------------------------------------------------------------------------------------------------------
                                                                    WRAPPER CLASSES
*---------------------------------------------------------------------------------------------------------------------------------------------------
*/
    
    
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        /*
        *   Wrapper Class to Handle request received 
        */    
        //Request Recieved in post 
        global class request {
            
            public String RemotePayID {get;set;} //Encoded' RemotePayID that is attached to a specific quote/order
            public String paymentType {get;set;}
            public ccDetails ccDetails {get;set;}
            public achDetails achDetails {get;set;}
            public String RemotePayDetails {get;set;} //Remote Pay details such as last 4 of cc, authorization token, etc. All of which is encrypted
          
        }

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        /*
        *   Wrapper Class to Handle Encrypted Json result
        */    
        //Request Recieved in post 
        global class RemotePayDetail {
            public    String  PayType     {get;set;}//Required field. Possible values are "Card" or "ACH".
            public    String  CardProfileID     {get;set;}//Required for Card Payment. This is the Customer Reference Number / Token from PaymentTech.
            public    String  CardExpiration     {get;set;}//Required for Card Payment. Format: MM/YYYY.
            public    String  CardLast4     {get;set;}//Required for Card Payment. Last 4 Digit on credit card.
            public    String  CardName     {get;set;}//Required for Card Payment. Name of the person / card account holder.
            public    String  CardType     {get;set;}//Required for Card Payment. Possible values are "American Express", "Discover", "Mastercard", "Visa".
            public    String  ACHRouting     {get;set;}//Required for ACH. The routing number.
            public    String  ACHAccount     {get;set;}//Required for ACH. The account number.
            public    String  ACHBank     {get;set;}//Required for ACH. The name of the bank.
            public    String  ACHName     {get;set;}//Required for ACH. The name of the person/account holder.
            public    String  ACHType     {get;set;}//Required for ACH. Possible values are "Checking" or "Saving".
            
          
        }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        global class ccDetails {
            public String profileId {get;set;}
            public String expiration  {get;set;}
            public String lastFourCardNumber {get;set;}
            public String cardHolderName {get;set;}
            public String type {get;set;}
        }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


        global class achDetails {
            public String routingNumber {get;set;}
            public String accountNumber {get;set;}
            public String accountType {get;set;}
            public String bankName {get;set;}
            public String accountHolderName {get;set;}
        }
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
        /*
        *   Wrapper Class to Handle request received 
        */    
        //Request Recieved in post 
        global class response {
            public Boolean CaptureSuccess {get;set;}
            public String Reason {get;set;}//Response Pending
            public String ErrorMessage{get;set;}
            public String Pending{get;set;}

        }//end response class
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */


}