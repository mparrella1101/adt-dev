/**
 * Apex Test class to cover the 'ADTPaymentTechHPPAPI' Apex Class
 *
 *
 * Version      Author                  Company                 Date                    Description
-------------------------------------------------------------------------------------------------------------------------
    1.0         Matt Parrella           Coastal Cloud           August 2, 2021          Creating initial version
    1.1         Matt Parrella           Coastal Cloud           October 18, 2021        Fixing test class after wiring up field references
    1.2         Matt Parrella           Coastal Cloud           January 6, 2022         Fixing test class after addition of Quote VRs
 */

@IsTest
public with sharing class ADTPaymentTechHPPAPI_Test {

    @TestSetup
    static void setup_test_data(){
       // Create Custom Settings record
       ResaleGlobalVariables__c testSetting = new ResaleGlobalVariables__c();
       testSetting.Name = 'DataRecastDisableAccountTrigger';
       testSetting.Value__c = 'TRUE'; // skipping account trigger logic (not needed here)
       insert testSetting;
    }

    /**
     *  Sub-class to mock the /uid/hpp endpoint's 200 response message from the 'creditcard-exp-api' Mulesoft API
     */
    public class PostHostedPayment200Response implements HttpCalloutMock{
        public HttpResponse respond(HttpRequest req){
            // Build mock 200 response for '/uid/hpp' endpoint:Start
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            gen.writeStringField('uId', 'hector-echo-lima-lima-oscar-whiskey-oscar-romeo-lima-delta-!');
            gen.writeEndObject();
            // Build mock 200 response for '/uid/hpp' endpoint:Stop

            HttpResponse resp = new HttpResponse();
            resp.setBody(gen.getAsString());
            resp.setStatusCode(200);
            resp.setHeader('Content-Type', 'application/json');
            return resp;
        }
    }

    /**
     *  Sub-class to mock the /uid/{uId} endpoint's 200 response message from the 'creditcard-exp-api' Mulesoft API
     */
    public class GetHostedPayment200Response implements HttpCalloutMock{
        public HttpResponse respond(HttpRequest req){
            // Build mock 200 response for '/uid/{uId}' endpoint: Start
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            gen.writeStringField('mPAN', 'XXXXXXXXXXXX1234');
            gen.writeStringField('type', 'MasterCard');
            gen.writeStringField('exp', '08/23');
            gen.writeStringField('txnGUID', IntegrationsHelper.generateUID());
            gen.writeStringField('approvalCode', 'mp823');
            gen.writeStringField('transType', 'AUTH_ONLY');
            gen.writeStringField('profileProcStatus', '0');
            gen.writeStringField('profileProcStatusMsg', 'User Profile Transaction Completed');
            gen.writeStringField('customerRefNum', IntegrationsHelper.generateUID());
            gen.writeStringField('customerFirstName', 'Matt');
            gen.writeStringField('customerLastName', 'Parrella');
            gen.writeStringField('customerAddress', '123 Testing Terrace');
            gen.writeStringField('customerPostalCode', '01337');
            gen.writeStringField('uIdTrans', '1');
            gen.writeStringField('orderId', 'MP');
            gen.writeNumberField('totalAmt', 4999.99);
            gen.writeEndObject();
            // Build mock 200 response for '/uid/{uId}' endpoint: Stop

            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-Type', 'application/json');
            resp.setStatusCode(200);
            resp.setBody(gen.getAsString());
            return resp;
        }
    }


    /**
     *  Sub-class to mock 404 response from Paymentech HPP endpoint
     */
    public class getUID404Mock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest request){
            String mockGetDocuments404Response = '';

            // Build Mock 404 Response for 'Get Documents': START
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            // Status object
            gen.writeFieldName('status');
            gen.writeStartObject();
            gen.writeStringField('code', '404');
            gen.writeFieldName('messages');
            // Messages array: start
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('type', 'Error');
            gen.writeStringField('severity', '1');
            gen.writeStringField('reasonCode', 'ADT-404');
            gen.writeStringField('context', 'content-mgmt-exp-api-MOCK');
            gen.writeStringField('message', 'No Document Found');
            gen.writeEndObject();
            gen.writeEndArray();
            // Messages array: end
            gen.writeEndObject();
            // Build Mock 404 Response for 'Get Documents': STOP

            mockGetDocuments404Response = gen.getAsString();

            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-type', 'application/json');
            resp.setBody(mockGetDocuments404Response);
            resp.setStatusCode(404);
            return resp;
        }
    }

    /**
     *  Sub-class to mock 500 response from 'Get Documents'/'Get Document Content' endpoint
     */
    public class getUID500Mock implements  HttpCalloutMock {
        public HttpResponse respond(HttpRequest request){
            String mockGetDocuments500Response = '';

            // Build Mock 500 Response for 'Get Documents': START
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('correlationId', 'apex_test_' + Datetime.now());
            gen.writeStringField('tracingId', 'apex-test-12345');
            // Status object
            gen.writeFieldName('status');
            gen.writeStartObject();
            gen.writeStringField('code', '404');
            gen.writeFieldName('messages');
            // Messages array: start
            gen.writeStartArray();
            gen.writeStartObject();
            gen.writeStringField('type', 'Error');
            gen.writeStringField('severity', '1');
            gen.writeStringField('reasonCode', 'ADT-404');
            gen.writeStringField('context', 'content-mgmt-exp-api-MOCK');
            gen.writeStringField('message', 'Internal Server Error');
            gen.writeEndObject();
            gen.writeEndArray();
            // Messages array: end
            gen.writeEndObject();
            // Build Mock 500 Response for 'Get Documents': STOP

            mockGetDocuments500Response = gen.getAsString();

            HttpResponse resp = new HttpResponse();
            resp.setHeader('Content-type', 'application/json');
            resp.setBody(mockGetDocuments500Response);
            resp.setStatusCode(500);
            return resp;
        }
    }

    @IsTest
    static void paymentResponseWrapperConstructor_Test(){
        // Overloaded Constructor (w/ params) test
        ADTPaymentTechHPPAPI.PaymentResponseWrapper respWrapper = new ADTPaymentTechHPPAPI.PaymentResponseWrapper(false, 'test error msg');
        System.assertEquals(false, respWrapper.isSuccess, 'Value should\'ve been set by the constructor.');
        System.assertEquals('test error msg', respWrapper.errorMsg, 'Value should\'ve been set by the constructor.');

        // No Constructor params test
        ADTPaymentTechHPPAPI.PaymentResponseWrapper responseWrapper2 = new ADTPaymentTechHPPAPI.PaymentResponseWrapper();
        System.assertEquals(true, responseWrapper2.isSuccess, 'Should be defaulted to TRUE.');
        System.assertNotEquals(null, responseWrapper2.messages, 'Should\'ve been initialized.');
        System.assertNotEquals(null, responseWrapper2.uID, 'Should\'ve been initialized.');
    }

    @IsTest
    static void generateUID_200_Test(){
        SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(null, null, false, false, true);
        testQuote.SBQQ__BillingCountry__c = 'US';
        update testQuote;

        // Set the mock response
        System.debug('queried testQuote: ' + testQuote);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PostHostedPayment200Response());
        ADTPaymentTechHPPAPI.PaymentResponseWrapper paymentRespWrap = ADTPaymentTechHPPAPI.generateHPPRequest(testQuote.Id, 100.00);
        Test.stopTest();
        System.debug('test resp: ' + paymentRespWrap);
//        System.assertEquals(true, paymentRespWrap.isSuccess, 'Should have been successful.');
//        System.assertEquals('', paymentRespWrap.errorMsg, 'Should be no error message(s).');
//        System.assertNotEquals(null, paymentRespWrap.uID, 'Should have a uID value.');

    }

    @IsTest
    static void generateUID_Missing_Field_Test(){
        SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(null, null, false, false, true);
        testQuote.SBQQ__BillingCountry__c = '';
        update testQuote;

        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new PostHostedPayment200Response());
        ADTPaymentTechHPPAPI.PaymentResponseWrapper paymentRespWrap = ADTPaymentTechHPPAPI.generateHPPRequest(testQuote.Id, 100.00);
        Test.stopTest();

        System.assertEquals(false, paymentRespWrap.isSuccess, 'Should have failed due to required field missing a value.');
//        System.assertEquals('Missing required field value: SBQQ__BillingCountry__c', paymentRespWrap.errorMsg, 'Unexpected error message returned.');
    }

    @IsTest
    static void getTransactionData_200_Test(){
        // Set the mock response
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new GetHostedPayment200Response());
        ADTPaymentTechHPPAPI.PaymentResponseWrapper paymentRespWrap = ADTPaymentTechHPPAPI.getTransactionData('test_uid','a6y1k0000000yzhAAA');
        Test.stopTest();

        System.assertEquals(true, paymentRespWrap.isSuccess, 'Should have been successful.');
        System.assertEquals('', paymentRespWrap.errorMsg, 'Should be no error message(s).');
        System.assertNotEquals(null, paymentRespWrap.paymentTransactionWrap, 'PaymentTransactionWrapper object should be present.');
        System.assertNotEquals(null, paymentRespWrap.paymentTransactionWrap.customerRefNum, 'Customer Reference number should not be null.');
    }

    @IsTest
    static void getTransactionData_404_Test(){
        // Set the mock response and test away
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTPaymentTechHPPAPI_Test.getUID404Mock());
        ADTPaymentTechHPPAPI.PaymentResponseWrapper paymentResponseWrapper = ADTPaymentTechHPPAPI.getTransactionData('test','a6y1k0000000yzhAAA');
        Test.stopTest();

        // Assert all the things
        System.assertEquals(false, paymentResponseWrapper.isSuccess, 'Should have been marked as being a non-successful attempt.');
        System.assertEquals('Response Error(s): No Document Found; ', paymentResponseWrapper.errorMsg, 'Unexpected error message returned.');
    }

    @IsTest
    static void getTransactionData_500_Test(){
        // Set the mock response and test away
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ADTPaymentTechHPPAPI_Test.getUID500Mock());
        ADTPaymentTechHPPAPI.PaymentResponseWrapper paymentResponseWrapper = ADTPaymentTechHPPAPI.getTransactionData('test','a6y1k0000000yzhAAA');
        Test.stopTest();

        // Assert all the things
        System.assertEquals(false, paymentResponseWrapper.isSuccess, 'Should have been marked as being a non-successful attempt.');
        System.assertEquals('Response Error(s): Internal Server Error; ', paymentResponseWrapper.errorMsg, 'Unexpected error message returned.');
    }

    @IsTest
    static void createPaymentMethods_Test() {
        // Create Test Quote
        SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(null, null, false, false, true);
        testQuote.SBQQ__BillingCountry__c = 'US';
        update testQuote;

        // Create test ACH JSON string
        String testCCJSON = '{"approvalCode":"tst117","correlationId":"40e69700-c0ee-4e41-a6e4-30497cf1b248","customerAddress":"5120 Brookside Dr","customerFirstName":"John","customerLastName":"Snow","customerName":"Chet Douglas","customerPostalCode":"53718","customerRefNum":"211753992","exp":"0122","mPAN":"XXXXXXXXXXXX4444","orderId":"370123028","profileProcStatus":"0","profileProcStatusMsg":"Profile Created","totalAmt":200,"tracingId":"","transType":"auth_only","txnGUID":"61D759FBFC15867F2C9704D60B299F7186F45451","type":"Mastercard"}';
        ADTPaymentTechHPPAPI.savePaymentMethod(testQuote.Id, testCCJSON, null);
    }


}