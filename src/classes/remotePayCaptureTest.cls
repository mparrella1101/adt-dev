@isTest
public class remotePayCaptureTest {

    @isTest static void remotePayCaptureTest() 
    {

        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://yourInstance.my.salesforce.com/services/apexrest/remotePayCapture/';
        request.httpMethod = 'POST';
        //set request body for CC
        request.requestBody = blob.valueOf('{"remotePayId":"RAUPVKP","paymentType":"Card","ccDetails":{"profileId":"87269799","expiration":"12/2020","lastFourCardNumber":"3432","cardHolderName":"John Smith","type":"Mastercard"},"achDetails":null}');

        //Set Request
        RestContext.request = request;
        remotePayCapture.response resp = new  remotePayCapture.response();
        resp.Pending = '';
        //Call method
        resp =  remotePayCapture.doPost(); 
        
        //set request body for ACH
        request.requestBody = blob.valueOf('{"remotePayId":"RAUPVKP","paymentType":"ACH","ccDetails":null,"achDetails":{"routingNumber":"211370545","accountNumber":"832992357","accountType":"Checking","bankName":"FCU","accountHolderName":"Joe Smith"}}');
        remotePayCapture.response resp2 = new remotePayCapture.response();
        RestContext.request = request;
        resp2.Pending = '';
        resp2 = remotePayCapture.doPost();
        
        
        remotePayCapture.request req = new  remotePayCapture.request();
        req.RemotePayID = '';
        req.RemotePayDetails = '';

        remotePayCapture.RemotePayDetail wrp = new  remotePayCapture.RemotePayDetail();
        wrp.PayType = '';
        wrp.CardProfileID = '';
        wrp.CardExpiration = '';
        wrp.CardLast4 = '';
        wrp.CardName = '';
        wrp.CardType = '';
        wrp.ACHAccount = '';
        wrp.ACHRouting = '';
        wrp.ACHBank = '';
        wrp.ACHName = '';
        wrp.ACHType = '';

                
    }
}