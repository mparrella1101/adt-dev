@isTest
private class SimpleSellControllerCPQTest {

    @TestSetup
    static void setup_test_data(){
        // Create Custom Settings record
        List<ResaleGlobalVariables__c> settings = new List<ResaleGlobalVariables__c>();

        ResaleGlobalVariables__c testSetting = new ResaleGlobalVariables__c();
        testSetting.Name = 'DataRecastDisableAccountTrigger';
        testSetting.Value__c = 'TRUE'; // skipping account trigger logic (not needed here)
        settings.add(testSetting);

        ResaleGlobalVariables__c testSetting2 = new ResaleGlobalVariables__c();
        testSetting2.Name = 'runEwcMarketing';
        testSetting2.Value__c = 'TRUE';
        settings.add(testSetting2);

        ResaleGlobalVariables__c testSetting3 = new ResaleGlobalVariables__c();
        testSetting3.Name = 'BypassQuoteSecurityProfiles';
        testSetting3.Value__c = 'TRUE';
        settings.add(testSetting3);

        insert settings;
    }
    
    @IsTest
    static void enableSSButton_Test(){
        // Create Acct
        Account a = CPQTestUtils.createAccount('Apex Test Account', true);
        User u = [SELECT Id FROM User WHERE IsActive = TRUE AND SimpleSell__c = TRUE LIMIT 1];

        Test.startTest();
        Boolean result = SimpleSellControllerCPQ.enableSimpleSellButton(a.Id);
        Test.stopTest();
    }

    @IsTest
    static void saveProducts_Test() {
        Account a = CPQTestUtils.createAccount('Apex Test Account', true);
        String testRowData = '[{"pName":"Cellguard","pQuantity":"2","index":1,"pNotes":"321654654"},{"pName":"CO Detector - $100.00","pQuantity":"2","index":2,"pNotes":"321654654"}]';

        Test.startTest();
        Boolean result = SimpleSellControllerCPQ.saveProducts(a.Id, testRowData);
        Test.stopTest();
    }

    @IsTest
    static void getExistingCatalog_Test() {
        Account a = CPQTestUtils.createAccount('Apex Test Account', true);
        ProductCatalog__c pc = new ProductCatalog__c(
                ProductAccount__c = a.Id
        );
        insert pc;

        Test.startTest();
        List<ProductCatalog__c> result = SimpleSellControllerCPQ.getExistingCatalog(a.Id);
        Test.stopTest();
    }

    @IsTest
    static void deleteProducts_Test() {
        Account a = CPQTestUtils.createAccount('Apex Test Account', true);
        ProductCatalog__c testCatalogue = new ProductCatalog__c(
          ProductAccount__c = a.Id
        );
        insert testCatalogue;

        Test.startTest();
        SimpleSellControllerCPQ.deleteProducts(a.Id);
        Test.stopTest();
    }
}