@isTest
private with sharing class QuoteSummaryControllerTest {
    private static String qspProductCode = 'AddQSP';
    private static String preDefQuote = 'preDefQuote';
    private static String promoProductCode = 'PROMO_DISCOUNT';
    private static String resiBundleProductCode = 'Residential Services';
    private static String installDiscountProductCode = 'SALES_INSTALL_DISCOUNT';
    private static String monthlyDiscountProductCode = 'SALES_MONTHLY_DISCOUNT';

    @TestSetup
    private static void makeData(){       
        
         ResaleGlobalVariables__c testGlobalVariable1 = CPQTestUtils.createResaleGlobalVariables('DataRecastDisableAccountTrigger', 'TRUE', false);
         ResaleGlobalVariables__c testGlobalVariable2 = CPQTestUtils.createResaleGlobalVariables('runEwcMarketing', 'TRUE', false);
         ResaleGlobalVariables__c testGlobalVariable3 = CPQTestUtils.createResaleGlobalVariables('BypassQuoteSecurityProfiles', 'TRUE', false);
         insert new List<ResaleGlobalVariables__c>{testGlobalVariable1, testGlobalVariable2, testGlobalVariable3};       
          
         Account testAccount = CPQTestUtils.createAccount('Test Account KF', true);
         Opportunity testOpp = CPQTestUtils.createOpportunity('Test Opportunity KF', testAccount, true);
         Product2 testResiBundleProduct = CPQTestUtils.createProduct(resiBundleProductCode, null, resiBundleProductCode, null, null, null, true, false);
         Product2 testQSPProduct = CPQTestUtils.createProduct('Supplemental Quality Service Plan', null, qspProductCode, null, null, null, true, false);
         Product2 testPromoProduct = CPQTestUtils.createProduct('Promotional Discount', null, promoProductCode, null, null, null, true, false);
         Product2 testInstallDiscountProduct = CPQTestUtils.createProduct('Discount', null, installDiscountProductCode, null, null, null, true, false);
         Product2 testMonthlyDiscountProduct = CPQTestUtils.createProduct('Discount', null, monthlyDiscountProductCode, null, null, null, true, false);
         insert new List<Product2>{testResiBundleProduct, testQSPProduct, testPromoProduct, testInstallDiscountProduct, testMonthlyDiscountProduct};
         SBQQ__ProductOption__c testQSPProductOption = new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c = testResiBundleProduct.Id, SBQQ__OptionalSKU__c = testQSPProduct.Id, SBQQ__Number__c = 300);
         SBQQ__ProductOption__c testPromoProductOption = new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c = testResiBundleProduct.Id, SBQQ__OptionalSKU__c = testPromoProduct.Id, SBQQ__Number__c = 301);
         SBQQ__ProductOption__c testInstallDiscountProductOption = new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c = testResiBundleProduct.Id, SBQQ__OptionalSKU__c = testInstallDiscountProduct.Id, SBQQ__Number__c = 302);
         SBQQ__ProductOption__c testMonthlyDiscountProductOption = new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c = testResiBundleProduct.Id, SBQQ__OptionalSKU__c = testMonthlyDiscountProduct.Id, SBQQ__Number__c = 303);
 		 insert new List<SBQQ__ProductOption__c>{testQSPProductOption, testPromoProductOption, testInstallDiscountProductOption, testMonthlyDiscountProductOption};
         SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(testAccount, testOpp, true, true, false);
         testQuote.Pre_Defined_Quote__c = preDefQuote;
         insert testQuote;
         Promotion__c testPromotion = CPQTestUtils.createPromotion(preDefQuote, true);
        
    }

    @isTest
    private static void testGetQuote(){
        Account queriedAccount = [SELECT Id FROM Account LIMIT 1];
        Opportunity queriedOpp = [SELECT Id FROM Opportunity LIMIT 1];
        // SBQQ__Quote__c testQuote = CPQTestUtils.createQuote(queriedAccount, queriedOpp, true, true, true);
        SBQQ__Quote__c testQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        QuoteSummaryController.getQuote(testQuote.Id);
        SBQQ__Quote__c queriedQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        System.assertEquals(testQuote.Id, queriedQuote.Id, 'We expect our getQuote method to have queried the correct quote.');
        
    }

    @isTest
    private static void testGetQuoteLines(){
        SBQQ__Quote__c queriedQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c testQuoteLine = CPQTestUtils.createQuoteLine(null, null, queriedQuote, true, true);
        QuoteSummaryController.getQuoteLines(queriedQuote.Id);
        SBQQ__QuoteLine__c queriedQuoteLine = [SELECT Id, SBQQ__Quote__c FROM SBQQ__QuoteLine__c LIMIT 1];
        System.assertEquals(testQuoteLine.Id, queriedQuoteLine.Id, 'We expect our getQuoteLine method to have queried the correct quote line.');
        System.assertEquals(queriedQuote.Id, queriedQuoteLine.SBQQ__Quote__c, 'We expect our getQuoteLine method to have queried the correct quote line associated to the correct quote.');
    }

    @isTest
    private static void testUpsertQSPQuoteLine(){
        // hasQSP set to false to insert new supplemental QSP quote line scenario.
        Product2 bundleProduct;
        Product2 qspProduct;
        List<Product2> queriedProducts = [SELECT Id, ProductCode FROM Product2];
        for(Product2 product : queriedProducts){
            switch on product.ProductCode{
                when 'Residential Services'{
                    bundleProduct = product;
                }
                when 'AddQSP'{
                    qspProduct = product;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        SBQQ__ProductOption__c testProductOption = [SELECT Id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :bundleProduct.Id AND SBQQ__OptionalSKU__c = :qspProduct.Id LIMIT 1];
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c resiBundleQuoteLine = CPQTestUtils.createQuoteLine(bundleProduct, null, queriedQuote, true, true);
        QuoteSummaryController.upsertQSPQuoteLine(queriedQuote.Id, 10, resiBundleQuoteLine.Id, bundleProduct.Id, 3, false);
        SBQQ__QuoteLine__c queriedQSPQuoteLine = [SELECT Id, SBQQ__ProductCode__c FROM SBQQ__QuoteLine__c WHERE SBQQ__ProductCode__c = :qspProductCode];
        System.debug('queriedQSPQuoteLine: ' + queriedQSPQuoteLine);
        System.assertEquals(qspProductCode, queriedQSPQuoteLine.SBQQ__ProductCode__c, 'We expect a new quote line to be inserted for Supplemental QSP');
    }

    @isTest
    private static void testUpsertQSPQuoteLineNegative(){
        // hasQSP set to true to delete/insert supplemental QSP quote line scenario.
        Product2 bundleProduct;
        Product2 qspProduct;
        List<Product2> queriedProducts = [SELECT Id, ProductCode FROM Product2];
        for(Product2 product : queriedProducts){
            switch on product.ProductCode{
                when 'Residential Services'{
                    bundleProduct = product;
                }
                when 'AddQSP'{
                    qspProduct = product;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        SBQQ__ProductOption__c testProductOption = [SELECT Id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :bundleProduct.Id AND SBQQ__OptionalSKU__c = :qspProduct.Id LIMIT 1];
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c resiBundleQuoteLine = CPQTestUtils.createQuoteLine(bundleProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c qspQuoteLine = CPQTestUtils.createQuoteLine(qspProduct, null, queriedQuote, true, false);
        qspQuoteLine.QSP__c = 7;
        insert new List<SBQQ__QuoteLine__c>{resiBundleQuoteLine, qspQuoteLine};
        QuoteSummaryController.upsertQSPQuoteLine(queriedQuote.Id, 10, resiBundleQuoteLine.Id, bundleProduct.Id, 3, true);
        SBQQ__QuoteLine__c queriedQSPQuoteLine = [SELECT Id, SBQQ__ProductCode__c, QSP__c FROM SBQQ__QuoteLine__c WHERE SBQQ__ProductCode__c = :qspProductCode];
        System.debug('queriedQSPQuoteLine: ' + queriedQSPQuoteLine);
        System.assertEquals(10, queriedQSPQuoteLine.QSP__c, 'We expect a new quote line to be inserted for Supplemental QSP with updated QSP__c field value.');
    }

    @isTest
    private static void testDeleteQSPQuoteLine(){
        Product2 qspProduct = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = :qspProductCode];
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c qspQuoteLine = CPQTestUtils.createQuoteLine(qspProduct, null, queriedQuote, true, false);
        QuoteSummaryController.deleteQSPQuoteLine(queriedQuote.Id, qspProductCode);
        System.assertEquals(0, queriedQuote.SBQQ__LineItemCount__c, 'We expect the QSP quote line to have been deleted.');

    }

    @isTest
    private static void testGetPromotions(){
        Promotion__c queriedPromotion = [SELECT Id FROM Promotion__c LIMIT 1];
        Product2 testLampProduct = CPQTestUtils.createProduct('On/Off Appliance/Lamp Module', null, 'AD075', null, null, null, true, false);
        Product2 testCommandTouchscreenProduct = CPQTestUtils.createProduct('Command 7in Touchscreen', null, 'ADT7AIO-2', null, null, null, true, false);
        Product2 testCellRadioProduct = CPQTestUtils.createProduct(' LTE Plug-in Radio Module, AT&T or Verizon Carrier version', null, 'CELLGUARD', null, null, null, true, false);
        Product2 testDoorbellProduct = CPQTestUtils.createProduct('ADT HD Doorbell Camera (Control version)', null, ' DBC835-V2-AC', null, null, null, true, false);
        Product2 testCompleteProduct = CPQTestUtils.createProduct('Complete', null, 'L4 CMD', null, null, null, true, false);
        Product2 testWirelessBundleProduct = CPQTestUtils.createProduct('Wireless Sensors Bundle with:', null, 'RFBUNCMDAPM', null, null, null, true, false);
        insert new List<Product2>{testLampProduct, testCommandTouchscreenProduct, testCellRadioProduct, testDoorbellProduct, testCompleteProduct, testWirelessBundleProduct};
        SBQQ__Quote__c queriedQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c lampQuoteLine = CPQTestUtils.createQuoteLine(testLampProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c commandQuoteLine = CPQTestUtils.createQuoteLine(testCommandTouchscreenProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c cellRadioQuoteLine = CPQTestUtils.createQuoteLine(testCellRadioProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c doorbellQuoteLine = CPQTestUtils.createQuoteLine(testDoorbellProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c completeQuoteLine = CPQTestUtils.createQuoteLine(testCompleteProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c wirelessBundleQuoteLine = CPQTestUtils.createQuoteLine(testWirelessBundleProduct, null, queriedQuote, true, false);
        insert new List<SBQQ__QuoteLine__c>{lampQuoteLine, commandQuoteLine, cellRadioQuoteLine, doorbellQuoteLine, completeQuoteLine, wirelessBundleQuoteLine};
        QuoteSummaryController.getPromotions(queriedQuote.Id);
        List<Promotion__c> promotionsList = new List<Promotion__c>();
        promotionsList = QuoteSummaryController.getPromotions(queriedQuote.Id);
        System.debug('promotionsList: ' + promotionsList);
        System.assertEquals(queriedPromotion.Id, promotionsList[0].Id, 'We expect our promotion to have been successfully applied to our test quote.');
    }
	
    @isTest
    private static void testQSPQuoteLinePromotion(){
        // testing scenario where promotion quote line exists.
        Promotion__c queriedPromotion = [SELECT Id, Name, Description__c FROM Promotion__c LIMIT 1];
        Product2 bundleProduct;
        Product2 promoProduct;
        List<Product2> queriedProducts = [SELECT Id, ProductCode FROM Product2];
        for(Product2 product : queriedProducts){
            switch on product.ProductCode{
                when 'Residential Services'{
                    bundleProduct = product;
                }
                when 'PROMO_DISCOUNT'{
                    promoProduct = product;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        SBQQ__ProductOption__c testProductOption = [SELECT Id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :bundleProduct.Id AND SBQQ__OptionalSKU__c = :promoProduct.Id LIMIT 1];
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c resiBundleQuoteLine = CPQTestUtils.createQuoteLine(bundleProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c promoQuoteLine = CPQTestUtils.createQuoteLine(promoProduct, null, queriedQuote, true, false);
        insert new List<SBQQ__QuoteLine__c>{resiBundleQuoteLine, promoQuoteLine};
        QuoteSummaryController.QSPQuoteLinePromotion(queriedQuote.Id, null, resiBundleQuoteLine.Id, bundleProduct.Id, 5, queriedPromotion.Description__c, queriedPromotion.Name, queriedPromotion.Id);
        List<SBQQ__QuoteLine__c> queriedPromoQuoteLines = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :queriedQuote.Id AND SBQQ__ProductCode__c = :promoProductCode];
        System.assertEquals(1, queriedPromoQuoteLines.size(), 'We expect only one promotion active at a time.');
    }

    @isTest
    private static void testQSPQuoteLinePromotionNegative(){
        // testing scenario where promotion quote line does not exist.
        Promotion__c queriedPromotion = [SELECT Id, Name, Description__c FROM Promotion__c LIMIT 1];
        Product2 bundleProduct;
        Product2 promoProduct;
        List<Product2> queriedProducts = [SELECT Id, ProductCode FROM Product2];
        for(Product2 product : queriedProducts){
            switch on product.ProductCode{
                when 'Residential Services'{
                    bundleProduct = product;
                }
                when 'PROMO_DISCOUNT'{
                    promoProduct = product;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        SBQQ__ProductOption__c testProductOption = [SELECT Id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :bundleProduct.Id AND SBQQ__OptionalSKU__c = :promoProduct.Id LIMIT 1];
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c resiBundleQuoteLine = CPQTestUtils.createQuoteLine(bundleProduct, null, queriedQuote, true, true);
        QuoteSummaryController.QSPQuoteLinePromotion(queriedQuote.Id, null, resiBundleQuoteLine.Id, bundleProduct.Id, 5, queriedPromotion.Description__c, queriedPromotion.Name, queriedPromotion.Id);
        List<SBQQ__QuoteLine__c> queriedPromoQuoteLines = [SELECT Id FROM SBQQ__QuoteLine__c WHERE SBQQ__Quote__c = :queriedQuote.Id AND SBQQ__ProductCode__c = :promoProductCode];
        System.assertEquals(1, queriedPromoQuoteLines.size(), 'We expect only one promotion active at a time.');
        // System.assert(queriedPromoQuoteLines.size() > 0);
    }

    @isTest
    private static void testApplyQuoteLineTax(){
        Integer adscTaxAmount = 20;
        Integer anscTaxAmount = 5;
        Integer qspTaxAmount = 1;
        Product2 testCommandTouchscreenProduct = CPQTestUtils.createProduct('Command 7in Touchscreen', null, 'ADT7AIO-2', null, null, null, true, false);
        Product2 testCompleteProduct = CPQTestUtils.createProduct('Complete', null, 'L4 CMD', null, null, null, true, false);
        insert new List<Product2>{testCommandTouchscreenProduct, testCompleteProduct};
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c commandQuoteLine = CPQTestUtils.createQuoteLine(testCommandTouchscreenProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c completeQuoteLine = CPQTestUtils.createQuoteLine(testCompleteProduct, null, queriedQuote, true, false);
        insert new List<SBQQ__QuoteLine__c>{commandQuoteLine, completeQuoteLine};
        String quoteLineModel = '['
        +'{"Id":"'+commandQuoteLine.Id+'","lineNumber":9,"productCode":"ADT7AIO-2","adscTax":'+adscTaxAmount+',"anscTax":null,"qspTax":null,"totalTax":'+adscTaxAmount+'},'
        +'{"Id":"'+completeQuoteLine.Id+'","lineNumber":2,"productCode":"L4 CMD","adscTax":null,"anscTax":'+anscTaxAmount+',"qspTax":'+qspTaxAmount+',"totalTax":'+qspTaxAmount+'}'
        // +'{"Id":"a6u1k000000Cw4sAAC","lineNumber":21,"productCode":"PROMO_DISCOUNT","adscTax":3.85,"anscTax":null,"qspTax":null,"totalTax":3.85}'
        +']';
        QuoteSummaryController.ApplyQuoteLineTax(quoteLineModel);
        Map<Id, SBQQ__QuoteLine__c> quoteLineMap = new Map<Id, SBQQ__QuoteLine__c>();
        List<SBQQ__QuoteLine__c> queriedQuoteLines = [SELECT Id, ADSC_Taxes__c, ANSC_Taxes__c, QSP_Taxes__c, Taxes__c FROM SBQQ__QuoteLine__c];
        for(SBQQ__QuoteLine__c ql : queriedQuoteLines){
            quoteLineMap.put(ql.Id, ql);
        }
        System.debug('quoteLineMap: ' + quoteLineMap);
        System.assertEquals(adscTaxAmount, quoteLineMap.get(commandQuoteLine.Id).ADSC_Taxes__c, 'We expect to have successfully retrieved the ADSC tax value for this quote line and update the line accordingly.');
        System.assertEquals(anscTaxAmount, quoteLineMap.get(completeQuoteLine.Id).ANSC_Taxes__c, 'We expect to have successfully retrieved the ANSC tax value for this quote line and update the line accordingly.');
        System.assertEquals(qspTaxAmount, quoteLineMap.get(completeQuoteLine.Id).QSP_Taxes__c, 'We expect to have successfully retrieved the QSP tax value for this quote line and update the line accordingly.');
        
    }

    @isTest
    private static void testGetDiscount(){
        List<String> discountProductCodeList = new List<String>{'SALES_INSTALL_DISCOUNT', 'SALES_MONTHLY_DISCOUNT'};
        Product2 queriedInstallProduct;
        Product2 queriedMonthlyProduct;
        List<Product2> queriedProducts = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode IN :discountProductCodeList];
        for(Product2 product : queriedProducts){
            switch on product.ProductCode{
                when 'SALES_INSTALL_DISCOUNT'{
                    queriedInstallProduct = product;
                }
                when 'SALES_MONTHLY_DISCOUNT'{
                    queriedMonthlyProduct = product;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        SBQQ__Quote__c queriedQuote = [SELECT Id FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c testInstallDiscountQuoteLine = CPQTestUtils.createQuoteLine(queriedInstallProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c testMonthlyDiscountQuoteLine = CPQTestUtils.createQuoteLine(queriedMonthlyProduct, null, queriedQuote, true, false);
        insert new List<SBQQ__QuoteLine__c>{testInstallDiscountQuoteLine, testMonthlyDiscountQuoteLine};
        QuoteSummaryController.getDiscounts(queriedQuote.Id);
        SBQQ__QuoteLine__c queriedInstallDiscountQuoteLine;
        SBQQ__QuoteLine__c queriedMonthlyDiscountQuoteLine;
        List<SBQQ__QuoteLine__c> queriedQuoteLines = [SELECT Id, SBQQ__Quote__c, SBQQ__ProductCode__c FROM SBQQ__QuoteLine__c];
        for(SBQQ__QuoteLine__c quoteLine : queriedQuoteLines){
            switch on quoteLine.SBQQ__ProductCode__c{
                when 'SALES_INSTALL_DISCOUNT'{
                    queriedInstallDiscountQuoteLine = quoteLine;
                }
                when 'SALES_MONTHLY_DISCOUNT'{
                    queriedMonthlyDiscountQuoteLine = quoteLine;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        System.assertEquals(testInstallDiscountQuoteLine.Id, queriedInstallDiscountQuoteLine.Id, 'We expect our getQuoteLine method to have queried the correct quote line.');
        System.assertEquals(testMonthlyDiscountQuoteLine.Id, queriedMonthlyDiscountQuoteLine.Id, 'We expect our getQuoteLine method to have queried the correct quote line.');
        System.assertEquals(queriedQuote.Id, queriedInstallDiscountQuoteLine.SBQQ__Quote__c, 'We expect our getQuoteLine method to have queried the correct install discount quote line associated to the correct quote.');
        System.assertEquals(queriedQuote.Id, queriedMonthlyDiscountQuoteLine.SBQQ__Quote__c, 'We expect our getQuoteLine method to have queried the correct monthly discount quote line associated to the correct quote.');
        System.assertEquals(installDiscountProductCode, queriedInstallDiscountQuoteLine.SBQQ__ProductCode__c, 'We expect our getQuoteLine method to have queried the correct install discount quote line.');
        System.assertEquals(monthlyDiscountProductCode, queriedMonthlyDiscountQuoteLine.SBQQ__ProductCode__c, 'We expect our getQuoteLine method to have queried the correct monthly discount quote line.');
    }

    @isTest
    private static void testUpsertDiscount(){
        Product2 bundleProduct;
        Product2 testInstallDiscountProduct;
        Product2 testMonthlyDiscountProduct;
        List<Product2> queriedProductList = [SELECT Id, ProductCode FROM Product2];
        Set<Id> discountProductIdSet = new Set<Id>();
        SBQQ__ProductOption__c testInstallDiscountProductOption;
        SBQQ__ProductOption__c testMonthlyDiscountProductOption;
        
        for(Product2 product : queriedProductList){
            switch on product.ProductCode{
                when 'Residential Services'{
                    bundleProduct = product;                    
                }
                when 'SALES_INSTALL_DISCOUNT'{
                    testInstallDiscountProduct = product;
                    discountProductIdSet.add(product.Id);
                }
                when 'SALES_MONTHLY_DISCOUNT'{
                    testMonthlyDiscountProduct = product;
                    discountProductIdSet.add(product.Id);
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        List<SBQQ__ProductOption__c> queriedProductOptionList = [SELECT Id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :bundleProduct.Id AND SBQQ__OptionalSKU__c IN :discountProductIdSet];
        for(SBQQ__ProductOption__c queriedProductOption : queriedProductOptionList){
            if(queriedProductOption.SBQQ__OptionalSKU__c == testInstallDiscountProduct.Id){
                testInstallDiscountProductOption = queriedProductOption;
            }
            if(queriedProductOption.SBQQ__OptionalSKU__c == testMonthlyDiscountProduct.Id){
                testMonthlyDiscountProductOption = queriedProductOption;
            }
        }
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c resiBundleQuoteLine = CPQTestUtils.createQuoteLine(bundleProduct, null, queriedQuote, true, false);
        // SBQQ__QuoteLine__c discountQuoteLineInstall = CPQTestUtils.createQuoteLine(testInstallDiscountProduct, null, queriedQuote, true, false);
        // discountQuoteLineInstall.SBQQ__ListPrice__c = 100;
        // SBQQ__QuoteLine__c discountQuoteLineMonthly = CPQTestUtils.createQuoteLine(testMonthlyDiscountProduct, null, queriedQuote, true, false);
        // discountQuoteLineMonthly.SBQQ__ListPrice__c = 10;
        // insert new List<SBQQ__QuoteLine__c>{resiBundleQuoteLine, discountQuoteLineInstall, discountQuoteLineMonthly};
        QuoteSummaryController.upsertDiscount(queriedQuote.Id, 100, 10, resiBundleQuoteLine.Id, bundleProduct.Id, 3, false);
        SBQQ__QuoteLine__c queriedInstallDiscountQuoteLine;
        SBQQ__QuoteLine__c queriedMonthlyDiscountQuoteLine;
        List<SBQQ__QuoteLine__c> queriedQuoteLines = [SELECT Id, SBQQ__ListPrice__c, Install__c, Monitoring__c, SBQQ__Quote__c, SBQQ__ProductCode__c FROM SBQQ__QuoteLine__c];
        for(SBQQ__QuoteLine__c quoteLine : queriedQuoteLines){
            switch on quoteLine.SBQQ__ProductCode__c{
                when 'SALES_INSTALL_DISCOUNT'{
                    queriedInstallDiscountQuoteLine = quoteLine;
                }
                when 'SALES_MONTHLY_DISCOUNT'{
                    queriedMonthlyDiscountQuoteLine = quoteLine;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        System.assertEquals(installDiscountProductCode, queriedInstallDiscountQuoteLine.SBQQ__ProductCode__c, 'We expect a new install discount quote line to be inserted.');
        System.assertEquals(-100, queriedInstallDiscountQuoteLine.SBQQ__ListPrice__c, 'We expect our new install discount quote line to be inserted with the correct Install__c value.');
        System.assertEquals(monthlyDiscountProductCode, queriedMonthlyDiscountQuoteLine.SBQQ__ProductCode__c, 'We expect a new monthly discount quote line to be inserted.');
        System.assertEquals(-10, queriedMonthlyDiscountQuoteLine.SBQQ__ListPrice__c, 'We expect our new monthly discount quote line to be inserted with the correct Monitoring__c value.');
    }

    @isTest
    private static void testUpsertDiscountHasDiscounts(){
        Product2 bundleProduct;
        Product2 testInstallDiscountProduct;
        Product2 testMonthlyDiscountProduct;
        List<Product2> queriedProductList = [SELECT Id, ProductCode FROM Product2];
        Set<Id> discountProductIdSet = new Set<Id>();
        SBQQ__ProductOption__c testInstallDiscountProductOption;
        SBQQ__ProductOption__c testMonthlyDiscountProductOption;
        for(Product2 product : queriedProductList){
            switch on product.ProductCode{
                when 'Residential Services'{
                    bundleProduct = product;                    
                }
                when 'SALES_INSTALL_DISCOUNT'{
                    testInstallDiscountProduct = product;
                    discountProductIdSet.add(product.Id);
                }
                when 'SALES_MONTHLY_DISCOUNT'{
                    testMonthlyDiscountProduct = product;
                    discountProductIdSet.add(product.Id);
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        List<SBQQ__ProductOption__c> queriedProductOptionList = [SELECT Id, SBQQ__ConfiguredSKU__c, SBQQ__OptionalSKU__c FROM SBQQ__ProductOption__c WHERE SBQQ__ConfiguredSKU__c = :bundleProduct.Id AND SBQQ__OptionalSKU__c IN :discountProductIdSet];
        for(SBQQ__ProductOption__c productOption : queriedProductOptionList){
            if(productOption.SBQQ__OptionalSKU__c == testInstallDiscountProduct.Id){
                testInstallDiscountProductOption = productOption;
            }
            if(productOption.SBQQ__OptionalSKU__c == testMonthlyDiscountProduct.Id){
                testMonthlyDiscountProductOption = productOption;
            }
        }
        SBQQ__Quote__c queriedQuote = [SELECT Id, SBQQ__LineItemCount__c FROM SBQQ__Quote__c LIMIT 1];
        SBQQ__QuoteLine__c resiBundleQuoteLine = CPQTestUtils.createQuoteLine(bundleProduct, null, queriedQuote, true, false);
        SBQQ__QuoteLine__c discountQuoteLineInstall = CPQTestUtils.createQuoteLine(testInstallDiscountProduct, null, queriedQuote, true, false);
        discountQuoteLineInstall.SBQQ__ListPrice__c = 100;
        SBQQ__QuoteLine__c discountQuoteLineMonthly = CPQTestUtils.createQuoteLine(testMonthlyDiscountProduct, null, queriedQuote, true, false);
        discountQuoteLineMonthly.SBQQ__ListPrice__c = 5;
        insert new List<SBQQ__QuoteLine__c>{resiBundleQuoteLine, discountQuoteLineInstall, discountQuoteLineMonthly};
        QuoteSummaryController.upsertDiscount(queriedQuote.Id, 100, 10, resiBundleQuoteLine.Id, bundleProduct.Id, 3, true);
        SBQQ__QuoteLine__c queriedInstallDiscountQuoteLine;
        SBQQ__QuoteLine__c queriedMonthlyDiscountQuoteLine;
        List<SBQQ__QuoteLine__c> queriedQuoteLines = [SELECT Id, SBQQ__ListPrice__c, Install__c, Monitoring__c, SBQQ__Quote__c, SBQQ__ProductCode__c FROM SBQQ__QuoteLine__c];
        for(SBQQ__QuoteLine__c quoteLine : queriedQuoteLines){
            switch on quoteLine.SBQQ__ProductCode__c{
                when 'SALES_INSTALL_DISCOUNT'{
                    queriedInstallDiscountQuoteLine = quoteLine;
                }
                when 'SALES_MONTHLY_DISCOUNT'{
                    queriedMonthlyDiscountQuoteLine = quoteLine;
                }
                when else{
                    System.debug('when else block');
                }
            }
        }
        System.assertEquals(installDiscountProductCode, queriedInstallDiscountQuoteLine.SBQQ__ProductCode__c, 'We expect a new install discount quote line to be inserted.');
        System.assertEquals(-100, queriedInstallDiscountQuoteLine.SBQQ__ListPrice__c, 'We expect our new install discount quote line to be inserted with the correct Install__c value.');
        System.assertEquals(monthlyDiscountProductCode, queriedMonthlyDiscountQuoteLine.SBQQ__ProductCode__c, 'We expect a new monthly discount quote line to be inserted.');
        System.assertEquals(-10, queriedMonthlyDiscountQuoteLine.SBQQ__ListPrice__c, 'We expect our new monthly discount quote line to be inserted with the correct Monitoring__c value.');
    }

}