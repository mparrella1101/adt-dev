public with sharing class ADTIVRPayment 
{

    /**
     * This method calls the agent which then initiates a three way call, where the customer can privately enter their info
     */
    @AuraEnabled(cacheable=true)
    public static responseWrapper putPaymentInfo() 
    {
        //Parameters that will be used later, just hardcoding for now to test
        //String paymentId, String phoneNumber, String appId, String language, String ccPaymentType
        String paymentId = '';
        responseWrapper output = new responseWrapper();

        Mulesoft_API_Settings__mdt msft;
        try
        {
            //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
            Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
            if(environment)
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, Triton_IVR_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
            }
            else
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, Triton_IVR_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
            }            
        }
        catch(QueryException ex)
        {
            responseWrapper error = new responseWrapper();
            error.queryError='Unable to find a mulesoft settings custom metadata record';
            return error;

        }

        //Store endpoint
        String endpointURL = msft.Endpoint_base_url__c + msft.Triton_IVR_Endpoint__c;

        //Insert paymentID into URL
        endpointURL = endpointURL.replace('{paymentId}',paymentId);
        
        //Create the new request
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        //Create instance of request body object to store values that will be sent in the PUT request
        requestObj reqBody = new requestObj();
        
        //Set values for request body object that will be sent in the PUT request
        //These will all need to be updated for prod, hardcoded for now for testing
        reqBody.phoneNumber = '5003';
        reqBody.appId = 'CPQ';
        reqBody.language = 'ESP';
        reqBody.ccPaymentType = 'CC';
        

        //Set endpoint, headers and body for request
        request.setEndpoint(endpointURL);
        request.setMethod('PUT');
        request.setHeader('Content-Type', 'application/json;');
        request.setHeader('client_id', msft.client_id__c);
        request.setHeader('client_secret', msft.client_secret__c);
        request.setHeader('tracing_id', generateTracingId());
        request.setBody(JSON.serialize(reqBody));

        System.debug('sending the request');
        //Send the POST request
        HttpResponse response = http.send(request);
        System.debug('request sent');
        switch on response.getStatusCode()
        {
            when 200
            {
                system.debug(response.getBody());
                output = (responseWrapper) System.JSON.deserialize(response.getBody(), responseWrapper.class);
                system.debug('SUCCESS! ' + response.getStatusCode());
                system.debug(output);
                return output;
            }
            when else
            {
                system.debug(response.getBody());
                output = (responseWrapper) System.JSON.deserialize(response.getBody().replace('status', 'status2'), responseWrapper.class);
                system.debug('ERROR! ' + response.getStatusCode());
                system.debug(output);
                return output;
            } 
        }
    }

    /**
     * This method takes a paymentId and returns payment info from triton for the associated paymentId, this is initiated by the agent by clicking a button, although I'd prefer to have it automated
     */

    @AuraEnabled(cacheable=true)
    public static responseWrapper getPaymentInfo() 
    {
        //Parameters that will be used later, just hardcoding for now to test
        String paymentId = '';
        String appId = '';
        responseWrapper output = new responseWrapper();

        Mulesoft_API_Settings__mdt msft;
        try
        {
            //Check if the current org is a sandbox and pick the appropriate metadata record to provide callout information
            Boolean environment = [SELECT Id, isSandbox From Organization LIMIT 1].isSandbox;
            if(environment)
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, Triton_IVR_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='DEV1' LIMIT 1];
            }
            else
            {
                msft = [SELECT MasterLabel, client_id__c, client_secret__c, Endpoint_base_url__c, Triton_IVR_Endpoint__c FROM Mulesoft_API_Settings__mdt WHERE MasterLabel='Production' LIMIT 1];
            }            
        }
        catch(QueryException ex)
        {
            responseWrapper error = new responseWrapper();
            error.queryError='Unable to find a mulesoft settings custom metadata record';
            return error;

        }

        //Store endpoint
        String endpointURL = msft.Endpoint_base_url__c + msft.Triton_IVR_Endpoint__c;

        //Insert paymentID into URL
        endpointURL = endpointURL.replace('{paymentId}',paymentId) + '?appId=' + appId;
        
        //Create the new request
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        //Set endpoint, headers and body for request
        request.setEndpoint(endpointURL);
        request.setMethod('GET');
        request.setHeader('Content-Type', 'application/json;');
        request.setHeader('client_id', msft.client_id__c);
        request.setHeader('client_secret', msft.client_secret__c);
        request.setHeader('tracing_id', generateTracingId());

        System.debug('sending the request');
        //Send the POST request
        HttpResponse response = http.send(request);
        System.debug('request sent');
        switch on response.getStatusCode()
        {
            when 200
            {
                system.debug(response.getBody());
                output = (responseWrapper) System.JSON.deserialize(response.getBody(), responseWrapper.class);
                system.debug('SUCCESS! ' + response.getStatusCode());
                system.debug(output);
                return output;
            }
            when else
            {
                system.debug(response.getBody());
                output = (responseWrapper) System.JSON.deserialize(response.getBody().replace('status', 'status2'), responseWrapper.class);
                system.debug('ERROR! ' + response.getStatusCode());
                system.debug(output);
                return output;
            } 
        }
    }

    /**
     * Method for generating a tracingId
     */

    private static String generateTracingId()
    {
        String rightNow = Datetime.now().format('MM-dd-yyyy-HH-mm-ss-sss');
        String tracingId = 'CC_SCPQ_' + rightNow.replaceAll('-',''); // Remove '-' characters to have 1 uninterrupted string representing datetime
        return tracingId;
    }

    /**
     * Method for retrieving user info to populate their phone number
     */
    @AuraEnabled
     public static String getUserPhone(Id userId)
     {
        String phone = '';
        phone = [SELECT Id, Phone FROM User WHERE Id =:userId].Phone;

        return phone;
     }

    public class requestObj 
    {
        @AuraEnabled public String phoneNumber;
        @AuraEnabled public String appId;
        @AuraEnabled public String language;
        @AuraEnabled public String ccPaymentType;
    }

    public class responseWrapper 
    {
        //Success Attributes
        @AuraEnabled public String correlationId;
        @AuraEnabled public String tracingId;
        @AuraEnabled public String appId;
        @AuraEnabled public String ccPaymentType;
        @AuraEnabled public String code;
        @AuraEnabled public String language;
        @AuraEnabled public String message;
        @AuraEnabled public String phoneNumber;
        @AuraEnabled public String status;
        @AuraEnabled public String paymentId;
        @AuraEnabled public String zipCode;
        @AuraEnabled public String bankAccount;
        @AuraEnabled public String ccDate;
        @AuraEnabled public String ccType;
        @AuraEnabled public String lastFourCard;
        @AuraEnabled public String achType;
        @AuraEnabled public String profileID;
        @AuraEnabled public String routingNumber;


        //Error Attributes
	    @AuraEnabled public Status status2;
        @AuraEnabled public String queryError;
    }

    public class Status 
    {
		@AuraEnabled public String code;
		@AuraEnabled public List<Messages> messages;
	}

	public class Messages 
    {
		@AuraEnabled public String type;
		@AuraEnabled public String reasonCode;
		@AuraEnabled public String context;
		@AuraEnabled public String message;
	}
}