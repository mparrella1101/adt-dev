import { LightningElement, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import initiateCall from '@salesforce/apex/ADTIVRPayment.putPaymentInfo';
import retrieveDetails from '@salesforce/apex/ADTIVRPayment.getPaymentInfo';
import getPhoneNumber from '@salesforce/apex/ADTIVRPayment.getUserPhone';
import Id from '@salesforce/user/Id';

export default class Cc_ivrPayment extends LightningElement 
{
    userId = Id;
    phoneNumber;

    connectedCallback()
    {
        //Retrieve phone number to populate IVR payment screen
        this.getPhoneNumber();
    }
    callAgent()
    {
        //Make call to agent by pinging Triton with data
        initiateCall()
        .then(result =>
            {
                //Get response and do something
                this.showToast('Success!', 'Phone call initiated with agent', 'success');
            })
        .catch(result =>
            {
                //Show error toast
                this.showToast('Error!', result, 'error');
            })
    }

   
    /**
     * Method for retrieving current user's phone number
     */

    retrievePhoneNumber()
    {
        getPhoneNumber({userId : this.userId})
        .then(result =>
            {
                this.phoneNumber = result.phoneNumber
            })
    }

    /**
     * Method for showing toasts
     */

     showToast(title, message, variant) 
     {
        const evt = new ShowToastEvent({
            title: this._title,
            message: this.message,
            variant: this.variant,
        });
        this.dispatchEvent(evt);
    }
}