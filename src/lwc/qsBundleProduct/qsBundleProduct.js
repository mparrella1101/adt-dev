import { LightningElement, api } from 'lwc';

export default class QsBundleProduct extends LightningElement {

    @api bundleId;
    @api quoteLines;
    @api showInstall;
    @api showMonthly;
    @api installmentValue;
    childProducts = [];
    monthlyValue;
    specialQuantityProducts = ['SIXCTA', 'HW BUNDLE BA -1'];

    connectedCallback(){
        console.log('>>>connectedCallback qsBundleProduct');
        this.productSort();
    }

    productSort(){
        console.log('>>>productSort()');
        this.quoteLines.forEach(item => {
            if(item.SBQQ__RequiredBy__c == this.bundleId){
                console.log('item.SBQQ__Quantity__c: ' + item.SBQQ__Quantity__c);
                console.log('item.SBQQ__ProductName__c: ' + item.SBQQ__ProductName__c);
                console.log('item.SBQQ__ProductCode__c: ' + item.SBQQ__ProductCode__c);
                console.log('item.Install__c: ' + item.Install__c);
                console.log('item.Monthly__c: ' + item.Monthly__c);
                console.log('this.calculateMonthly(): ' + this.calculateMonthly(item.Install__c, this.installmentValue))
                this.childProducts.push({
                    Id: item.Id,
                    quantity: this.specialQuantityProducts.includes(item.SBQQ__ProductCode__c) ? (item.SBQQ__Quantity__c * 3) : item.SBQQ__Quantity__c, // doing this logic to multiple quantity by 3 for PILOT ONLY.
                    productName: item.SBQQ__ProductName__c,
                    install: item.Install__c,
                    monthly: this.calculateMonthly(item.Install__c, this.installmentValue)});
            }
        });
    }

    calculateMonthly(install, installmentValue){
        console.log('>>>calculateMonthly()');
        console.log('install: ' + install);
        console.log('installmentValue: ' + installmentValue);
        
        if(install === 0 || installmentValue === 'Pay In Full'){
            return 0;
        } else {
            switch(installmentValue){
                case '3Pay':{
                    return (install / 3);
                    break;
                }
                case '12 Months':{
                    return (install / 12);
                    break;
                }
                case '24 Months':{
                    return (install / 24);
                    break;
                }
                case '36 Months':{
                    return (install / 36);
                    break;
                }
                case '60 Months':{
                    return (install / 60);
                    break;
                }
            }
        }        
    }

    @api refreshChildLines(installmentValue){
        console.log('>>>refreshChildLines()');
        console.log('installmentValue: ' + installmentValue);
        this.childProducts = [];
        this.installmentValue = installmentValue;
        this.productSort();
    }

}