/*
*  LWC used for displaying documents fetched from the CM8 Content Management System that are
*  related to a Quote
*/

import { LightningElement, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import getDocuments from '@salesforce/apex/ADTContentMgmtAPI.getDocuments';
import getDocumentContent from '@salesforce/apex/ADTContentMgmtAPI.getDocumentContent';

// Define actions for datatable
const actions = [
    { label: 'View Document', name: 'view_document' }
];


// Define columns for datatable

const columns = [
    {
        type: 'action',
        typeAttributes: { rowActions: actions },
    },
    { label: 'Account Type', fieldName: 'accountType', wrapText: true },
    { label: 'Type', fieldName: 'documentType' },
    { label: 'Order No.', fieldName: 'orderNumber' },
    { label: 'Customer Name', fieldName: 'customerName' },
    { label: 'Customer No.', fieldName: 'customerNumber' },
    { label: 'Job No.', fieldName: 'jobNumber' },
    { label: 'Site No.', fieldName: 'siteNumber' },
    { label: 'Import Date', fieldName: 'importDate' }
];

export default class CcViewQuoteDocs extends NavigationMixin(LightningElement) {

    // var declarations

    isLoading = false; // used to set loading spinner, defaulted to true since a callout is being made once the lwc is loaded
    documents; // will hold returned document records from CM8
    columns = columns;
    error; // holds error objects for displaying
    isModalOpen = true;
    showDocsList = false;
    showDocDetail = false;
    docContents;
    showNoDocsError = false;
    noDocsErrorText = 'No Documents found!';
    @api recordId;
    url;

    // TODO: 7/29/21 - Add a way to show which document is currently being viewed


    showToast(title, message, variant){
        let toastEvt = new ShowToastEvent({
            'title': title,
            'message': message,
            'variant': variant
        });
        this.dispatchEvent(toastEvt);
    }

    callGetDocuments(){
        this.isLoading = true;
        getDocuments({ quoteId : this.recordId })
        .then(result => {
            console.log('result: ', result);
            if (result.isSuccess){
                if (result.payload.length > 0){
                    this.isLoading = false;
                    this.documents = result.payload;
                    this.showDocsList = true;
                } else {
                    this.isLoading = false;
                    this.documents = undefined;
                    this.showNoDocsError = true;
                    this.showDocsList = false;
                }
            } else {
                this.showDocsList = false;
                this.documents = undefined;
                this.error = result.errorMsg;
                this.showToast('', this.error, 'error');
                    this.dispatchEvent(new CloseActionScreenEvent());
            }
        })
        .catch(error => {
            this.error = 'Unknown Error';
            this.showDocsList = false;
            if (Array.isArray(error.body)){
                this.error = error.body.map(e => e.message).join(', ');
                this.isLoading = false;
                this.showToast('', this.error, 'error');
                    this.dispatchEvent(new CloseActionScreenEvent());
            } else if (typeof error.body.message === 'string'){
                this.error = error.body.message;
                this.isLoading = false;
                this.showToast('', this.error, 'error');
                    this.dispatchEvent(new CloseActionScreenEvent());
            }
        });
    }

    handleRowAction(event){
        let actionName = event.detail.action.name;
        let docURIvar = event.detail.row.docURI;
        if (actionName === 'view_document'){

            this.isLoading = true;
            this.docContents = '';
            this.url = '/apex/CC_DisplayDocument?docData=';
            getDocumentContent({
                docURI : docURIvar
            })
            .then(result => {
                if (result.isSuccess){
                    this.isLoading = false;
                    this.docContents = result.docContents;
                    this.url += this.docContents;
                } else {
                    this.isLoading = false;
                    this.error = result.errorMsg;
                    this.showToast('', this.error, 'error');
                }
            })
            .catch(error => {
                this.error = 'Unknown Error';
                this.isLoading = false;
                if (Array.isArray(error.body)){
                    this.error = error.body.map(e => e.message).join(', ');
                    this.showToast('', this.error, 'error');
                } else if (error.body.message !== null && typeof error.body.message === 'string'){
                    this.error = error.body.message;
                   this.showToast('', this.error, 'error');
                }
            });
        }
    }


}