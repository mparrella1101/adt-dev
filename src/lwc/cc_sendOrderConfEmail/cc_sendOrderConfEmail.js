/**
 * Created by Matt on 10/8/2021.
 */

import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import sendOrderEmailConfirmation from '@salesforce/apex/ADTOrderMgmtSysAPI.sendOrderEmailConfirmation';

export default class CcSendOrderConfEmail extends LightningElement {
    isLoading = true;
    loadingText = 'Sending Order Email Confirmation...';
    @api recordId;

    /*
     * Wired method to re-send the email confirmation
     */
     @wire(sendOrderEmailConfirmation, { quoteId : '$recordId' })
     wiredSendEmail({ error, data }) {
         if (data) {
             if (data.isSuccess){
                 this.showToastMessage('', 'Order Confirmation Email successfully sent!', 'success');
                 this.closeQuickAction();
             } else {
                 this.showToastMessage('', data.errorMsg, 'error');
                 this.closeQuickAction();
             }
         } else if (error) {
             console.log('Error: ', error);
             this.isLoading =false;
             if (Array.isArray(error.body)){
                 this.error = error.body.map(e => e.message).join(', ');
                 this.isLoading = false;
                 this.showToastMessage('', this.error, 'error');
                 this.closeQuickAction();
             } else if (typeof error.body.message === 'string'){
                 this.error = error.body.message;
                 this.isLoading = false;
                 this.showToastMessage('', this.error, 'error');
                 this.closeQuickAction();
             }
         }
     }


    /*
     *  Method to display toast messages
     */
     showToastMessage(inputTitle, inputMsg, inputVariant){
         const toastEvt = new ShowToastEvent({
             title : inputTitle,
             message : inputMsg,
             variant : inputVariant
         });
         this.dispatchEvent(toastEvt);
     }

     /*
      * Method to close the quick action dialogue box
      */
      closeQuickAction() {
          this.dispatchEvent(new CloseActionScreenEvent());
      }
}