import { LightningElement, api, wire } from 'lwc';
import { getRecord, updateRecord } from 'lightning/uiRecordApi';
// import getQuoteData from '@salesforce/apex/QuotePaymentController.getQuoteData';
import TOTAL_INSTALL_CHARGE from '@salesforce/schema/SBQQ__Quote__c.Total_Install_Charge__c';
// import MINIMUM_PAYMENT from '@salesforce/schema/SBQQ__Quote__c.Minimum_Payment__c';
import ADDITIONAL_PAYMENT from '@salesforce/schema/SBQQ__Quote__c.Additional_Payment__c';
import DUE_AT_SALE from '@salesforce/schema/SBQQ__Quote__c.Due_at_Sale__c';

const QUOTE_FIELDS = [ID_FIELD,
                    TOTAL_INSTALL_CHARGE,
                    MINIMUM_PAYMENT,
                    ADDITIONAL_PAYMENT,
                    DUE_AT_SALE]

export default class InstallationFee extends LightningElement {

    @api id;
    @api options;
    // @api quote;
    value = '';
    totalInstallCharge;
    minimumPayment;
    additionalPayment;
    dueAtSale;
    threePayEligible;
    financeEligible;
    showThreePayScreen = false;
    showFinancingScreen = false;

    // @wire(getRecord, { recordId : this.id, fields: QUOTE_FIELDS })
    // wireQuote({ error, data }){
    //     if(error){
    //         this.error = error;
    //         this.showToast('Error getting record data', error.message, 'error');
    //     } else if(data){
    //         console.log('quote: ' + JSON.stringify(data));
    //         this.totalInstallCharge = data.fields.Total_Install_Charge__c.value;
    //         this.minimumPayment = data.fields.Minimum_Payment__c.value;
    //         this.additionalPayment = data.fields.Additional_Payment__c.value;
    //         this.dueAtSale = data.fields.Due_at_Sale__c.value;
    //         console.log('totalInstallCharge: ' + this.totalInstallCharge);
    //         console.log('minimumPayment: ' + this.minimumPayment);
    //         console.log('additionalPayment: ' + this.additionalPayment);
    //         console.log('dueAtSale: ' + this.dueAtSale);
    //     }
    // }

    // connectedCallback(){
    //     getQuoteData({ quoteId : this.id })
    //     .then((result)=>{
    //         console.log('result: ' + JSON.stringify(result));
    //     }).catch(error=>{
    //         this.showToast(error.name, error.message, 'error');
    //     });
    // }

    handleInstallChannelChange(event) {
        const selectedOption = event.detail.value;

        switch(selectedOption){
            case 'option1':
                this.type = 'install';
                this.showInstallPaymentScreen = false;
                this.installAchValidation = false;
                this.installCCPayment = false;
                console.log('remote payment');
                console.log('installAchValidation: ' + this.installAchValidation);
                console.log('ccPayment: ' + this.installCCPayment);
            break;
            case 'option2':
                this.type = 'install';
                this.showInstallPaymentScreen = true;
                this.installAchValidation = false;
                this.installCCPayment = true;
                console.log('Credit card payment');
                console.log('installAchValidation: ' + this.installAchValidation);
                console.log('installCCPayment: ' + this.installCCPayment);
            break;
            case 'option3':
                this.type = 'install';
                this.showInstallPaymentScreen = true;
                this.installAchValidation = true;
                this.installCCPayment = false;
                console.log('direct deposit ACH');
                console.log('installAchValidation: ' + this.installAchValidation);
                console.log('installCCPayment: ' + this.installCCPayment);
            break;
        }
    }

    // connectedCallback(){
    //     console.log('quote: ' + JSON.stringify(this.quote));
    //     this.totalInstallCharge = this.quote.data.fields.Total_Install_Charge__c.value;
    // }

    financingHandleChange(event){
        console.log('financing!');        
        this.showThreePayScreen = false;
        this.showFinancingScreen = event.detail.checked ? true : false;
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[FINANCING.fieldApiName] = event.detail.checked;        
        fields[THREEPAY.fieldApiName] = false;        
        const recordInput = { fields };
        updateRecord(recordInput)
        .then(()=>{
            console.log('financing?: ' + recordInput.fields.Financing__c);
            console.log('threepay?: ' + recordInput.fields.X3_Payment_Option__c);
        }).catch(error=>{
            this.showToast(error.name, error.message, 'error');
        });
    }

    threePayHandleChange(event){
        console.log('threepay!');
        this.showFinancingScreen = false;
        this.showThreePayScreen = event.detail.checked ? true : false;
        const fields = {};
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[FINANCING.fieldApiName] = false;        
        fields[THREEPAY.fieldApiName] = event.detail.checked;        
        const recordInput = { fields };
        updateRecord(recordInput)
        .then(()=>{
            console.log('financing?: ' + recordInput.fields.Financing__c);
            console.log('threepay?: ' + recordInput.fields.X3_Payment_Option__c);
        }).catch(error=>{
            this.showToast(error.name, error.message, 'error');
        });
    }

    showToast(title, message, variant){
        this.dispatchEvent(new ShowToastEvent({
            title, message, variant
        }));
    }

}