// Modified | Matt Parrella | CoastalCloud | 11-17-2021 | SFCPQ-491: Making UI more intuitive for Payment Instrument selection

import { LightningElement, api, wire } from 'lwc';
import { getRecord, updateRecord } from 'lightning/uiRecordApi';
import { CloseActionScreenEvent } from 'lightning/actions';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import USER_ID from '@salesforce/user/Id';
import NO_DIRECT_PAYMENT from '@salesforce/schema/User.NoDirectPayment__c';
import ID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Id';
import TOTAL_INSTALL_CHARGE from '@salesforce/schema/SBQQ__Quote__c.Total_Installation__c';
import MINIMUM_PAYMENT from '@salesforce/schema/SBQQ__Quote__c.Minimum_Payment__c';
import ADDITIONAL_PAYMENT from '@salesforce/schema/SBQQ__Quote__c.Additional_Payment__c';
import DUE_AT_SALE from '@salesforce/schema/SBQQ__Quote__c.Due_at_Sale__c';
import MONITORING_FEE from '@salesforce/schema/SBQQ__Quote__c.Monitoring_Fee__c';
import MONTHLY_MONITORING_FEE from '@salesforce/schema/SBQQ__Quote__c.Monthly_Monitoring_Fees__c';
import FINANCE_ELIGIBLE from '@salesforce/schema/SBQQ__Quote__c.Finance_Eligible_at_Submit__c';
import FINANCE_ELIGIBLE_AMOUNT from '@salesforce/schema/SBQQ__Quote__c.Finance_Eligible_Amount__c';
import THREEPAY_ELIGIBLE from '@salesforce/schema/SBQQ__Quote__c.Three_Pay_Eligible_at_Submit__c';
import THREEPAY from '@salesforce/schema/SBQQ__Quote__c.X3_Payment_Option__c';
import FINANCING from '@salesforce/schema/SBQQ__Quote__c.Financing__c';
import EASY_PAY_ENROLL from '@salesforce/schema/SBQQ__Quote__c.EasyPay_Enrollment__c';
import EASY_PAY_REQUIRED from '@salesforce/schema/SBQQ__Quote__c.EasyPay_Required__c';
import RISK_GRADE from '@salesforce/schema/SBQQ__Quote__c.Risk_Grade__c';
import PAYMENT_INTERVAL from '@salesforce/schema/SBQQ__Quote__c.Payment_Interval__c';
import PAYMENT_TERMS from '@salesforce/schema/SBQQ__Quote__c.SBQQ__PaymentTerms__c';
import PAYMENT_TERMS_FORMULA from '@salesforce/schema/SBQQ__Quote__c.Payment_Terms_Formula__c';
import INVOICE1 from '@salesforce/schema/SBQQ__Quote__c.Invoice_1_Due_at_Install__c';
import INVOICE2 from '@salesforce/schema/SBQQ__Quote__c.Invoice_2_Due_30_days_after__c';
import INVOICE3 from '@salesforce/schema/SBQQ__Quote__c.Invoice_3_Due_60_days_after__c';
import RENT_OWN from '@salesforce/schema/SBQQ__Quote__c.Profile_Rent_Own__c';
import MONTHLY_FEE from '@salesforce/schema/SBQQ__Quote__c.Monthly_Payment__c';
import FIRST_PAYMENT_DUE from '@salesforce/schema/SBQQ__Quote__c.First_Payment_Due_at_Install__c';
import INSTALLMENTS from '@salesforce/schema/SBQQ__Quote__c.Installments__c';
import TOTAL_FEES_TAXES from '@salesforce/schema/SBQQ__Quote__c.Total_Activation_and_Permit_Fees_w_Taxes__c';
import ACCOUNT_ID from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Account__c';
import System__c from '@salesforce/schema/SoftwareSupportAgreement__ChangeEvent.System__c';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
//import getPaymentInfo from '@salesforce/apex/QuotePaymentController.getValidatedPaymentInfo';
import PAYMENT_MC from '@salesforce/messageChannel/paymentMessageChannel__c'; // MParrella | SFCPQ-491
import savePaymentMethod from '@salesforce/apex/ADTPaymentTechHPPAPI.savePaymentMethod';
/* MParrella | SFCPQ-71: Importing fields to update: START */
import DEPOSIT_PAID from '@salesforce/schema/SBQQ__Quote__c.Deposit_Trip_Fee_Paid__c';
import TX_TIMESTAMP_FIELD from '@salesforce/schema/SBQQ__Quote__c.Transaction_Timestamp__c';
import INSTALL_PAY_METHOD_FIELD from '@salesforce/schema/SBQQ__Quote__c.Payment_Type__c';
import RECUR_PAY_METHOD_FIELD from '@salesforce/schema/SBQQ__Quote__c.Payment_Type_Service__c';
import CONTRACT_TERMS_FIELD from '@salesforce/schema/SBQQ__Quote__c.Payment_Terms_Formula__c';
import INSTALLMENTS_FIELD from '@salesforce/schema/SBQQ__Quote__c.Installments__c';
import BANK_ACCOUNT_NUM_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Bank_Account_Number__c';
import BANK_ACCOUNT_NAME_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Bank_Credit_Union_Name__c';
import GIACT_TOKEN_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Giact_Token__c';
import AMOUNT_AUTHED_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Amount_Authorized__c';
import PAYMENTECH_GUID_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_GUID__c';
import PAYMENTECH_REF_NUM_FIELD from '@salesforce/schema/SBQQ__Quote__c.TX_Reference_Number__c';
import CARD_TYPE_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Card_Type__c';
import EXP_DATE_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Exp_Date__c';
import LAST_FOUR_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Last_4_Used__c';
import ACH_NAME_ON_ACCT_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Name_on_Account__c';
import CC_NAME_ON_ACCT_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Name_on_CC__c';
import QUOTE_STATUS_FIELD from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Status__c';
import HAS_CC_INFO from '@salesforce/schema/SBQQ__Quote__c.Has_CC_Info__c';
import HAS_ACH_INFO from '@salesforce/schema/SBQQ__Quote__c.Has_ACH_Info__c';
import ROUTING_NUM_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Bank_Routing_Number__c';
import CC_INSTALL_METHOD_FIELD from '@salesforce/schema/SBQQ__Quote__c.CC_install_Method__c';
import ACH_INSTALL_METHOD_FIELD from '@salesforce/schema/SBQQ__Quote__c.ACH_Install_Method__c';
import USER_PROFILE_NAME_FIELD from '@salesforce/schema/User.Profile.Name';
import USER_EMP_NUM_FIELD from '@salesforce/schema/User.EmployeeNumber__c';
import USER_NAME_FIELD from '@salesforce/schema/User.Name';
import USER_PHONE_FIELD from '@salesforce/schema/User.MobilePhone';
import COMM_REP_HRID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Comm_Rep_HRID__c';
import COMM_REP_NAME_FIELD from '@salesforce/schema/SBQQ__Quote__c.Comm_Rep_Name__c';
import COMM_REP_PHONE_FIELD from '@salesforce/schema/SBQQ__Quote__c.Comm_Rep_Phone__c';
import COMM_REP_TYPE_FIELD from '@salesforce/schema/SBQQ__Quote__c.Comm_Rep_Type__c';
import COMM_REP_USR_FIELD from '@salesforce/schema/SBQQ__Quote__c.CommissionUser__c';
/* MParrella | SFCPQ-71: Importing fields to update: STOP */

const USER_FIELDS = [NO_DIRECT_PAYMENT, USER_PHONE_FIELD, USER_EMP_NUM_FIELD, USER_PROFILE_NAME_FIELD, USER_NAME_FIELD];
const QUOTE_FIELDS = [
                        ID_FIELD,
                        TOTAL_INSTALL_CHARGE,
                        MINIMUM_PAYMENT,
                        ADDITIONAL_PAYMENT,
                        DUE_AT_SALE,
                        MONITORING_FEE,
                        FINANCE_ELIGIBLE,
                        FINANCE_ELIGIBLE_AMOUNT,
                        THREEPAY_ELIGIBLE,
                        THREEPAY,
                        FINANCING,
                        EASY_PAY_ENROLL,
                        EASY_PAY_REQUIRED,
                        RISK_GRADE,
                        PAYMENT_TERMS_FORMULA,
                        MONTHLY_MONITORING_FEE,
                        PAYMENT_INTERVAL,
                        RENT_OWN,
                        MONTHLY_FEE,
                        FIRST_PAYMENT_DUE,
                        PAYMENT_TERMS,
                        INSTALLMENTS,
                        TOTAL_FEES_TAXES,
                        ACCOUNT_ID,
                        QUOTE_STATUS_FIELD
                    ];

const formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 2 });

export default class ReviewQuotePaymentLWC extends LightningElement {
    @api recordId;
    type;
    showInstallPaymentScreen = false;
    showMonitoringPaymentScreen = false;
    showSecondGrid = false;
    installAchValidation = false;
    installCCPayment = false;
    value = '';
    monitoringAchValidation = false;
    monitoringCCPayment = false;
    remoteUser;
    quote;
    totalInstallCharge;
    // totalInstallChargeFormatted;
    minimumPayment;
    // minimumPaymentFormatted;
    additionalPayment;
    dueAtSale;
    // dueAtSaleFormatted;
    financeEligible;
    financeEligibleAmount;
    threePayEligible;
    showThreePayScreen;
    showFinancingScreen;
    monitoringFee;
    monthlyMonitoringFee;
    // monitoringFeeFormatted;
    easyPayEnroll;
    easyPayRequired;
    requireEasyPay;
    paymentTermsLabel;
    paymentTermsValue;
    invoice1;
    // invoice1Formatted;
    invoice2;
    // invoice2Formatted;
    invoice3;
    // invoice3Formatted;
    paymentIntervalValue;
    paymentIntervalLabel;
    paymentIntervalHeaderLabel = 'Monthly';
    riskGrade;
    amountToFinance;
    // amountToFinanceFormatted;
    // firstPaymentDueAtInstall; // this is sharing monthlyPayment
    monthlyPayment;
    // monthlyPaymentFormatted; 
    paymentTermsFormula;
    rentOrOwn;
    badRisks = ["N", "X"];
    installmentsLabel;
    installmentsValue;
    payingInFull;
    totalFeesTaxes;
    maxFinanceValue = 5000;
    minFinanceValue = 599;
    channelName = "/event/Remote_Pay_Verified__e";
    subscription = {};
    error = undefined;
    AccountId;
    installOptions = [];
    monitoringOptions = [];
    financeEligibility;
    disableSave = false;
    remainingMonths;
    financeVerbaige;
    remotePayCaptured = false;
    installMap = [];
    quoteStatus;
    // MParrella | SFCPQ-491: Variables for controlling payment instrument UI: START
    recurPayMethodSelected; // Holds the type of payment instrument selected for Monthly charges
    installPayMethodSelected; // Holds the type of payment instrument selected for Install charges
    ccInstallSelected = false; // Var to represent if CreditCard instrument is selected for Install charges
    ccRecurSelected = false; // Var to represent if CreditCard instrument is selected for Recur charges
    achInstallSelected = false; // Var to represent if ACH instrument is selected for Install charges
    achRecurSelected = false; // Var to represent if ACH instrument is selected for Recur charges
    showCCPaymentLWC = false; // Var to control showing the 'Goto Chase Paymentech' button
    showACHPaymentLWC = false; // Var to control showing the ACH Payment Input fields
    remotePaySelected = false; // Var to indicate that Remote Payment is selected
    ccAuthorized = false; // Var to represent if a Credit Card has been authorized via Paymentech HPP or not
    achAuthorized = false; // Var to represent if a Direct Deposit instrument has been validated via Giact or not
    disableCCButton = true; // Var to disable the 'Paymentech HPP' LWC, if ACH is selected and is not validated
    ccHelpText = 'Please enter ACH information first.'; // Text displayed beneath 'Goto Chase Paymentech' button when ACH is also selected
    showCCHelpText = false; // Var to show/not-show the CC help text
    achInfo; // Var to hold entered ACH information to be passed to Paymentech LWC for validating
    needsACHValidation = false; // Var to determine if we need to validate ACH info or not
    achData; // Var that holds successfully validated ach information
    ccData; // Var that holds successfully authorized cc information
    achFieldsValid = false; // Var to represent if form data is valid for ACH
    isWindowOpen = false; // Var to represent if the HPP portal has been launched or not
    showConfirmation = false; // Var to indicate if we should be showing a confirmation modal when Due At Sale price changes
    resetValues = false; // Var used in conjunction with confirmation modal
    prevAdditionalPayment; // used for restoring previous values when modal is canceled
    prevDueAtSale; // used for restoring previous values when modal is canceled
    tempAdditionalPayment;
    tempDueAtSale;
    cancelModalOpen = false; // Used to control visibility of confirmation modal when user cancels out of payment window
    showDisabledCCText = false; // Used to show help text when only CC is selected for Install and no Monthly is selected (or vice versa)
    disabledCCText = 'Please select a payment method for both Install and Monthly charges';
    showACHHelpText = false; // Used to show ACH Help text when all fields are valid and CC is selected for other charge
    achHelpText = 'Click \'Go to Chase Paymentech\' button';
    txComplete = false; // Used to indicate when a tx is completed and data can be committed to the DB (i.e. after ach and cc have been authorized)
    hasACHData = false; // Used to indicate when a transaction includes ACH payments (we only want to save payment info if both cc and ach auth successfully)
    achInfo = {}; // Object to hold the entered authorized ach information
    ccInfo = {}; // Object to hold the entered authorized cc information
    routingNumber; // Holds ACH Info
    accountNumber; // Holds ACH Info
    bankName; // Holds ACH Info
    accountName; // Holds ACH Info
    achType; // Holds ACH Info
    currUsr; // Holds current user information
    quoteData; // Holds CPQ Quote data
    // MParrella | SFCPQ-491: Variables for controlling payment instrument UI: STOP

    @wire(getRecord, { recordId: USER_ID, fields: USER_FIELDS })
    wireUser({ error, data }) {
        if(error) {
            this.error = error;
            this.showToast('Error getting data', error.message, 'error');
        } else if (data) {
            this.remoteUser = data.fields.NoDirectPayment__c.value;
            this.currUsr = data; // MParrella | SFCPQ-75
        }
    }

    @wire(getRecord, { recordId : '$recordId', fields: QUOTE_FIELDS })
    wireQuote({ error, data }){
        if(error){
            this.error = error;
            this.quoteData = null;
            this.showToast('Error getting record data', error.message, 'error');
        } else if(data){
            this.quoteData = data;
            this.totalInstallCharge = data.fields.Total_Installation__c.value;
            // this.totalInstallChargeFormatted = formatter.format(this.totalInstallCharge);
            this.minimumPayment = data.fields.Minimum_Payment__c.value;
            // this.minimumPaymentFormatted = formatter.format(this.minimumPayment);
            this.additionalPayment = this.handleNulls(data.fields.Additional_Payment__c.value, 'additionalPayment');
            // this.additionalPaymentFormatted = formatter.format(data.fields.Additional_Payment__c.value);
            this.dueAtSale = data.fields.Due_at_Sale__c.value;
            // this.dueAtSaleFormatted = formatter.format(this.dueAtSale);
            this.monitoringFee = data.fields.Monitoring_Fee__c.value;
            this.monthlyMonitoringFee = data.fields.Monthly_Monitoring_Fees__c.value;
            // this.monitoringFeeFormatted = formatter.format(this.monitoringFee);
            this.financeEligible = data.fields.Finance_Eligible_at_Submit__c.value;
            this.financeEligibleAmount = data.fields.Finance_Eligible_Amount__c.value;
            this.threePayEligible = data.fields.Three_Pay_Eligible_at_Submit__c.value;
            this.showThreePayScreen = data.fields.X3_Payment_Option__c.value;
            this.showFinancingScreen = data.fields.Financing__c.value;
            this.easyPayEnroll = data.fields.EasyPay_Enrollment__c.value;
            this.easyPayRequired = data.fields.EasyPay_Required__c.value;
            this.riskGrade = data.fields.Risk_Grade__c.value;            
            this.paymentTermsFormula = data.fields.Payment_Terms_Formula__c.value;
            this.paymentIntervalLabel = data.fields.Payment_Interval__c.value;
            this.rentOrOwn = data.fields.Profile_Rent_Own__c.value;
            this.monthlyPayment = data.fields.Monthly_Payment__c.value;
            this.installmentsLabel = data.fields.Installments__c.value !== null ? data.fields.Installments__c.value : this.setDefaultInstallmentsValue();
            // this.paymentTermsLabel = this.installmentsLabel; // data.fields.SBQQ__PaymentTerms__c.value;
            this.paymentTermsLabel = data.fields.SBQQ__PaymentTerms__c.value;
            this.setFinancingOrThreePay();
            //this.paymentTermsValue = use this.installmentsLabel or data.fields.SBQQ__PaymentTerms__c.value?
            this.totalFeesTaxes = data.fields.Total_Activation_and_Permit_Fees_w_Taxes__c.value;
            this.AccountId = data.fields.SBQQ__Account__c.value;
            this.quoteStatus = data.fields.SBQQ__Status__c.value;
            this.getPaymentRecords();
            this.setPayingInFull();
            if(this.installmentsLabel == 'Pay In Full'){
                this.payingInFull = true;
                this.showFinancingScreen = !this.payingInFull;
                this.showThreePayScreen = !this.payingInFull;
            }

            if((this.totalInstallCharge < this.minFinanceValue) && this.quoteStatus === 'Created'){
                this.showToast('error', 'This quote will become eligible for financing if the financeable total is increased to ' + formatter.format(this.minFinanceValue), 'info');
            }

            if(this.showThreePayScreen){
                this.setPaymentIntervalLabel('Monthly');
            } else if(this.paymentIntervalLabel != undefined){
                this.setPaymentIntervalLabel(this.paymentIntervalLabel);
            } // else {
            //     this.setPaymentIntervalLabel('Monthly');
            // }

            if(this.easyPayRequired){
                this.requireEasyPay = true;
            }
            
            this.handleCalculate();
            // this.checkEasyPay();               
        }
    }

    connectedCallback() {
      window.addEventListener('beforeunload', this.beforeUnloadHandler.bind(this));
    }

    beforeUnloadHandler(event){
        this.cancelModalOpen = true;
    }

    
    getPaymentRecords()
    {
//        getPaymentInfo({accountId: this.AccountId})
//        .then((result =>
//            {
//                if(result)
//                {
//                    this.installMap = result.map(element =>
//                    {
//                        if(element.Has_CC_Info__c == true)
//                        {
//                            return { label: 'CC' + ' - ' + '************' + element.CardLast4__c, value: 'option1', data: element };
//                        }
//                        else
//                        {
//                            return { label: 'ACH' + ' - ' + element.ACHAccount__c.replace(/\S(?=\S{4})/g, "*"), value: 'option1', data: element};
//                        }
//                    });
//                    if(!this.remoteUser)
//                    {
                        this.installMap.unshift({ label: 'Credit Card', value: 'option2' });
//                    }
//                    if(!this.remotePayCaptured)
//                    {
//                        this.installMap.push({ label: 'Remote Payment', value: 'option1' });
//                    }
                    this.installOptions =  this.installMap.slice();
                    this.monitoringOptions = this.installMap.slice();

//                }
//            }))
//        .catch((result =>
//            {
//                //console.log('Wiring payment info didnt work :/ ', result);
//            }))
        
    }      

    setDefaultInstallmentsValue(){
        if(this.badRisks.includes(this.riskGrade)){
            return '36 Months';
        } else if(this.rentOrOwn == 'R'){
            return '36 Months';
        } else if(this.rentOrOwn == 'O'){
            return '60 Months';
        }
    }

    setFinancingOrThreePay(){
        if(!this.showThreePayScreen && (this.totalInstallCharge > this.minFinanceValue) && this.financeEligible){
            switch(this.installmentsLabel){                    
                case 'Pay In Full':{
                    this.showThreePayScreen = false;
                    this.showFinancingScreen = false;
                    break;
                }
                case '3Pay':{
                    this.showThreePayScreen = true;
                    this.showFinancingScreen = false;
                    break;                        
                }
                case '12 Months':
                case '24 Months':
                case '36 Months':
                case '60 Months':{
                    this.showThreePayScreen = false;
                    this.showFinancingScreen = true;
                    break;
                }
            }
        }        
    }

    checkEasyPay(){ 
        let easyPayDefault = ["A", "B", "C", "N", "X"];
        if(this.easyPayRequired){
            this.easyPayEnroll = true;
            this.requireEasyPay = true;
        } else {
            if(this.showThreePayScreen || this.showFinancingScreen){ // include check for financing as well?
                this.easyPayEnroll = true;
                this.requireEasyPay = true;
            } else if(easyPayDefault.includes(this.riskGrade)){
                this.easyPayEnroll = true;
                this.requireEasyPay = false;
            }
        }
    }

    handleEasyPay(event){
        this.easyPayEnroll = event.detail.checked ? true : false;
    }

    resetFields(type){
        switch(type){
            case 'financing':
                let initialValueToFinance = this.totalInstallCharge - this.minimumPayment;
                this.monthlyPayment = '';
                this.amountToFinance = initialValueToFinance;
                this.dueAtSale = this.minimumPayment + this.additionalPayment;
                console.log('finanacing dueAtSale: ', this.dueAtSale);
            break;
            case 'threePay':
                this.dueAtSale = this.minimumPayment + this.additionalPayment;
                console.log('threePay dueAtSale: ', this.dueAtSale);
                this.invoice1 = '';
                this.invoice2 = '';
                this.invoice3 = '';
            break;
        }
    }

    handleNulls(value, field){
        if(value === '' || value === null){
            return 0;
        } else {
            return parseFloat(value);
        }
    }

    calculateMinimumPayment(){
        const maxValue = 100;
        if(!this.financeEligible){
            this.minimumPayment = this.totalInstallCharge;
        } else {
            if(!this.showThreePayScreen && !this.showFinancingScreen){
                if(this.totalInstallCharge >= maxValue && this.totalFeesTaxes < maxValue){
                    this.minimumPayment = maxValue;
                } else if(this.totalFeesTaxes > maxValue){
                    this.minimumPayment = this.totalFeesTaxes;
                }
            } else if(this.showFinancingScreen || this.showThreePayScreen){
                this.minimumPayment = this.totalFeesTaxes;
            } else {
                this.minimumPayment = this.totalInstallCharge;
            }
        }


            // if(this.showThreePayScreen || this.showFinancingScreen){
            //     this.minimumPayment =  this.totalFeesTaxes > maxValue ? maxValue : this.totalFeesTaxes;
            // } else if(!this.showFinancingScreen && !this.showThreePayScreen){
            //     this.minimumPayment = this.totalInstallCharge > maxValue ? maxValue : this.totalInstallCharge;
            // } else {
            //     this.minimumPayment = 123;
            // }

        //}
    }

    handleFinanceOverage(){
        let initialValue = this.totalInstallCharge - this.minimumPayment - this.maxFinanceValue;
        this.showToast('Deposit adjusted', 'The deposit has been adjusted to meet maximum financed amount of ' + formatter.format(this.maxFinanceValue), 'info');
        
        // let financeValue = this.totalInstallCharge - this.minimumPayment - this.additionalPayment;
        // if(financeValue > this.maxFinanceValue){
        //     this.showToast('error', 'test message', 'error');
        // } else {
            // this.additionalPayment = this.roundToNearestHundreth(this.amountToFinance - this.maxFinanceValue);
            this.additionalPayment = this.roundToNearestHundreth(initialValue);
            this.amountToFinance = this.maxFinanceValue;
            if (this.achInstallSelected && this.ccRecurSelected){
                this.dueAtSale = 2.00;
            } else {
                this.dueAtSale = this.minimumPayment + this.additionalPayment;
            }
            console.log('handleFinanceOverage dueAtSale: ', this.dueAtSale);
        // }
    }

    handleFinanceMinimum(){
        this.showToast('Deposit adjusted', 'The deposit has been adjusted to meet the minimum financed amount of ' + formatter.format(this.minFinanceValue), 'info');
        this.additionalPayment = this.roundToNearestHundreth(this.totalInstallCharge - this.minFinanceValue - this.minimumPayment);
        this.amountToFinance = this.minFinanceValue;
        if (this.achInstallSelected && this.ccRecurSelected){
            this.dueAtSale = 2.00;
        } else {
            this.dueAtSale = this.minimumPayment + this.additionalPayment;
        }
        console.log('finance min: ', this.dueAtSale);
    }

    checkFinance(){
        if(this.financeEligible && (this.financeEligibleAmount >= this.minFinanceValue)){
            this.financeEligibility = true;
        } else {            
            this.financeEligibility = false;
        }
    }

    /* MParrella | SFCPQ-56: Adding logic when 'Due At Sale' changes to check if HPP window is already open, if so -> display confirmation: START */
    handleSalePriceChange() {
        if (this.isWindowOpen){
            // Show confirmation modal
            this.showConfirmation = true;
        }
    }
    /* MParrella | SFCPQ-56: Adding logic when 'Due At Sale' changes to check if HPP window is already open, if so -> display confirmation: STOP */

    /* MParrella | SFCPQ-491: Adding event handling to make Giact callout: START */
    handleValidateACH(event) {
        // Call Giact
         this.template.querySelector('c-ach-validation').isLoading = true;
         this.template.querySelector('c-ach-validation').submitForm();
    }
    /* MParrella | SFCPQ-491: Adding event handling to make Giact callout: STOP */

    handleCalculate(){
        if(this.additionalPayment < 0){
            this.showToast('error', 'Negative amount not allowed', 'error');
            this.additionalPayment = '';
        }
        this.setPaymentIntervalValue();
        this.setInstallmentsValue();
        this.checkEasyPay();
        this.calculateMinimumPayment();
        this.checkFinance();

        if (this.achInstallSelected && this.ccRecurSelected){
            this.dueAtSale = 2.00;
        } else {
            this.dueAtSale = this.minimumPayment + this.additionalPayment;
        }
        console.log('handleCalc dueAtSale: ', this.dueAtSale);
        this.amountToFinance = this.totalInstallCharge - this.minimumPayment - this.additionalPayment;

        if(this.showFinancingScreen){
            if(this.amountToFinance < this.minFinanceValue){
                this.handleFinanceMinimum();
            }
            
            if(this.amountToFinance > this.maxFinanceValue){
                this.handleFinanceOverage();
            }
            this.monthlyPayment = this.roundToNearestHundreth((this.totalInstallCharge - this.minimumPayment - this.additionalPayment) / this.installmentsValue);

        } 
        this.remainingMonths = (this.installmentsValue - 1);
        this.financeVerbaige = this.showFinancingScreen ? '$' + this.monthlyPayment.toFixed(2) + ' due at install, then $' + this.monthlyPayment.toFixed(2) + ' monthly for the remaining ' + this.remainingMonths + ' months.' : '';       

        if(this.showThreePayScreen){
            this.additionalPayment = '';
            this.calculateThreePay();
        }        
        this.disableSave = false;
    }

    setInstallmentsValue(){
        switch(this.installmentsLabel){
            case '12 Months':
                this.installmentsValue = 12;
                this.paymentTermsValue = 12;
            break;
            case '24 Months':
                this.installmentsValue = 24;
                this.paymentTermsValue = 24;
            break;
            case '36 Months':
                this.installmentsValue = 36;
                this.paymentTermsValue = 36;
            break;
            case '60 Months':
                this.installmentsValue = 60;
                this.paymentTermsValue = 60;
            break;
            // case 'Pay In Full': // eliminated hardcoding value for PIF here, delegating to default term logic
            //     this.installmentsLabel = '36 Months';
            //     this.paymentTermsLabel = '36 Months';
            //     this.installmentsValue = 36;
            //     this.paymentTermsValue = 36;
            // break;
            case '3Pay':
                this.installmentsValue = 0;
                this.paymentTermsValue = 0;
            break;
            case null:
                this.installmentsValue = 0;
                this.paymentTermsValue = 0;
            break;
        }
    }

    setPaymentIntervalValue(){
        switch(this.paymentIntervalLabel){
            case 'Monthly':
                this.paymentIntervalValue = 1;
            break;
            case 'Quarterly':
                this.paymentIntervalValue = 3;
            break;
            case 'Semi-Annually':
                this.paymentIntervalValue = 6;
            break;
            case 'Annually':
                this.paymentIntervalValue = 12;
            break;
        }
    }

    handleSaveAndClose(){
        if((this.amountToFinance < this.minFinanceValue) && this.showFinancingScreen){
            this.showToast('alert', 'This quote will become eligible for financing if the financeable total is increased to ' + formatter.format(this.minFinanceValue), 'error');
        } else {
            /* MParrella | SFCPQ-491: Adding logic to handle different payment scenarios: START */

            // Scenario: No payment instruments selected for Install or Monthly charges
            if (!this.showCCPaymentLWC && !this.showACHPaymentLWC && !this.remotePaySelected){
                this.showToast('', 'Select a payment method for installation charge and Monthly service charge', 'error');
            }
            // Scenario: Install instrument selected, but no Monthly selected
            else if((this.ccInstallSelected || this.achInstallSelected || this.remotePaySelected) && (!this.ccRecurSelected && !this.achRecurSelected)){
                this.showToast('', 'Select a payment method for installation charge and Monthly service charge', 'error');
            }
            // Scenario: No Install instrument selected, but Monthly selected
            else if ((!this.ccInstallSelected && !this.achInstallSelected && !this.remotePaySelected) && (this.ccRecurSelected || this.achRecurSelected)){
                this.showToast('', 'Select a payment method for installation charge and Monthly service charge', 'error');
            }
            // Scenario: Only CC is selected for both Install and Monthly, but has not been authorized yet
            else if (this.showCCPaymentLWC && !this.ccAuthorized && !this.showACHPaymentLWC){
                this.showToast('', 'Click \'Goto Chase Paymentech\' button to authorize Credit Card', 'warning');
            }
            // Scenario: ACH is selected for both, but all fields are not populated
            else if (!this.showCCPaymentLWC && this.showACHPaymentLWC && !this.achFieldsValid){
                this.showToast('', 'All fields are required and must be completed', 'error');
            }
            // Scenario: ACH is selected but not validated, and CC has not been authorized yet
            else if (this.showCCPaymentLWC && !this.ccAuthorized && this.showACHPaymentLWC && !this.achFieldsValid ){
                this.showToast('', 'Enter details of the Direct Deposit', 'error');
            }
            // Scenario: ACH is selected and fields are populated, CC is selected but not authorized
            else if (this.showCCPaymentLWC && this.showACHPaymentLWC && this.achFieldsValid && !this.ccAuthorized){
                this.showToast('', 'Click \'Goto Chase Paymentech\' button to authorize Credit Card', 'warning');
            }
            /* MParrella | SFCPQ-491: Adding logic to handle different payment scenarios: STOP */

            // Determine the payment scenario we're dealing with (CC/ACH, ACH-only, RemotePay(TBD))
            /* ACH-ONLY: START */
            // Scenario: ACH is selected, so call 'submit form' function from within the achValidation LWC
            else if (this.showACHPaymentLWC && this.achFieldsValid) {
                this.template.querySelector('c-ach-validation').submitForm();
            }
            /* ACH-ONLY: STOP */

            /* CC/ACH Combo: START */
            if (this.showCCPaymentLWC && this.showACHPaymentLWC && this.ccAuthorized && this.achAuthorized){

            }
            /* CC/ACH Combo: STOP */



        }
    }

    /* MParrella | SFCPQ-56: Adding function to handle the event of the HPP window opening: START */
    handleWindowOpen(){
        this.isWindowOpen = true;
    }
    /* MParrella | SFCPQ-56: Adding function to handle the event of the HPP window opening: STOP */

    calculateThreePay(){
        // let total = this.amountToFinance; // != undefined ? this.amountToFinance : this.totalInstallCharge;
        // let oneThird = Math.floor(total / 3);
        // let oneThird = this.amountToFinance / 3;
        let oneThird = parseFloat(this.roundToNearestHundreth(this.amountToFinance / 3));
        this.invoice1 = oneThird;
        this.invoice2 = oneThird;
        // this.invoice3 = total - (oneThird * 2);
        this.invoice3 = this.roundToNearestHundreth(this.amountToFinance - (oneThird + oneThird));
    }

    roundToNearestHundreth(value) {
        return parseFloat(value.toFixed(2));
      }

    /*
    get installOptions() {
        
        if(this.remoteUser){
            return [
                { label: 'Remote Payment', value: 'option1' }
            ];
        } else {
            return [
                { label: 'Remote Payment', value: 'option1' },
                { label: 'Credit Card', value: 'option2' },
                { label: 'Direct Deposit (ACH)', value: 'option3' },
            ];
        }
          
                        
    }*/
    /*
    get monitoringOptions() {

        if(this.remoteUser){
            return [
                { label: 'Remote Payment', value: 'option1' }
            ];
        } else {
            return [
                { label: 'Remote Payment', value: 'option1' },
                { label: 'Credit Card', value: 'option2' },
                { label: 'Direct Deposit (ACH)', value: 'option3' },
            ];
        }        
    }
    */
    get paymentTermsOptions(){
        if(this.badRisks.includes(this.riskGrade)){
            return [
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' }
            ]
        } else 
        if(this.rentOrOwn == 'R'){
            return [ 
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' }
                // { label: '48 Months', value: '48' }
            ]
        } else if(this.rentOrOwn == 'O'){
            return [ 
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' },
                // { label: '48 Months', value: '48' },
                { label: '60 Months', value: '60 Months' }
            ]
        }
        
    }

    get installmentsOptions(){
        if(this.badRisks.includes(this.riskGrade)){
            return [
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' }
            ]
        } else {
            return [ 
                // { label: 'Pay In Full', value: 'Pay In Full' },
                // { label: '3Pay', value: '3Pay' },
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' },
                { label: '60 Months', value: '60 Months' }
            ]  
        }
              
    }

    get paymentIntervalOptions(){
        const goodRisk = ["A", "B", "C", "D", "N", "Q", "U", "X"];
        const badRisk = ["E", "F", "G", "J", "W", "Y"];

        if(this.showFinancingScreen){
            return [
                { label: 'Monthly', value: 'Monthly' }
            ]
        } else {
            if(goodRisk.includes(this.riskGrade)){
                return [
                    { label: 'Monthly', value: 'Monthly' },
                    { label: 'Quarterly', value: 'Quarterly' },
                    { label: 'Semi-Annually', value: 'Semi-Annually' },
                    { label: 'Annually', value: 'Annually' }
                ]
            } else if(badRisk.includes(this.riskGrade)){
                return [
                    { label: 'Annually', value: 'Annually' }
                ]
            }
        }
    }

    handleInstallChannelChange(event) {
        const selectedOption = event.detail.value;
        this.achInstallSelected = false; // MParrella | SFCPQ-491: Reset the variable
        this.ccInstallSelected = false; // MParrella | SFCPQ-491: Reset the variable
        this.remotePaySelected = false; // MParrella | SFCPQ-491: Reset the variable
        this.disableCCButton = true; // MParrella | SFCPQ-491: Reset the variable
        this.showCCHelpText = false; // MParrella | SFCPQ-491: Reset the variable
        this.needsACHValidation = false; // MParrella | SFCPQ-491: Reset the variable
        this.showDisabledCCText = false; // MParrella | SFCPQ-491: Reset the variable
        this.type = 'install';

        switch(selectedOption){
            case 'option1':
                this.showInstallPaymentScreen = false;
                this.installAchValidation = false;
                this.remotePaySelected = true; // MParrella | SFCPQ-491
            break;
            case 'option2':
                this.showSecondGrid = true;
                this.showInstallPaymentScreen = true;
                this.installAchValidation = false;
                this.installCCPayment = true;
                this.ccInstallSelected = true; // MParrella | SFCPQ-491
                if (this.ccRecurSelected){
                    this.disableCCButton = false;
                }
            break;
            case 'option3':                
                this.showSecondGrid = true;
                this.showInstallPaymentScreen = true;
                this.installAchValidation = true;
                this.installCCPayment = false;
                this.achInstallSelected = true; // MParrella | SFCPQ-491
                this.disableCCButton = true; // MParrella | SFCPQ-491
            break;
        }

        // MParrella | SFCPQ-56: Showing help text if CC is selected and there is no other payment method selected yet: START
        if ((this.ccInstallSelected && !this.ccRecurSelected && !this.achRecurSelected) || (this.ccRecurSelected && !this.ccInstallSelected && !this.achInstallSelected)){
            this.showDisabledCCText = true;
        } else {
            this.showDisabledCCText = false;
        }
        // MParrella | SFCPQ-56: Showing help text if CC is selected and there is no other payment method selected yet: STOP

        this.showCCPaymentLWC = this.ccInstallSelected || this.ccRecurSelected; // MParrella | SFCPQ-491
        this.showACHPaymentLWC = this.achRecurSelected || this.achInstallSelected; // MParrella | SFCPQ-491
        this.showMonitoringPaymentScreen = this.showACHPaymentLWC; // MParrella | SFCPQ-491
        this.showCCHelpText = this.disableCCButton && this.showCCPaymentLWC && this.showACHPaymentLWC; // MParrella | SFCPQ-491
        this.needsACHValidation = this.showACHPaymentLWC && this.showCCPaymentLWC; // MParrella | SFCPQ-491
   }

    handleMonitoringChannelChange(event) {
        const selectedOption = event.detail.value;
        this.type = 'monitoring';
        this.showInstallPaymentScreen = true; // MParrella | SFCPQ-491: Setting to true so we can display LWCs if only Monthly is selected
        this.achRecurSelected = false; // MParrella | SFCPQ-491: Reset the variable
        this.ccRecurSelected = false; // MParrella | SFCPQ-491: Reset the variable
        this.remotePaySelected = false; // MParrella | SFCPQ-491: Reset the variable
        this.disableCCButton = true; // MParrella | SFCPQ-491: Reset the variable
        this.showCCHelpText = false; // MParrella | SFCPQ-491: Reset the variable
        this.needsACHValidation = false; // MParrella | SFCPQ-491: Reset the variable

        switch(selectedOption){
            case 'option1':                
                this.showMonitoringPaymentScreen = false;
                this.monitoringAchValidation = false;
                this.monitoringCCPayment = false;
                this.remotePaySelected = true;
            break;
            case 'option2':
                this.showSecondGrid = true;
                this.showMonitoringPaymentScreen = true;
                this.monitoringAchValidation = false;
                this.monitoringCCPayment = true;
                this.ccRecurSelected = true; // MParrella | SFCPQ-491
                if (this.ccInstallSelected) { // MParrella | SFCPQ-491:
                    this.disableCCButton = false; // MParrella | SFCPQ-491:
                }
            break;
            case 'option3':
                this.showSecondGrid = true;
                this.showMonitoringPaymentScreen = true;
                this.monitoringAchValidation = true;
                this.monitoringCCPayment = false;
                this.achRecurSelected = true; // MParrella | SFCPQ-491
                this.disableCCButton = true; // MParrella | SFCPQ-491
            break;
        }

        // MParrella | SFCPQ-56: Showing help text if CC is selected and there is no other payment method selected yet: START
        if ((this.ccInstallSelected && !this.ccRecurSelected && !this.achRecurSelected && !this.remotePaySelected) || (this.ccRecurSelected && !this.ccInstallSelected && !this.achInstallSelected && !this.remotePaySelected)){
            this.showDisabledCCText = true;
        } else {
            this.showDisabledCCText = false;
        }
        // MParrella | SFCPQ-56: Showing help text if CC is selected and there is no other payment method selected yet: STOP
        this.showCCPaymentLWC = this.ccInstallSelected || this.ccRecurSelected; // MParrella | SFCPQ-491
        this.showACHPaymentLWC = this.achRecurSelected || this.achInstallSelected; // MParrella | SFCPQ-491
        this.showMonitoringPaymentScreen = this.showACHPaymentLWC; // MParrella | SFCPQ-491
        this.showCCHelpText = this.disableCCButton && this.showCCPaymentLWC && this.showACHPaymentLWC; // MParrella | SFCPQ-491
        this.needsACHValidation = this.showACHPaymentLWC && this.showCCPaymentLWC; // MParrella | SFCPQ-491
    }

    /* MParrela | SFCPq-491: Adding function to handle the failure of giact validation: START */
    handleValidateFailure() {
        if (this.showCCPaymentLWC){
            this.template.querySelector('c-cc_payment-H-P-P').reset();
        }
    }
    /* MParrela | SFCPq-491: Adding function to handle the failure of giact validation: STOP */

    /* MParrella | SFCPQ-491: Adding function to handle the successful validation of ACH: START */
    handleValidateSuccess(event) {
        this.achData = JSON.parse(JSON.stringify(event.detail));
        this.achAuthorized = true;
        this.achData.fieldData = this.achInfo;
        console.log(this.achData);
        this.disableSave = true;
        this.showACHHelpText = false;
        this.showCCHelpText = false;
        // If only ACH is selected, the transaction will be complete
        if (this.showACHPaymentLWC && this.achAuthorized && !this.showCCPaymentLWC){
            this.hasACHData = false;
            this.txComplete = true;
        }
        else if ( this.showACHPaymentLWC && this.achAuthorized && this.showCCPaymentLWC){
            this.hasACHData = true;
            this.txComplete = false; // still need to auth the CC
        }
        // Call 'getUID' function within the paymentech LWC if CC is selected
        if (this.showCCPaymentLWC){
            if (this.ccRecurSelected && !this.ccInstallSelected){
                this.dueAtSale = 2.00;
            }
            this.template.querySelector('c-cc_payment-H-P-P').getUID();
        }
        if (this.txComplete){
            this.saveData();
        }
    }
    /* MParrella | SFCPQ-491: Adding function to handle the successful validation of ACH: STOP */

    // MParrella | SFCPQ-491: Adding function to enable/disable the CC Paymentech LWC button: START
    handleValidFields(event) {
        if (event.detail.split('|')[0] === 'true'){
            this.achFieldsValid = true;
            let fieldsPayload = event.detail.split('|');
            // Assign the entered field data
            this.achInfo.routingNumber = fieldsPayload[2];
            this.achInfo.accountNumber = fieldsPayload[1];
            this.achInfo.bankName = fieldsPayload[3];
            this.achInfo.accountName = fieldsPayload[5];
            this.achInfo.achType = fieldsPayload[4];
            this.disableCCButton = false;
            this.showCCHelpText = false;
            this.showACHHelpText = true;
            if (this.showCCPaymentLWC){
                this.achHelpText = 'Click \'Go to Chase Paymentech\' button';
            } else {
                this.achHelpText = 'Click \'Save & Close\' button to continue';
            }
        } else {
            this.disableCCButton = true;
            this.achFieldsValid = false;
            this.showACHHelpText = false;
            if (this.showCCPaymentLWC){
                this.showCCHelpText = true;
            }
        }
    }
    // MParrella | SFCPQ-491: Adding function to enable/disable the CC Paymentech LWC button: STOP

    // MParrella | SFCPQ-491: Adding function to get CC transaction data: START
    handleCCSuccess(event) {
        this.ccData = JSON.parse(JSON.stringify(event.detail));
        this.ccAuthorized = true;
        this.disableSave = true;
        if (this.showACHPaymentLWC && this.achAuthorized && this.ccAuthorized) {
            this.txComplete = true;
            this.hasACHData = true;
        }
        else if (this.showCCPaymentLWC && this.ccAuthorized && !this.showACHPaymentLWC){
            this.txComplete = true;
        }
        if (this.txComplete === true){
            this.saveData();
            let installmentsValue;
            if (this.threePay){
                installmentsValue = '3Pay';
            }
            else if (this.payingInFull){
                installmentsValue = 'Pay In Full';
            }
            else {
                installmentsValue = this.installmentsLabel;
            }
            this.dispatchEvent(new CustomEvent('txcomplete', { detail: installmentsValue }));
        }
    }
    // MParrella | SFCPQ-491: Adding function to get CC transaction data: STOP

    financingHandleChange(event){
        // this.additionalPaymentLogic();
        this.paymentIntervalHeaderLabel = 'Monthly';
        this.showThreePayScreen = false;
        // if(this.payingInFull){
            // this.showToast('error', 'customer is paying in full, financing or three pay not allowed', 'error');
        // } else {
            if(event.detail.checked){
                this.showFinancingScreen = true;
                this.paymentIntervalLabel = 'Monthly';
                this.payingInFull = false;
                this.installmentsLabel = this.paymentTermsLabel != null ? this.paymentTermsLabel : this.installmentsLabel;
                this.resetFields('threePay');
            } else {
                this.showFinancingScreen = false;
                this.paymentInterval = '';
                // this.additionalPayment = 0;
                this.setPayingInFull();
                this.resetFields('finance');
            }
        // }   
        this.handleCalculate();          
    }

    threePayHandleChange(event){
        // if(this.payingInFull){
            // this.showToast('error', 'customer is paying in full, financing or three pay not allowed', 'error');
        // } else {
            if(event.detail.checked){
                this.additionalPayment = '';
                this.showFinancingScreen = false;
                this.showThreePayScreen = true;
                this.payingInFull = false;
                this.paymentIntervalLabel = 'Monthly';
                this.resetFields('financing');
            } else {
                this.showThreePayScreen = false;
                this.setPayingInFull();
                this.resetFields('threepay');
            }
            
            this.handleCalculate();
            this.setPaymentIntervalLabel(this.paymentIntervalLabel);
        // }        
    }

    setPayingInFull(){
        if(!this.showThreePayScreen && !this.showFinancingScreen){
            console.log('financing and 3pay not selected, paying in full');
            this.payingInFull = true;
            this.installmentsLabel = 'Pay In Full';
        } else {
            console.log('financing or three pay selected, not paying in full');
            console.log('this.showFinancingScreen: ' + this.showFinancingScreen);
            console.log('this.showThreePayScreen: ' + this.showThreePayScreen);
        }
        
    }

    handlePaymentTermsChange(event){
        // this.paymentTermsLabel = event.detail.value;
        // this.handleCalculate();
        this.handleInstallmentsChange(event);
    }

    handleInstallmentsChange(event){
        // this.payingInFull = 'Pay In Full' ? true : false;
        this.installmentsLabel = event.detail.value;
        this.paymentTermsLabel = event.detail.value;
        this.handleCalculate();
    }

    handlePaymentIntervalChange(event){
        this.setPaymentIntervalLabel(event.detail.value);                
    }

    setPaymentIntervalLabel(value){
        switch(value){
            case 'Monthly':                
                this.paymentIntervalLabel = 'Monthly';
                this.paymentIntervalValue = '1';
                this.paymentIntervalHeaderLabel = 'Monthly';
            break;
            case 'Quarterly':
                this.paymentIntervalLabel = 'Quarterly';
                this.paymentIntervalValue = '3';
                this.paymentIntervalHeaderLabel = 'Quarterly';
            break;
            case 'Semi-Annually':
                this.paymentIntervalLabel = 'Semi-Annually';
                this.paymentIntervalValue = '6';
                this.paymentIntervalHeaderLabel = 'Semi-Annual';
            break;
            case 'Annually':
                this.paymentIntervalLabel = 'Annually';
                this.paymentIntervalValue = '12';
                this.paymentIntervalHeaderLabel = 'Annual';
            break;
        }
        this.monitoringFee = this.monthlyMonitoringFee * this.paymentIntervalValue;
        // this.monitoringFeeFormatted = formatter.format(this.monitoringFee);
    }

    handleAdditionalPaymentChange(event){
//        this.additionalPayment = this.handleNulls(event.detail.value, 'additionalPayment');
        this.tempAdditionalPayment = this.handleNulls(event.detail.value, 'additionalPayment'); // MParrella | SFCPQ-56
    }

    handleKeyPress(event){
        if(event.which == 13 || event.which == 9){
            // MParrella | SFCPQ-56: Storing old values so we can revert if they click 'cancel' on the modal
            this.prevAdditionalPayment = this.additionalPayment; // MParrella | SFCPQ-56
            this.prevDueAtSale = this.dueAtSale; // MParrella | SFCPQ-56
            this.additionalPayment = this.tempAdditionalPayment; // MParrella | SFCPQ-56
            this.additionalPaymentLogic();
        }
    }

    focusOut(){
//        this.prevAdditionalPayment = this.additionalPayment;
//        this.prevDueAtSale = this.dueAtSale;
//        this.additionalPayment = this.tempAdditionalPayment;
            // MParrella | SFCPQ-56: Storing old values so we can revert if they click 'cancel' on the modal
        this.additionalPayment = this.tempAdditionalPayment; // MParrella | SFCPQ-56
        this.additionalPaymentLogic();
    }

    additionalPaymentLogic(){
        let financeValue = this.totalInstallCharge - this.minimumPayment - this.additionalPayment;

        if(this.installmentsValue == undefined && this.showFinancingScreen){
            this.showToast('error', 'please select installments value before calculating a financed value', 'error');
            this.resetFields('financing');
        } else if(this.additionalPayment > (this.totalInstallCharge - this.minimumPayment)){
            this.showToast('error', 'Additional Payment must be less than Total Install Charge', 'error');
            this.additionalPayment = (this.totalInstallCharge - this.minimumPayment);
            this.handleCalculate();
        } else if(this.showFinancingScreen && financeValue > this.maxFinanceValue){
            this.resetAdditionalPayment();
        } else {
            this.handleCalculate();
        }

        // MParrella | SFCPQ-56: Checking if we should show confirmation
        if (this.isWindowOpen){
            this.showConfirmation = true;
        }
    }

    resetAdditionalPayment(){
        let initialValue = this.totalInstallCharge - this.minimumPayment - this.maxFinanceValue;

        this.disableSave = true;
        this.showToast('Additional Payment amount too low for financing', 'Minimum Additional Payment amount is ' + formatter.format(initialValue) + ', please enter higher amount.', 'error');
        this.additionalPayment = this.roundToNearestHundreth(initialValue);
        this.amountToFinance = this.maxFinanceValue;
        this.handleCalculate();
        this.dueAtSale = this.minimumPayment + this.additionalPayment;
    }

    showToast(title, message, variant){
        this.dispatchEvent(new ShowToastEvent({
            title, message, variant
        }));
    }

    /* MParrella | SFCPQ-56: Modal controls: START */
    closeModal(event){
        console.log(event);
        // Check which button was pressed
        if (event){
            if (event.target.dataset.id === 'cancel'){
                this.resetValues = true;
            }
        } else {
            this.resetValues = false;
        }
        if (this.resetValues){
            this.additionalPayment = this.prevAdditionalPayment;
            this.dueAtSale = this.prevDueAtSale;
        }
        this.showConfirmation = false;
    }

    submitDetails(){
      this.closeModal();
      this.template.querySelector('c-cc_payment-H-P-P').closeHPPWindow();
      this.isWindowOpen = false;
    }
    /* MParrella | SFCPQ-56: Modal controls: STOP */

    /* MParrella | SFCPQ-56: Adding function to handle when HPP window is closed via 'cancel' button: START */
    handleWindowClose() {
        this.isWindowOpen = false;
    }
    /* MParrella | SFCPQ-56: Adding function to handle when HPP window is closed via 'cancel' button: STOP */

    connectedCallback()
    {
        this.handleSubscribe();
    }
    
    disconnectedCallback()
    {
        this.handleUnsubscribe();
    }

    handleSubscribe() 
    {
        //Workaround for passing 'this' into messageCallback
        let self = this;
        //refresh options on receipt of platform event associated to this quote, show toast indicating successful capture
        const messageCallback = (response) => 
        {
            self.showToast('Success', 'Payment method captured!', 'success');
            self.remotePayCaptured = true;
//            self.getPaymentRecords();
            // Response contains the payload of the new message received
        }
        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.channelName, -1, messageCallback).then(response => {
            this.subscription = response;
        });
    }

    handleUnsubscribe() 
    {
        // Invoke unsubscribe method of empApi
        unsubscribe(this.subscription, response => {
            // Response is true for successful unsubscribe
        });
    }

    showCancelModal() {
        this.cancelModalOpen = true;
    }

    confirmCancel() {
        this.dispatchEvent(new CustomEvent('cancelconfirm'));
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    closeCancelModal(){
        this.cancelModalOpen = false;
    }

    saveData() {
        if (this.hasACHData){
            this.savePaymentMethodApex(this.ccData, this.achData)
        } else {
            this.savePaymentMethodApex(this.ccData, null);
        }
    }

    savePaymentMethodApex(ccInfo, achInfo){
        savePaymentMethod({
            recordId: this.recordId,
            ccJSONString: ccInfo != null ? JSON.stringify(this.ccData) : null,
            achJSONString: achInfo != null ? JSON.stringify(this.achData) : null
        })
        .then(result => {
            if (result.isSuccess){
                // update the quote record fields
                const fields = {};
                fields[ID_FIELD.fieldApiName] = this.recordId;
                fields[FINANCING.fieldApiName] = this.showFinancingScreen;
                fields[THREEPAY.fieldApiName] = this.showThreePayScreen;
                fields[INVOICE1.fieldApiName] = this.invoice1;
                fields[INVOICE2.fieldApiName] = this.invoice2;
                fields[INVOICE3.fieldApiName] = this.invoice3;
                fields[ADDITIONAL_PAYMENT.fieldApiName] = this.additionalPayment;
                fields[MONITORING_FEE.fieldApiName] = this.monitoringFee;
                fields[PAYMENT_INTERVAL.fieldApiName] = this.paymentIntervalLabel;
                fields[MONTHLY_FEE.fieldApiName] = this.monthlyPayment;
                fields[FIRST_PAYMENT_DUE.fieldApiName] = this.monthlyPayment;
                fields[PAYMENT_TERMS.fieldApiName] = this.paymentTermsLabel;
                let installmentsValue;
                if (this.showThreePayScreen){
                    installmentsValue = '3Pay';
                }
                else if (this.payingInFull){
                    installmentsValue = 'Pay In Full';
                }
                else {
                    installmentsValue = this.installmentsLabel;
                }
                fields[INSTALLMENTS.fieldApiName] = installmentsValue;
                fields[EASY_PAY_ENROLL.fieldApiName] = this.easyPayEnroll;
                // Update payment fields
                fields[HAS_CC_INFO.fieldApiName] = (this.ccInstallSelected || this.ccRecurSelected);
                fields[HAS_ACH_INFO.fieldApiName] = (this.achInstallSelected || this.achRecurSelected);
                let amount = 0;
                if (this.ccData){
                    if (this.ccData.totalAmt != 200) {
                        amount = this.ccData.totalAmt;
                    }
                }
                fields[DEPOSIT_PAID.fieldApiName] = parseFloat(amount / 100);
                fields[INSTALL_PAY_METHOD_FIELD.fieldApiName] = this.achInstallSelected ? 'Direct Deposit (ACH)' : 'Credit Card';
                fields[RECUR_PAY_METHOD_FIELD.fieldApiName] = this.ccRecurSelected ? 'Credit Card' : 'Direct Deposit (ACH)';
                fields[BANK_ACCOUNT_NAME_FIELD.fieldApiName] = this.achData ? this.achData.fieldData.bankName : '';
                fields[BANK_ACCOUNT_NUM_FIELD.fieldApiName] = this.achData ? this.achData.fieldData.accountNumber : '';
                fields[ACH_NAME_ON_ACCT_FIELD.fieldApiName] = this.achData ? this.achData.fieldData.accountName : '' ;
                fields[ROUTING_NUM_FIELD.fieldApiName] = this.achData ? this.achData.fieldData.routingNumber : '';
                fields[GIACT_TOKEN_FIELD.fieldApiName] = this.achData ? this.achData.itemReferenceId : '';
                fields[AMOUNT_AUTHED_FIELD.fieldApiName] = this.ccData ? this.ccData.totalAmt / 100 : 0.00;
                fields[PAYMENTECH_GUID_FIELD.fieldApiName] = this.ccData ? this.ccData.txnGUID : '';
                fields[PAYMENTECH_REF_NUM_FIELD.fieldApiName] = this.ccData ? this.ccData.customerRefNum : '';
                // Card type extraction
                let cardName = this.ccData.type;
                if (cardName.indexOf('+') !== -1){
                    cardName = cardName.replace('+', ' ');
                }

                fields[CARD_TYPE_FIELD.fieldApiName] = cardName;
                fields[EXP_DATE_FIELD.fieldApiName] = this.ccData ? this.ccData.exp : '';

                // Get Last 4 of CC
                let returnedNum = this.ccData.mPAN;
                let lastFour =  returnedNum.substring(returnedNum.length - 4, returnedNum.length);

                fields[LAST_FOUR_FIELD.fieldApiName] = lastFour;
                fields[CC_NAME_ON_ACCT_FIELD.fieldApiName] = this.ccData ? this.ccData.customerFirstName + ' ' + this.ccData.customerLastName : '';
                fields[CC_INSTALL_METHOD_FIELD.fieldApiName] = this.ccInstallSelected;
                fields[ACH_INSTALL_METHOD_FIELD.fieldApiName] = this.achInstallSelected;
                fields[QUOTE_STATUS_FIELD.fieldApiName] = 'Paid';
                let todaysDate = new Date();
                let newTodaysDate = todaysDate.toISOString();
                fields[TX_TIMESTAMP_FIELD.fieldApiName] = newTodaysDate;
                /* MParrella | Commissionable Rep (SFCPQ-75) Field Updates: START */
                // Determine user type
                let usrType;
                let userProfileName = this.currUsr.fields.Profile.displayValue;
                if (userProfileName.indexOf('NSC') !== -1) { usrType = 'Phone'; }
                else if (userProfileName.indexOf('NA') !== -1) { usrType = 'Field'; }
                else if (userProfileName.indexOf('Admin') !== -1) { usrType = 'Admin'; }
                else if (userProfileName.indexOf('Integration User') !== -1) { usrType = 'Admin'; }
                else { usrType = '';}

                // Determine HRID Value
                let hrId = this.currUsr.fields.EmployeeNumber__c.value;
                if (userProfileName.indexOf('ADT NSC Sales') !== -1){
                    if (!hrId) {
                        hrId = 'TMK';
                    }
                }
                fields[COMM_REP_HRID_FIELD.fieldApiName] = hrId;
                fields[COMM_REP_TYPE_FIELD.fieldApiName] = usrType;
                fields[COMM_REP_NAME_FIELD.fieldApiName] = this.currUsr.fields.Name.value;
                fields[COMM_REP_PHONE_FIELD.fieldApiName] = this.currUsr.fields.MobilePhone.value;
                fields[COMM_REP_USR_FIELD.fieldApiName] = USER_ID;
                /* MParrella | Commissionable Rep (SFCPQ-75) Field Updates: STOP */
                const recordInput = { fields };
                updateRecord(recordInput)
                .then(()=>{
                    this.showToast('', 'Payment Information Saved', 'success');
                    let installmentsValue;
                    if (this.showThreePayScreen){
                        installmentsValue = '3Pay';
                    }
                    else if (this.payingInFull){
                        installmentsValue = 'Pay In Full';
                    }
                    else {
                        installmentsValue = this.installmentsLabel;
                    }
                    this.dispatchEvent(new CustomEvent('txcomplete', { detail: installmentsValue }));
                    this.dispatchEvent(new CloseActionScreenEvent());
                }).catch((error)=>{
                    console.log(error);
                    this.showToast('error', error.body.message, 'error');
                });
            } else {
                this.showToast('', result.errorMsg, 'error');
            }
        })
        .catch(error => {
            console.log(error);
        });
    }
    
}