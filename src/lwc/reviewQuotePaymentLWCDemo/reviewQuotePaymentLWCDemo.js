import { LightningElement, api, wire } from 'lwc';
import { getRecord, updateRecord } from 'lightning/uiRecordApi';
import { CloseActionScreenEvent } from 'lightning/actions';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import USER_ID from '@salesforce/user/Id';
import NO_DIRECT_PAYMENT from '@salesforce/schema/User.NoDirectPayment__c';
import ID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Id';
import TOTAL_INSTALL_CHARGE from '@salesforce/schema/SBQQ__Quote__c.Total_Installation__c';
import MINIMUM_PAYMENT from '@salesforce/schema/SBQQ__Quote__c.Minimum_Payment__c';
import ADDITIONAL_PAYMENT from '@salesforce/schema/SBQQ__Quote__c.Additional_Payment__c';
import DUE_AT_SALE from '@salesforce/schema/SBQQ__Quote__c.Due_at_Sale__c';
import MONITORING_FEE from '@salesforce/schema/SBQQ__Quote__c.Monitoring_Fee__c';
import MONTHLY_MONITORING_FEE from '@salesforce/schema/SBQQ__Quote__c.Monthly_Monitoring_Fees__c';
import FINANCE_ELIGIBLE from '@salesforce/schema/SBQQ__Quote__c.Finance_Eligible_at_Submit__c';
import FINANCE_ELIGIBLE_AMOUNT from '@salesforce/schema/SBQQ__Quote__c.Finance_Eligible_Amount__c';
import THREEPAY_ELIGIBLE from '@salesforce/schema/SBQQ__Quote__c.Three_Pay_Eligible_at_Submit__c';
import THREEPAY from '@salesforce/schema/SBQQ__Quote__c.X3_Payment_Option__c';
import FINANCING from '@salesforce/schema/SBQQ__Quote__c.Financing__c';
import EASY_PAY_ENROLL from '@salesforce/schema/SBQQ__Quote__c.EasyPay_Enrollment__c';
import EASY_PAY_REQUIRED from '@salesforce/schema/SBQQ__Quote__c.EasyPay_Required__c';
import RISK_GRADE from '@salesforce/schema/SBQQ__Quote__c.Risk_Grade__c';
import PAYMENT_INTERVAL from '@salesforce/schema/SBQQ__Quote__c.Payment_Interval__c';
import PAYMENT_TERMS from '@salesforce/schema/SBQQ__Quote__c.SBQQ__PaymentTerms__c';
import PAYMENT_TERMS_FORMULA from '@salesforce/schema/SBQQ__Quote__c.Payment_Terms_Formula__c';
import INVOICE1 from '@salesforce/schema/SBQQ__Quote__c.Invoice_1_Due_at_Install__c';
import INVOICE2 from '@salesforce/schema/SBQQ__Quote__c.Invoice_2_Due_30_days_after__c';
import INVOICE3 from '@salesforce/schema/SBQQ__Quote__c.Invoice_3_Due_60_days_after__c';
import RENT_OWN from '@salesforce/schema/SBQQ__Quote__c.Profile_Rent_Own__c';
import MONTHLY_FEE from '@salesforce/schema/SBQQ__Quote__c.Monthly_Payment__c';
import FIRST_PAYMENT_DUE from '@salesforce/schema/SBQQ__Quote__c.First_Payment_Due_at_Install__c';
import INSTALLMENTS from '@salesforce/schema/SBQQ__Quote__c.Installments__c';
import TOTAL_FEES_TAXES from '@salesforce/schema/SBQQ__Quote__c.Total_Activation_and_Permit_Fees_w_Taxes__c';
import ACCOUNT_ID from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Account__c';
import System__c from '@salesforce/schema/SoftwareSupportAgreement__ChangeEvent.System__c';
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';
import getPaymentInfo from '@salesforce/apex/QuotePaymentController.getValidatedPaymentInfo';

const USER_FIELDS = [NO_DIRECT_PAYMENT];
const QUOTE_FIELDS = [
                        ID_FIELD,
                        TOTAL_INSTALL_CHARGE,
                        MINIMUM_PAYMENT,
                        ADDITIONAL_PAYMENT,
                        DUE_AT_SALE,
                        MONITORING_FEE,
                        FINANCE_ELIGIBLE,
                        FINANCE_ELIGIBLE_AMOUNT,
                        THREEPAY_ELIGIBLE,
                        THREEPAY,
                        FINANCING,
                        EASY_PAY_ENROLL,
                        EASY_PAY_REQUIRED,
                        RISK_GRADE,
                        PAYMENT_TERMS_FORMULA,
                        MONTHLY_MONITORING_FEE,
                        PAYMENT_INTERVAL,
                        RENT_OWN,
                        MONTHLY_FEE,
                        FIRST_PAYMENT_DUE,
                        PAYMENT_TERMS,
                        INSTALLMENTS,
                        TOTAL_FEES_TAXES,
                        ACCOUNT_ID
                    ];

const formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD', minimumFractionDigits: 2 });

export default class reviewQuotePaymentLWCDemo extends LightningElement {
    @api quoteid;
    type;
    showInstallPaymentScreen = false;
    showMonitoringPaymentScreen = false;
    showSecondGrid = false;
    installAchValidation = false;
    installCCPayment = false;
    value = '';
    monitoringAchValidation = false;
    monitoringCCPayment = false;
    remoteUser;
    quote;
    totalInstallCharge;
    // totalInstallChargeFormatted;
    minimumPayment;
    // minimumPaymentFormatted;
    additionalPayment;
    dueAtSale;
    // dueAtSaleFormatted;
    financeEligible;
    financeEligibleAmount;
    threePayEligible;
    showThreePayScreen;
    showFinancingScreen;
    monitoringFee;
    monthlyMonitoringFee;
    // monitoringFeeFormatted;
    easyPayEnroll;
    easyPayRequired;
    paymentTermsLabel;
    paymentTermsValue;
    invoice1;
    // invoice1Formatted;
    invoice2;
    // invoice2Formatted;
    invoice3;
    // invoice3Formatted;
    paymentIntervalValue;
    paymentIntervalLabel;
    paymentIntervalHeaderLabel = 'Monthly';
    riskGrade;
    amountToFinance;
    // amountToFinanceFormatted;
    // firstPaymentDueAtInstall; // this is sharing monthlyPayment
    monthlyPayment;
    // monthlyPaymentFormatted;
    paymentTermsFormula;
    rentOrOwn;
    installmentsLabel;
    installmentsValue;
    payingInFull;
    totalFeesTaxes;
    maxFinanceValue = 5000;
    minFinanceValue = 599;
    channelName = "/event/Remote_Pay_Verified__e";
    subscription = {};
    error = undefined;
    AccountId;
    installOptions = [];
    monitoringOptions = [];
    financeEligibility;
    remotePayCaptured = false;
    installMap = [];

    @wire(getRecord, { recordId: USER_ID, fields: USER_FIELDS })
    wireUser({ error, data }) {
        if(error) {
            this.error = error;
            this.showToast('Error getting data', error.message, 'error');
        } else if (data) {
            this.remoteUser = data.fields.NoDirectPayment__c.value;
        }
    }

    @wire(getRecord, { recordId : '$quoteid', fields: QUOTE_FIELDS })
    wireQuote({ error, data }){
        if(error){
            this.error = error;
            this.showToast('Error getting record data', error.message, 'error');
        } else if(data){
            console.log('header data: ' + JSON.stringify(data));
            console.log('finance eligible?: ' + data.fields.Finance_Eligible_at_Submit__c.value);
            console.log('threepay eligible?: ' + data.fields.Three_Pay_Eligible_at_Submit__c.value);
            this.totalInstallCharge = data.fields.Total_Installation__c.value;
            // this.totalInstallChargeFormatted = formatter.format(this.totalInstallCharge);
            this.minimumPayment = data.fields.Minimum_Payment__c.value;
            // this.minimumPaymentFormatted = formatter.format(this.minimumPayment);
            this.additionalPayment = this.handleNulls(data.fields.Additional_Payment__c.value, 'additionalPayment');
            // this.additionalPaymentFormatted = formatter.format(data.fields.Additional_Payment__c.value);
            this.dueAtSale = data.fields.Due_at_Sale__c.value;
            // this.dueAtSaleFormatted = formatter.format(this.dueAtSale);
            this.monitoringFee = data.fields.Monitoring_Fee__c.value;
            this.monthlyMonitoringFee = data.fields.Monthly_Monitoring_Fees__c.value;
            // this.monitoringFeeFormatted = formatter.format(this.monitoringFee);
            this.financeEligible = data.fields.Finance_Eligible_at_Submit__c.value;
            this.financeEligibleAmount = data.fields.Finance_Eligible_Amount__c.value;
            this.threePayEligible = data.fields.Three_Pay_Eligible_at_Submit__c.value;
            this.showThreePayScreen = data.fields.X3_Payment_Option__c.value;
            this.showFinancingScreen = data.fields.Financing__c.value;
            this.easyPayEnroll = data.fields.EasyPay_Enrollment__c.value;
            this.easyPayRequired = data.fields.EasyPay_Required__c.value;
            this.riskGrade = data.fields.Risk_Grade__c.value;            
            this.paymentTermsFormula = data.fields.Payment_Terms_Formula__c.value;
            this.paymentIntervalLabel = data.fields.Payment_Interval__c.value;
            this.rentOrOwn = data.fields.Profile_Rent_Own__c.value;
            this.monthlyPayment = data.fields.Monthly_Payment__c.value;
            this.installmentsLabel = data.fields.Installments__c.value;
            this.paymentTermsLabel = this.installmentsLabel; // data.fields.SBQQ__PaymentTerms__c.value;
            //this.paymentTermsValue = use this.installmentsLabel or data.fields.SBQQ__PaymentTerms__c.value?
            this.totalFeesTaxes = data.fields.Total_Activation_and_Permit_Fees_w_Taxes__c.value;
            console.log('installmentsLabel: ' + JSON.stringify(data.fields.Installments__c));
            this.AccountId = data.fields.SBQQ__Account__c.value;
            this.getPaymentRecords();
            if(this.installmentsLabel == 'Pay In Full'){
                this.payingInFull = true;
                this.showFinancingScreen = !this.payingInFull;
                this.showThreePayScreen = !this.payingInFull;
            }

            if(this.showThreePayScreen){
                this.setPaymentIntervalLabel('Monthly');
            } else if(this.paymentIntervalLabel != undefined){
                this.setPaymentIntervalLabel(this.paymentIntervalLabel);
            } // else {
            //     this.setPaymentIntervalLabel('Monthly');
            // }

            console.log('totalInstall value: ' + data.fields.Total_Installation__c.value);
            console.log('totalInstall type: ' + typeof(data.fields.Total_Installation__c.value));
            console.log('minimumPayment value: ' + data.fields.Minimum_Payment__c.value);
            console.log('minimumPayment type: ' + typeof(data.fields.Minimum_Payment__c.value));
            console.log('additionalPayment value: ' + this.additionalPayment);
            console.log('additionalPayment type: ' + typeof(this.additionalPayment));
            console.log('monthlyPayment data val: ' + data.fields.Monthly_Payment__c.value);
            console.log('monthlyPayment: ' + this.monthlyPayment);
            console.log('paymentIntervalValue: ' + this.paymentIntervalValue);
            console.log('paymentIntervalLabel: ' + this.paymentIntervalLabel);
            
            this.handleCalculate();               
        }
    }
    
    getPaymentRecords()
    {
        getPaymentInfo({accountId: this.AccountId})
        .then((result => 
            {
                if(result)
                {
                    console.log('WIRE RESULT: ', result);
                    this.installMap = result.map(element => 
                    {
                        if(element.Has_CC_Info__c == true)
                        {
                            return { label: 'CC' + ' - ' + '************' + element.CardLast4__c, value: 'option1', data: element };
                        }
                        else
                        {
                            return { label: 'ACH' + ' - ' + element.ACHAccount__c.replace(/\S(?=\S{4})/g, "*"), value: 'option1', data: element};
                        } 
                    });
                    if(!this.remoteUser)
                    {
                        this.installMap.unshift({ label: 'Credit Card', value: 'option2' }, { label: 'Direct Deposit (ACH)', value: 'option3' });
                        console.log('this.installMap: ', this.installMap);
                    }
                    if(!this.remotePayCaptured)
                    {
                        this.installMap.push({ label: 'Remote Payment', value: 'option1' });
                    }
                    this.installOptions =  this.installMap.slice();
                    this.monitoringOptions = this.installMap.slice();

                }
            }))
        .catch((result => 
            {
                console.log('Wiring payment info didnt work :/ ', result);
            }))
        
    }      

    checkEasyPay(){ 
        if(this.easyPayRequired){
            console.log('easy pay required');
            this.easyPayEnroll = true;
        } else if(this.showThreePayScreen || this.showFinancingScreen){ // include check for financing as well?    
            console.log('easypay enrolled by choice of three pay/financing');
            this.easyPayEnroll = true;
        } else {                
            console.log('easypay not required');
            this.easyPayEnroll = false;
        }
        console.log('easy pay enrolled: ' + true);
    }

    resetFields(type){
        this.additionalPayment = '';
        console.log('resetFields(type): ' + type);
        switch(type){
            case 'financing':
                let initialValueToFinance = this.totalInstallCharge - this.minimumPayment;
                console.log('initialValueToFinance: ' + initialValueToFinance);
                this.monthlyPayment = '';
                // this.monthlyPaymentFormatted = '';
                this.amountToFinance = initialValueToFinance;
                // this.amountToFinanceFormatted = formatter.format(initialValueToFinance);
                this.dueAtSale = this.minimumPayment + this.additionalPayment;
                // this.dueAtSaleFormatted = formatter.format(this.dueAtSale);
            break;
            case 'threePay':
                this.invoice1 = '';
                // this.invoice1Formatted = '';
                this.invoice2 = '';
                // this.invoice2Formatted = '';
                this.invoice3 = '';
                // this.invoice3Formatted = '';
            break;
        }
    }

    handleNulls(value, field){
        console.log('<><><>handleNulls!');
        if(value === '' || value === null){            
            console.log('blank value: ' + field + ' converted to 0');
            return 0;
        } else {
            console.log('returning value: ' + value + ' for: ' + field);
            return parseInt(value);
        }
    }

    calculateMinimumPayment(){
        const maxValue = 100;
        if(!this.financeEligible){
            console.log('finance ineligible, returning totalInstallCharge: ' + this.totalInstallCharge);
            this.minimumPayment = this.totalInstallCharge;
        } else {
            if(this.showThreePayScreen || this.showFinancingScreen){
                console.log('3pay or finance true');
                this.minimumPayment =  this.totalFeesTaxes > maxValue ? maxValue : this.totalFeesTaxes;
            } else if(!this.showFinancingScreen && !this.showThreePayScreen){
                console.log('3pay and financing false, paying in full, calculate totalInstallCharge');
                this.minimumPayment = this.totalInstallCharge > maxValue ? maxValue : this.totalInstallCharge;
            } else {
                console.log('error in minimumPayment calculation');
                this.minimumPayment = 123;
            }            
        }
    }

    handleFinanceOverage(){
        console.log('handleFinanceOverage');
        this.showToast('Financed amount adjusted', 'amount financed must be less than ' + formatter.format(this.maxFinanceValue), 'info');
        this.additionalPayment = this.amountToFinance - this.maxFinanceValue;
        this.amountToFinance = this.maxFinanceValue;
        this.dueAtSale = this.minimumPayment + this.additionalPayment;
    }

    handleFinanceMinimum(){
        console.log('handleFinanceMinimum!');
        this.showToast('alert', 'This quote will become eligible for financing if the financeable total is increased to ' + formatter.format(this.minFinanceValue), 'info');
        this.additionalPayment = this.totalInstallCharge - this.minFinanceValue - this.minimumPayment;
        this.amountToFinance = this.minFinanceValue;
        this.dueAtSale = this.minimumPayment + this.additionalPayment;
    }

    checkFinance(){
        if(this.financeEligible && (this.financeEligibleAmount > this.minFinanceValue)){
            this.financeEligibility = true;
        } else {            
            this.financeEligibility = false;
        }
    }

    handleCalculate(){
        console.log('***** calculate');
        this.setPaymentIntervalValue();
        this.setInstallmentsValue();
        this.checkEasyPay();
        this.calculateMinimumPayment();
        this.checkFinance();
        console.log('due at sale min payment: ' + this.minimumPayment);
        console.log('due at sale additional payment: ' + this.additionalPayment);
        this.dueAtSale = this.minimumPayment + this.additionalPayment;
        // this.dueAtSaleFormatted = formatter.format(this.dueAtSale);
        this.amountToFinance = this.totalInstallCharge - this.minimumPayment - this.additionalPayment;
        // this.amountToFinanceFormatted = formatter.format(this.amountToFinance);

        if(this.showFinancingScreen){
            if(this.amountToFinance < this.minFinanceValue){
                this.handleFinanceMinimum();
            } else if(this.amountToFinance > this.maxFinanceValue){
                this.handleFinanceOverage();
            }

            console.log('amountToFinance: ' + this.amountToFinance);
            console.log('amountToFinance type: ' + typeof(this.amountToFinance));
            console.log('installmentsValue: ' + this.installmentsValue);
            this.monthlyPayment = ((this.totalInstallCharge - this.minimumPayment - this.additionalPayment) / this.installmentsValue);
            // this.monthlyPayment = this.amountToFinance / this.installmentsValue;
            // this.monthlyPaymentFormatted = formatter.format(this.monthlyPayment);
            console.log('monthlyPayment: ' + this.monthlyPayment);
            console.log('monthlyPayment type: ' + typeof(this.monthlyPayment));
            console.log('totalInstall value: ' + this.totalInstallCharge);
            console.log('totalInstall type: ' + typeof(this.totalInstallCharge));
            console.log('minimumPayment value: ' + this.minimumPayment);
            console.log('minimumPayment type: ' + typeof(this.minimumPayment));
            console.log('additionalPayment value: ' + this.additionalPayment);
            console.log('additionalPayment type: ' + typeof(this.additionalPayment));
            console.log('amountToFinance: ' + this.amountToFinance);
            console.log('amountToFinance type: ' + typeof(this.amountToFinance));
        }        

        if(this.showThreePayScreen){
            this.additionalPayment = '';
            this.calculateThreePay();
        }        
    }

    setInstallmentsValue(){
        console.log('setInstallments value installmentLabel: ' + this.installmentsLabel);
        switch(this.installmentsLabel){
            case '12 Months':
                console.log('case12');
                this.installmentsValue = 12;
                this.paymentTermsValue = 12;
            break;
            case '24 Months':
                console.log('case24');
                this.installmentsValue = 24;
                this.paymentTermsValue = 24;
            break;
            case '36 Months':
                console.log('case36');
                this.installmentsValue = 36;
                this.paymentTermsValue = 36;
            break;
            case '60 Months':
                console.log('case60');
                this.installmentsValue = 60;
                this.paymentTermsValue = 60;
            break;
            case 'Pay In Full':
                console.log('pay in full');
                this.installmentsValue = 0;
                this.paymentTermsValue = 0;
            break;
            case '3Pay':
                console.log('3Pay');
                this.installmentsValue = 0;
                this.paymentTermsValue = 0;
            break;
            case null:
                console.log('case null');
                this.installmentsValue = 0;
                this.paymentTermsValue = 0;
            break;
        }
        console.log('setInstallments value installmentsValue: ' + this.installmentsValue);
    }

    setPaymentIntervalValue(){
        console.log('setPaymentIntervalValue value paymentIntervalLabel: ' + this.paymentIntervalLabel);
        switch(this.paymentIntervalLabel){
            case 'Monthly':
                this.paymentIntervalValue = 1;
            break;
            case 'Quarterly':
                this.paymentIntervalValue = 3;
            break;
            case 'Semi-Annually':
                this.paymentIntervalValue = 6;
            break;
            case 'Annually':
                this.paymentIntervalValue = 12;
            break;
        }
        console.log('setPaymentIntervalValue value paymentIntervalValue: ' + this.paymentIntervalValue);
    }

    handleSaveAndClose(){
        if(this.amountToFinance < this.minFinanceValue){
            this.showToast('error', 'Amount financed must be greater than ' + formatter.format(this.minFinanceValue), 'error');
        } else {
            const fields = {};
            fields[ID_FIELD.fieldApiName] = this.recordId;
            fields[FINANCING.fieldApiName] = this.showFinancingScreen;        
            fields[THREEPAY.fieldApiName] = this.showThreePayScreen;        
            fields[INVOICE1.fieldApiName] = this.invoice1;        
            fields[INVOICE2.fieldApiName] = this.invoice2;        
            fields[INVOICE3.fieldApiName] = this.invoice3;        
            fields[ADDITIONAL_PAYMENT.fieldApiName] = this.additionalPayment;        
            fields[MONITORING_FEE.fieldApiName] = this.monitoringFee;        
            fields[PAYMENT_INTERVAL.fieldApiName] = this.paymentIntervalLabel;        
            fields[MONTHLY_FEE.fieldApiName] = this.monthlyPayment;        
            fields[FIRST_PAYMENT_DUE.fieldApiName] = this.monthlyPayment;        
            fields[PAYMENT_TERMS.fieldApiName] = this.paymentTermsLabel;        
            fields[INSTALLMENTS.fieldApiName] = this.installmentsLabel;        
            fields[EASY_PAY_ENROLL.fieldApiName] = this.easyPayEnroll;       
            const recordInput = { fields };
            updateRecord(recordInput)
            .then(()=>{
                this.showToast('Success', 'Record successfully updated!', 'success');
                this.dispatchEvent(new CloseActionScreenEvent());
            }).catch((error)=>{
                this.showToast('error', error.body.message, 'error');
                console.log('error body ' + JSON.stringify(error.body));
                // console.log('errors ' + JSON.stringify(error.body.output.errors));
                // console.log('error.body.message: ' + error.body.message);
                // console.log('errorCode ' + JSON.stringify(error.body.output.errors[0].errorCode));
                // console.log('errorMessage ' + JSON.stringify(error.body.output.errors[0].message));
            });
            if(this.subscription)
            {
                this.handleUnsubscribe();
            }
        }
        
    }

    calculateThreePay(){
        console.log('this.totalInstallCharge: ' + this.totalInstallCharge);
        console.log('this.amountToFinance: ' + this.amountToFinance);
        // let total = this.amountToFinance; // != undefined ? this.amountToFinance : this.totalInstallCharge;
        // console.log('total: ' + total);
        // let oneThird = Math.floor(total / 3);
        let oneThird = this.amountToFinance / 3;
        console.log('oneThird: ' + oneThird);
        this.invoice1 = oneThird;
        // this.invoice1Formatted = formatter.format(this.invoice1);
        this.invoice2 = oneThird;
        // this.invoice2Formatted = formatter.format(this.invoice2);
        // this.invoice3 = total - (oneThird * 2);
        this.invoice3 = oneThird;
        // this.invoice3Formatted = formatter.format(this.invoice3);
    }
    /*
    get installOptions() {
        
        if(this.remoteUser){
            return [
                { label: 'Remote Payment', value: 'option1' }
            ];
        } else {
            return [
                { label: 'Remote Payment', value: 'option1' },
                { label: 'Credit Card', value: 'option2' },
                { label: 'Direct Deposit (ACH)', value: 'option3' },
            ];
        }
          
                        
    }*/
    /*
    get monitoringOptions() {

        if(this.remoteUser){
            return [
                { label: 'Remote Payment', value: 'option1' }
            ];
        } else {
            return [
                { label: 'Remote Payment', value: 'option1' },
                { label: 'Credit Card', value: 'option2' },
                { label: 'Direct Deposit (ACH)', value: 'option3' },
            ];
        }        
    }
    */
    get paymentTermsOptions(){
        if(this.rentOrOwn == 'R'){
            return [ 
                { label: '12 months', value: '12' },
                { label: '24 months', value: '24' },
                { label: '36 months', value: '36' },
                { label: '48 months', value: '48' }
            ]
        } else if(this.rentOrOwn == 'O'){
            return [ 
                { label: '12 months', value: '12' },
                { label: '24 months', value: '24' },
                { label: '36 months', value: '36' },
                { label: '48 months', value: '48' },
                { label: '60 months', value: '60' }
            ]
        }
        
    }

    get installmentsOptions(){
        return [ 
            // { label: 'Pay In Full', value: 'Pay In Full' },
            // { label: '3Pay', value: '3Pay' },
            { label: '12 Months', value: '12 Months' },
            { label: '24 Months', value: '24 Months' },
            { label: '36 Months', value: '36 Months' },
            { label: '60 Months', value: '60 Months' }
        ]        
    }

    get paymentIntervalOptions(){
        const goodRisk = ["A", "B", "C", "D", "N", "Q", "U", "X"];
        const badRisk = ["E", "F", "G", "J", "W", "Y"];

        if(this.showFinancingScreen){
            return [
                { label: 'Monthly', value: 'Monthly' }
            ]
        } else {
            console.log('goodRisk includes riskGrade: ' + goodRisk.includes(this.riskGrade));
            console.log('badRisk includes riskGrade: ' + badRisk.includes(this.riskGrade));
            if(goodRisk.includes(this.riskGrade)){
                return [
                    { label: 'Monthly', value: 'Monthly' },
                    { label: 'Quarterly', value: 'Quarterly' },
                    { label: 'Semi-Annually', value: 'Semi-Annually' },
                    { label: 'Annually', value: 'Annually' }
                ]
            } else if(badRisk.includes(this.riskGrade)){
                return [
                    { label: 'Annually', value: 'Annually' }
                ]
            }
        }
    }

    handleInstallChannelChange(event) {
        console.log('Install : event.detail.value: ', event.detail.value);
        const selectedOption = event.detail.value;
        this.type = 'install';

        switch(selectedOption){
            case 'option1':
                this.showInstallPaymentScreen = false;
                this.installAchValidation = false;
                this.installCCPayment = false;
                console.log('remote payment');
                console.log('installAchValidation: ' + this.installAchValidation);
                console.log('ccPayment: ' + this.installCCPayment);
            break;
            case 'option2':
                this.showSecondGrid = true;
                this.showInstallPaymentScreen = true;
                this.installAchValidation = false;
                this.installCCPayment = true;
                console.log('Credit card payment');
                console.log('installAchValidation: ' + this.installAchValidation);
                console.log('installCCPayment: ' + this.installCCPayment);
            break;
            case 'option3':                
                this.showSecondGrid = true;
                this.showInstallPaymentScreen = true;
                this.installAchValidation = true;
                this.installCCPayment = false;
                console.log('direct deposit ACH');
                console.log('installAchValidation: ' + this.installAchValidation);
                console.log('installCCPayment: ' + this.installCCPayment);
            break;
        }
    }

    handleMonitoringChannelChange(event) {
        const selectedOption = event.detail.value;
        console.log('Monitoring : event.detail.value: ', event.detail.value);
        this.type = 'monitoring';

        switch(selectedOption){
            case 'option1':                
                this.showMonitoringPaymentScreen = false;
                this.monitoringAchValidation = false;
                this.monitoringCCPayment = false;
                console.log('monitoring remote payment');
                console.log('monitoringAchValidation: ' + this.monitoringAchValidation);
                console.log('monitoringCCPaymentment: ' + this.monitoringCCPayment);
            break;
            case 'option2':
                this.showSecondGrid = true;
                this.showMonitoringPaymentScreen = true;
                this.monitoringAchValidation = false;
                this.monitoringCCPayment = true;
                console.log('monitoring Credit card payment');
                console.log('monitoringAchValidation: ' + this.monitoringAchValidation);
                console.log('monitoringCCPayment: ' + this.monitoringCCPayment);
            break;
            case 'option3':
                this.showSecondGrid = true;
                this.showMonitoringPaymentScreen = true;
                this.monitoringAchValidation = true;
                this.monitoringCCPayment = false;
                console.log('monitoring direct deposit ACH');
                console.log('monitoringAchValidation: ' + this.monitoringAchValidation);
                console.log('monitoringCCPayment: ' + this.monitoringCCPayment);
            break;
        }
    }

    financingHandleChange(event){
        console.log('financing!');        
        console.log('event.detail: ' + JSON.stringify(event.detail));        
        console.log('event.detail.checked: ' + event.detail.checked);     
        this.showThreePayScreen = false;
        this.showFinancingScreen = event.detail.checked ? true : false;
        if(this.payingInFull){
            this.showToast('error', 'customer is paying in full, financing or three pay not allowed', 'error');
        } else {
            if(event.detail.checked){
                this.paymentIntervalLabel = 'Monthly';
                console.log('this.paymentIntervalLabel: ' + this.paymentIntervalLabel);
                this.resetFields('threePay');
            } else {
                this.paymentInterval = '';
                console.log('this.paymentIntervalLabel: ' + this.paymentIntervalLabel);
            }
        }   
        this.handleCalculate();     
    }

    threePayHandleChange(event){
        console.log('threepay!');
        console.log('event.detail: ' + JSON.stringify(event.detail));        
        console.log('event.detail.checked: ' + event.detail.checked);
        if(this.payingInFull){
            this.showToast('error', 'customer is paying in full, financing or three pay not allowed', 'error');
        } else {
            this.showFinancingScreen = false;
            this.showThreePayScreen = event.detail.checked ? true : false;
            this.paymentIntervalLabel = 'Monthly';
            this.resetFields('financing');
            this.handleCalculate();
            this.setPaymentIntervalLabel(this.paymentIntervalLabel);
        }        
    }

    handlePaymentTermsChange(event){
        // console.log('handlePaymentTermsChange');
        // console.log('paymentTermsValue: ' + event.detail.value);
        // this.paymentTermsLabel = event.detail.value;
        // this.handleCalculate();
        this.handleInstallmentsChange(event);
    }

    handleInstallmentsChange(event){
        // this.payingInFull = 'Pay In Full' ? true : false;
        console.log('handleInstallmentsChange');
        // console.log('installmentsValue: ' + this.installmentsValue);
        console.log('installmentsLabel: ' + event.detail.value);
        this.installmentsLabel = event.detail.value;
        this.paymentTermsLabel = event.detail.value;
        this.handleCalculate();
    }

    handlePaymentIntervalChange(event){
        console.log('handlePaymentIntervalChange');
        console.log('payment interval: ' + event.detail.value);
        this.setPaymentIntervalLabel(event.detail.value);                
    }

    setPaymentIntervalLabel(value){
        switch(value){
            case 'Monthly':                
                this.paymentIntervalLabel = 'Monthly';
                this.paymentIntervalValue = '1';
                this.paymentIntervalHeaderLabel = 'Monthly';
            break;
            case 'Quarterly':
                this.paymentIntervalLabel = 'Quarterly';
                this.paymentIntervalValue = '3';
                this.paymentIntervalHeaderLabel = 'Quarterly';
            break;
            case 'Semi-Annually':
                this.paymentIntervalLabel = 'Semi-Annually';
                this.paymentIntervalValue = '6';
                this.paymentIntervalHeaderLabel = 'Semi-Annual';
            break;
            case 'Annually':
                this.paymentIntervalLabel = 'Annually';
                this.paymentIntervalValue = '12';
                this.paymentIntervalHeaderLabel = 'Annual';
            break;
        }
        console.log('monthlyMonitoringFee: ' + this.monthlyMonitoringFee);        
        console.log('paymentIntervalValue: ' + this.paymentIntervalValue);
        this.monitoringFee = this.monthlyMonitoringFee * this.paymentIntervalValue;
        console.log('monitoringFee: ' + this.monitoringFee);
        // this.monitoringFeeFormatted = formatter.format(this.monitoringFee);
        console.log('paymentIntervalLabel: ' + this.paymentIntervalLabel);
    }

    handleAdditionalPaymentChange(event){
        console.log('handleAdditionalPaymentChange');
        console.log('event.detail: ' + JSON.stringify(event.detail));        
        this.additionalPayment = this.handleNulls(event.detail.value, 'additionalPayment');      
    }

    handleKeyPress(event){
        console.log('key pressed keyCode: ' + event.keyCode);
        console.log('key pressed which: ' + event.which);
        console.log('key pressed this.installmentsValue: ' + this.installmentsValue);
        
        if(event.which == 13){
            console.log('13!');
            if(this.installmentsValue == undefined && this.showFinancingScreen){
                this.showToast('error', 'please select installments value before calculating a financed value', 'error');
                this.resetFields('financing');
            } else {
                this.handleCalculate();
            }
        }            
    }
    

    showToast(title, message, variant){
        this.dispatchEvent(new ShowToastEvent({
            title, message, variant
        }));
    }

    connectedCallback()
    {
        this.handleSubscribe();
    }
    
    disconnectedCallback()
    {
        this.handleUnsubscribe();
    }

    handleSubscribe() 
    {
        //Workaround for passing 'this' into messageCallback
        let self = this;
        //refresh options on receipt of platform event associated to this quote, show toast indicating successful capture
        const messageCallback = (response) => 
        {
            self.showToast('Success', 'Payment method captured!', 'success');
            self.remotePayCaptured = true;
            self.getPaymentRecords();
            // Response contains the payload of the new message received
        }
        // Invoke subscribe method of empApi. Pass reference to messageCallback
        subscribe(this.channelName, -1, messageCallback).then(response => {
            // Response contains the subscription information on subscribe call
            console.log('Subscription request sent to: ', JSON.stringify(response.channel));
            this.subscription = response;
        });
    }

    handleUnsubscribe() 
    {
        // Invoke unsubscribe method of empApi
        unsubscribe(this.subscription, response => {
            console.log('unsubscribe() response: ', JSON.stringify(response));
            // Response is true for successful unsubscribe
        });
    }
    
}