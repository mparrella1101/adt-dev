import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import sendPost from '@salesforce/apex/GiactValidateBankAcctInfo.sendPost';

export default class ADTACHValidationLWC extends LightningElement
{
    @api isLoading = false;
    showForm = true;
    success = false;
    notSuccess = false;

    statusOptions = [
        {value: 'Checking', label: 'Checking'},
        {value: 'Savings', label: 'Savings'}
    ];

    accountType = "";
    routingNumber = "";
    institutionName = "";
    bankAccountNumber = "";
    nameOnAccount = "";
    output;
    errorMessages = "";
	@api recordId;
	validateSuccessful = false; // MParrella | SFCPQ-491

	inputDisabled = false;

    handleChange(event)
    {
        if(event.detail.value)
        {
            this.accountType = event.detail.value;
        }
        this.checkFields(); // MParrella | SFCPQ-491

    }
    handleRoutingNumber(event)
    {
        if(event.detail.value)
        {
            this.routingNumber = event.detail.value;
        }
        this.checkFields(); // MParrella | SFCPQ-491
    }
    handleInstitutionName(event)
    {
        if(event.detail.value)
        {
            this.institutionName = event.detail.value;
        }
        this.checkFields(); // MParrella | SFCPQ-491
    }
    handleBankAccountNumber(event)
    {
        if(event.detail.value)
        {
            this.bankAccountNumber = event.detail.value;
        }
        this.checkFields(); // MParrella | SFCPQ-491
    }
    handleNameOnAccount(event)
    {
        if(event.detail.value)
        {
            this.nameOnAccount = event.detail.value.toString();
        }
        this.checkFields(); // MParrella | SFCPQ-491
    }
	setValues( event ){
		console.log("Enter setValues Function");
        const prop = event.target.name;
        const value = event.target.value;
		console.log(event.target.name);
        //console.log(prop)
        //console.log(value)

        this[prop] = value;
    }

    // MParrella | SFCPQ-491: Adding function to alert Parent LWC that all fields are valid: START
    checkFields() {
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')]
                    .reduce((validSoFar, inputField) => {
                        inputField.reportValidity();
                        return validSoFar && inputField.checkValidity();
                    }, true);

        const comboBoxValid = [...this.template.querySelectorAll('lightning-input')]
        .reduce((validSoFar, inputField) => {
            inputField.reportValidity();
            return validSoFar && inputField.checkValidity();
        }, true);

        if (isInputsCorrect && comboBoxValid && this.accountType !== ""){
            console.log('valid');
            // construct concatenated string of data to be used in Paymentech LWC
            let payload = 'true' + '|' + this.bankAccountNumber + '|' + this.routingNumber + '|' + this.institutionName + '|' + this.accountType + '|' + this.nameOnAccount;
            this.dispatchEvent(new CustomEvent('fieldsvalid', { detail: payload }));
        } else {
            console.log('not valid');
            this.dispatchEvent(new CustomEvent('fieldsvalid', { detail: 'false' }));
        }
    }
    // MParrella | SFCPQ-491: Adding function to alert Parent LWC that all fields are valid: STOP

    @api submitForm()
    {
        const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')]
            .reduce((validSoFar, inputField) => {
                inputField.reportValidity();
                return validSoFar && inputField.checkValidity();
            }, true);

        const comboBoxValid = [...this.template.querySelectorAll('lightning-input')]
        .reduce((validSoFar, inputField) => {
            inputField.reportValidity();
            return validSoFar && inputField.checkValidity();
        }, true);


        if (isInputsCorrect && comboBoxValid)
        {
            this.isLoading = true;

            sendPost({tracingId: '12345', routingNumber: this.routingNumber, bankAccountNumber: this.bankAccountNumber, institutionName: this.institutionName, recordId: this.recordId, accountType: this.accountType})
            .then((result) =>
            {
                this.output = result;
                //Hide the form and loading spinner
                this.isLoading = false;
                //Will change this to 'Pass' when this is ready to be rolled out, currently verificationResponse only returns 'NoData'
                if(this.output.verificationResponse == "Pass")
                {
                    this.inputDisabled = true;
                    this.validateSuccessful = true;
                    this.success = true;
                    this.dispatchEvent(new CustomEvent('validate', { detail: this.output }));
//                    this.dispatchEvent(new ShowToastEvent({
//                        title : '',
//                        message: 'Payment Information Saved',
//                        variant: 'success'
//                    }));
                }
                else
                {
                    this.notSuccess = true;
                    this.dispatchEvent(new CustomEvent('validatefailure'));
                    this.dispatchEvent(new ShowToastEvent({
                        title: "",
                        variant: "error",
                        message: this.output.verificationResponse
                    }));
                    if(this.output.messages)
                    {
                        this.output.messages.forEach(element => {
                            this.errorMessages = this.errorMessages + element
                        });
                    }
                }

            })
            .catch((error) =>
            {
                console.log(error);
                this.output = error;
                    this.dispatchEvent(new CustomEvent('validatefailure'));
                //Hide the form and loading spinner
                this.showForm = true;
                this.isLoading = false;
                this.data = true;
                this.dispatchEvent(new ShowToastEvent({
                    title: "Call to validate ACH information failed",
                    variant: "error"
                }));
            });
        }
    }
}