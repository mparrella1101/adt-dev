import { LightningElement, api, wire } from 'lwc';

export default class ADTACHValidationLWC extends LightningElement 
{
    statusOptions = [
        {value: 'Checking', label: 'Checking'},
        {value: 'Saving', label: 'Saving'}
    ];

    accountType;
    routingNumber;
    institutionName;
    bankAccountNumber;
    nameOnAccount;
    
    handleChange(event) {
        // Get the string of the "value" attribute on the selected option
        this.value = event.detail.value;
    }
}