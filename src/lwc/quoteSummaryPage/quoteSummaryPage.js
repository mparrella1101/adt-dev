import { LightningElement, api } from 'lwc';
import { getRecord, getFieldValue, createRecord, getRecordCreateDefaults, updateRecord, deleteRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import getQuoteLines from '@salesforce/apex/QuoteSummaryController.getQuoteLines';
import getQuote from '@salesforce/apex/QuoteSummaryController.getQuote';
import getQLEParams from '@salesforce/apex/modifyQuoteRedirectController.getQuote';
import upsertQSPQuoteLine from '@salesforce/apex/QuoteSummaryController.upsertQSPQuoteLine';
import deleteQSPQuoteLine from '@salesforce/apex/QuoteSummaryController.deleteQSPQuoteLine';
import getPromotions from '@salesforce/apex/QuoteSummaryController.getPromotions';
import QSPQuoteLinePromotion from '@salesforce/apex/QuoteSummaryController.QSPQuoteLinePromotion';
import sendQuoteLineModelToVertex from '@salesforce/apex/ADTVertexAPI.ADTVertexAPIManager';
import processVertexResponse from '@salesforce/apex/QuoteSummaryController.ApplyQuoteLineTax';
import upsertDiscount from '@salesforce/apex/QuoteSummaryController.upsertDiscount';
import ID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Id';
// import ORS from '@salesforce/schema/SBQQ__Quote__c.ORS__c'; // out of scope for pilot, leaving commented for later use.
import INSTALLMENTS from '@salesforce/schema/SBQQ__Quote__c.Installments__c';
import CONTRACT_TERM from '@salesforce/schema/SBQQ__Quote__c.SBQQ__PaymentTerms__c';
import THREEPAY from '@salesforce/schema/SBQQ__Quote__c.X3_Payment_Option__c';
import FINANCING from '@salesforce/schema/SBQQ__Quote__c.Financing__c';
import PROMO_CODE from '@salesforce/schema/SBQQ__Quote__c.Promotion_Code__c';
import PROMO_DESC from '@salesforce/schema/SBQQ__Quote__c.Promotion_Description__c';
import PROMO_AMOUNT from '@salesforce/schema/SBQQ__Quote__c.Installation_Promotion_Amt__c'; // this will hold value of promotion applied
import INSTALL_DISCOUNT from '@salesforce/schema/SBQQ__Quote__c.Installation_Discount_Amt__c';
import INSTALL_DISCOUNT_CALC from '@salesforce/schema/SBQQ__Quote__c.Installation_Discount_Pct__c';
import MONTHLY_DISCOUNT from '@salesforce/schema/SBQQ__Quote__c.Monthly_Monitoring_Discount_Amt__c';
import MONTHLY_DISCOUNT_CALC from '@salesforce/schema/SBQQ__Quote__c.Monthly_Monitoring_Discount_Pct__c';
import PRE_DEF_QUOTE from '@salesforce/schema/SBQQ__Quote__c.Pre_Defined_Quote__c';
import TOTAL_INSTALL_AFTER_PROMOS_DISCOUNTS from '@salesforce/schema/SBQQ__Quote__c.Total_Install_Charge__c';
import TOTAL_INSTALL_PROMOS_DISCOUNTS from '@salesforce/schema/SBQQ__Quote__c.Installation_Total_Discount_Amt__c';
import TOTAL_MONTHLY_PROMOS_DISCOUNTS from '@salesforce/schema/SBQQ__Quote__c.Monthly_Monitoring_Total_Discount_Amt__c';
// import MONTHLY_MONITORING_CHARGE from '@salesforce/schema/SBQQ__Quote__c.Monthly_Monitoring_Charge__c';
import ANSC from '@salesforce/schema/SBQQ__Quote__c.ANSC__c';
import QSP from '@salesforce/schema/SBQQ__Quote__c.QSP__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

// import GetDataPowerDealerCalloutMockWithNoDealerInfo from '@salesforce/resourceUrl/GetDataPowerDealerCalloutMockWithNoDealerInfo';
// import areAddOnAndConversionButtonsEnabled from '@salesforce/apex/ScreenPopCPQController.areAddOnAndConversionButtonsEnabled';
// import { BUSINESS_ID_COMMERCIAL } from '../insideSalesFlowConstants/insideSalesFlowConstants';

const FIELDS = [
    // ACCOUNT_NAME // maybe use later
    ID_FIELD,
    // ORS,
    INSTALLMENTS,
    CONTRACT_TERM,
    THREEPAY,
    FINANCING,
    // MONTHLY_MONITORING_CHARGE,
    ANSC,
    QSP,
    PROMO_CODE,
    PROMO_DESC,
    PROMO_AMOUNT,
    INSTALL_DISCOUNT,
    INSTALL_DISCOUNT_CALC,
    MONTHLY_DISCOUNT,
    MONTHLY_DISCOUNT_CALC,
    PRE_DEF_QUOTE,
    TOTAL_INSTALL_AFTER_PROMOS_DISCOUNTS,
    TOTAL_INSTALL_PROMOS_DISCOUNTS,
    TOTAL_MONTHLY_PROMOS_DISCOUNTS
];

const promoCOLUMNS = [
    {
        label: 'Promotion Name',
        fieldName: 'Description__c',
        type: 'text'
    }
];
export default class QuoteSummaryPage extends NavigationMixin(LightningElement){

    @api recordId;
    quoteId;
    customActionId;
    @api quotePaymentComplete;
    @api viewOnly = false;
    loading;
    showLoadingModal = false;
    promoColumns = promoCOLUMNS;
    promoData;
    quoteBody;
    selectedRows= [];
    lineItemCount;
    installmentsValue;
    accountName;
    quoteNumber;
    displayORSCheckbox;
    orsValue = 2000;
    ors;
    orsVerbaige = 'ORS - Customer requested ownership';
    creditCheckVerbaige = 'Choices other than Pay In Full subject to credit check and qualified approval';
    showModal = false;
    showDiscountModal = false;
    filteredData;
    additionalCharge = 0;
    data;
    quoteLineData;
    error;
    minInstallValue = 2000;
    hasQSP = false;
    resiBundleId;
    resiBundleProductId;
    upsertComplete = false;
    refreshData = false;
    quoteLineNumber;
    qspProduct = 'S0008-ANSC';
    supplementalQSPProductCode = 'AddQSP';
    supplementalQSPProductId;
    rentOrOwn;
    riskGrade;
    badRisks = ["N", "X"];
    goodRisks = ["A", "B", "C"];
    tableHeader = [];
    tableBody = [];
    showInstall;
    showMonthly;
    contractTerm;
    disableButtons = false;
    finance;
    threePay;
    minFinanceValue = 599;
    maxFinanceValue = 5000;
    dueAtSaleDeposit = 0;
    totalAmountFinanced = 0;
    financing;
    monthlyPayment;
    financeVerbaige;
    remainingMonths;
    threePayOption;
    payingInFull;
    ansc = 0;
    qsp = 0;

    // system product table properties
    systemProducts;
    systemPS = '1-System';
    systemVerbaige = 'HERE\'S YOUR SYSTEM';
    systemBundle = [];
    systemProduct = [];
    sysInstallTotal = 0;
    sysMonthlyTotal = 0;

    //added tech product table properties
    addedTechProducts;
    addedTechPS = '2-Added Tech';
    addedTechVerbaige = 'HERE\'S YOUR ADDED TECH';
    addedTechBundle = [];
    addedTechProduct = [];
    atInstallTotal = 0;
    atMonthlyTotal = 0;

    //activation + permit fees product table properties
    apfProducts;
    apfPS = '3-Activation + Permit Fees';
    apfVerbaige = 'ACTIVATION + PERMIT FEES'; 
    apfBundle = [];
    apfProduct = [];
    apfInstallTotal = 0;
    apfMonthlyTotal = 0;

    // real protection service product table properties
    realProtectionProducts;
    realProtectionPS = '4-Real Protection Services';
    realProtectionVerbaige = 'REAL PROTECTION SERVICES';
    realProtectionBundle = [];
    realProtectionProducts = [];
    realProtectionMonitoringTotal = 0;
    realProtectionMonthlyTotal = 0;

    // promotions properties
    disablePromotions = true; // handles enabling/disabling promotion button
    promoRows = [];
    discountRows = [];
    discountApplied = false;
    promoApplied = false;
    promoVerbaige = 'ADDITIONAL SAVINGS & DISCOUNTS';
    promotion;
    promosAvailable = false;

    // totals table properties
    totalsVerbaige = 'YOUR TOTAL';
    installTotal = 0;
    monitoringTotal = 0;
    monthlyMonitoringTotal = 0; // will hold monitoring recurring monthly charge + totalMonthlyDiscount pre tax
    monthlyTotal = 0;
    adscTaxes = 0;
    anscTaxes = 0;
    qspTaxes = 0;
    monthlyTaxes = 0;
    atInstallTotalWithTaxes = 0;
    realProtectionMonthlyWithTaxesTotal = 0;
    totalMonthlyTaxes = 0;
    paymentComplete = false;
    systemAndAddedTechInstallSubtotal = 0;
    systemAndAddedTechMonthlySubtotal = 0;
    systemAndAddedTechTotalInstall = 0;    
    systemAndAddedTechTotalMonthly = 0;
    totalAfterDiscounts = 0;
    totalAfterDiscountsMonthly = 0;
    promoProductCode = 'PROMO_DISCOUNT';
    installDiscountProductCode = 'SALES_INSTALL_DISCOUNT';
    monthlyDiscountProductCode = 'SALES_MONTHLY_DISCOUNT';
    promoDiscountProductCodes = ['PROMO_DISCOUNT', 'SALES_INSTALL_DISCOUNT', 'SALES_MONTHLY_DISCOUNT']; // add more discount product codes here as they arise.
    installDiscountVerbaige = 'Additional Discounts/Equipment & Installation';
    monthlyDiscountVerbaige = 'Additional Discounts/Monthly Service';
    promoId = '';
    promoAmount = 0;
    promoNames = [];
    promoDescriptions = [];
    promoMap = new Map();
    totalPromoValue = 0;
    totalMonthlyPromoValue = 0;
    totalInstallDiscount = 0;
    totalMonthlyDiscount = 0; //  will hold monthly discount
    monthlyInstallDiscount = 0; // this will hold monthly installment of install discount
    monthlyPromo = 0; // this will hold monthly installment value of applied promotion
    totalDiscount = 0;
    totalMonthlyPromoDiscount = 0;
    lessDiscounts = 0;
    discountTotal = 0; // might not need
    anscQspMonthlyTotal = 0;
    monthlyMonitoringTaxes = 0;
    //-------------------------
    globalMapKey;

    connectedCallback(){
        // console.log('connectedCallback()');
        // console.log('recordId: ' + this.recordId);
        // console.log('viewOnly: ' + this.viewOnly);
        this.loading = true;
        if(this.recordId != null){
            // console.log('quote paid, passing recordId to this.getQuote()');
            this.quoteId = this.recordId;
            this.getQuote(this.recordId);
        } else {
            // this.getQuoteIdFromURL()
            this.getQuoteIdFromURLSearchParam()
            .then(result => this.getQuote(result))
            .catch(error => {
                // console.log('error getting quote from URL');
            });
        }
    }

    getQuoteIdFromURL(){     
        return new Promise(resolve => {
            let url = new URL(window.location.href);
            // console.log('url: ' + url);
            let quoteId = url.searchParams.get("c__quoteId");
            // console.log('quoteId: ' + quoteId);
            this.quoteId = quoteId;
            resolve(quoteId);
        });
    }

    getQuoteIdFromURLSearchParam(){
        return new Promise(resolve => {
            let query = location.search;
            // console.log('query: ' + query);        
            let params = new URLSearchParams(query);
            // console.log('params:' + params);
            let quoteId = params.get("c__quoteId");  
            // console.log('quoteId from location.search:' + quoteId); 
            this.quoteId = quoteId;
            resolve(quoteId);
        });
    }

    getQuote(quoteId){
        // get QLE url params for later use
        getQLEParams({ quoteId: this.quoteId })
            .then(result => {
                this.customActionId = result.customActionId;
                // console.log('this.customActionId: ' + this.customActionId);
            })
        // get quote Id from URL
        // console.log('>>>getQuote()');
        // console.log('quoteId: ' + quoteId);
        if(quoteId === null){
            setTimeout(() => {
                this.getQuoteIdFromURL()
                .then(result => this.getQuote(result))
                .catch(error => {
                })}, 5000);
            
        } else {
            getQuote({quoteId: quoteId})
            .then(result => {
                    // console.log('result: ' + JSON.stringify(result));
                    // this.installmentsValue = result.Installments__c;
                    this.quoteBody = result;
                    this.accountName = result.SBQQ__Account__r.Name;
                    this.quoteNumber = result.SFCPQ_Global_ID__c;
                    this.rentOrOwn = result.Profile_Rent_Own__c;
                    this.riskGrade = result.Risk_Grade__c;
                    this.lineItemCount = result.SBQQ__LineItemCount__c;
                    // this.adscTaxes = result.ADSC_Taxes__c; // writing this value and below value from QSP vertex call
                    // this.monthlyMonitoringTaxes = result.Monthly_Monitoring_Taxes__c;
                    this.financing = result.Financing__c;
                    // this.threePayOption = result.X3_Payment_Option__c; // are we using different field for 3 pay?
                    this.threePayOption = result.Installments__c == '3Pay' ? true : false;
                    // console.log('result.X3_Payment_Option__c: ' + result.X3_Payment_Option__c);
                    this.monthlyPayment = result.Monthly_Payment__c;
                    this.dueAtSaleDeposit = (result.Minimum_Payment__c + this.handleNulls(result.Additional_Payment__c));
                    this.quotePaymentComplete = result.SBQQ__Status__c != 'Created' ? true : false;
                    // console.log('this.dueAtSaleDeposit: ' + this.dueAtSaleDeposit);                             
    
                    if(result.Installments__c === 'Pay In Full'){
                        this.showMonthly = false;
                        this.showInstall = true;
                        this.payingInFull = true;
                    } else {
                        this.showMonthly = true;
                        this.showInstall = false;
                        this.payingInFull = false;
                    }
    
                    this.installmentsValue = result.Installments__c != null ? result.Installments__c : this.setDefaultInstallmentValue(); // add 3 pay logic here
                     // this.contractTerm = result.Installments__c == "Pay In Full" ? '' : this.installmentsValue;
                    this.contractTerm = result.SBQQ__PaymentTerms__c != null ? result.SBQQ__PaymentTerms__c : this.determineContractTerm(this.installmentsValue);

                    this.remainingMonths = (this.getInstallmentValue(result.Installments__c) - 1);
                    if(this.quotePaymentComplete && !this.payingInFull){
                        this.financeVerbaige = this.financing ? '$' + this.monthlyPayment.toFixed(2) + ' due at install, then $' + this.monthlyPayment.toFixed(2) + ' monthly for the remaining ' + this.remainingMonths + ' months.' : '';
                    }                    
                    
                    // this.ors = result.ORS__c; out of scope for pilot, leaving commented for later use.
                    // console.log('result.Total_Installation__c: ' + result.Total_Installation__c);
                    // console.log('result.Installments__c: ' + result.Installments__c);
                    // console.log('this.installmentsValue: ' + this.installmentsValue);
                    // this.displayORSCheckbox = result.Total_Installation__c > this.orsValue ? true : false;
                    this.getQuoteLineData(result);
                    this.loading = false;
            })        
            .catch(error => { 
                // console.log('error on getQuote(): ' + JSON.stringify(error));
            });
        }        
    }

    getInstallmentValue(installment){
        switch(installment){
            case 'Pay In Full':{
                return 1;
            }
            case '3Pay':{
                return 3;
            }
            case '12 Months':{
                return 12;
            }
            case '24 Months':{
                return 24;
            }
            case '36 Months':{
                return 36;
            }
            case '60 Months':{
                return 60;
            }
        }
    }

    setDefaultInstallmentValue(){
        if(this.installmentsValue === '3Pay'){
            return '3Pay';
        } else if(this.badRisks.includes(this.riskGrade)){
            return '36 Months';
            // this.installmentsValue = 36;
            // console.log('this.installmentsValue badRisks: ' + this.installmentsValue);
        } else if(this.rentOrOwn == 'R'){
            return '36 Months';
            // this.installmentsValue = 36;
            // console.log('this.installmentsValue RENT: ' + this.installmentsValue);
        } else if(this.rentOrOwn == 'O'){
            return '60 Months';
            // this.installmentsValue = 60;
            // console.log('this.installmentsValue OWN: ' + this.installmentsValue);
        }
    }

    getQuoteLineData(quoteData){
        // console.log('>>>getQuoteLineData()');
        getQuoteLines({ quoteId: quoteData.Id })
        .then(result => {
            result.forEach(item => {
                this.determineInstallmentValue(item);
                if(item.SBQQ__ProductCode__c === 'Residential Services'){
                    this.resiBundleId = item.Id;
                    this.resiBundleProductId = item.SBQQ__Product__c;
                }
            });
            this.data = result;
            // console.log('getQuoteLines result data: ' + JSON.stringify(result));
            this.error = undefined;
            // console.log('this.data length ' + this.data.length);
            this.quoteLineNumber = this.data.length;
            if(!this.quotePaymentComplete){
                this.calculateQSP(result)
                .then(() => {
                    this.getPromotionData();
                })
                .catch(error => {
                    // console.log('error calculating supplemental QSP.');
                });
            } else {
                this.productGrouping();
            }
        })
        .catch(error => {
            // console.log('error on getQuoteLineData(): ' + JSON.stringify(error));
        });
        
    }    
    //Get Promotions 
    getPromotionData(){
        // console.log('>>>getPromotionData()');
        getPromotions({ quoteId: this.quoteId })
        .then(result => {
            this.promoData = result;
            // console.log('getPromotions result data: ' + JSON.stringify(result));
            this.error = undefined;
            // console.log('this.data length ' + this.data.length);
            this.promosAvailable = result.length > 0 ? true : false;
            // console.log('promos available: ' + this.promosAvailable);
            if(this.promosAvailable){
                this.LineQuote; 
                let my_ids = [];
                // console.log('Before For: ');
                // console.log('Before For: '+this.promoData);
                for(let i=0; i<this.promoData.length; i++){
                    // console.log('Inside For: ');
                    // console.log('Inside For: '+this.promoData[i].Default_Selected__c);
                    //Check which row should be auto-selected on load 
                    if( this.promoData[i].Default_Selected__c == true && my_ids.length == 0){
                        // console.log('Selected Row Default Selected: '+this.promoData[i].Id);
                        this.LineQuote = this.promoData[i];
                        my_ids.push(this.promoData[i].Id);
                    }
                    this.promoMap.set(this.promoData[i].Id.toString(), this.promoData[i]);                
                }
                this.selectedRows = my_ids;
                // console.log('Selected Rows: '+this.selectedRows);
                //If there was a pre-selection use that to update quote line with the promotion
                if(my_ids.length > 0){
                    // console.log('Selected Row Adjustment: '+ this.LineQuote.Max_Adjustment__c);
                    // console.log('Selected Row Quote Id: '+ this.LineQuote.Max_Adjustment__c);
                    // console.log('Selected Row BundleId: '+ this.resiBundleId);
                    // console.log('Selected Row bundleProductId: '+ this.resiBundleProductId);
                    // console.log('Selected Row quoteLineNumber: '+ this.quoteLineNumber);
                    // console.log('Selected Row promoDescription(Name): '+ this.LineQuote.Description__c);
                    // console.log('Selected Row promoName: '+ this.LineQuote.Name);
                    QSPQuoteLinePromotion({ quoteId: this.quoteId,
                                        qspAmount: this.LineQuote.Max_Adjustment__c,
                                        bundleId: this.resiBundleId,
                                        bundleProductId: this.resiBundleProductId,
                                        quoteLineNumber: this.quoteLineNumber,
                                        promoName: this.LineQuote.Description__c ,
                                        promoCode: this.LineQuote.Name,
                                        promoID:   ''})
                    .then(result => {                        
                        if(result){
                            this.refreshQuoteLineData();
                        } else {
                            // console.log('error getting result from QSPQuoteLinePromotion call');
                            throw new Error('error on upsertCall');
                        }            
                    })
                    .catch(error => {
                        // console.log('error on QSPQuoteLinePromotion(): ' + JSON.stringify(error));
                    });
                    //end if there is a pre-selection
                } else {
                    this.refreshQuoteLineData();
                }
            } else {
                this.refreshQuoteLineData();
            }
        })
        .catch(error => {
            // console.log('error on getPromotionData(): ' + JSON.stringify(error));
        });
        
    } 
    
    handleResetClick() {
        // console.log('Selected Rows: '+this.selectedRows);
       this.selectedRows = undefined;
       this.selectedRows = this.promoData;
       deleteQSPQuoteLine({ quoteId: this.quoteId,ProductCode: this.promoProductCode })
       .then(() => {            
         
            //   this.refreshQuoteLineData();
                
         
         })
         .catch(error => {
            //  console.log('error on deleteQSPQuoteLine(): ' + JSON.stringify(error));
         });
     
    }
    handleRowSelect(event){
        // console.log('>>>handleRowSelect()');
        // console.log('event.detail.selectedRows: ' + JSON.stringify(event.detail.selectedRows));
        // console.log('event.detail.selectedRows.length: ' + event.detail.selectedRows.length);
        // this.disablePromotions = event.detail.selectedRows.length > 0 ? false : true;
        // console.log('this.disablePromotions: ' + this.disablePromotions);
        // this.promoRows = event.detail.selectedRows;
        // this.selectedRows = event.detail.selectedRows;
        this.disableButtons = true;
        this.showLoadingModal = true;
        // this.promoNames = [];
        // this.promoDescriptions = [];
        let rows =  event.detail.selectedRows.length;
        let selectedRow = event.detail.selectedRows[0];
        // console.log('selectedRow: ' + JSON.stringify(selectedRow));
        if(rows > 0){
            // console.log('selectedRow[0]: ' + selectedRow[0]);
        }

        switch(rows){
            case 0:{
                // Run delete promo
                // console.log('case 0, deleting promos.');
                this.promoRows = [];
                // console.log('0 this.promoRows: ' + JSON.stringify(this.promoRows));
                // console.log('0 this.selectedRows: ' + JSON.stringify(this.selectedRows));
            
                deleteQSPQuoteLine({ quoteId: this.quoteId, ProductCode: this.promoProductCode })
                .then(() => {            
                    this.promoApplied = false;
                    // this.promoNames = []; // TODO: handle delete on unselect for multiple rows, selecting by index.
                    this.refreshQuoteLineData();
            
                })
                .catch(error => {
                    // console.log('error on deleteQSPQuoteLine(): ' + JSON.stringify(error));
                    this.showLoadingModal = false;
                    this.showToast('error', 'error deleting promotion', 'error');
                });
                break;
            }
            case 1:{
                // console.log('case 1, calling QSPQuoteLinePromotion');
                // console.log('case 1, calling QSPQuoteLinePromotion promoDesc(Name)'+ selectedRow.Description__c );
                // console.log('case 1, calling QSPQuoteLinePromotion promoName: '+ selectedRow.Name);
                // console.log('case 1, calling QSPQuoteLinePromotion promoID'+ this.selectedRows.toString());
                let el = this.template.querySelector('lightning-datatable');
                this.globalMapKey  = el.selectedRows.slice(0).toString();
                var currentRow = el.selectedRows.slice(1);
                QSPQuoteLinePromotion({
                    quoteId: this.quoteId,
                    qspAmount: selectedRow.Max_Adjustment__c,
                    bundleId: this.resiBundleId,
                    bundleProductId: this.resiBundleProductId,
                    quoteLineNumber: this.quoteLineNumber,
                    promoName: selectedRow.Description__c ,
                    promoCode: selectedRow.Name,
                    promoID:   currentRow.toString()
                })
                .then(result => {
                    // console.log('QSPQuoteLinePromotion call result: ' + result);
                    if(result){
                        // console.log('refresh data after insert');
                        // this.upsertComplete = true;
                        // this.refreshData = true;
                        this.refreshQuoteLineData();
                        // refreshApex(this.quoteLineData);
                    } else {
                        // console.log('error getting result from QSPQuoteLinePromotion call');
                        throw new Error('error on upsertCall');
                    }            
                })
                .catch(error => {
                    // console.log('error on inserting promotion: ' + JSON.stringify(error));
                    this.showLoadingModal = false;
                    this.showToast('error', 'error inserting promotion', 'error');
                });
                break;
            }
            default:{
                deleteQSPQuoteLine({ quoteId: this.quoteId, ProductCode: this.promoProductCode })
                .then(() => {            
                    //Set selected rows to only the clicked value
                    // console.log('case rows > 1, handle delete');
                    let el = this.template.querySelector('lightning-datatable');
                    this.selectedRows = el.selectedRows = el.selectedRows.slice(1);
                    let mapIdKey = el.selectedRows.slice(0).toString();
                    this.globalMapKey  = el.selectedRows.slice(0).toString();
                    // console.log('Selected Row '+ this.selectedRows);
                    // console.log('Selected Row 1 '+ el.selectedRows.slice(1));
                    // console.log('Selected Row '+ el.selectedRows);
                    // console.log('Promo Description ' + this.promoMap.get(mapIdKey).Description__c);
                    // console.log('Promo Name ' + this.promoMap.get(mapIdKey).Name);
                    // console.log('this.promoMap.get(this.selectedRows) ' + JSON.stringify(this.promoMap.get(mapIdKey)));
                    event.preventDefault();
                    //Do Callout with selection 
                    QSPQuoteLinePromotion({ 
                        quoteId: this.quoteId,
                        qspAmount: this.selectedRows.Max_Adjustment__c,
                        bundleId: this.resiBundleId,
                        bundleProductId: this.resiBundleProductId,
                        quoteLineNumber: this.quoteLineNumber,
                        promoName: this.selectedRows.Description__c,
                        promoCode: this.selectedRows.Name,
                        promoID:   this.selectedRows.toString()
                    })
                    .then(result => {
                        // console.log('QSPQuoteLinePromotion call result: ' + result);
                        if(result){
                            // console.log('refresh data after insert');
                            // this.upsertComplete = true;
                            // this.refreshData = true;
                        this.refreshQuoteLineData();
                            // refreshApex(this.quoteLineData);
                        } else {
                            // console.log('error getting result from QSPQuoteLinePromotion call');
                            throw new Error('error on upsertCall');
                        }            
                    })
                })
                .catch(error => {
                    // console.log('error inserting promo(): ' + JSON.stringify(error));
                    this.showLoadingModal = false;
                    this.showToast('error', 'error inserting promotion', 'error');
                });
                break;
            }
        }
    }

    applyPromotions(){
        let dataContainer = this.data;
        this.promoRows.forEach(promo => {
            dataContainer.push(promo);
        });
        // this.data.push(this.promoRows);
        // console.log('data after promotion add: ' + JSON.stringify(this.data));
    }

    vertexCall(){
        // console.log('>>>vertexCall()');
        let quoteLineData = this.data;
        this.showLoadingModal = true;
        if(this.lineItemCount > 0) {
            let quoteLinesToTax = [];
            let adscTax = 0;
            let anscTax = 0;
            let qspTax = 0;
            let totalTax = 0;
            // console.log('vertex quoteLineData: ' + JSON.stringify(quoteLineData));
            quoteLineData.forEach(line => {
                // console.log('line.SBQQ__ProductName__c: ' + line.SBQQ__ProductName__c);
                // console.log('line.SBQQ__Number__c: ' + line.SBQQ__Number__c);
                if(line.SBQQ__ProductCode__c === this.promoProductCode || line.SBQQ__ProductCode__c === this.installDiscountProductCode || line.SBQQ__ProductCode__c === this.monthlyDiscountProductCode){
                    // console.log('converting negative amount to positive for PROMO_DISCOUNT or DISCOUNT)');
                    line.Install__c = line.Install__c * -1;
                    // console.log('line.Install__c: ' + line.Install__c);
                    line.Monitoring__c = line.Monitoring__c * -1;
                    // console.log('line.Monitoring__c: ' + line.Monitoring__c);
                }
                // console.log('line: ' + JSON.stringify(line));
            })
            sendQuoteLineModelToVertex({ quoteBody: JSON.stringify(this.quoteBody), quoteLines: JSON.stringify(this.data) })
            .then(response => {
                // console.log('vertex response: ' + JSON.stringify(response));
                let parsedResponse = JSON.parse(response);
                quoteLineData.forEach(line => {
                    let taxableLine = parsedResponse.lineItems.find(lineItem => {
                        if(line.SBQQ__Number__c === lineItem.lineItemNumber){
                            return lineItem;
                        }
                    });
                    
                    if(taxableLine != undefined){
                        // console.log('taxableLine: ' + JSON.stringify(taxableLine));
                        // console.log('matched tax line product name: ' + line.SBQQ__ProductName__c);
                        // console.log('matched tax line product code: ' + line.SBQQ__ProductCode__c);
                        // console.log('matched tax line number: ' + line.SBQQ__Number__c);
                        // console.log('matched tax line number type: ' + typeof(line.SBQQ__Number__c));
                        // console.log('matched tax line Id: ' + line.Id);                    
                        quoteLinesToTax.push({
                            Id: line.Id,
                            lineNumber: line.SBQQ__Number__c,
                            productCode: line.SBQQ__ProductCode__c,                      
                            adscTax: taxableLine.ADSCTax,
                            anscTax: taxableLine.ANSCTax,                        
                            qspTax: taxableLine.QSPTax,
                            totalTax: taxableLine.totalTax
                        });
                    }
                });
                // console.log('quoteLinesToTax: ' + JSON.stringify(quoteLinesToTax));
                // processVertexResponse({ quoteLineModel: JSON.parse(response) })
                processVertexResponse({ quoteLineModel: JSON.stringify(quoteLinesToTax) })
                .then(response => {
                    if(response){
                        this.showLoadingModal = false;
                        this.adscTaxes = 0;
                        this.anscTaxes = 0;
                        this.qspTaxes = 0;
                        quoteLinesToTax.forEach(line => {
                            this.adscTaxes += this.promoDiscountProductCodes.includes(line.productCode) ? line.adscTax * -1 : line.adscTax;
                            this.anscTaxes += this.promoDiscountProductCodes.includes(line.productCode) ? line.anscTax * -1 : line.anscTax;
                            this.qspTaxes += line.qspTax
                        });
                        this.monthlyMonitoringTaxes = (this.anscTaxes + this.qspTaxes); // monitoring + qsp taxes
                        // console.log('response successful, calculate totals with taxes.');
                        // this.adscTaxes = adscTax; // install taxes
                        // this.anscTaxes = anscTax; 
                        // this.qspTaxes = qspTax;
                        // this.monthlyMonitoringTaxes = (anscTax + qspTax); // monitoring + qsp taxes
                        // console.log('total anscTax: ' + anscTax);
                        // console.log('total qspTax: ' + qspTax);
                        // console.log('vertex response this.monthlyMonitoringTaxes: ' + this.monthlyMonitoringTaxes);
                        // this.refreshQuoteLineData();
                        this.calculateTotalsWithTaxes();
                        
                    } else {
                        // console.log('error applying tax to quote lines.');
                    }
                    
                })
                // this.loading = false;
            })
            .catch(error => {
                // console.log('error on vertex call: ' + JSON.stringify(error));
                this.showLoadingModal = false;
                this.showToast('error', 'error retreiving tax values', 'error');
            });
        } else {
            this.showLoadingModal = false;
        }
    }

    calculateQSP(quoteLineData){
        return new Promise(resolve => {
            // console.log('>>>calculateQSP()');
            let installTotalWithoutDiscounts = 0;
                quoteLineData.forEach(item => {                
                if(!this.promoDiscountProductCodes.includes(item.SBQQ__ProductCode__c)){
                    installTotalWithoutDiscounts += item.Install__c;
                    // console.log('installTotalWithoutDiscounts: ' + installTotalWithoutDiscounts);
                }
                if(item.SBQQ__ProductCode__c === this.supplementalQSPProductCode){
                    this.hasQSP = true;
                    this.supplementalQSPProductId = item.Id;
                    // console.log('this.supplementalQSPProductId: ' + this.supplementalQSPProductId);
                    // console.log('Supplemental QSP Quote Line exists');
                    // console.log('this.hasQSP: ' + this.hasQSP);
                }
            });    
            if(installTotalWithoutDiscounts > this.minInstallValue){
                // console.log('upserting supplemental qsp product, none exist and installTotal(' + installTotalWithoutDiscounts + ') > this.minInstallValue(' + this.minInstallValue +').');
                upsertQSPQuoteLine({ quoteId: this.quoteId,
                    qspAmount: parseFloat(this.determineQSPAmount(installTotalWithoutDiscounts)),
                    bundleId: this.resiBundleId,
                    bundleProductId: this.resiBundleProductId,
                    quoteLineNumber: this.quoteLineNumber,
                    hasQSP: this.hasQSP })
                .then(result => {
                    // console.log('upsertQSPQuoteLine call result: ' + result);
                    if(result){
                            resolve();                                               
                    }           
                })
                .catch(error => {
                    // console.log('error on insertQSPQuoteLine(): ' + JSON.stringify(error));
                });
            } else if(this.hasQSP && installTotalWithoutDiscounts < this.minInstallValue){
                // console.log('deleting existing supplemental qsp product, installTotal(' + installTotalWithoutDiscounts + ') < this.minInstallValue(' + this.minInstallValue +').');
                deleteRecord(this.supplementalQSPProductId)
                .then(() => {        
                    // console.log('supplementalQsp deleted.');
                    // console.log('delete data: ' + JSON.stringify(this.data));    
                    resolve();
                })
                .catch(error => {
                    // console.log('error on deleteQSPQuoteLine(): ' + JSON.stringify(error));
                });
            } else resolve();
        });
        
    }

    productGrouping(){
        let quoteLines = this.data;
        // console.log('>>>productGrouping()');
        quoteLines.forEach(line => {
            if(line.SBQQ__RequiredBy__c == this.resiBundleId){
                this.bundleSplit(line);
            }
            this.calculateSubtotals(line);
            this.determineMonthlyQSP(line);            
        });
        this.checkForPromos(quoteLines);
        this.systemAndAddedTechInstallSubtotal = (this.sysInstallTotal + this.atInstallTotal);
        this.systemAndAddedTechMonthlySubtotal = (this.sysMonthlyTotal + this.atMonthlyTotal);
        this.totalAfterDiscounts = (this.systemAndAddedTechInstallSubtotal + this.apfInstallTotal + this.totalDiscount); // KF: will be adding discount amounts from applied promotions here
        // console.log('original systemAndAddedTechInstallSubtotal value: ' + this.systemAndAddedTechInstallSubtotal);
        // console.log('original systemAndAddedTechMonthlySubtotal value: ' + this.systemAndAddedTechMonthlySubtotal);
        this.systemAndAddedTechTotalInstall = this.systemAndAddedTechInstallSubtotal + this.adscTaxes + this.apfInstallTotal;
        this.systemAndAddedTechTotalMonthly = this.systemAndAddedTechMonthlySubtotal + this.apfMonthlyTotal;
        // console.log('discounted systemAndAddedTechTotalInstall value: ' + this.systemAndAddedTechInstallSubtotal);
        // console.log('discounted systemAndAddedTechTotalMonthly value: ' + this.systemAndAddedTechTotalMonthly);
        // console.log('this.totalAfterDiscounts: ' + this.totalAfterDiscounts);
        // console.log('this.systemAndAddedTechMonthlySubtotal: ' + this.systemAndAddedTechMonthlySubtotal);
        // console.log('this.systemAndAddedTechMonthlySubtotal + this.handleNulls(this.lessDiscounts) : ' + (this.systemAndAddedTechMonthlySubtotal + this.handleNulls(this.lessDiscounts)));
        // console.log('((this.systemAndAddedTechMonthlySubtotal + this.handleNulls(this.lessDiscounts))) / this.getInstallmentValue(this.installmentsValue).toFixed(2)) : ' + ((this.systemAndAddedTechMonthlySubtotal + this.handleNulls(this.lessDiscounts))) / this.getInstallmentValue(this.installmentsValue).toFixed(2));
        // console.log('this.getInstallmentValue(this.installmentsValue): ' + this.getInstallmentValue(this.installmentsValue));
        // console.log('this.realProtectionMonitoringTotal: ' + this.realProtectionMonitoringTotal);
        // console.log('this.lessDiscounts: ' + this.lessDiscounts);
        // console.log('Number(this.lessDiscounts): ' + Number(this.lessDiscounts));
        // console.log('parseFloat(this.lessDiscounts): ' + parseFloat(this.lessDiscounts));
        // console.log('type of this.lessDiscounts: ' + typeof(this.lessDiscounts));
        this.monthlyMonitoringTotal = this.realProtectionMonitoringTotal + this.totalMonthlyDiscount;
        // console.log('this.monthlyMonitoringTotal: ' + this.monthlyMonitoringTotal);
        this.totalAfterDiscountsMonthly = this.systemAndAddedTechTotalMonthly + this.realProtectionMonitoringTotal + this.monthlyMonitoringTaxes + parseFloat((this.adscTaxes / this.getInstallmentValue(this.installmentsValue)).toFixed(2)) + this.totalMonthlyDiscount + this.monthlyPromo + this.monthlyInstallDiscount /* + this.handleNulls(this.lessDiscounts) */;
        // console.log('this.systemAndAddedTechTotalMonthly: ' + this.systemAndAddedTechTotalMonthly);
        // console.log('this.lessDiscounts: ' + this.lessDiscounts);
        // console.log('this.realProtectionMonitoringTotal: ' + this.realProtectionMonitoringTotal);
        // console.log('this.anscTaxes: ' + this.anscTaxes);
        // console.log('this.qspTaxes: ' + this.qspTaxes);
        // console.log('this.monthlyMonitoringTaxes: ' + this.monthlyMonitoringTaxes);
        // console.log('this.totalAfterDiscountsMonthly: ' + this.totalAfterDiscountsMonthly);
        // this.data = quoteLines;
        // this.vertexCall(quoteLines); 
        this.vertexCall(); 
    }

    bundleSplit(bundle){
        // console.log('>>>bundleSplit()');
        // console.log('bundle.Plan_Summary__c: ' + bundle.Plan_Summary__c);
        // console.log('bundle.SBQQ__ProductName__c: ' + bundle.SBQQ__ProductName__c);
        switch(bundle.Plan_Summary__c){
            case this.systemPS: {
                this.systemProducts = true;
                this.systemBundle.push(bundle);
                // console.log('pushed ' + bundle.SBQQ__ProductName__c + ' to system bundle');
                break;
            }
            case this.addedTechPS: {
                this.addedTechProducts = true;
                this.addedTechBundle.push(bundle);
                // console.log('pushed ' + bundle.SBQQ__ProductName__c + ' to added tech bundle');
                break;
            }
            case this.apfPS: {
                this.apfProducts = true;
                this.apfBundle.push(bundle);
                // console.log('pushed ' + bundle.SBQQ__ProductName__c + ' to activation + permit fees bundle');
                break;
            }
            case this.realProtectionPS: {
                this.realProtectionProducts = true;
                this.realProtectionBundle.push(bundle);
                // console.log('pushed ' + bundle.SBQQ__ProductName__c + ' to real protection bundle');
                break;
            }
        }
    }

    calculateSubtotals(line){
        // console.log('>>>calculateSubtotals()');
        switch(line.Plan_Summary__c){
            case this.systemPS: {
                this.sysInstallTotal += line.Install__c;
                this.sysMonthlyTotal += line.Monthly__c;
                // console.log('this.sysInstallTotal ' + this.sysInstallTotal);
                // console.log('this.sysMonthlyTotal ' + this.sysMonthlyTotal);
                break;
            }
            case this.addedTechPS: {
                this.atInstallTotal += line.Install__c;
                this.atMonthlyTotal += line.Monthly__c;
                // console.log('this.atInstallTotal ' + this.atInstallTotal);
                // console.log('this.atMonthlyTotal ' + this.atMonthlyTotal);
                break;
            }
            case this.apfPS: {
                this.apfInstallTotal += line.Install__c;
                this.apfMonthlyTotal += line.Monthly__c;
                // console.log('this.apfInstallTotal ' + this.apfInstallTotal);
                // console.log('this.apfMonthlyTotal ' + this.apfMonthlyTotal);
                break;
            }
            case this.realProtectionPS: {
                this.realProtectionMonitoringTotal += line.Monitoring__c;
                this.realProtectionMonthlyTotal += line.Monthly__c;
                // console.log('this.realProtectionMonthlyTotal ' + this.realProtectionMonthlyTotal);
                break;
            }
        }
    }

    updateDiscounts(event){
        // console.log('>>>updateDiscounts()');
        this.closeDiscountModal();
        this.refreshQuoteLineData();
    }

    upsertDiscount(event){
        // console.log('>>upsertDiscount()');
        // console.log('event.detail: ' + JSON.stringify(event.detail));
        this.discountApplied = event.detail.hasDiscounts;
        let installDiscountAmount = event.detail.installDiscount;
        let monthlyDiscountAmount = event.detail.monthlyDiscount;
        upsertDiscount({ quoteId: this.quoteId,
                         installDiscAmount: installDiscountAmount,
                         monthlyDiscAmount: monthlyDiscountAmount,
                         bundleId: this.resiBundleId,
                         bundleProductId: this.resiBundleProductId,
                         quoteLineNumber: this.data.length,
                         hasDiscount : this.discountApplied })
        .then(result => {
            if(result){
                const fields = {};
                fields[ID_FIELD.fieldApiName] = this.quoteId;      
                fields[INSTALL_DISCOUNT.fieldApiName] = installDiscountAmount * -1; 
                fields[INSTALL_DISCOUNT_CALC.fieldApiName] = installDiscountAmount > 0 ? this.calculateDiscountPercent(installDiscountAmount, parseFloat(this.systemAndAddedTechInstallSubtotal + this.apfInstallTotal + this.totalPromoValue)) : '';
                fields[MONTHLY_DISCOUNT.fieldApiName] = monthlyDiscountAmount * -1;
                fields[MONTHLY_DISCOUNT_CALC.fieldApiName] = monthlyDiscountAmount > 0 ? this.calculateDiscountPercent(monthlyDiscountAmount, parseFloat((this.realProtectionMonitoringTotal + this.totalMonthlyPromoValue))) : '';
                const recordInput = { fields };
                updateRecord(recordInput)
                .then(() => {
                    // console.log('update discount fields successful.');
                    this.refreshQuoteLineData();
                    this.closeDiscountModal();
                })
                .catch((error) => {
                    this.showToast('error', error.body.message, 'error');
                    // console.log('error body ' + JSON.stringify(error.body));
                });                
            } else {
                // console.log('error inserting discount');
            }
        })
        .catch(error => {
            // console.log('error inserting discount: ' + JSON.stringify(error));
            this.showToast('error', 'error inserting discount', 'error');
        })
    }

    calculateDiscountPercent(discountAmount, totalAmount){
        // console.log('>>>calcualteDiscountPercent()');
        // console.log('discountAmount = ' + discountAmount);
        // console.log('totalAmount = ' + totalAmount);
        // console.log('discountAmount / totalAmount = ' + parseFloat((discountAmount / totalAmount) * 100).toFixed(6));
        return parseFloat((discountAmount / totalAmount) * 100).toFixed(6);
        // return parseFloat((discountAmount / totalAmount)).toFixed(6);
    }

    handleNulls(value){
        // console.log('handleNulls()');
        if(value === '' || value === null || value === undefined || value == 0){            
            // console.log('blank value, converted to 0');
            return 0;
        } else {
            // console.log('returning value: ' + value );
            return parseFloat(value);
        }
    }

    determineInstallmentValue(item){
        // console.log('>>>determineInstallmentValue()');
        // console.log('this.installmentsValue: ' + this.installmentsValue);
        // console.log('determineInstallmentValue this.installTotal: ' + this.installTotal);
        // console.log('determineInstallmentValue this.monitoringTotal: ' + this.monitoringTotal);
        // console.log('determineInstallmentValue this.monthlyTotal: ' + this.monthlyTotal);
        // if(item.UniqueIdentifier__c !== null){
            if(item.UniqueIdentifier__c == 'S0008-ANSC'){ // qsp product
                this.additionalCharge = item.QSP__c;
            }
        // }
        switch(this.installmentsValue){                    
            case 'Pay In Full':{
                // console.log('Pay in Full');
                item.Monthly__c = 0 + this.additionalCharge;
                //Accumulate totals here
                this.installTotal += item.Install__c;
                this.monitoringTotal += item.Monitoring__c;
                this.monthlyTotal += item.Monthly__c;
                break;
            }
            case '3Pay':{
                // console.log('3pay div');
                // item.Monthly__c = item.Install__c / 3 + item.Monitoring__c + this.additionalCharge;
                item.Monthly__c = this.calculateMonthly(item, 3);
                this.installTotal += item.Install__c;
                this.monitoringTotal += item.Monitoring__c;
                this.monthlyTotal += item.Monthly__c;
                break;                        
            }
            case '12 Months':{
                // console.log('12 months div');
                // console.log(item.Monthly__c);
                // item.Monthly__c = item.Install__c / 12 + item.Monitoring__c + this.additionalCharge;
                item.Monthly__c = this.calculateMonthly(item, 12);
                this.installTotal += item.Install__c;
                this.monitoringTotal += item.Monitoring__c;
                this.monthlyTotal += item.Monthly__c;
                // console.log(item.Monthly__c);
                break;
            }
            case '24 Months':{
                // console.log('24 months div');
                // item.Monthly__c = item.Install__c / 24 + item.Monitoring__c + this.additionalCharge;
                item.Monthly__c = this.calculateMonthly(item, 24);
                this.installTotal += item.Install__c;
                this.monitoringTotal += item.Monitoring__c;
                this.monthlyTotal += item.Monthly__c;
                break;
            }
            case '36 Months':{
                // console.log('36 months div');
                // item.Monthly__c = item.Install__c / 36 + item.Monitoring__c + this.additionalCharge;
                item.Monthly__c = this.calculateMonthly(item, 36);
                this.installTotal += item.Install__c;
                this.monitoringTotal += item.Monitoring__c;
                this.monthlyTotal += item.Monthly__c;
                break;
            }
            case '60 Months':{
                // console.log('60 months div');
                // item.Monthly__c = item.Install__c / 60 + item.Monitoring__c + this.additionalCharge;
                item.Monthly__c = this.calculateMonthly(item, 60);
                this.installTotal += item.Install__c;
                this.monitoringTotal += item.Monitoring__c;
                this.monthlyTotal += item.Monthly__c;
                break;
            }
        }
    }

    calculateTotalsWithTaxes(){
        // final tax amounts calculate here
        // console.log('calculateTotalsWithTaxes()');
        // console.log('this.systemAndAddedTechInstallSubtotal: ' + this.systemAndAddedTechInstallSubtotal);
        // console.log('this.adscTaxes: ' + this.adscTaxes);
        // console.log('this.installmentsValue: ' + this.installmentsValue);
        // console.log('this.adscTaxes / this.getInstallmentValue(this.installmentsValue): ' + (this.adscTaxes / this.getInstallmentValue(this.installmentsValue)));
        // console.log('this.anscTaxes: ' + this.anscTaxes);
        // console.log('this.qspTaxes: ' + this.qspTaxes);
        // console.log('(this.systemAndAddedTechInstallSubtotal + this.adscTaxes): ' + (this.systemAndAddedTechInstallSubtotal + this.adscTaxes));
        this.atInstallTotalWithTaxes = (this.systemAndAddedTechInstallSubtotal + this.adscTaxes + this.totalDiscount);
        this.realProtectionMonthlyWithTaxesTotal = this.showInstall ? (this.realProtectionMonitoringTotal + this.monthlyTaxes) : (this.realProtectionMonthlyTotal + this.monthlyTaxes);
        this.totalMonthlyTaxes = (this.realProtectionMonitoringTotal + this.monthlyMonitoringTaxes + this.totalMonthlyDiscount);
        // this.totalAfterDiscountsMonthly = this.systemAndAddedTechTotalMonthly + this.realProtectionMonitoringTotal + this.monthlyMonitoringTaxes + parseFloat((this.adscTaxes / this.getInstallmentValue(this.installmentsValue)).toFixed(2)) + this.totalMonthlyDiscount + this.monthlyPromo + this.monthlyInstallDiscount /* + this.handleNulls(this.lessDiscounts) */;
        this.totalAfterDiscountsMonthly = this.realProtectionMonitoringTotal + this.monthlyMonitoringTaxes + parseFloat(((this.atInstallTotalWithTaxes) / this.getInstallmentValue(this.installmentsValue)).toFixed(2)) + this.totalMonthlyDiscount;
        // console.log('this.totalAfterDiscountsMonthly: ' + this.totalAfterDiscountsMonthly);
        // this.monthlyMonitoringTaxes = (this.realProtectionMonitoringTotal + this.anscTaxes + this.qspTaxes);
        // console.log('this.monthlyMonitoringTaxes: ' + this.monthlyMonitoringTaxes);
        this.totalAmountFinanced = (this.atInstallTotalWithTaxes - this.dueAtSaleDeposit);
        // console.log('this.totalAmountFinanced: ' + this.totalAmountFinanced); 
    }

    updateMonthlyPayment(event){
        // console.log('>>>updateMonthlyPayment');
        this.loading = true;
        this.disableButtons = true;
        this.installmentsValue = event.detail.value;
        this.threePayOption = event.detail.value == '3Pay' ? true : false;
        // console.log('threePayOption: ' + this.threePayOption);
        this.resetValues();
        let childProducts = this.template.querySelectorAll('c-qs-bundle-product');        
        childProducts.forEach(child => {
            child.refreshChildLines(event.detail.value);
        });
        this.contractTerm = this.determineContractTerm(event.detail.value);        
        // console.log('updateMonthlyPayment event.detail.value: ' + event.detail.value);
        // console.log('this.installmentsValue = ' + this.installmentsValue);
        this.updateDataTable();
        if(this.promoApplied){
            // console.log('promo exists, calculate discount / installment term');
            this.calculateDiscount(event.detail.value);
        }
    }

    calculateDiscount(installmentValue){
        // console.log('>>> calculateDiscount()');
        // console.log('this.lessDiscounts: ' + this.lessDiscounts);
        // console.log('installmentValue: ' + this.getInstallmentValue(installmentValue));
        this.totalDiscount = (this.totalPromoValue + this.totalInstallDiscount);
        this.totalMonthlyPromoDiscount = (this.totalMonthlyPromoValue + this.totalMonthlyDiscount);
        this.lessDiscounts = (this.totalDiscount / this.getInstallmentValue(installmentValue)).toFixed(2);
        // console.log('this.totalDiscount after calculateDiscount() ' + this.totalDiscount);
        // console.log('this.lessDiscounts after calculateDiscount() ' + this.lessDiscounts);
    }

    resetValues(){
        // console.log('>>>resetValues()');
        this.ansc = 0;
        this.qsp = 0;
        this.anscQspMonthlyTotal = 0;
        this.installTotal = 0;
        this.monitoringTotal = 0;
        this.monthlyTotal = 0;
        this.sysInstallTotal = 0
        this.sysMonthlyTotal = 0;
        this.atInstallTotal = 0;
        this.atMonthlyTotal = 0;
        this.apfInstallTotal = 0;
        this.apfMonthlyTotal = 0;
        this.realProtectionMonitoringTotal = 0;
        this.realProtectionMonthlyTotal = 0;
        this.systemAndAddedTechTotalInstall = 0;
        this.systemAndAddedTechInstallSubtotal = 0;
        this.totalAfterDiscounts = 0;
        this.totalPromoValue = 0
        this.totalDiscount = 0;
        this.promoRows = [];
        this.discountRows = [];
        this.lessDiscounts = 0;
        this.adscTaxes = 0;
        this.anscTaxes = 0;
        this.qspTaxes = 0;
        this.monthlyInstallDiscount = 0;
        this.monthlyPromo = 0;
    }

    determineContractTerm(eventValue){       
    //    console.log('>>>determineContractTerm()');
    //    console.log('eventValue: ' + eventValue);

       if((eventValue === 'Pay In Full') || (eventValue === '3Pay')){
        //    console.log('eventValue pay in full');
           if(this.badRisks.includes(this.riskGrade)){
               return '36 Months';
               // this.installmentsValue = 36;
            //    console.log('this.installmentsValue badRisks: ' + this.installmentsValue);
           } else if(this.rentOrOwn == 'R'){
               return '36 Months';
               // this.installmentsValue = 36;
            //    console.log('this.installmentsValue RENT: ' + this.installmentsValue);
           } else if(this.rentOrOwn == 'O'){
               return '60 Months';
               // this.installmentsValue = 60;
            //    console.log('this.installmentsValue OWN: ' + this.installmentsValue);
           }
       } else {
        //    console.log('eventValue finance value');
           return eventValue;
       }
       
   }

    updateDataTable(){
        // console.log('>>>updateDataTable()');

        let displayData = this.data;
        // console.log('displayData: ' + JSON.stringify(displayData));
        this.checkForPromos(displayData);
        displayData.forEach(element => {               
            // if(element.UniqueIdentifier__c !== null){
                if(element.UniqueIdentifier__c == this.qspProduct){
                    this.additionalCharge = element.QSP__c;
                }
            // }
            // console.log('switch this.installmentsValue: ' + this.installmentsValue);
            // console.log('displayData.SBQQ__ProductName__c: ' + element.SBQQ__ProductName__c);
            // console.log('displayData.Monthly__c: ' + element.Monthly__c);                        
            switch(this.installmentsValue){                    
                case 'Pay In Full':{
                    // console.log('pay in full udt');
                    element.Monthly__c = this.calculateMonthly(element, 'Pay In Full');
                    this.installTotal += element.Install__c;
                    this.monitoringTotal += element.Monitoring__c;
                    this.monthlyTotal += element.Monthly__c;
                    this.showInstall = true;
                    this.showMonthly = false;
                    this.threePay = false;
                    this.finance = false;
                    return element;
                }
                case '3Pay':{
                    // console.log('3pay udt');
                    element.Monthly__c = this.calculateMonthly(element, 3);
                    this.installTotal += element.Install__c;
                    this.monitoringTotal += element.Monitoring__c;
                    this.monthlyTotal += element.Monthly__c;
                    this.showInstall = false;
                    this.showMonthly = true;
                    this.threePay = true;
                    // this.threePayOption = true;
                    this.finance = false;
                    return element;                        
                }
                case '12 Months':{
                    // console.log('12 months udt');
                    // console.log(element.Monthly__c);
                    element.Monthly__c = this.calculateMonthly(element, 12);
                    this.installTotal += element.Install__c;
                    this.monitoringTotal += element.Monitoring__c;
                    this.monthlyTotal += element.Monthly__c;
                    // console.log(element.Monthly__c);
                    this.showInstall = false;
                    this.showMonthly = true;
                    this.threePay = false;
                    if(this.installTotal > this.minFinanceValue){
                        this.finance = true;
                    }                    
                    return element;
                }
                case '24 Months':{
                    // console.log('24 months udt');
                    element.Monthly__c = this.calculateMonthly(element, 24);
                    this.installTotal += element.Install__c;
                    this.monitoringTotal += element.Monitoring__c;
                    this.monthlyTotal += element.Monthly__c;
                    this.showInstall = false;
                    this.showMonthly = true;
                    this.threePay = false;
                    if(this.installTotal > this.minFinanceValue){
                        this.finance = true;
                    } 
                    return element;
                }
                case '36 Months':{
                    // console.log('36 months udt');
                    element.Monthly__c = this.calculateMonthly(element, 36);
                    this.installTotal += element.Install__c;
                    this.monitoringTotal += element.Monitoring__c;
                    this.monthlyTotal += element.Monthly__c;
                    this.showInstall = false;
                    this.showMonthly = true;
                    this.threePay = false;
                    if(this.installTotal > this.minFinanceValue){
                        this.finance = true;
                    } 
                    return element;
                }
                case '60 Months':{
                    // console.log('60 months udt');
                    element.Monthly__c = this.calculateMonthly(element, 60);
                    this.installTotal += element.Install__c;
                    this.monitoringTotal += element.Monitoring__c;
                    this.monthlyTotal += element.Monthly__c;
                    this.showInstall = false;
                    this.showMonthly = true;
                    this.threePay = false;
                    if(this.installTotal > this.minFinanceValue){
                        this.finance = true;
                    } 
                    return element;
                }
            }            
        });
            this.data = displayData;
            this.loading = false;
            this.resetProductGrouping();            
    }

    checkForPromos(dataModel){
        // console.log('>>>checkForPromos()')
        this.promoRows = [];
        this.discountRows = [];
        this.totalPromoValue = 0;
        this.totalDiscount = 0;
        this.totalMonthlyDiscount = 0;
        this.totalInstallDiscount = 0;
        this.lessDiscounts = 0;
        this.promoAmount = 0; // will need to be changed when more than one promotion is allowed at a time.
        this.promoNames = [];
        this.promoDescriptions = [];
        dataModel.forEach(promo => {
            if(promo.SBQQ__ProductCode__c === this.promoProductCode){
                this.promoId = promo.Id;
                this.promoAmount = promo.Install__c;
                if(promo.Install__c == 0){
                    // console.log('promo no longer applicable, deleting.');
                    deleteRecord(promo.Id)
                    .then(() => {
                        // console.log('promo deleted.');
                        this.promoApplied = false;
                    })
                    .catch(error => {
                        // console.log('error deleting promo');
                    });
                } else {
                    promo.Install__c = promo.Install__c > 0 ? (promo.Install__c * -1) : promo.Install__c;
                    // console.log('promotion found, adding to this.promoRows');
                    // console.log('promo: ' + JSON.stringify(promo));
                    // console.log('promo.Id: ' + promo.Id);
                    // console.log('promo.Description__c: ' + promo.Description__c);
                    // console.log('promo.Max_Adjustment__c: ' + promo.Max_Adjustment__c);
                    // console.log('promo.SBQQ__ProductName__c: ' + promo.SBQQ__ProductName__c);
                    // console.log('promo.SBQQ__Description__c: ' + promo.SBQQ__Description__c);
                    // console.log('promo.Install__c: ' + promo.Install__c);
                    // console.log('promo.Promotion_Code__c: ' + promo.Promotion_Code__c);
                    this.promoApplied = true;
                    this.promoRows.push({
                        Id: promo.Id,
                        description: promo.SBQQ__Description__c,
                        // value: this.calculateMonthlyPromoValue(promo),
                        value: promo.Install__c,
                        name: promo.SBQQ__ProductName__c
                    });
                    this.totalPromoValue += promo.Install__c;
                    this.totalMonthlyPromoValue += promo.Monitoring__c;
                    this.promoDescriptions.push(promo.SBQQ__Description__c);
                    this.promoNames.push(promo.Promotion_Code__c);
                    // console.log('promoDescriptions: ' + this.promoDescriptions);
                    // console.log('promoNames: ' + this.promoNames);
                }                
            } else if(promo.SBQQ__ProductCode__c == this.installDiscountProductCode || promo.SBQQ__ProductCode__c == this.monthlyDiscountProductCode){
                this.promoApplied = true;
                promo.Install__c = promo.Install__c > 0 ? (promo.Install__c * -1) : promo.Install__c;
                promo.Monitoring__c = promo.Monitoring__c > 0 ? (promo.Monitoring__c * -1) : promo.Monitoring__c;
                // console.log('discount found, adding to this.discountRows');
                // console.log('discount: ' + JSON.stringify(promo));
                // this.discountApplied = true;
                this.promoApplied = true;
                this.discountRows.push({
                    Id: promo.Id,
                    description: promo.UOM__c == 'ADSC' ? this.installDiscountVerbaige : this.monthlyDiscountVerbaige,
                    installValue: promo.SBQQ__ProductCode__c == this.installDiscountProductCode ? promo.Install__c : '',
                    monthlyValue: promo.SBQQ__ProductCode__c == this.monthlyDiscountProductCode ? promo.Monitoring__c : '',
                    name: promo.SBQQ__ProductName__c
                });
                this.totalInstallDiscount += promo.Install__c;
                this.totalMonthlyDiscount += promo.Monitoring__c;
            } else {
                this.promoApplied = false;
            }
        });
        if(this.promoApplied){
            this.calculateDiscount(this.installmentsValue);
        }
        // console.log('promoRows after apex call: ' + JSON.stringify(this.promoRows));
        // console.log('promoRows.length after apex call: ' + this.promoRows.length);
        // console.log('discountRows after apex call: ' + JSON.stringify(this.discountRows));
    }

    calculateMonthlyPromoValue(promo){
        if(this.installmentsValue == 'Pay In Full' || this.installmentsValue == '3Pay'){
            return promo.Install__c;            
        } else {
            let promoMonthlyAmount = (promo.Install__c / this.getInstallmentValue(this.installmentsValue)).toFixed(2);
            this.monthlyPromo = parseFloat(promoMonthlyAmount);
            return promoMonthlyAmount;
        }
    }

    calculateMonthlyDiscountValue(discount){
        if(discount.UOM__c == 'ADSC'){
            if(this.installmentsValue == 'Pay In Full' || this.installmentsValue == '3Pay'){ // SFCPQ-644 AC-3 calls for 3pay to show monthly value.
                return null;
            } else {
                let discountAmount = (discount.Install__c / this.getInstallmentValue(this.installmentsValue)).toFixed(2)
                this.monthlyInstallDiscount = parseFloat(discountAmount);
                return discountAmount;
            }
        } 
        if(discount.UOM__c == 'ANSC') {
            return discount.Monitoring__c;
        }
    }

    resetProductGrouping(){
        // console.log('>>>resetProductGrouping()');
        this.systemBundle = [];
        this.addedTechBundle = [];
        this.apfBundle = [];
        this.realProtectionBundle = [];
        // console.log('this.systemBundle: ' + this.systemBundle);
        // console.log('this.addedTechBundle: ' + this.addedTechBundle);
        // console.log('this.apfBundle: ' + this.apfBundle);
        // console.log('this.realProtectionBundle: ' + this.realProtectionBundle);
        this.productGrouping();
    }

    calculateMonthly(item, installmentValue){
        // console.log('>>>calculateMonthly()');
        // console.log('item.SBQQ__ProductCode__c: ' + item.SBQQ__ProductCode__c);
        // console.log('item.SBQQ__ProductName__c: ' + item.SBQQ__ProductName__c);
        // console.log('installmentValue: ' + installmentValue);
        if(item.SBQQ__ProductCode__c == this.supplementalQSPProductCode){
            // console.log('supplemental QSP product');
            // console.log('supplemental QSP__c: ' + item.QSP__c);
            return item.QSP__c;
        } else if(installmentValue == 'Pay In Full'){
            // console.log('calculate monthly pay in full selected');
            return this.additionalCharge;            
        } else {
            // console.log('item(' + item.SBQQ__ProductName__c + ') / ' + installmentValue + ' = monthly charge of ' + ((item.Install__c / installmentValue) + item.Monitoring__c + this.additionalCharge));
            return ((item.Install__c / installmentValue) + item.Monitoring__c + this.additionalCharge + this.handleNulls(item.QSP__c)); // checking this logic
        }
    }

    // updateSubtotals(){
    //     let originalSysMonthlyTotal;
    //     let originalAddedTechTotal;
    //     let originalApfMonthlyTotal;
    //     let originalRealProtectionMonthlyTotal;
    //     this.sysMonthlyTotal
    // }

    // insertQSPQuoteLine(){
    //     // console.log('>>>addQSPQuoteLine()');
    //     upsertQSPQuoteLine({ quoteId: this.quoteId,
    //                          qspAmount: parseFloat(this.determineQSPAmount()),
    //                          bundleId: this.resiBundleId,
    //                          bundleProductId: this.resiBundleProductId,
    //                          quoteLineNumber: this.quoteLineNumber,
    //                          hasQSP: this.hasQSP })
    //     .then(result => {
    //         // console.log('upsertQSPQuoteLine call result: ' + result);
    //         if(result){
    //             // console.log('refresh data after insert');
    //             this.refreshQuoteLineData();
    //         } else {
    //             // console.log('error getting result from upsertQSPQuoteLine call');
    //             throw new Error('error on upsertCall');
    //         }            
    //       })
    //       .catch(error => {
    //         //   console.log('error on insertQSPQuoteLine(): ' + JSON.stringify(error));
    //       });
    // }

    // deleteQSPQuoteLine(){
    //     // console.log('>>>deleteQSPQuoteLine()');
    //     // deleteQSPQuoteLine({ quoteId: this.quoteId, ProductCode: this.supplementalQSPProductCode })        
    //     deleteRecord(this.supplementalQSPProductId)
    //     .then(() => {        
    //         // console.log('supplementalQsp deleted, refresh data');    
    //         this.refreshQuoteLineData();
    //       })
    //       .catch(error => {
    //         //   console.log('error on deleteQSPQuoteLine(): ' + JSON.stringify(error));
    //       });
    // }

    refreshQuoteLineData(){
        // console.log('>>>refreshQuoteLineData');
        this.resetValues();
        getQuoteLines({ quoteId: this.quoteId })
        .then(result => {
            // console.log('refreshQuoteLineData result: ' + JSON.stringify(result));
            this.data = result;
            this.updateDataTable();
            this.calculateTotals();           
        })
        .catch(error => {
            // console.log('error on refreshQuoteLineData(): ' + JSON.stringify(error));
        });
    }

    determineMonthlyQSP(line){
        // console.log('>>>determineMonthlyQSP');
        if(line.UOM__c === 'ANSC'){
            // console.log('line.Monitoring__c: ' + line.Monitoring__c);
            // console.log('line.QSP__c: ' + line.QSP__c);
            if(line.UniqueIdentifier__c === this.qspProduct){
                // console.log('qsp ansc product');                        
                this.ansc += 0;
                // console.log('line.ANSC: ' + this.ansc);
            } else {
                this.ansc += this.handleNulls(line.Monitoring__c);
                this.anscQspMonthlyTotal += this.handleNulls(line.Monitoring__c);
                // console.log('line.ANSC: ' + this.ansc);
            }
        }                
        if(line.UOM__c === 'QSP'){
            // console.log('refreshQuoteLineData.QSP__c: ' + line.QSP__c);
            this.qsp += line.QSP__c;
            this.anscQspMonthlyTotal += line.QSP__c;
        }
    }

    calculateTotals(){
        this.installTotal = 0;
        this.monitoringTotal = 0;
        this.monthlyTotal = 0;
        // console.log('>>>calculateTotals()');
        this.data.forEach(item => {
            this.installTotal += item.Install__c;
            this.monitoringTotal += item.Monitoring__c;
            this.monthlyTotal += parseFloat(item.Monthly__c);
        })
        // console.log('calculateTotals() installTotal = ' + this.installTotal);
        // console.log('calculateTotals() monitoringTotal = ' + this.monitoringTotal);
        // console.log('calculateTotals() monthlyTotal = ' + this.monthlyTotal);
    }

    determineQSPAmount(installTotalWithoutDiscounts){
        // console.log('>>>determineQSPAmount');
        switch(true){
            case installTotalWithoutDiscounts > this.minInstallValue && installTotalWithoutDiscounts <= 2500:                
                // console.log('installTotalWithoutDiscounts > 2000 && <= 2500, returning 7');
                return 7;
            break;
            case installTotalWithoutDiscounts > 2500 && installTotalWithoutDiscounts <= 3000:                
                // console.log('installTotalWithoutDiscounts > 2500 && <= 3000, returning 9');
                return 9;
            break;
            case installTotalWithoutDiscounts > 3000 && installTotalWithoutDiscounts <= 3500:                
                // console.log('installTotalWithoutDiscounts > 3000 && <= 3500, returning 11');
                return 11;
            break;
            case installTotalWithoutDiscounts > 3500 && installTotalWithoutDiscounts <= 4000:                
                // console.log('installTotalWithoutDiscounts > 3500 && <= 4000, returning 12');
                return 12;
            break;
            case installTotalWithoutDiscounts > 4000 && installTotalWithoutDiscounts <= 4500:                
                // console.log('installTotalWithoutDiscounts > 4000 && <= 4500, returning 14');
                return 14;
            break;
            case installTotalWithoutDiscounts > 4500 && installTotalWithoutDiscounts <= 5000:                
                // console.log('installTotalWithoutDiscounts > 4500 && <= 5000, returning 15');
                return 15;
            break;
            case installTotalWithoutDiscounts > 5000 && installTotalWithoutDiscounts <= 6000:                
                // console.log('installTotalWithoutDiscounts > 5000 && <= 6000, returning 18');
                return 18;
            break;
            case installTotalWithoutDiscounts > 6000 && installTotalWithoutDiscounts <= 7000:                
                // console.log('installTotalWithoutDiscounts > 6000 && <= 7000, returning 21');
                return 21;
            break;
            case installTotalWithoutDiscounts > 7000:                
                // console.log('installTotalWithoutDiscounts > 7000 returning 24');
                return 24;
            break;
        }
    }

    // buildDisplayData(item){
        // console.log('buildDisplayData item: ' + JSON.stringify(item));
        // console.log('buildDisplayData item.SBQQ__ProductName__c: ' + item.SBQQ__ProductName__c);
        // console.log('buildDisplayData item.Plan_Summary__c: ' + item.Plan_Summary__c);
    //     if(item.Plan_Summary__c != undefined){
            // console.log('item.SBQQ__ProductName__c: ' + item.SBQQ__ProductName__c);
            // console.log('item.Plan_Summary__c: ' + item.Plan_Summary__c);
    //         this.displayData.push(item);
            // console.log('displayData: ' + JSON.stringify(this.displayData));
            // console.log('displayData.length: ' + this.displayData.length());
    //     }        
    // }

    // handleORS(event){                                                            // out of scope for pilot, leaving commented for later use.
        // console.log('handleORS event.detail.checked: ' + event.detail.checked);
    //     this.ors = event.detail.checked;
    // }

    handleSaveRecord(){
        // console.log('>>>handleSaveRecord');
        // console.log('this.ansc total: ' + this.ansc);
        // console.log('this.qsp total: ' + this.qsp);
        const fields = {};
            fields[ID_FIELD.fieldApiName] = this.quoteId;      
            // fields[ORS.fieldApiName] = this.ors;     // out of scope for pilot, leaving commented for later use. 
            fields[INSTALLMENTS.fieldApiName] = this.installmentsValue;      
            fields[CONTRACT_TERM.fieldApiName] = this.contractTerm;        
            fields[THREEPAY.fieldApiName] = this.threePay;      
            fields[FINANCING.fieldApiName] = this.finance;
            fields[ANSC.fieldApiName] = this.ansc; 
            fields[QSP.fieldApiName] = this.qsp;
            fields[PROMO_CODE.fieldApiName] = this.printPromoName();
            fields[PROMO_DESC.fieldApiName] = this.printPromoDescription();
            fields[PROMO_AMOUNT.fieldApiName] = this.promoAmount > 0 ? this.promoAmount * -1 : this.promoAmount;
            fields[TOTAL_INSTALL_AFTER_PROMOS_DISCOUNTS.fieldApiName] = this.totalAfterDiscounts;
            fields[TOTAL_INSTALL_PROMOS_DISCOUNTS.fieldApiName] = this.totalDiscount;
            fields[TOTAL_MONTHLY_PROMOS_DISCOUNTS.fieldApiName] = this.totalMonthlyPromoDiscount;
        const recordInput = { fields };
        updateRecord(recordInput)
        .then(()=>{
            this.disableButtons = false;
            this.showToast('Success', 'Record saved successfully', 'success');
        }).catch((error)=>{
            this.showToast('error', error.body.message, 'error');
            // console.log('error body ' + JSON.stringify(error.body));
        });
    }

    get paymentTermsOptions(){
        // console.log('>>>get paymentTermsOptions()');
        // console.log('this.riskGrade: ' + this.riskGrade);
        // console.log('this.rentOrOwn: ' + this.rentOrOwn);
        if(this.badRisks.includes(this.riskGrade)){
            // console.log('this.installmentsValue: ' + this.installmentsValue);
            return [
                { label: 'Pay In Full', value: 'Pay In Full' },
                { label: '3Pay', value: '3Pay' },
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' }
            ]
        } else if(this.rentOrOwn == 'R'){
            // console.log('this.installmentsValue: ' + this.installmentsValue);
            return [ 
                { label: 'Pay In Full', value: 'Pay In Full' },
                { label: '3Pay', value: '3Pay' },
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' }
            ]
        } else if(this.rentOrOwn == 'O'){
            // console.log('this.installmentsValue: ' + this.installmentsValue);
            return [ 
                { label: 'Pay In Full', value: 'Pay In Full' },
                { label: '3Pay', value: '3Pay' },
                { label: '12 Months', value: '12 Months' },
                { label: '24 Months', value: '24 Months' },
                { label: '36 Months', value: '36 Months' },
                { label: '60 Months', value: '60 Months' }
            ]
        }
        
    }

    softSave(){
        return new Promise(resolve => {
            // console.log('>>>softSave()');
            // console.log('this.ansc total: ' + this.ansc);
            // console.log('this.qsp total: ' + this.qsp);        
            const fields = {};
                fields[ID_FIELD.fieldApiName] = this.quoteId;      
                fields[ANSC.fieldApiName] = this.ansc; 
                fields[QSP.fieldApiName] = this.qsp;
                fields[PROMO_CODE.fieldApiName] = this.printPromoName();
                fields[PROMO_DESC.fieldApiName] = this.printPromoDescription();
                fields[PROMO_AMOUNT.fieldApiName] = this.promoAmount > 0 ? this.promoAmount * -1 : this.promoAmount;
                fields[TOTAL_INSTALL_AFTER_PROMOS_DISCOUNTS.fieldApiName] = this.totalAfterDiscounts;
                fields[TOTAL_INSTALL_PROMOS_DISCOUNTS.fieldApiName] = this.totalDiscount;
                fields[TOTAL_MONTHLY_PROMOS_DISCOUNTS.fieldApiName] = this.totalMonthlyPromoDiscount;
            const recordInput = { fields };
            updateRecord(recordInput)
            .then(()=>{
                // console.log('softSave successful.');
                resolve();
            }).catch((error)=>{
                this.showToast('error', error.body.message, 'error');
                // console.log('error body ' + JSON.stringify(error.body));
            });
        });
    }

    printPromoName(){
        // console.log('>>>printPromoName()');
        // console.log('this.promoApplied: ' + this.promoApplied);
        // console.log('this.promoNames: ' + this.promoNames);
        if(this.promoApplied){
            return this.promoNames.join("; ");
        } else {
            // return 'NONE';
            return '';
        }
    }

    printPromoDescription(){
        // console.log('>>>printPromoDescription()');
        // console.log('this.promoApplied: ' + this.promoApplied);
        // console.log('this.promoDescriptions: ' + this.promoDescriptions);
        if(this.promoApplied){
            return this.promoDescriptions.join("; ");
        } else {
            return '';
        }
    }

    handleNavigate(event){
        this.softSave()
        .then(() => {
            const config = {
                type: "standard__recordPage",
                attributes: {
                  recordId: this.quoteId,
                  objectApiName: "SBQQ__Quote__c",
                  actionName: "view"
                  }
              };
            //   console.log('config', config);
              this[NavigationMixin.Navigate](config, true);
        })
        .catch(error =>{
            // console.log('error updating record on way back to quote detail');
        });
      }

    gotoQLE(event){
        getQuote({quoteId: this.quoteId})
            .then(result => {                
                // console.log('goToQLE >> result.SBQQ__Status__c: ' + result.SBQQ__Status__c);
                if(result.SBQQ__Status__c === 'Paid'){
                    this.showToast('error', 'Operation disabled, this quote has already been paid.', 'error');
                    this.dispatchEvent(new CustomEvent('refreshTab'));
                } else {
                    this.loading = true;
                    this.softSave()
                    .then(() => {
                        const fields = {};
                        fields[ID_FIELD.fieldApiName] = this.quoteId;      
                        // fields[ORS.fieldApiName] = this.ors;     // out of scope for pilot, leaving commented for later use. 
                        fields[INSTALLMENTS.fieldApiName] = this.installmentsValue;      
                        const recordInput = { fields };
                        updateRecord(recordInput)
                        .then(() => {
                            return new Promise(resolve =>{
                                let deletePromo = false;
                                let promoId;
                                this.data.forEach(line => {
                                    if(line.SBQQ__ProductCode__c == this.promoProductCode){
                                        // console.log('promo exists, moving to delete.');
                                        deletePromo = true;
                                        promoId = line.Id;
                                    } else {                                        
                                        resolve();
                                    }
                                });
                                if(deletePromo){
                                    // console.log('deleting promo for navigation: ' + promoId);
                                    deleteRecord(promoId)
                                    .then(() =>{
                                        // console.log('successfully deleted promo: ' + this.promoId);
                                        this.promoAmount = 0;
                                        resolve();
                                    });
                                } else {    
                                    // console.log('no promo to delete.');
                                    resolve();
                                }                        
                            })
                        })
                        .then(() =>{
                            this.loading = false;
                            //Link for navigating to the configure products page
                            let qleUrl = '/apex/sbqq__sb?scontrolCaching=1&id='+ this.quoteId + '#quote/le?qId=' + this.quoteId;
                            let resiBundleUrl = '/apex/sbqq__sb?scontrolCaching=1&id=' + this.quoteId + '#/product/pc?lineKey=2&qId=' + this.quoteId + '&ignoreCache=true';
                            // let resiBundleUrl = '/apex/sbqq__sb?scontrolCaching=1&id='+this.quoteId;
                            // resiBundleUrl += '#/product/pc?qId='+this.quoteId;
                            // resiBundleUrl += '&aId='+this.customActionId;
                            // resiBundleUrl += '&pId='+this.resiBundleProductId;
                            // resiBundleUrl += '&redirectUrl=LineEditor&open=0';
                            let link = this.lineItemCount > 0 ? resiBundleUrl : qleUrl;
                            // let link = resiBundleUrl;
                            //This is the url for going to the QLE if we ever need that
                            // let link = '/apex/sbqq__sb?scontrolCaching=1&id='+ this.quoteId + '#quote/le?qId=' + this.quoteId;
            
                            this[NavigationMixin.Navigate]({
                            type: 'standard__webPage',
                            attributes: {
                            url: link
                            }
                            }, true);
                            // console.log('navigated');
                            // /apex/sbqq__sb?scontrolCaching=1&id=a6y1k0000000zEnAAI#/product/pc?lineKey=2&qId=a6y1k0000000zEnAAI&ignoreCache=true
                        })
                    })        
                    .catch(error =>{
                        // console.log('error updating record on way back to QLE');
                    });
                }
            })        
            .catch(error => {
                // console.log('error on getQuote(): ' + JSON.stringify(error));
            });
        
    }

    showToast(title, message, variant){
        this.dispatchEvent(new ShowToastEvent({
            title, message, variant
        }));
    }

    openPaymentModal(event){
        getQuote({quoteId: this.quoteId})
        .then(result => {                
            // console.log('openPaymentModal >> result.SBQQ__Status__c: ' + result.SBQQ__Status__c);
            if(result.SBQQ__Status__c === 'Paid'){
                this.showToast('error', 'Operation disabled, this quote has already been paid.', 'error');
                this.dispatchEvent(new CustomEvent('refreshTab'));
            } else {
                this.softSave()
                .then(() => {
                this.showModal = true;
                })
                .catch(error =>{
                // console.log('error updating record navigating to payment screen');
                });
            }
        })        
        .catch(error => {
            // console.log('error on getQuote(): ' + JSON.stringify(error));
        });
    }

    openDiscountModal(event){
        // console.log('>>>openDiscountModal()');
        getQuote({quoteId: this.quoteId})
        .then(result => {                
            // console.log('openDiscountModal >> result.SBQQ__Status__c: ' + result.SBQQ__Status__c);
            if(result.SBQQ__Status__c != 'Created'){
                this.showToast('error', 'Operation disabled, this quote no longer eligible for discount.', 'error');
                this.dispatchEvent(new CustomEvent('refreshTab'));
            } else {
                this.softSave()
                .then(() => {
                this.showDiscountModal = true;
                })
                .catch(error =>{
                // console.log('error updating record navigating to discount screen');
                });
            }
        })        
        .catch(error => {
            // console.log('error on getQuote(): ' + JSON.stringify(error));
        });
    }

    closeModal(){
        this.showModal = false;
    }

    closeDiscountModal(){
        // console.log('>>>closeDiscountModal()');
        this.showDiscountModal = false;
    }

    closeQuickAction(){
        // console.log('>>>closeQuickAction()');
        this.dispatchEvent(new CustomEvent('closeQuickAction'));
    }

    // MParrella | 12-02-2021 | CC Fixes: Handling event when CC TX is completed: START
    handleTXComplete(installment){
        // console.log('>>>handleTXComplete');
        // console.log('force tab refresh, heading back to Quote Summary Page');
        this.loading = true;
        setTimeout(() => {
            this.loading = false;
            this.dispatchEvent(new CustomEvent('refreshTab'));
        }, 5000);
        
        // location.reload(true);
        // this.quotePaymentComplete = true;
        // this.showModal = false;
        // switch(installment.detail){
        //     case 'Pay In Full':{
        //         this.payingInFull = true;
        //         this.threePayOption = false;
        //         this.financing = false;
        //         break;
        //     }
        //     case '3Pay':{
        //         this.payingInFull = false;
        //         this.threePayOption = true;
        //         this.financing = false;
        //         break;
        //     }
        //     default:{
        //         this.payingInFull = false;
        //         this.threePayOption = false;
        //         this.financing = true;
        //     }
        // }
        // this.resetValues();
        // this.data = '';
        // this.systemProducts = false;
        // this.addedTechProducts = false;
        // this.apfProducts = false;
        // this.realProtectionProducts = false;
        // this.systemBundle = [];
        // this.addedTechBundle = [];
        // this.apfBundle = [];
        // this.realProtectionBundle = [];
        // this.getQuote(this.quoteId);
        // // this.updateDataTable();
    }

    handleCancelConfirm() {
        this.showModal = false;
    }
    // MParrella | 12-02-2021 | CC Fixes: Handling event when CC TX is completed: STOP
}