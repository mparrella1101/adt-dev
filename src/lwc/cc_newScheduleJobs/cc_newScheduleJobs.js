/**
 * Created by Matt on 11/2/2021.
 */

import { LightningElement, wire, api } from 'lwc';
import { getRecord, updateRecord } from 'lightning/uiRecordApi';
import { refreshApex } from '@salesforce/apex';
import { CloseActionScreenEvent } from 'lightning/actions';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import USER_ID from '@salesforce/user/Id';
import MMB_EMPLOYEE_ID_FIELD from '@salesforce/schema/User.MMB_Employee_ID__c';
import ID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Id';
import STATUS from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Status__c';
import JOB_DURATION from '@salesforce/schema/SBQQ__Quote__c.Estimated_Install_Duration__c';

/* Apex Method Imports: START */
import postOffers from '@salesforce/apex/ADTJstSchedulingExpApi.postOffers';
import postSchedule from '@salesforce/apex/ADTJstSchedulingExpApi.postSchedule';
import putReschedule from '@salesforce/apex/ADTJstSchedulingExpApi.putReschedule';
import getUpdateWorkOrder from '@salesforce/apex/ADTJstSchedulingExpApi.getUpdateWorkOrder';
import deleteAppointment from '@salesforce/apex/ADTJstSchedulingExpApi.deleteAppointment';
/* Apex Method Imports: STOP */

export default class CcNewScheduleJobs extends NavigationMixin(LightningElement) {
    userId = USER_ID;
    buttonText = 'Schedule Install';
    loadingText = 'Gathering Job Data. Please wait...';
    isLoading = true;
    jobsList = []; // Will hold all WorkOrder objects
    @api recordId;
    selectedJobLabel;
    selectOptions; // Holds the key/value pair for the select options
    showJobScheduleSection = false; // Used to control the showing of the job scheduling screen
    error; // Used to store any error objects returned from callout
    input_startDate; // Start date used for lower bound of range of dates sent to JST
    endDate; // End date used for upper bound of range of dates sent to JST
    apptOffers = []; // Array used to store returned Appointment time slots
    jobSchedulingError = false; // KF: Boolean property used with error handling in handleSubmit() to satisfy requirements in SFCPQ-475 when scheduling error occurs.
    jobSchedulingErrorVerbaige = 'e-Contracts is unable to schedule your job. Please contact Scheduling at 866-764-2465';
    isRescheduling = false; // Flag that is set to TRUE when the selected WorkOrder/Job is already in a 'Scheduled' state
    disableInputFields = false;
    acceptableJobDuration = 12; // KF: defining global variable for longest acceptable job duration, SFCPQ-475.
    maxJobDurationExceeded = false; // KF: property to conditionally display different logic based on if job duration exceeds this.acceptableJobDuration, SFCPQ-475.
    maxJobDurationExceededVerbaige = 'Please call the OSC at 1-866-671-4470 to schedule installations longer than 12 hrs.'; // KF: Verbaige used when max job duration is exceeded, referenced from SFCPQ-475.
    /* Job Specific vars: START */
    selectedJob; // Holds the 'select' option representing the WorkOrder record
    selectedJobId; // Holds the Id of the select job in the dropdown input
    selectedJobDuration = 0.0; // Holds the 'Duration' field value on the selected WorkOrder record
    selectedJobNumber; // Holds the 'Job Number' field value on the selected WorkOrder record
    selectedJobStatus; // Holds the 'Status' field value on the selected WorkOrder record
    selectedJobWorkOrder; // Holds the 'WorkOrderNumber' field value on the selected WorkOrder record
    selectedJobIsScheduled = false; // True - Job is already scheduled, False - Job is not currently scheduled
    selectedJobScheduledDate; // Holds the StartDate - EndDate string
    selectedJobDisplayName; // Holds the 'Display_Name__c' field value on the selected WorkOrder record
    /* Job Specific vars: STOP */
    showScheduleBtn = false;
    recordPageUrl;
    mmbEmpId;
    statusClass = 'cc-job-data-text ';
    isCanceling = false; // Flag that is set to TRUE when the user clicks 'Cancel Appointment' button

    /* Wired method will query for the WorkOrder records and populate the 'Select Job' dropdown */
    setupData() {
        console.log(this.recordId);
        getUpdateWorkOrder({
            quoteId : this.recordId,
             jobId: null,
             updateRecord: false,
             apptData: null
        })
        .then(result => {
            if (result.isSuccess){
                this.isLoading = false;
                if (result.workOrders.length > 0){
                    this.jobsList = result.workOrders;
                    this.buildSelectOptions(this.jobsList);
                    // If there's only 1 job, auto-select it and make the call-out
                    if (this.jobsList.length === 1) {
                        this.selectedJob = this.selectOptions[0].value;
                        this.selectedJobId = this.jobsList[0].Id;
                        this.selectedJobDuration = this.jobsList[0].Duration;
                        this.selectedJobNumber = this.jobsList[0].Job_Number__c;
                        this.selectedJobStatus = this.jobsList[0].Status;
                        this.selectedJobWorkOrder = this.jobsList[0].WorkOrderNumber;
                        this.selectedJobDisplayName = this.jobsList[0].Display_Name__c;
                        if (this.selectedJobStatus === 'Scheduled'){
                            console.log(this.jobsList[0]);
                            this.isRescheduling = true;
                            this.selectedJobStartDate = this.jobsList[0].StartDate;
                            this.selectedJobEndDate = this.jobsList[0].EndDate;
                            this.selectedJobScheduledDate = this.jobsList[0].Installation_Date_Time__c;
                            this.statusClass += 'job-scheduled';
                            this.buttonText = 'Re-schedule Install';
                            this.selectedJobIsScheduled = true;
                        } else {
                            this.statusClass = 'cc-job-data-text';
                        }
                        this.doInitialCallout();
                    }
                } else {
                    this.showToastEvent('', 'Error: No WorkOrder records found.', 'error');
                    this.closeQuickAction();
                }
            } else {
                this.isLoading = false;
                this.showToastEvent('','Error: ' + result.message, 'error');
                this.closeQuickAction();
            }
        })
        .catch(error => {
            this.isLoading = false;
            this.showToastEvent('','Error: ' + error.message, 'error');
            this.closeQuickAction();
        });
    }


    // KF: added getQuote wire method to grab Estimated_Install_Duration__c field from quote to satisfy requirements in SFCPQ-475.
     @wire(getRecord, { recordId: '$recordId', fields: [JOB_DURATION, ID_FIELD] })
        getQuote({ error, data }) {
            if (data) {
                this.estimatedJobDuration = data.fields.Estimated_Install_Duration__c.value;
                this.setupData();
            }
            else if (error) {
                console.log('wired quote error: ', error);
            }
        }


    /* Helper method that will populate today's date into the Datepicker field and make a call-out to JST to get available appointments */
    doInitialCallout() {
        try {

            // Check if we are > 12 hours in duration
            if (this.estimatedJobDuration > this.acceptableJobDuration) {
                this.showToastEvent('', this.maxJobDurationExceededVerbaige, 'warning');
                this.closeQuickAction();
            } else {
                this.error = '';
                let today = new Date();
                this.input_startDate = today.toISOString().split('T')[0];

                // Calc the end date: start
                let splitDate = this.input_startDate.split('-');

                // Convert back into date object
                let tempDate = new Date(`${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`);

                // Add 7 days to today's date, put it in string format and assign to to endDate
                this.endDate = new Date(tempDate.setDate(tempDate.getDate() + 7)).toISOString().split('T')[0];
                // Calc the end date: stop

                this.apptOffers = this.getAvailableOffers(this.input_startDate, this.endDate);
            }
        }
        catch(e) {
            this.isLoading = false;
            this.disableInputFields = false;
            this.error = e.message;
            this.showToastEvent('Exception caught:', this.error, 'error');
        }
    }

    /* Helper method to build Select objects to display in drop-down input */
    buildSelectOptions(data){
        let optionsArray = [];
        data.forEach(record => {
            let labelName = record.Display_Name__c;
            // Concatenate a bunch of data to the 'value' attribute, to be parsed out on selection
           let selectObj = {
               label : labelName,
               value :  record.Id + '|' +
                        record.Job_Number__c + '|' +
                        record.Duration + '|' +
                        record.Status + '|' +
                        record.WorkOrderNumber + '|' +
                        record.StartDate + '|' +
                        record.EndDate + '|' +
                        record.Display_Name__c,
               description: record.Description
           };
           optionsArray.push(selectObj);
        });
        this.selectOptions = optionsArray;
    }

    handleChange(event) {
        this.selectedJob = event.detail.value;
        this.showScheduleBtn = false;

        // Parse out concatenated data: START
        let dataArray = event.detail.value.split('|');
        this.selectedJobId = dataArray[0]; // Store WorkOrder.Id
        this.selectedJobNumber = dataArray[1]; // Store WorkOrder.Job_Number__c
        this.selectedJobDuration = dataArray[2]; // Store WorkOrder.Duration
        this.selectedJobStatus = dataArray[3]; // Store WorkOrder.Status
        this.selectedJobWorkOrder = dataArray[4]; // Store WorkOrder.WorkOrderNumber
        this.selectedJobStartDate = dataArray[5]; // Store WorkOrder.StartDate
        this.selectedJobEndDate = dataArray[6]; // Store WorkOrder.EndDate
        this.selectedJobDisplayName = dataArray[7]; // Store WorkOrder.Display_Name__c
        // Parse out concatenated data: STOP

        if (this.selectedJobStatus === 'Scheduled'){
            this.statusClass += 'job-scheduled';
            this.selectedJobIsScheduled = true;
            this.buttonText = 'Re-schedule Install';
            this.isRescheduling = true;
        } else {
            this.statusClass = 'cc-job-data-text';
            this.isRescheduling = false;
            this.buttonText = 'Schedule Install';
            this.selectedJobIsScheduled = false;
        }
        this.doInitialCallout();
    }

    /*
     * Helper function to redirect user to WorkOrder reocrd
     */
     openWorkOrder() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__recordPage',
            attributes: {
                recordId: this.selectedJobId,
                actionName: 'view',
                objectApiName: 'WorkOrder'
            },
        }).then((url) => {
            window.open(url);
        })
     }

     /*
      * Helper function to flip the 'isCanceling' flag
      */
      toggleCancel(){
          this.isCanceling = !this.isCanceling;
      }


    /*
     *  Helper function to close the quick action window on successful appointment booking
     */
    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    /**
     * Function to make callout ot JST to get available Offers
     */
     getAvailableOffers(startDate, endDate) {
         this.error = '';
         this.isLoading = true;
         this.loadingText = 'Retrieving available appointment times. Please wait...';
         this.disableInputFields = true;

        postOffers({
            jobID : parseInt(this.selectedJobNumber),
            startDate : startDate,
            endDate : endDate
        })
        .then((result) => {
            if (result.isSuccess){
                this.isLoading = false;
                this.showJobScheduleSection = true;
                this.disableInputFields = false;
                return this.formatInputOptions(result.offers);
            } else {
                this.isLoading = false;
                this.showJobScheduleSection = false;
                this.disableInputFields = false;
                this.error = result.responseMessage;
                this.showToastEvent('', this.error, 'error');
                this.closeQuickAction();
            }
        })
        .catch(error => {
            console.log('getAvailableOffers callout error: ', error);
                this.isLoading = false;
                this.showJobScheduleSection = false;
            this.disableInputFields = false;
            if (Array.isArray(error.body)){
                this.error = error.body.map(e => e.message).join(', ');
                this.isLoading = false;
                this.showToastEvent('', this.error, 'error');
            } else if (typeof error.body.message === 'string'){
                this.error = error.body.message;
                this.isLoading = false;
                this.showToastEvent('', this.error, 'error');
            }
            this.closeQuickAction();
        });
    }

    /*
     * Helper function for displaying toast messages
     */

    showToastEvent(titleInput, msgInput, variantInput){
      const toastEvt = new ShowToastEvent({
        title : titleInput,
        message : msgInput,
        variant : variantInput
      });
      this.dispatchEvent(toastEvt);
    }

    /*
     * Function to handle when a new date is selected. This method will make a callout to JST with the new
     * selected date value
     */

    handleDateChange(event) {
          this.showScheduleBtn = false;

          // Get the date value from the event
          this.input_startDate = event.detail.value;

          // Check if the date is < today's date
          let newDate = new Date();
          let todaysDate = newDate.toISOString().split('T')[0]; // Get just the date value
          if (this.input_startDate < todaysDate) {
                this.input_startDate = todaysDate;
                this.showToastEvent('', 'Cannot select a date in the past.', 'warning');
                this.apptOffers = [];
          } else {
              // Split via '-' character into array
              let splitDate = this.input_startDate.split('-');

              // Convert back into date object
              let tempDate = new Date(`${splitDate[1]}-${splitDate[2]}-${splitDate[0]}`);

              // Add 7 days to today's date, put it in string format and assign to to endDate
              this.endDate = new Date(tempDate.setDate(tempDate.getDate() + 7)).toISOString().split('T')[0];

              this.apptOffers =  this.getAvailableOffers(this.input_startDate, this.endDate);

              if (this.apptOffers){
                this.showOffers = true;
              }
          }
    }

    /*
     * Function to handle canceling an appointment
     */
     handleCancel(){
         this.isLoading = true;
         this.loadingText = 'Attempting to cancel appointment. Please wait...';
        deleteAppointment({
            jobId: parseInt(this.selectedJobNumber),
            quoteId: this.recordId
        })
        .then(result => {
            if (result.isSuccess){
                
                this.isLoading = false;
                this.showToastEvent('','Appointment successfully cancelled!', 'success');
                this.closeQuickAction();
            } else {
                this.isLoading = false;
                this.showToastEvent('', 'Error encountered: ' + result.message, 'error');
            }
        })
        .catch(error => {
            console.log('deleteAppointment callout error: ', error);
            this.isLoading = false;
            this.showJobScheduleSection = false;
            this.disableInputFields = false;
            if (Array.isArray(error.body)){
                this.error = error.body.map(e => e.message).join(', ');
                this.isLoading = false;
                this.showToastEvent('', this.error, 'error');
            } else if (typeof error.body.message === 'string'){
                this.error = error.body.message;
                this.isLoading = false;
                this.showToastEvent('', this.error, 'error');
            }
        });
     }

    /*
     * Helper function to transform callout response 'offer' data into Radio input elements
     */
    formatInputOptions(data){
        let inputOptions = [];
        data.forEach(offer => {
              let newLabel = this.formatOffer(offer);
              let newObj = { 'label' : newLabel, 'value' :  offer.date_Z +'|'+
                                                            offer.timeBandId +'|'+
                                                            offer.startDateTime +'|'+
                                                            offer.endDateTime };
              inputOptions.push(newObj);
          });
          this.apptOffers = inputOptions;
        }


        /*
         *  Helper function that will take in a returned 'offer' object that represents an appointment slot,
         *  and will build a nice looking, easy to read label for each radio button selection that is
         *  displayed
         */
        formatOffer(offer){
          let finalLabel; // Stores what will be displayed to the user
          let startDate; // Stores date value of start date
          let startTime; // Stores time value of start time
          let endDate; // Stores the date value of the end date
          let endTime; // Stores the time value of the end time
          let weekDay; // Stores the day of the week

          /* START DATE PROCESSING: START */
          // Extract the start date and time
          startDate = offer.startDateTime.split('T')[0];
          startTime = offer.startDateTime.split('T')[1];

          let monthVal = startDate.split('-')[1];
          let yearVal = startDate.split('-')[0];
          let dayVal = startDate.split('-')[2];

          // Convert start date string into Date object for manipulation
          let newStartDate = new Date(startDate);

          let hourVal = new Date(offer.startDateTime);
          let suffix = hourVal.getHours() >= 12 ? 'PM' : 'AM';

          // Modify data for easier readability on front-end (putting it in to 12-hour format)
          if (hourVal.getHours() > 12){
            let newHourVal = `${hourVal.getHours() - 12}`;
            let minutesVal = hourVal.getMinutes() === 0 ? '00' : '0';
            hourVal.setHours(newHourVal);
            hourVal.setMinutes(minutesVal);
            startTime = `${newHourVal}:${minutesVal}`;
          } else {
            startTime = `${hourVal.getHours()}:${hourVal.getMinutes() === 0 ? '00' : hourVal.getMinutes()}`;
          }
          /* START DATE PROCESSING: STOP */

          /* END DATE PROCESSING: START */

          // Extract the end date and time
          endDate = offer.endDateTime.split('T')[0];
          endTime = offer.endDateTime.split('T')[1];

          let endMonthVal = endDate.split('-')[1];
          let endYearVal = endDate.split('-')[0];
          let endDayVal = endDate.split('-')[2];

          // Convert end date string into Date object for manipulation
          let newEndTime = new Date(offer.endDateTime);
          let endSuffix = newEndTime.getHours() >= 12 ? 'PM' : 'AM';

          if (newEndTime.getHours() > 12){
            let newHourVal = `${newEndTime.getHours() - 12}`;
            let minutesVal = newEndTime.getMinutes() === 0 ? '00' : '0';
            newEndTime.setHours(newHourVal);
            newEndTime.setMinutes(minutesVal);
            endTime = `${newHourVal}:${minutesVal}`;
          } else {
            endTime = `${newEndTime.getHours()}:${newEndTime.getMinutes() === 0 ? '00' : newEndTime.getMinutes()}`;
          }

          /* END DATE PROCESSING: STOP */

          weekDay = this.getWeekDay(newStartDate.getDay());

          finalLabel = `${weekDay} ${monthVal}/${dayVal}/${yearVal} ${startTime}${suffix} - ${endTime}${endSuffix}`;

          return finalLabel;
        }

    /*
     * Function called when an appt offer is selected, used to update the selected Date and timeband information as this is needed
     * when the 'Schedule Install' button is pressed
     */
    handleOfferSelect(event){
      this.showScheduleBtn = true;

      // Parse out the selected date and timeBandId values
      let dataArray = event.target.dataset.id.split('|');
      this.selectedDate = dataArray[0];
      this.selectedTimeBand = dataArray[1];
      this.selectedStartTime = dataArray[2];
      this.selectedEndTime = dataArray[3];
    }

     /*
         *  Function called when 'Schedule Install' button clicked, makes callout to JST to schedule the install
         *  for the selected appointment offer
         */
        handleSubmit(){
            this.error = ''; // reset error message var
            this.isLoading = true;
            this.loadingText = 'Booking appointment slot. Please wait...';
            this.disableInputFields = true;
         // Parse out the Job Id
            let jobId = parseInt(this.selectedJobNumber);
                // Check if we are rescheduling an existing job, or scheduling a new job
                if (this.isRescheduling) {
                    // Make 'PUT' callout
                    putReschedule({
                        jobId : jobId,
                        timeBandId : this.selectedTimeBand,
                        date_Z : this.selectedDate,
                        quoteId: this.recordId
                    })
                    .then(result => {
                        if (result.isSuccess){      
                            const fields = {};
                            fields[ID_FIELD.fieldApiName] = this.recordId;
                            fields[STATUS.fieldApiName] = 'Scheduled';
                            const recordInput = { fields };
                            updateRecord(recordInput)
                            .then(() => {
                                
                                this.isLoading = false;
                                this.disableInputFields = false;
                                this.showToastEvent('','Appointment successfully re-scheduled.', 'success');
                                refreshApex(this.returnedData);
                                this.closeQuickAction();
                            })
                            .catch(error => {
                                console.log('error updating rescheduled record status: ', error);
                            });
                        } else {
                            this.isLoading = false;
                            this.disableInputFields = false;
                            this.error = result.responseMessage;
                            this.showToastEvent('', this.error, 'error');
                        }
                    })
                    .catch(error => {
                        this.isLoading = false;
                        this.disableInputFields = false;
                        this.jobSchedulingError = true;
                        if (error.body.message){
                            if (typeof error.body.message === 'string'){
                                this.error = error.body.message;
                                this.showToastEvent('', this.error, 'error');
                            }
                            else if (Array.isArray(error.body)){
                                this.error = error.body.map(e => e.message).join(', ');
                                this.showToastEvent('', this.error, 'error');
                            }
                            else {
                                this.error = error.body.message;
                                this.disableInputFields = false;
                            }
                        }
                    });
                } else {
                // Make 'POST' callout
                postSchedule({
                    jobId : jobId,
                    timeBandId : this.selectedTimeBand,
                    date_Z : this.selectedDate,
                    quoteId: this.recordId
                })
                .then(result => {
                    // KF: adding update record functionality to update Quote record to 'Scheduled' upon submit. SFCPQ-54 AC5
                    if (result.isSuccess){
                        const fields = {};
                        fields[ID_FIELD.fieldApiName] = this.recordId;
                        fields[STATUS.fieldApiName] = 'Scheduled';
                        const recordInput = { fields };
                        updateRecord(recordInput)
                        .then(()=> {
                                                    
                            this.isLoading = false;
                            this.disableInputFields = false;
                            this.showToastEvent('','Appointment successfully scheduled.','success');
                            refreshApex(this.returnedData);
                            this.closeQuickAction();
                        })
                        .catch(error => {
                            console.log('error updating record status: ', error);
                        });                        
                    } else {
                        this.isLoading = false;
                        this.disableInputFields = false;
                        this.error = result.responseMessage;
                        this.showToastEvent('', this.error, 'error');
                    } 
                })
                .catch(error => {
                    console.log('error: ', error);
                    this.isLoading = false;
                    this.disableInputFields = false;
                    this.jobSchedulingError = true;
                    if (error.body.message){
                        if (typeof error.body.message === 'string'){
                            this.error = error.body.message;
                            this.showToastEvent('', this.error, 'error');
                        }
                        else if (Array.isArray(error.body)){
                            this.error = error.body.map(e => e.message).join(', ');
                            this.showToastEvent('', this.error, 'error');
                        }
                        else {
                            this.error = error.body.message;
                            this.disableInputFields = false;
                        }
                    }
                });
            }
        }

    /*
     *  Helper function that will take in a returned 'offer' object that represents an appointment slot,
     *  and will build a nice looking, easy to read label for each radio button selection that is
     *  displayed
     */
    formatAppointment(){
      let finalLabel; // Stores what will be displayed to the user
      let startDate; // Stores date value of start date
      let startTime; // Stores time value of start time
      let endDate; // Stores the date value of the end date
      let endTime; // Stores the time value of the end time
      let weekDay; // Stores the day of the week

      /* START DATE PROCESSING: START */
      // Extract the start date and time
      console.log('selectedJobStartDate: ', this.selectedJobStartDate);
      startDate = this.selectedJobStartDate.split('T')[0];
      startTime = this.selectedJobStartDate.split('T')[1];

      console.log('startTime: ', startTime);


      let monthVal = startDate.split('-')[1];
      let yearVal = startDate.split('-')[0];
      let dayVal = startDate.split('-')[2];

      // Convert start date string into Date object for manipulation
      let newStartDate = new Date(startDate);

      let hourVal = new Date(this.selectedJobStartDate);
      let suffix = hourVal.getHours() >= 12 ? 'PM' : 'AM';

      // Modify data for easier readability on front-end (putting it in to 12-hour format)
      if (hourVal.getHours() > 12){
        let newHourVal = `${hourVal.getHours() - 12}`;
        let minutesVal = hourVal.getMinutes() === 0 ? '00' : '0';
        hourVal.setHours(newHourVal);
        hourVal.setMinutes(minutesVal);
        startTime = `${newHourVal}:${minutesVal}`;
      } else {
        startTime = `${hourVal.getHours()}:${hourVal.getMinutes() === 0 ? '00' : hourVal.getMinutes()}`;
      }
      /* START DATE PROCESSING: STOP */

      /* END DATE PROCESSING: START */

      // Extract the end date and time
      endDate = this.selectedJobEndDate.split('T')[0];
      endTime = this.selectedJobEndDate.split('T')[1];

      let endMonthVal = endDate.split('-')[1];
      let endYearVal = endDate.split('-')[0];
      let endDayVal = endDate.split('-')[2];

      // Convert end date string into Date object for manipulation
      let newEndTime = new Date(this.selectedJobEndDate);
      let endSuffix = newEndTime.getHours() >= 12 ? 'PM' : 'AM';

      if (newEndTime.getHours() > 12){
        let newHourVal = `${newEndTime.getHours() - 12}`;
        let minutesVal = newEndTime.getMinutes() === 0 ? '00' : '0';
        newEndTime.setHours(newHourVal);
        newEndTime.setMinutes(minutesVal);
        endTime = `${newHourVal}:${minutesVal}`;
      } else {
        endTime = `${newEndTime.getHours()}:${newEndTime.getMinutes() === 0 ? '00' : newEndTime.getMinutes()}`;
      }

      /* END DATE PROCESSING: STOP */

      weekDay = this.getWeekDay(newStartDate.getDay());

      finalLabel = `${weekDay} ${monthVal}/${dayVal}/${yearVal} ${startTime}${suffix} - ${endTime}${endSuffix}`;

      return finalLabel;
    }

    /*
     *  Helper function for getting the day of the week, based on the passed in date
     */

    getWeekDay(value){
      switch (value) {
        case 0:
          return 'Mon';
        case 1:
          return 'Tues';
        case 2:
          return 'Wed';
        case 3:
          return 'Thur';
        case 4:
          return 'Fri';
        case 5:
          return 'Sat';
        case 6:
          return 'Sun';
        default:
      }
    }
}