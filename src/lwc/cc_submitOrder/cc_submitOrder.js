import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import { updateRecord, getRecord } from 'lightning/uiRecordApi';
import { CloseActionScreenEvent } from 'lightning/actions';
import submitOrder from '@salesforce/apex/ADTRedVentureAPI.submitOrder';
import queryWorkOrderRecords from '@salesforce/apex/ADTRedVentureAPI.queryWorkOrderRecords'; // MParrella | 03-03-2022 | PROD HOT FIX
import ID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Id';
import STATUS_FIELD from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Status__c';
import CONTRACT_STATUS_FIELD from '@salesforce/schema/SBQQ__Quote__c.Contract_Status__c';
import SUBMITTED_DATETIME_FIELD from '@salesforce/schema/SBQQ__Quote__c.Submitted_Date_Time__c'; // MParrella | 02-03-2022 | SFCPQ-693//
import { subscribe, unsubscribe, onError, setDebugFlag, isEmpEnabled } from 'lightning/empApi';

const FIELDS = [ID_FIELD, STATUS_FIELD];

export default class CcSubmitOrder extends LightningElement {
    channelName = '/event/Job_Data_Received__e';
    isLoading = true;
    loadingText = 'Submitting order to OMS...';
    @api recordId;
    subscription = {};
    timer; // used to control the SOQL check for WorkOrders (MParrella | 03-03-2022 | PROD HOT FIX)
    initialDelay = 20000; // Delay (in ms) between when the component is rendered and when the SOQL interval loop will start executing (MParrella | 03-03-2022 | PROD HOT FIX)
    soqlDelay = 8000; // Delay (in ms) for how often we will query looking for WorkOrder Objects related to the quote (MParrella | 03-03-2022 | PROD HOT FIX)
    successfulSubmit = false; // This will be set to true if order submission is successful; Controls whether or not we start querying for WorkOrders or not

    connectedCallback() {
        this.handleSubscribe();
        // MParrella | 03-03-2022 | PROD HOT FIX: START
        // Waiting 30 seconds, then start polling for WorkOrder record + MMB Job # related to Quote
        setTimeout(() => {
            this.setWaitTimeElapsed();
        }, this.initialDelay);
        // MParrella | 03-03-2022 | PROD HOT FIX: STOP
    }


    // MParrella | 03-03-2022 | PROD HOT FIX: START
    setWaitTimeElapsed(){
        // Check to see if submission was successful, if yes, continue with checking for WorkOrder records
        if (this.successfulSubmit){
            console.log('Starting WorkOrder SOQL check shortly...');
            // Execute 'queryWorkOrderRecords' Apex method every ~8 seconds until a record is returned, or the platform event fires a response from the trigger (whichever comes first)
            this.timer = setInterval(() => {
                console.log('## Firing WorkOrder SOQL query ##');
               queryWorkOrderRecords({
                  quoteId : this.recordId
               })
               .then(result => {
                   if (result === true){
                       console.log('Found WorkOrder record!');
                       clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX // stop the interval method
                       this.isLoading = false;
                       this.successfulSubmit = false;
                       this.updateStatus();
                       this.showToastEvent('', 'Contract email sent to customer', 'success');
                       this.closeQuickAction();
                   }
               })
               .catch(error => {
                   clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
                    console.log('***Error encountered when checking for WorkOrders: ', error);
               });
            }, this.soqlDelay);
        }
    }
    // MParrella | 03-03-2022 | PROD HOT FIX: STOP

    doCallout() {
        submitOrder({
               quoteId: this.recordId
           })
           .then(result => {
               if (result.isSuccess){
                   /* MParrella | 03-03-2022 | PROD HOT FIX: START */
                    this.successfulSubmit = true;
                    console.log('SubmitOrder Response: ', result);
                   /* MParrella | 03-03-2022 | PROD HOT FIX: STOP */
               } else {
                   this.showToastEvent('', result.errorMsg, 'error');
                   this.successfulSubmit = false;
                   clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX 
                   this.closeQuickAction();
               }
           })
           .catch(error => {
               clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
           });
    }

    disconnectedCallback() {
        this.handleUnsubscribe();
        clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
    }

    handleChannelName(event) {
        this.channelName = event.target.value;
    }

    handleSubscribe() {
        let self = this;
        const messageCallback = (response) => {
            let responseObj = JSON.parse(JSON.stringify(response));
            console.log('response: ' , responseObj);
            if (responseObj.data.payload.IsSuccess__c) {
                self.isLoading = false;
                clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
                self.successfulSubmit = false; // MParrella | 03-03-2022 | PROD HOT FIX
                self.showToastEvent('', 'Contract email sent to customer', 'success');
                self.updateStatus();
                self.closeQuickAction();
            } else {
                self.isLoading = false;
               clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
                self.showToastEvent('', 'ERROR: ' + responseObj.data.payload.Message__c, 'error');
                self.closeQuickAction();
            }
        }

        subscribe(this.channelName, -1, messageCallback).then(response => {
//           console.log('Successfully subscribed to: ', JSON.stringify(response.channel)); // MParrella | 03-03-2022 | PROD HOT FIX
        });

    }

    handleUnsubscribe()
        {
            // Invoke unsubscribe method of empApi
            unsubscribe(this.subscription, response => {
                console.log('unsubscribe() response: ', JSON.stringify(response));
                // Response is true for successful unsubscribe
            });
        }



    registerErrorListener() {
            // Invoke onError empApi method
            onError((error) => {
                this.showToastEvent('', 'ERROR: ' + JSON.stringify(error), 'error');
                console.log('Received error from server: ', JSON.stringify(error));
                // Error contains the server-side error
            });
        }

    @wire(getRecord, { recordId : '$recordId', fields : FIELDS})
    wiredQuote({ data, error }) {
        if (data){
            if (data.fields.SBQQ__Status__c.value !== 'Paid') {
                this.showToastEvent('', 'Quote is not in the correct Status for submitting an order.', 'error');
               clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
                this.closeQuickAction();
            } else {
                refreshApex(this.wiredQuote);
                this.doCallout();
            }
        } else if (error) {
            console.log('error: ', error);
               clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
        }
    }


    showToastEvent(inputTitle, inputMsg, inputVariant) {
        this.dispatchEvent(new ShowToastEvent({
            title: inputTitle,
            message: inputMsg,
            variant: inputVariant
        }));
    }

    closeQuickAction() {
       this.successfulSubmit = false; // MParrella | 03-03-2022 | PROD HOT FIX
       clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
     this.dispatchEvent(new CloseActionScreenEvent());
    }

    updateStatus() {
        const fields = {};
        // MParrella | 02/03/2022 | SFCPQ-693: Populating 'Submitted Date Time' field after successful submission: START
        let todaysDate = new Date();
        let newTodaysDate = todaysDate.toISOString();
        fields[ID_FIELD.fieldApiName] = this.recordId;
        fields[SUBMITTED_DATETIME_FIELD.fieldApiName] = newTodaysDate;
        // MParrella | 02/03/2022 | SFCPQ-693: Populating 'Submitted Date Time' field after successful submission: STOP//

        const recordInput = { fields };

        updateRecord(recordInput)
        .then(()=> {

        })
        .catch((error) => {
           clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
            console.log(error);
        });
    }


   // MParrella | 03-03-2022 | PROD HOT FIX: START
   queryForWorkOrder() {
       console.log('## Firing WorkOrder SOQL query ##');
       queryWorkOrderRecords({
          quoteId : this.recordId
       })
       .then(result => {
           if (result === true){
               this.isLoading = false;
               clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
               this.showToastEvent('', 'Contract email sent to customer', 'success');
               this.updateStatus();
               this.closeQuickAction();
           }
       })
       .catch(error => {
           clearInterval(this.timer); // MParrella | 03-03-2022 | PROD HOT FIX
            console.log('***Error encountered when checking for WorkOrders: ', error);
       });
   }
   // MParrella | 03-03-2022 | PROD HOT FIX: STOP
}