/**
 * Created by Matt on 8/4/2021.
 */

import { LightningElement,wire,api } from 'lwc';
//import handlePayments from '@salesforce/apex/cc_paymentHPP_Controller.handlePayments';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { createRecord, getRecord, updateRecord } from 'lightning/uiRecordApi';
import { CloseActionScreenEvent } from 'lightning/actions';
import { refreshApex } from '@salesforce/apex';

/* Apex Method Import: START */
import generateHPPRequest from '@salesforce/apex/ADTPaymentTechHPPAPI.generateHPPRequest';
import getTransactionData from '@salesforce/apex/ADTPaymentTechHPPAPI.getTransactionData';
/* Apex Method Import: STOP */

/* Custom Label Import: START */
import Domain_VFPAGE_url from '@salesforce/label/c.Domain_VFPAGE_url';
/* Custom Label Import: STOP */

/* Transaction Field Import: START */
import TXN_CC_NAME_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Name_on_CC__c';
import TXN_LAST_4_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Last_4_Used__c';
import TXN_CARD_TYPE_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Card_Type__c';
import TXN_EXP_DATE_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Exp_Date__c';
import TXN_AMT_AUTHED_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_Amount_Authorized__c';
import TXN_GUID_FIELD from '@salesforce/schema/SBQQ__Quote__c.TXN_GUID__c';
import TXN_REF_NUM_FIELD from '@salesforce/schema/SBQQ__Quote__c.TX_Reference_Number__c';
import TXN_TIMESTAMP_FIELD from '@salesforce/schema/SBQQ__Quote__c.Transaction_Timestamp__c';
/* Transaction Field Import: STOP */

/* Quote Field Import: START */
import QUOTE_ID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Id';
import QUOTE_STATUS_FIELD from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Status__c';
import QUOTE_ACCOUNT_FIELD from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Account__c';
/* Quote Field Import: STOP */

/* Payment Method Field Import: START */
import PAYMENT_INFO_OBJ from '@salesforce/schema/blng__PaymentMethod__c'
import PI_ACCOUNT_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__Account__c';
import PI_CC_EXP_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__CardExpirationYear__c';
import PI_CC_EXP_MONTH_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__CardExpirationMonth__c';
import PI_CC_LAST4_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__CardLastFour__c';
import PI_CC_NAME_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__Nameoncard__c';
import PI_CC_TYPE_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__CardType__c';
import PI_PAY_TYPE_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__PaymentType__c';
import PI_QUOTE_FIELD from '@salesforce/schema/blng__PaymentMethod__c.Quote__c';
import PI_PROFILE_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__PaymentGatewayToken__c';
import PI_ORDER_FIELD from '@salesforce/schema/blng__PaymentMethod__c.Order__c';
import PI_FIRSTNAME_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__BillingFirstName__c';
import PI_LASTNAME_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__BillingLastName__c';
import PI_STREET_ADDRESS_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__BillingStreet__c';
import PI_STREET_ADDRESS2_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__StreetAddress2__c';
import PI_STATE_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__BillingStateProvince__c';
import PI_CITY_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__BillingCity__c';
import PI_ZIP_FIELD from '@salesforce/schema/blng__PaymentMethod__c.blng__BillingZipPostal__c';
/* Payment Method Field Import: STOP */

export default class CcPaymentHpp extends NavigationMixin(LightningElement) {
    @api isLoading = false;
    subscription = null;
    @api buttonDisabled;
    errorMessage;
    buttonText = 'Go to Chase Paymentech';
    loadingText; // The text displayed for each of the loading screens
    uID; // UID obtains from Paymentech
    paymentWindow; // JS 'window' object used to open HPP and send messages back to Salesforce
    vfPageDomain = Domain_VFPAGE_url; // Holds the domain url of VF Pages (used to ensure we're handling the correct message)
    returnedData; // Holds response message data from Paymentech HPP
    @api recordId; // Passed-in Quote record Id
    @api totalAmt = 0.0;
    @api accountId; // Passed-in related account Id
    isWindowClose = false; // Used for communicating with parent LWC (changed when user chooses to close HPP after Due at Sale amount change)
    @api achProcessingReq; // Flag to determine if we need to validate ACH info before launching HPP or not
    @api achInfo; // Holds ACH info that was entered, used for making callout
    txData; // Holds Transaction data
    @api ccRecur; // Boolean flag that when set to TRUE means we're only using CC for recurring charges, and total amount authorized should only be $2
    @api ccInstall; // Boolean flag that when set to TRUE means we're using CC for the Installation charge (used in conjunction with 'ccRecur' to determine proper auth amount)

    @api updateTotalAmt(value){
        console.log('updating totalAmt to: ', value);
        this.totalAmt = value;
    }

    /* Helper method to fire an event to notify the parent LWC that the HPP window has been opened */
    fireWindowOpenEvent() {
        this.dispatchEvent(new CustomEvent('windowopen'));
    }


    fireWindowCloseEvent() {
        this.dispatchEvent(new CustomEvent('windowclose'));
    }

    // This event listener is what listens for the redirect from Paymentech and is how we know that authorization was successful from another window
    constructor(){
        super();
        window.addEventListener('message', this.handleMessage);
    }

    disconnectedCallback(){
        window.removeEventListener('message', this.handleMessage);
    }

    /* This method handles the returned data from the HPP browser tab after a redirect */
    handleMessage = (event) => {
        // Check to ensure we're receiving this message from the expected VF Page
        if (event.origin === this.vfPageDomain){
            this.returnedData = JSON.parse(JSON.stringify(event.data));

            if (this.returnedData.code === '000'){
                this.uID = this.returnedData.uid;
                this.loadingText = "Status: Transaction data retrieved. Processing results...";
                this.getTransactionInformation(this.uID);

                //Call method to create payment
               //handlePayments({Id: this.RecordId, Amount: this.order.data.fields.Installment_Amount__c.value });

            } else if (this.returnedData.code === ''){ // 'Cancel' button click on HPP
                this.fireWindowCloseEvent();
                this.closeQuickAction();
                this.isLoading = false;
                this.buttonDisabled = false;
                this.buttonText = 'Goto Chase Paymentech';
                this.loadingText = '';
                this.errorMessage = this.returnedData.message;
                this.fireCompletedEvent(false);
                this.showToastEvent('', 'Transaction cancelled.', 'error');
                this.closeQuickAction();
            } else {
                this.closeQuickAction();
                this.isLoading = false;
                this.buttonDisabled = false;
                this.buttonText = 'Goto Chase Paymentech';
                this.loadingText = '';
                this.errorMessage = this.returnedData.message;
                this.fireCompletedEvent(false);
                this.showToastEvent('', 'Transaction cancelled.', 'error');
            }
        }
    }

    showToastEvent(titleInput, messageInput, variantInput){
        const toastEvt = new ShowToastEvent({
            title: titleInput,
            message: messageInput,
            variant: variantInput
        });
        this.dispatchEvent(toastEvt);
    }

    /* Method to determine if we need to call Giact or Paymentech first */
    determineCallout() {
        if (this.achProcessingReq){
            this.isLoading = true;
            this.loadingText = 'Validating Direct Deposit information. Please Wait...';

            // Fire event that ACH LWC will handle and make call-out
            let payload = this.achInfo;
            this.dispatchEvent(new CustomEvent('validateach', { detail:  payload }));
        } else {
            // Initiate Paymentech HPP
            this.getUID();
        }
    }

    @api reset(){
        this.isLoading = false;
        this.loadingText = '';
    }


    /* Method to do initial callout to Paymentech to get UID */
    @api getUID(){
        console.log('CC dueAtSale: ', this.totalAmt);
        let amount = 0.0;
        if (this.totalAmt == 0){
            amount = 2.00;
        } else {
            console.log('due at sale in else: ', this.totalAmt);
            amount = this.totalAmt;
        }
        console.log('totalAmt adjusted', amount);
        this.buttonDisabled = true;
        this.isLoading = true;
        this.loadingText = 'Status:     Attempting to establish secure connection with server.';
        generateHPPRequest({
            recordId: this.recordId,
           total_amt: amount
        })
        .then(result => {
            let url = result.paymentTechURL;
            if (result.isSuccess){
                this.uID = result.uID;
                this.openPortalWindow(url + result.uID);
            } else {
                this.isLoading = false;
                this.loadingText = '';
                this.buttonText = 'Go to Chase Paymentech';
                this.buttonDisabled = false;
                this.showToastEvent('', result.errorMsg, 'error');
                console.log('Error: ', result);
            }
        })
        .catch(error => {
            this.isLoading = false;
            this.loadingText = '';
            this.buttonDisabled = false;
            this.buttonText = 'Go to Chase Paymentech';
            if (error.body != null){
                if (Array.isArray(error.body)){
                    this.error = error.body.map(e => e.message).join(', ');
                    this.showToastEvent('', this.error, 'error');
                } else if (typeof error.body.message === 'string'){
                    this.error = error.body.message;
                    this.showToastEvent('', this.error, 'error');
                }
            }
            console.log('Uncaught exception: ', error);
        });
    }

    // Opens the Paymentech Hosted Pay Page in a new tab
    openPortalWindow(url){
        this.paymentWindow =  window.open(url);
        this.paymentWindow.opener.openingEntity = this.window;
        this.isLoading = true;
        this.fireWindowOpenEvent(); // Let Parent LWC know that the HPP is open in a separate tab
        this.buttonText = 'Opened in separate browser tab';
        this.loadingText = 'Status:     Connection successfully established in separate browser tab. Waiting for transaction to complete...';
    }

    // Used to get Transaction Details
    getTransactionInformation(uIDinput){
        getTransactionData({
            uID : uIDinput,
            recordId : this.recordId
         })
        .then(result => {
            if (result.isSuccess){
                this.txData = result.paymentTransactionWrap;
                this.dispatchEvent(new CustomEvent('ccvalidated', { details: this.txData }));
                this.fireCompletedEvent(true);
                this.buttonText = 'Payment Collected';
                this.isLoading = false;
                this.showToastEvent('','Payment Information Saved' ,'success');
                this.closeQuickAction();
            } else {
                this.isLoading = false;
                this.loadingText = '';
                this.errorMessage = result.errorMsg;
                this.buttonText = 'Launch Secure Portal';
                this.fireCompletedEvent(false);
                this.buttonDisabled = false;
                this.showToastEvent('', result.errorMsg, 'error');
                this.closeQuickAction();
                console.log('Error: ', result);
            }
        })
        .catch(error => {
            this.isLoading = false;
            this.loadingText = '';
            this.buttonDisabled = false;
            this.buttonText = 'Launch Secure Portal';
            if (error.body != null){
                if (Array.isArray(error.body)){
                    this.error = error.body.map(e => e.message).join(', ');
                    this.showToastEvent('', this.error, 'error');
                } else if (typeof error.body.message === 'string'){
                    this.error = error.body.message;
                    this.showToastEvent('', this.error, 'error');
                }
            }
            console.log('Uncaught exception: ', error);
        });
    }

    closeQuickAction() {
        // Check if LWC record context is Order or CPQ Quote
        if (this.recordId.slice(0,3) === '801') {
            const closeEvt = new CustomEvent('close');
            this.dispatchEvent(closeEvt);
        }
        else  {
            this.dispatchEvent(new CloseActionScreenEvent());
        }
    }

    /* Helper method to fire off 'tx_complete' event to rerender parent LWC */
    fireCompletedEvent(isSuccess) {
        if (isSuccess){
            this.dispatchEvent(new CustomEvent('txcomplete', { detail: this.txData}));
        } else {
            this.dispatchEvent(new CustomEvent('txfailure', { detail: this.errorMessage }));
        }
    }

    /* Helper method to close the HPP Window if it's open */
    @api closeHPPWindow() {
        this.paymentWindow.close();
        this.isLoading = false;
        this.buttonDisabled = false;
        this.loadingText = '';
        this.buttonText = 'Goto Chase Paymentech';
    }
}