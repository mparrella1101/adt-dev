import { LightningElement, wire, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord, getFieldValue} from 'lightning/uiRecordApi';
import LINE_ITEM_COUNT from '@salesforce/schema/SBQQ__Quote__c.SBQQ__LineItemCount__c';
import getAddProductsAction from '@salesforce/apex/modifyQuoteRedirectController.getAddProductsAction';
import getProductId from '@salesforce/apex/modifyQuoteRedirectController.getProductId';

export default class ModifyQuoteRedirect extends NavigationMixin(LightningElement) 
{
    @api 
    recordId;

    productId;
    actionId;


    @wire(getRecord, { recordId: '$recordId', fields:[LINE_ITEM_COUNT]})
    wiredRecord({ error, data }) {
        if (error) 
        {
            console.log('ERROR: ', error);
        }
        if (data) 
        {
            console.log('if data block');
            let lineItemCount = getFieldValue(data, LINE_ITEM_COUNT);

                //Go to product configurator
                if(lineItemCount == 0)
                {
                    Promise.all([
                        getAddProductsAction().then(result => 
                            {
                                console.log(result);
                                this.actionId = result;
                            }),
                        getProductId().then(result => 
                            {
                                console.log(result);
                                this.productId = result;
                            })])
                    .then(()=> 
                    {
                        //This is for dynamically getting domain
                        const pageUrl = (' ' + window.location.href).slice(1);
                        const pagePrefix = pageUrl.substring(0,pageUrl.search('.lightning'));
                        //this.navigateToPage('/apex/sbqq__sb?scontrolCaching=1&id=' + this.recordId + '#/product/pc?lineKey=2&qId=' + this.recordId + '&ignoreCache=true');
                        //Action Ids should probably get stored in a custom label -> this going to be a problem later. I can also query for this, not sure what best practice is
                        const url = pagePrefix + '--sbqq.visualforce.com/apex/sbqq__sb?scontrolCaching=1&id=' + this.recordId + '#/product/pc?qId=' + this.recordId + '&aId=' + this.actionId + '&pId=' + this.productId + '&redirectUrl=LineEditor&open=0';
                        console.log(url);
                        this.navigateToPage(url);
                        console.log('this is pagePrefix');
                        console.log(pagePrefix);
                    })
                }
                //Go to finalize quote lwc
                else
                {
                    this.navigateToPage('/lightning/n/Quote_Summary?c__quoteId=' + this.recordId);
                }
            
        }
    }

    


    navigateToPage(link)
    {
        this[NavigationMixin.Navigate]({
            type: 'standard__webPage',
            attributes: {
                url: link
            }
        }, true);
        console.log('navigated');
    }

}