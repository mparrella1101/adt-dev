import { LightningElement, wire } from 'lwc';
import getQuoteLines from '@salesforce/apex/finalizeQuoteDemoController.getQuoteLines';
import getInstallments from '@salesforce/apex/finalizeQuoteDemoController.getInstallments';
import { NavigationMixin } from 'lightning/navigation';

const COLUMNS = 
        [{label: 'PRODUCT NAME/CODE', fieldName: 'SBQQ__ProductName__c', type: 'text', cellAttributes: {
            class: {
                fieldName: 'format'
        }}},
        {label: 'DESCRIPTION', fieldName: 'SBQQ__Description__c', type: 'text', cellAttributes: {
            class: {
                fieldName: 'format'
        }}},
        {label: 'QUANTITY', fieldName: 'SBQQ__Quantity__c', type: 'text', cellAttributes: {
            class: {
                fieldName: 'format'
        }}},
        {label: 'INSTALL', fieldName: 'Install__c', type: 'currency', cellAttributes: {
            class: {
                fieldName: 'format'
        }}},
        {label: 'MONITORING', fieldName: 'Monitoring__c', type: 'currency', cellAttributes: {
            class: {
                fieldName: 'format'
        }}},
        {label: 'MONTHLY', fieldName: 'Monthly__c', type: 'currency', cellAttributes: {
            class: {
                fieldName: 'format'
        }}}];

const promoCOLUMNS = [{label: 'Promotion Name', fieldName: 'promo', type: 'text'}];


const promoDATA = [{
                    id: 'a',
                    promo: '$200 Off Installation of an Outdoor Camera'}];


export default class FinalizeQuoteDemo extends NavigationMixin(LightningElement) 
{
    quoteId;
    columns = COLUMNS;
    promoColumns = promoCOLUMNS;
    promoData = promoDATA;
    value;
    showModal = false;
    filteredData;
    additionalCharge = 0;
    data;
    error;

    installTotal = 0;
    monitoringTotal = 0;
    monthlyTotal = 0;
    totalsString;

    connectedCallback()
    {
        this.getQuoteIdFromURL();
        this.getQuoteInstallments();
        console.log('connectedCallback()');
    }

    getQuoteIdFromURL()
    {
        var url_stringtime = window.location.href;
        var url = new URL(url_stringtime);
        var recId = url.searchParams.get("c__quoteId");
        this.quoteId = recId;
    }

    @wire(getQuoteLines, { quoteId: '$quoteId'})
    quoteData(result){
        if (result.data) {
            this.data = result.data;
            this.error = undefined;
            this.updateDataTable(this.value);

        } else if (result.error) {
            this.error = result.error;
            this.data = undefined;
        }
        
    }

    updateMonthlyPayment(event)
    {
        this.updateDataTable(event.detail.value)
    }
    
    updateDataTable(installment)
    {
        console.log('updateMonthlyPayment()');

        let parseData = JSON.parse(JSON.stringify(this.data));
        console.log('PARSE DATA');
        
        
        parseData.forEach(element =>
            {
                

                if(element.UniqueIdentifier__c !== null)
                {
                    if(element.UniqueIdentifier__c == 'S0008-ANSC')
                    {
                        this.additionalCharge = element.QSP__c;
                    }
                }

                switch(installment)
                {
                    case 'Pay In Full':
                    {
                        console.log('#1');
                        element.Monthly__c = 0 + this.additionalCharge;
                        //Accumulate totals here
                        this.installTotal += element.Install__c;
                        this.monitoringTotal += element.Monitoring__c;
                        this.monthlyTotal += element.Monthly__c;
                        return element;
                    }
                    case '3Pay':
                    {
                        console.log('#2');
                        element.Monthly__c = element.Install__c / 3 + element.Monitoring__c + this.additionalCharge;
                        this.installTotal += element.Install__c;
                        this.monitoringTotal += element.Monitoring__c;
                        this.monthlyTotal += element.Monthly__c;
                        return element;
                        
                    }
                    case '12 Months':
                    {
                        console.log('#3');
                        console.log(element.Monthly__c);
                        element.Monthly__c = element.Install__c / 12 + element.Monitoring__c + this.additionalCharge;
                        this.installTotal += element.Install__c;
                        this.monitoringTotal += element.Monitoring__c;
                        this.monthlyTotal += element.Monthly__c;
                        console.log(element.Monthly__c);
                        return element;
                    }
                    case '24 Months':
                    {
                        console.log('#4');
                        element.Monthly__c = element.Install__c / 24 + element.Monitoring__c + this.additionalCharge;
                        this.installTotal += element.Install__c;
                        this.monitoringTotal += element.Monitoring__c;
                        this.monthlyTotal += element.Monthly__c;
                        return element;
                    }
                    case '36 Months':
                    {
                        console.log('#5');
                        element.Monthly__c = element.Install__c / 36 + element.Monitoring__c + this.additionalCharge;
                        this.installTotal += element.Install__c;
                        this.monitoringTotal += element.Monitoring__c;
                        this.monthlyTotal += element.Monthly__c;
                        return element;
                    }
                    case '60 Months':
                    {
                        console.log('#6');
                        element.Monthly__c = element.Install__c / 60 + element.Monitoring__c + this.additionalCharge;
                        this.installTotal += element.Install__c;
                        this.monitoringTotal += element.Monitoring__c;
                        this.monthlyTotal += element.Monthly__c;
                        return element;
                    }
                }
            });
            if(this.totalsString == null)
            {
                this.totalsString = {Id: '123abc', Install__c: this.installTotal, Monitoring__c: this.monitoringTotal, Monthly__c: this.monthlyTotal, SBQQ__ProductName__c: 'Totals', SBQQ__Quantity__c: '', UniqueIdentifier__c: "-"};
                this.totalsString.format = 'slds-theme_shade slds-text-heading_medium';
                this.totalsString = JSON.stringify(this.totalsString);
                parseData = JSON.parse(JSON.stringify(parseData).replace(']', ',' + this.totalsString + ']'));
            }
            
            this.data = parseData;
    }
    
    
    get comboOptions() {
        console.log('comboOptions()');
        return [
            { label: '-None-', value: 'new' },
            { label: 'Pay In Full', value: 'Pay In Full' },
            { label: '3Pay', value: '3Pay' },
            { label: '12 Months', value: '12 Months' },
            { label: '24 Months', value: '24 Months' },
            { label: '36 Months', value: '36 Months' },
            { label: '60 Months', value: '60 Months' }
        ];
    }

    getQuoteInstallments()
    {
        console.log('getQuoteInstallments()');
        getInstallments({quoteId: this.quoteId})
        .then(result => 
            {
                this.value = result;
            })
    }

    handleNavigate(event) 
    {
        const config = {
          type: "standard__recordPage",
          attributes: {
            recordId: this.quoteId,
            objectApiName: "SBQQ__Quote__c",
            actionName: "view"
          }
        };
        console.log('config', config);
        this[NavigationMixin.Navigate](config);
      }

    gotoQLE(event)
    {
        //Link for navigating to the configure products page
        let link = '/apex/sbqq__sb?scontrolCaching=1&id=' + this.quoteId + '#/product/pc?lineKey=2&qId=' + this.quoteId + '&ignoreCache=true';
                       //This is the url for going to the QLE if we ever need that
                       //'/apex/sbqq__sb?scontrolCaching=1&id='+ this.quoteId + '#quote/le?qId=' + this.quoteId;

              this[NavigationMixin.Navigate]({
                   type: 'standard__webPage',
                   attributes: {
                        url: link
                   }
              }, true);
              console.log('navigated');
             // /apex/sbqq__sb?scontrolCaching=1&id=a6y1k0000000zEnAAI#/product/pc?lineKey=2&qId=a6y1k0000000zEnAAI&ignoreCache=true
    }

    openPaymentModal(event)
    {
        this.showModal = true;
    }

    closeModal()
    {
        this.showModal = false;
    }
}