/**
 * Created by Matt on 8/25/2021.
 */

import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { updateRecord, getRecord } from 'lightning/uiRecordApi';
import postOffers from '@salesforce/apex/ADTJstSchedulingExpApi.postOffers';
import postSchedule from '@salesforce/apex/ADTJstSchedulingExpApi.postSchedule';
import putReschedule from '@salesforce/apex/ADTJstSchedulingExpApi.putReschedule';
import deleteAppointment from '@salesforce/apex/ADTJstSchedulingExpApi.deleteAppointment';
import { CloseActionScreenEvent } from 'lightning/actions';
import USER_ID from '@salesforce/user/Id';
import JOB_DURATION_FIELD from '@salesforce/schema/SBQQ__Quote__c.Estimated_Install_Duration__c';
import JOB_START_FIELD from '@salesforce/schema/SBQQ__Quote__c.ADTJob_Start__c';
import JOB_END_FIELD from '@salesforce/schema/SBQQ__Quote__c.ADTJob_End__c';
import JOB_SCHED_ON_FIELD from '@salesforce/schema/SBQQ__Quote__c.ADTJob_Scheduled_on__c';
import JOB_SCHED_BY_FIELD from '@salesforce/schema/SBQQ__Quote__c.ADTJob_Scheduled_By__c';
import JOB_NUMBER_FIELD from '@salesforce/schema/SBQQ__Quote__c.MMB_Job__c';
import JOB_STATUS_FIELD from '@salesforce/schema/SBQQ__Quote__c.ADTJob_Status__c';
import JOB_EARLIEST_FIELD from '@salesforce/schema/SBQQ__Quote__c.ADTJob_Earliest_Available__c';
import MMB_EMPLOYEE_ID_FIELD from '@salesforce/schema/User.MMB_Employee_ID__c';
import QUOTE_STATUS_FIELD from '@salesforce/schema/SBQQ__Quote__c.SBQQ__Status__c';
import QUOTE_ID_FIELD from '@salesforce/schema/SBQQ__Quote__c.Id';
// Placeholder: import Estimated Install Duration field

export default class Cc_scheduleJobs extends LightningElement {
    userId = USER_ID;
    installDuration;
    input_startDate; // Start date used for lower bound of range of dates sent to JST
    endDate; // End date used for upper bound of range of dates sent to JST
    apptOffers = []; // Array used to store returned Appointment time slots
    showOffers = true; // Used to conditionally hide/show the list of available appointments
    calloutResp; // used to store the response object from the callout
    isLoading = false; // Used to conditionally show lightning spinner
    disableInputFields = false; // Used to control the input field availability (disabled when making callout)
    datePickerMinVariable; // Used to disable dates in the past
    showScheduleBtn = false; // Controls the display of the 'Schedule Install' button
    error; // Used to hold error information
    selectedDate; // Used to hold the date value of the appointment offer chosen
    selectedTimeBand; // Used to hold the timeband ID value of the appt offer chosen
    selectedStartTime; // Used to hold the start datetime of the chosen offer
    selectedEndTime; // Used to hold the end datetime of the chosen offer
    @api recordId;
    showCancellationSection = false;
    showScheduleSection = false;
    jobData; // Used to hold job data info
    isRescheduling = false; // When true, we are rescheduling a job which means we need to issue a 'PUT' command to the API ('POST' is for scheduling net new job)
    earliestAvailable; // Will hold the earliest appointment slot returned from JST (used for reporting purposes)
    showConfirmCancelSection = false; // Used to conditionally show the confirmation of cancelling a job (if 'Cancel Job' is pressed)
    confirmCancelText = 'Are you sure you wish to cancel the appointment?';
    firstCallout = true; // When true, we will capture the earliest available appointment slot
    durationOverride = 0; // Holds the value of the 'Duration Override' field
    overrideApplied = false; // Flag for when a duration override value is provided

    @wire(getRecord,{ recordId: '$recordId', fields: [JOB_DURATION_FIELD, JOB_NUMBER_FIELD, JOB_STATUS_FIELD, JOB_START_FIELD, JOB_END_FIELD] })
    wiredData({ error, data }) {
        if (data) {
            if (data.fields.ADTJob_Status__c.value === 'Scheduled'){
                this.jobData = data;
                this.jobID = data.fields.MMB_Job__c.value;
                // Show cancellation section
                this.showScheduleSection = false;
                this.showCancellationSection = true;
            }
            else if (data.fields.MMB_Job__c.value !== null) {
                this.installDuration = data.fields.Estimated_Install_Duration__c.value;
                this.showScheduleSection = true;
                this.showCancellationSection = false;
                this.jobID = data.fields.MMB_Job__c.value;
                this.doInitialCallout();
            }
        } else if (error) {
            console.log('wired error: ', error);
        }
    }

    @wire(getRecord, { recordId: USER_ID, fields: [MMB_EMPLOYEE_ID_FIELD] })
    wiredUserData({ error, data }) {
        if (data) {
            if (data.fields.MMB_Employee_ID__c.value !== null) {
                this.mmbEmpId = data.fields.MMB_Employee_ID__c.value;
            } else {
                this.showToastEvent('','No MMB Employee ID found for current user.', 'error');
                this.closeQuickAction();
            }
        }
        else if (error) {
            console.log('wired user error: ', error);
        }
    }

    /*
     *  Function that gets executed after LWC loads, will populate the 'Start Date' field
     *  with today's date and makes an initial callout to get the closest available appt for
     *  today
     */
    doInitialCallout() {
        try {
            this.error = '';
            if (this.jobID){
                let today = new Date();
                this.input_startDate = today.toISOString().split('T')[0];

                // Calc the end date: start
                let splitDate = this.input_startDate.split('-');

                // Convert back into date object
                let tempDate = new Date(`${splitDate[0]}-${splitDate[1]}-${splitDate[2]}`);

                // Add 7 days to today's date, put it in string format and assign to to endDate
                this.endDate = new Date(tempDate.setDate(tempDate.getDate() + 7)).toISOString().split('T')[0];
                // Calc the end date: stop

                this.apptOffers = this.getAvailableOffers(this.jobID, this.input_startDate, this.endDate);
            } else {
                this.setIsLoading(false);
                this.disableInputFields = false;
                this.error = 'Quote does not have a Job Number!';
                this.showToastEvent('', this.error, 'error');
                this.closeQuickAction();
            }
        }
        catch(e) {
            this.setIsLoading(false);
            this.disableInputFields = false;
            this.error = e.message;
            this.showToastEvent('Exception caught:', this.error, 'error');
        }
    }

    disconnectedCallback() {  }

    /*
     * Helper function to set the isLoading flag for showing the spinner
     */

    setIsLoading(val){
        if (typeof val === 'boolean'){
            this.isLoading = val;
        }
    }

    /*
     * Helper function for displaying toast messages
     */

    showToastEvent(titleInput, msgInput, variantInput){
      const toastEvt = new ShowToastEvent({
        title : titleInput,
        message : msgInput,
        variant : variantInput
      });
      this.dispatchEvent(toastEvt);
    }


    /*
     * Function to handle when a new date is selected. This method will make a callout to JST with the new
     * selected date value
     */

    handleDateChange(event) {
      this.showScheduleBtn = false;

      // Get the date value from the event
      this.input_startDate = event.detail.value;

      // Split via '-' character into array
      let splitDate = this.input_startDate.split('-');

      // Convert back into date object
      let tempDate = new Date(`${splitDate[1]}-${splitDate[2]}-${splitDate[0]}`);

      // Add 7 days to today's date, put it in string format and assign to to endDate
      this.endDate = new Date(tempDate.setDate(tempDate.getDate() + 7)).toISOString().split('T')[0];

      this.apptOffers =  this.getAvailableOffers(this.jobID,this.input_startDate, this.endDate);

      if (this.apptOffers){
        this.showOffers = true;
      }
    }

    /**
     * Function to make callout ot JST to get available Offers
     */
     getAvailableOffers(jobID, startDate, endDate) {
         this.error = '';
         this.setIsLoading(true);
         this.disableInputFields = true;
        postOffers({
            jobID : this.jobID,
            startDate : startDate,
            endDate : endDate
        })
        .then((result) => {
            if (result.isSuccess){
                this.setIsLoading(false);
                this.disableInputFields = false;
                if (this.firstCallout) {
                    this.earliestAvailable = result.offers[0].startDateTime;
                    this.firstCallout = false;
                }
                return this.formatInputOptions(result.offers);
            } else {
                this.setIsLoading(false);
                this.disableInputFields = false;
                this.error = result.responseMessage;
                this.showToastEvent('', this.error, 'error');
            }
        })
        .catch(error => {
            console.log('getAvailableOffers callout error: ', error);
            this.setIsLoading(false);
            this.disableInputFields = false;
            if (Array.isArray(error.body)){
                this.error = error.body.map(e => e.message).join(', ');
                this.isLoading = false;
                this.showToastEvent('', this.error, 'error');
            } else if (typeof error.body.message === 'string'){
                this.error = error.body.message;
                this.isLoading = false;
                this.showToastEvent('', this.error, 'error');
            }
        });
    }

    /*
    * Helper function to transform callout response 'offer' data into Radio input elements
    */
    formatInputOptions(data){
        let inputOptions = [];
        data.forEach(offer => {
              let newLabel = this.formatOffer(offer);
              let newObj = { 'label' : newLabel, 'value' : offer.date_Z +'|'+ offer.timeBandId+'|'+offer.startDateTime+'|'+offer.endDateTime };
              inputOptions.push(newObj);
          });
          this.apptOffers = inputOptions;
    }


    /*
     *  Helper function that will take in a returned 'offer' object that represents an appointment slot,
     *  and will build a nice looking, easy to read label for each radio button selection that is
     *  displayed
     */
    formatOffer(offer){
      let finalLabel; // Stores what will be displayed to the user
      let startDate; // Stores date value of start date
      let startTime; // Stores time value of start time
      let endDate; // Stores the date value of the end date
      let endTime; // Stores the time value of the end time
      let weekDay; // Stores the day of the week

      /* START DATE PROCESSING: START */
      // Extract the start date and time
      startDate = offer.startDateTime.split('T')[0];
      startTime = offer.startDateTime.split('T')[1];

      let monthVal = startDate.split('-')[1];
      let yearVal = startDate.split('-')[0];
      let dayVal = startDate.split('-')[2];

      // Convert start date string into Date object for manipulation
      let newStartDate = new Date(startDate);

      let hourVal = new Date(offer.startDateTime);
      let suffix = hourVal.getHours() >= 12 ? 'PM' : 'AM';

      // Modify data for easier readability on front-end (putting it in to 12-hour format)
      if (hourVal.getHours() > 12){
        let newHourVal = `${hourVal.getHours() - 12}`;
        let minutesVal = hourVal.getMinutes() === 0 ? '00' : '0';
        hourVal.setHours(newHourVal);
        hourVal.setMinutes(minutesVal);
        startTime = `${newHourVal}:${minutesVal}`;
      } else {
        startTime = `${hourVal.getHours()}:${hourVal.getMinutes() === 0 ? '00' : hourVal.getMinutes()}`;
      }
      /* START DATE PROCESSING: STOP */

      /* END DATE PROCESSING: START */

      // Extract the end date and time
      endDate = offer.endDateTime.split('T')[0];
      endTime = offer.endDateTime.split('T')[1];

      let endMonthVal = endDate.split('-')[1];
      let endYearVal = endDate.split('-')[0];
      let endDayVal = endDate.split('-')[2];

      // Convert end date string into Date object for manipulation
      let newEndTime = new Date(offer.endDateTime);
      let endSuffix = newEndTime.getHours() >= 12 ? 'PM' : 'AM';

      if (newEndTime.getHours() > 12){
        let newHourVal = `${newEndTime.getHours() - 12}`;
        let minutesVal = newEndTime.getMinutes() === 0 ? '00' : '0';
        newEndTime.setHours(newHourVal);
        newEndTime.setMinutes(minutesVal);
        endTime = `${newHourVal}:${minutesVal}`;
      } else {
        endTime = `${newEndTime.getHours()}:${newEndTime.getMinutes() === 0 ? '00' : newEndTime.getMinutes()}`;
      }

      /* END DATE PROCESSING: STOP */

      weekDay = this.getWeekDay(newStartDate.getDay());

      finalLabel = `${weekDay} ${monthVal}/${dayVal}/${yearVal} ${startTime}${suffix} - ${endTime}${endSuffix}`;

      return finalLabel;
    }

    /*
     *  Helper function for getting the day of the week, based on the passed in date
     */

    getWeekDay(value){
      switch (value) {
        case 0:
          return 'Mon';
        case 1:
          return 'Tues';
        case 2:
          return 'Wed';
        case 3:
          return 'Thur';
        case 4:
          return 'Fri';
        case 5:
          return 'Sat';
        case 6:
          return 'Sun';
        default:
      }
    }

    /*
     * Function called when an appt offer is selected, used to update the selected Date and timeband information as this is needed
     * when the 'Schedule Install' button is pressed
     */
    handleOfferSelect(event){
      this.showScheduleBtn = true;

      // Parse out the selected date and timeBandId values
      let dataArray = event.target.dataset.id.split('|');
      this.selectedDate = dataArray[0];
      this.selectedTimeBand = dataArray[1];
      this.selectedStartTime = dataArray[2];
      this.selectedEndTime = dataArray[3];
    }

    /*
     *  Function called when 'Schedule Install' button clicked, makes callout to JST to schedule the install
     *  for the selected appointment offer
     */
    handleSubmit(){
        this.error = ''; // reset error message var
        this.setIsLoading(true);
        this.disableInputFields = true;

        // Check that job duration is less than appointment window
        if (!this.validateDuration()) {
            this.setIsLoading(false);
            this.disableInputFields = false;
            this.showToastEvent('', 'e-Contracts is unable to schedule your job. Please contact Scheduling at 866-764-2465', 'warning');
        } else {
            // Check if we are rescheduling an existing job, or scheduling a new job
            if (this.isRescheduling) {
                // Make 'PUT' callout
                putReschedule({
                    jobId : this.jobID,
                    timeBandId : this.selectedTimeBand,
                    date_Z : this.selectedDate,
                    changeUserEmployeeId : this.mmbEmpId
                })
                .then(result => {
                    if (result.isSuccess){
                        this.setIsLoading(false);
                        this.disableInputFields = false;
                        this.updateQuoteJobDetails(result);
                        this.showToastEvent('','Appointment successfully scheduled.', 'success');
                        this.closeQuickAction();
                    } else {
                        this.setIsLoading(false);
                        this.disableInputFields = false;
                        this.error = result.responseMessage;
                        this.showToastEvent('', this.error, 'error');
                    }
                })
                .catch(error => {
                    this.setIsLoading(false);
                    this.disableInputFields = false;
                    if (error.body.message){
                        if (typeof error.body.message === 'string'){
                            this.error = error.body.message;
                            this.isLoading = false;
                            this.showToastEvent('', this.error, 'error');
                        }
                        else if (Array.isArray(error.body)){
                            this.error = error.body.map(e => e.message).join(', ');
                            this.isLoading = false;
                            this.showToastEvent('', this.error, 'error');
                        }
                        else {
                            this.error = error.body.message;
                            this.setIsLoading(false);
                            this.disableInputFields = false;
                        }
                    }
                });
            } else {
            // Make 'POST' callout
            postSchedule({
                jobId : this.jobID,
                timeBandId : this.selectedTimeBand,
                date_Z : this.selectedDate,
                changeUserEmployeeId : this.mmbEmpId
            })
            .then(result => {
                if (result.isSuccess){
                    this.setIsLoading(false);
                    this.disableInputFields = false;
                    this.updateQuoteJobDetails(result);
                    this.showToastEvent('','Appointment successfully scheduled.','success');
                    this.closeQuickAction();
                } else {
                    this.setIsLoading(false);
                    this.disableInputFields = false;
                    this.error = result.responseMessage;
                    this.showToastEvent('', this.error, 'error');
                }
            })
            .catch(error => {
                console.log('error: ', error);
                this.setIsLoading(false);
                this.disableInputFields = false;
                if (error.body.message){
                    if (typeof error.body.message === 'string'){
                        this.error = error.body.message;
                        this.isLoading = false;
                        this.showToastEvent('', this.error, 'error');
                    }
                    else if (Array.isArray(error.body)){
                        this.error = error.body.map(e => e.message).join(', ');
                        this.isLoading = false;
                        this.showToastEvent('', this.error, 'error');
                    }
                    else {
                        this.error = error.body.message;
                        this.setIsLoading(false);
                        this.disableInputFields = false;
                    }
                }
            });

            }
        }


    }

    /*
     *  Helper function to close the quick action window on successful appointment booking
     */
    closeQuickAction() {
        this.dispatchEvent(new CloseActionScreenEvent());
    }

    /*
     *  Helper function to update the ADTJob fields on the Quote record
     */
     updateQuoteJobDetails(data){
         if (data.appointment){
             // Get today's date in ISO format
             let todaysDate = new Date();
             let newTodaysDate = todaysDate.toISOString();

             // Construct new Date objects to place on the Quote record
             let newStartDateTime = new Date(data.appointment.startDateTime);
             let newEndDateTime = new Date(data.appointment.endDateTime);
             let newEarliestDateTime = new Date(this.earliestAvailable);

             const fields = {};
             fields[QUOTE_ID_FIELD.fieldApiName] = this.recordId;
             fields[QUOTE_STATUS_FIELD.fieldApiName] = 'Scheduled';
             fields[JOB_START_FIELD.fieldApiName] = newStartDateTime.toISOString();
             fields[JOB_END_FIELD.fieldApiName] = newEndDateTime.toISOString();
             fields[JOB_EARLIEST_FIELD.fieldApiName] = newEarliestDateTime.toISOString();
             fields[JOB_SCHED_BY_FIELD.fieldApiName] = this.userId;
             fields[JOB_SCHED_ON_FIELD.fieldApiName] = newTodaysDate;
             fields[JOB_STATUS_FIELD.fieldApiName] = 'Scheduled';
             const recordInput = { fields };

             updateRecord(recordInput)
             .then(() => {
                // Do nothing
             })
             .catch((error) => {
                 console.log('Error updating record: ', error);
                 this.setIsLoading(false);
                 this.disableInputFields = false;
                 if (error.body.message){
                     if (typeof error.body.message === 'string'){
                         this.error = error.body.message;
                         this.isLoading = false;
                         this.showToastEvent('', this.error, 'error');
                     }
                     else if (Array.isArray(error.body)){
                         this.error = error.body.map(e => e.message).join(', ');
                         this.isLoading = false;
                         this.showToastEvent('', this.error, 'error');
                     }
                     else {
                         this.error = error.body.message;
                         this.setIsLoading(false);
                         this.disableInputFields = false;
                         this.showToastEvent('', this.error, 'error');
                     }
                 }
             });
         }
     }

     handleReschedule() {
         this.isRescheduling = true; // Setting the flag to true, so we can tell the backend to execute a PUT method instead of POST method on the endpoint
         this.showCancellationSection = false;
         this.showScheduleSection = true;
         this.doInitialCallout();
     }

     // Method to cancel appointment in JST system

     handleCancel() {
         // Check if we are in the confirmation section already
         if (!this.showConfirmCancelSection) {
             this.showConfirmCancelSection = true;
             return;
         } else {
             this.setIsLoading(true);
             this.disableInputFields = true;
            deleteAppointment({
                jobId: this.jobID,
                changeUserEmployeeId: this.mmbEmpId
            })
            .then(result => {
                if (result.isSuccess) {
                    this.showConfirmCancelSection = false;
                    this.handleCancelJobUpdate(); // Update Job fields on record
                     this.setIsLoading(false);
                     this.disableInputFields = false;
                     this.showToastEvent('', 'Job successfully cancelled.', 'success');
                     this.closeQuickAction();
                } else {

                    this.showConfirmCancelSection = false;
                    this.setIsLoading(false);
                    this.disableInputFields = false;
                    this.error = result.responseMessage;
                    this.showToastEvent('', this.error, 'error');
                }
            })
            .catch(error => {
                console.log('Error updating record: ', error);
                this.showConfirmCancelSection = false;
                this.setIsLoading(false);
                this.disableInputFields = false;
                if (error.body.message){
                    if (typeof error.body.message === 'string'){
                      this.error = error.body.message;
                      this.isLoading = false;
                      this.showToastEvent('', this.error, 'error');
                    }
                    else if (Array.isArray(error.body)){
                      this.error = error.body.map(e => e.message).join(', ');
                      this.isLoading = false;
                      this.showToastEvent('', this.error, 'error');
                    }
                    else {
                      this.error = error.body.message;
                      this.setIsLoading(false);
                      this.disableInputFields = false;
                      this.showToastEvent('', this.error, 'error');
                    }
                }
            });
         }
     }

     /*
      * Helper method to clear out the Job related fields on the Quote record when a job is canceled
      */
     handleCancelJobUpdate() {
         const fields = {};

         let todaysDate = new Date();
         let newTodaysDate = todaysDate.toISOString();

         fields[QUOTE_ID_FIELD.fieldApiName] = this.recordId;
         fields[JOB_START_FIELD.fieldApiName] = null;
         fields[JOB_END_FIELD.fieldApiName] = null;
         fields[JOB_STATUS_FIELD.fieldApiName] = 'Cancelled';
         fields[JOB_SCHED_BY_FIELD.fieldApiName] = this.userId;
         fields[JOB_SCHED_ON_FIELD.fieldApiName] = newTodaysDate;
         fields[JOB_EARLIEST_FIELD.fieldApiName] = null;
         const recordInput = { fields };

         updateRecord(recordInput)
         .then(() => {
         })
         .catch(error => {
             this.setIsLoading(false);
              console.log('Error updating record: ', error);
              this.setIsLoading(false);
              this.disableInputFields = false;
              if (error.body.message){
                  if (typeof error.body.message === 'string'){
                      this.error = error.body.message;
                      this.isLoading = false;
                      this.showToastEvent('', this.error, 'error');
                  }
                  else if (Array.isArray(error.body)){
                      this.error = error.body.map(e => e.message).join(', ');
                      this.isLoading = false;
                      this.showToastEvent('', this.error, 'error');
                  }
                  else {
                      this.error = error.body.message;
                      this.setIsLoading(false);
                      this.disableInputFields = false;
                      this.showToastEvent('', this.error, 'error');
                  }
              }
         });

     }

    /*
     * Method that is called when scheduling a job. It checks to ensure the duration time is less than the appointment window time
     */

     validateDuration() {
         let startHour = this.selectedStartTime.split('T')[1].split(':')[0];
         let endHour = this.selectedEndTime.split('T')[1].split(':')[0];
         if (this.overrideApplied){
             return (endHour - startHour) >= this.durationOverride;
         } else {
            return (endHour - startHour) >= this.installDuration;
         }
     }

     /*
      * Method to set the 'showConfirmCancelSection' to false, when the user decides they do not want to cancel a job0
      */

     cancelConfirm() {
         if (this.showConfirmCancelSection) {
             this.showConfirmCancelSection = !this.showConfirmCancelSection;
         }
     }

     // Method to change the display when the user decides to NOT reschedule a job and wants to go back to the summary screen

     handleBackEvent() {
         this.showScheduleBtn = false;
         this.showScheduleSection = false;
         this.showCancellationSection = true;
     }

     /*
      * Method to handle when a duration override is entered
      */
      handleOverrideChange(event){
          this.durationOverride = event.target.value;
          if (this.durationOverride !== null && this.durationOverride > 0){
              this.overrideApplied = true;
              this.durationOverride = event.target.value;
          } else {
              this.durationOverride = 0;
              this.overrideApplied = false;
          }
      }
}