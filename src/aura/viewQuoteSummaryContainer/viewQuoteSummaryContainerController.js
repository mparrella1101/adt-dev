({
    doInit : function(component, event, helper) {
        console.log('aura viewQuoteSummaryContainer doInit');
        console.log('aura recordId = ' + component.get('v.recordId'));
    },
    
    closeQuickAction : function(component, event, helper){
        console.log('{!c.closeAction}');
        $A.get("e.force:closeQuickAction").fire();
    }
})