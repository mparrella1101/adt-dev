({
    doInit : function(component, event, helper) {
        console.log('aura doInit');
        console.log('aura recordId = ' + component.get('v.recordId'));
    },

    refreshTab : function(component, event, helper){
        console.log('>>>refreshTab<<<');
        let workspaceAPI = component.find("workspace");
        workspaceAPI.getFocusedTabInfo()
        .then((response) => {
            let focusedTabId = response.tabId;
            workspaceAPI.refreshTab({
                tabId : focusedTabId
            });
        })
        .catch(error =>{
            console.log('error refreshing lightning console tab');
        });
    },

    closeQuickAction : function(component, event, helper){
        console.log('{!c.closeAction}');
        $A.get("e.force:closeQuickAction").fire();
    }
})