({
    makeApexCallWithParams : function (cmp, apexAction, params){ // reusable apex callback with params
        return new Promise($A.getCallback(function(resolve, reject) {
            let action = cmp.get('c.' + apexAction + '');
            action.setParams(params);
            action.setCallback(this, function(response){
                if(response.getState() === 'SUCCESS'){
                    resolve(response.getReturnValue());
                } else {
                    console.log('error making apex call with params: ' + apexAction + ', ' + response.getError());
                    reject(response.getError());
                }
            });

            $A.enqueueAction(action);
        }));
    },

    makeApexCall : function (cmp, apexAction){ // reusable apex callback
        return new Promise($A.getCallback(function(resolve, reject) {
            let action = cmp.get('c.' + apexAction + '');
            action.setCallback(this, function(response){
                if(response.getState() === 'SUCCESS'){
                    resolve(response.getReturnValue());
                } else {
                    console.log('error making apex call: ' + apexAction + ', ' + response.getError());
                    reject(response.getError());
                }
            });

            $A.enqueueAction(action);
        }));
    },

    getQLELink : function(cmp){ // this dynamically retrieves the domain.
        return new Promise($A.getCallback(function(resolve) {
            let hostName = window.location.hostname;
            let instance = 'https://' + hostName.split(".")[0];
            let recordId = cmp.get('v.recordId');
            let actionId = cmp.get('v.customActionId');
            let resiBundleId = cmp.get('v.resiBundleId');
            let cpqUrl = '--sbqq.visualforce.com/apex/sbqq__sb?scontrolCaching=1&id=' + recordId + '#/product/pc?qId=' + recordId + '&aId=' + actionId + '&pId=' + resiBundleId + '&redirectUrl=LineEditor&open=0';
            let qleLink = instance + cpqUrl;

            console.log('hostName = ' + hostName);
            console.log('instance = ' + instance);
            console.log('recordId = ' + recordId);
            console.log('actionId = ' + actionId);
            console.log('resiBundleId = ' + resiBundleId);
            console.log('cpqUrl = ' + cpqUrl);
            console.log('qleLink = ' + qleLink);
            
            resolve(qleLink);
        }));
    },

    navigateToPage : function(cmp, link){
        console.log('navigation link: ' + link);
        const workspaceAPI = cmp.find('workspace');
        workspaceAPI.getFocusedTabInfo() // get parent tab info from current tab.
        .then(tabInfo => {
            console.log('enclosingTabId: ' + JSON.stringify(tabInfo));
            if(tabInfo.isSubtab){ // check if in subtab context.
                workspaceAPI.openSubtab({ // if so, open new subtab and close previously focused tab.
                    parentTabId: tabInfo.parentTabId,
                    url: link,
                    focus: true
                })
                .then(() => {
                    workspaceAPI.closeTab({
                        tabId: tabInfo.tabId
                    })
                })
                .catch(error => {
                    console.log('error opening new subtab/closing focused tab ' + error);                    
                });
                
            } else {
                workspaceAPI.openTab({ // if not in subtab context, but in console app, open new tab.
                    url: link,
                    focus: true
                })
                .catch(error => {
                    console.log('error opening new tab: ' + error + ' navigating using urlEvent instead.');
                    this.navigateToURL(link);
                });
            }            
        })
        .catch(error => {
            console.log('error opening new subtab, navigating using urlEvent instead.: ' + error); // if not in console context, use this.navigateToUrl to navigate normally.
            this.navigateToURL(link);
        });        
    },

    navigateToURL: function(link){
        let urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": link
        });

        urlEvent.fire();
    }
    
})