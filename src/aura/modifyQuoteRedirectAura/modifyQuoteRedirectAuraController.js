({
    doInit : function(component, event, helper) {
        console.log('modifyQuoteRedirectAura');
        const recordId = component.get('v.recordId');
        console.log('recordId: ' + recordId);
        const qspLink = '/lightning/n/Quote_Summary?c__quoteId=' + recordId;
        helper.makeApexCallWithParams(component, 'getQuote', { quoteId: recordId }) // this recieves the SBQQ__LineItemCount__c value from quote record
            .then(result => {
                component.set('v.quoteData', result);
                console.log('quote data: ' + JSON.stringify(component.get('v.quoteData')));
                console.log('line item count =  ' + result.SBQQ__LineItemCount__c);

                if(result.SBQQ__LineItemCount__c > 0){ // quote lines exist, navigating to quote summary page
                    helper.navigateToPage(component, qspLink);
                } else {  // no quote lines, navigating to QLE into 'Residential Services' bundle.
                    Promise.all([
                        helper.makeApexCall(component, 'getAddProductsAction') // this retrieves the 'Add Products' custom action Id.
                        .then(result => {
                            console.log('getProductAction result: ' + result);
                            component.set('v.customActionId', result);
                            console.log('v.customActionId = ' + component.get('v.customActionId'));
                        }),
                        helper.makeApexCall(component, 'getProductId') // this retrieves Residential Services Bundle product Id.
                        .then(result => {
                            console.log('getProductId result: ' + result);
                            component.set('v.resiBundleId', result);
                            console.log('v.resiBundleId = ' + component.get('v.resiBundleId'));
                        })
                    ])
                    .then(() => {
                        console.log('generating QLE link');
                        helper.getQLELink(component)
                        .then(result => {
                            helper.navigateToPage(component, result);
                        });
                    })
                }
            })
            .catch(error => {
                console.log('error navigating ' + JSON.stringify(error));
            });
    }
})